package com.journepassenger.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ExpandableListView;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.journepassenger.activity.DashBoardActivity;
import com.journepassenger.activity.GPSTracker;
import com.journepassenger.adapter.LandMarkList_Adapter;
import com.journepassenger.adapter.ParentExpandableListAdapter;
import com.journepassenger.bean.City_bean;
import com.journepassenger.bean.LandMark_bean;
import com.journepassenger.bean.Landmark;
import com.journepassenger.bean.RoutesBean;
import com.journepassenger.utilities.ConnectionDetector;
import com.journepassenger.utilities.GlobalValues;
import com.journepassenger.utilities.Group;
import com.journepassenger.utilities.MethodManager_Listner;
import com.journepassenger.utilities.SessionClass;
import com.journepassenger.utilities.Session_manager;
import com.journepassenger.utilities.UtilMethod;
import com.journyapp.journyapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * Created by administrator on 1/2/16.
 */
public class HomeScreenFragment extends Fragment
{
    public static String HomeTag="home";
    public static Fragment fragment;
    static FragmentTransaction ft;
    static public FragmentManager fragmentmanger;
    static public Context mContext;
    public static String TAG = "HOME";
    static ExpandableListView ExpandList;
    ArrayList<Group> ExpListItems;
    //static ParentExpandableListAdapter listAdapter;

    static ParentExpandableListAdapter listAdapter;
    //MyExpandableListAdapter ExpAdapter;
    List<String> groupList;
    AutoCompleteTextView actyourlocationid, actselectdestinid;
    static ArrayList<City_bean> citylist;
    String city_data;
    String show_city;
    static String cityid;
    static ArrayList<RoutesBean> routeList;
    static ArrayList<Landmark> stopList;
    static String City_id;
    ImageButton btn_search;
    ArrayList<String> busstoplist= new ArrayList<String>() ;
    ArrayAdapter<String>adap;
    ArrayList<LandMark_bean> landmark_arrays;
    GPSTracker gps;
    static ProgressDialog pdialog;
     double lat,longt;
    static String city_lat="";
    static String city_id_lat;
    static LinearLayout liner_button;
    TextView go_btn;
    String getfrom,getto;
    public static Fragment getInstance(Context ct, FragmentManager fm) {
        mContext = ct;
        fragmentmanger = fm;

        fragment = new HomeScreenFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.homescreen_xml, container, false);
        ExpandList = (ExpandableListView) view.findViewById(R.id.myexpandlistid);
        actyourlocationid = (AutoCompleteTextView) view.findViewById(R.id.edyourlocationid);
        actselectdestinid = (AutoCompleteTextView) view.findViewById(R.id.edselectdestinid);
        btn_search=(ImageButton)view.findViewById(R.id.btn_search);
        liner_button=(LinearLayout)view.findViewById(R.id.liner_button);
        go_btn=(TextView)view.findViewById(R.id.go_btn);
        DashBoardActivity.hideSearchButton(mContext);
        DashBoardActivity.showlocationButton(mContext);
        DashBoardActivity.hideafteredit(mContext);
        DashBoardActivity.hidebeforeedit(mContext);
        DashBoardActivity.showMenuDrawerButton(mContext);
        DashBoardActivity.hideBackButton(mContext);
        DashBoardActivity.updatedTitle("All Routes");
        DashBoardActivity.hideLoacationButton(mContext);
        DashBoardActivity.hidehomeButton(mContext);
        DashBoardActivity.hidePrintIcon(mContext);
        citylist = new ArrayList<City_bean>();

         lat= SessionClass.getCurrentLatitude();
         longt=SessionClass.getCurrentLongitude();

         //Toast.makeText(mContext,"lat"+lat+"long"+longt,Toast.LENGTH_LONG).show();
          cityid= Session_manager.GetcityId(mContext);
          show_city=Session_manager.GetcityName(mContext);
         if(cityid==null)
         {
             cityid="8";
         }
        if(show_city==null)
        {
            show_city="Mumbai";
        }
       if(lat==0.0&&longt==0.0)
       {
           if (ConnectionDetector.isNetworkAvailable(mContext))

           {
               showprogress();
               GetexpandList_data(cityid);
               DashBoardActivity.setdialogtitle(show_city);
               Getlandmark_auto(cityid);
           }
           else
           {
               Toast.makeText(mContext, "You don't have Internet Connection", Toast.LENGTH_LONG).show();
                  hideprogress();
           }
       }
        else
       {
           if (ConnectionDetector.isNetworkAvailable(mContext))

           {
               showprogress();
               GetexpandList_datafromLat_long(lat, longt);
           }
           else
           {
               Toast.makeText(mContext, "You don't have Internet Connection", Toast.LENGTH_LONG).show();
               hideprogress();
           }
       }

        btn_search.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v) {
                if (actyourlocationid.getText().toString().equals(""))
                {
                  Toast.makeText(mContext,"Please Select location",Toast.LENGTH_LONG).show();
                } else if (actselectdestinid.getText().toString().equals(""))
                {
                    Toast.makeText(mContext,"Please Select Destination",Toast.LENGTH_LONG).show();
                }
                else  if (ConnectionDetector.isNetworkAvailable(mContext))


                {
                    Bundle b=new Bundle();
                    b.putString("fromcity",actyourlocationid.getText().toString());
                    b.putString("tocity",actselectdestinid.getText().toString());
                    DashBoardActivity.displayview(DashBoardActivity.searchbus, b);
                }
                else
                {
                    Toast.makeText(mContext, "You don't have Internet Connection", Toast.LENGTH_LONG).show();

                }
            }
        });

        ExpandList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            int previousGroup = -1;

            @Override
            public void onGroupExpand(int groupPosition) {
                if (groupPosition != previousGroup)
                    ExpandList.collapseGroup(previousGroup);
                previousGroup = groupPosition;
                ParentExpandableListAdapter.to = -1;
                ParentExpandableListAdapter.from = -1;
                Session_manager.setfromcity(mContext, null);
                Session_manager.setTocity(mContext, null);
                listAdapter.notifyDataSetChanged();
                changeButtonGray();

            }
        });

        ExpandList.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int groupPosition) {

                changeButtonGray();
            }
        });
        go_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                getfrom = Session_manager.getfromcity(mContext);
                getto = Session_manager.gettocity(mContext);
                if (getfrom == null) {

                    Toast.makeText(mContext, "Select Your Source", Toast.LENGTH_LONG).show();

                } else if (getto == null) {

                    Toast.makeText(mContext, "Select Your Destination", Toast.LENGTH_LONG).show();

                } else if (ConnectionDetector.isNetworkAvailable(mContext))
                {
                    Bundle b=new Bundle();
                    b.putString("fromcity",getfrom);
                    b.putString("tocity",getto);
                    DashBoardActivity.displayview(DashBoardActivity.searchbus, b);
                    Session_manager.setTocity(mContext, null);
                    Session_manager.setfromcity(mContext, null);
                    DashBoardActivity.displayview(DashBoardActivity.searchbus, b);
                }

                else
                {
                    Toast.makeText(mContext, "You don't have Internet Connection", Toast.LENGTH_LONG).show();

                }
            }
        });

        return view;
    }

    public static ArrayList<City_bean> getcitylist(String json) {
        ArrayList<City_bean> getcityarray = new ArrayList<City_bean>();
        try {
            JSONObject jobj = new JSONObject(json);
            JSONArray jarray = jobj.getJSONArray("city");
            for (int i = 0; i < jarray.length(); i++) {
                City_bean cbean = new City_bean();
                cbean.setCity_id(jarray.getJSONObject(i).getString("city_id"));
                cbean.setCity_name(jarray.getJSONObject(i).getString("city_name"));

                getcityarray.add(cbean);
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return getcityarray;
    }





    public static void GetexpandList_data(String cityid) {
        Map<String, Object> params = new HashMap<String, Object>();

        params.put("city_id",cityid);
        params.put("type","2");

        GlobalValues.getMethodManagerObj(mContext).GetRoute_list(params, new MethodManager_Listner() {
            @Override
            public void onError() {

                UtilMethod.showToast("onError ", mContext);
                hideprogress();
            }

            @Override
            public void onSuccess(String json1) {

                //UtilMethod.showToast("onSuccess ", mContext);

                try {

                    JSONObject json = new JSONObject(json1);

                    String message = json.getString("msg");
                    String status = json.getString("status");

                    if (status.equalsIgnoreCase("false")) {

                        routeList = new ArrayList<RoutesBean>();

                        listAdapter = new ParentExpandableListAdapter(mContext, routeList);
                        // setting list adapter
                        ExpandList.setAdapter(listAdapter);
                        hideprogress();
                        showDialogForConfirmEmail("Message", message);


                    } else if (status.equalsIgnoreCase("false") && message.equalsIgnoreCase("No bus route found"))
                    {
                        hideprogress();
                        showDialogForConfirmEmail("Message", message);

                    } else if (status.equals("true"))
                    {
                        hideprogress();
                        JSONArray responseJsonArray = json.getJSONArray("routes");
                        routeList = new ArrayList<RoutesBean>();

                        for (int j = 0; j < responseJsonArray.length(); j++)

                        {
                            JSONObject friendsObj = responseJsonArray.getJSONObject(j);
                            RoutesBean routesBean = new RoutesBean();

                            String bus_route_id = friendsObj.getString("bus_route_id");
                            routesBean.setBus_route_id(bus_route_id);
                            Log.v("school_id = ", bus_route_id);

                            String bus_operator_id = friendsObj.getString("bus_operator_id");
                            routesBean.setBus_operator_id(bus_operator_id);
                            Log.v("bus_operator_id  ", bus_operator_id);

                            String bus_city_id = friendsObj.getString("bus_city_id");
                            routesBean.setBus_city_id(bus_city_id);
                            Log.v("bus_city_id  ", bus_city_id);

                            String bus_route_title = friendsObj.getString("bus_route_title");
                            routesBean.setBus_route_title(bus_route_title);
                            Log.v("bus_route_title = ", bus_route_title);

                            String bus_route_from = friendsObj.getString("bus_route_from");
                            routesBean.setBus_route_from(bus_route_from);
                            Log.v("bus_route_from  ", bus_route_from);

                            String bus_route_to = friendsObj.getString("bus_route_to");
                            routesBean.setBus_route_to(bus_route_to);
                            Log.v("bus_route_to = ", bus_route_to);


                            String bus_route_start_lat = friendsObj.getString("bus_route_start_lat");
                            routesBean.setBus_route_start_lat(bus_route_start_lat);
                            Log.v("bus_route_start_lat  ", bus_route_start_lat);

                            String bus_route_start_long = friendsObj.getString("bus_route_start_long");
                            routesBean.setBus_route_start_long(bus_route_start_long);
                            Log.v("bus_route_start_long  ", bus_route_start_long);

                            String bus_route_end_lat = friendsObj.getString("bus_route_end_lat");
                            routesBean.setBus_route_end_lat(bus_route_end_lat);
                            Log.v("bus_route_end_lat = ", bus_route_end_lat);

                            String bus_route_end_long = friendsObj.getString("bus_route_end_long");
                            routesBean.setBus_route_end_long(bus_route_end_long);
                            Log.v("bus_route_end_long  ", bus_route_end_long);

                            String bus_route_status = friendsObj.getString("bus_route_status");
                            routesBean.setBus_route_status(bus_route_status);
                            Log.v("bus_route_status  ", bus_route_status);


                            JSONArray stop_list = friendsObj.getJSONArray("landmarks");
                            stopList = new ArrayList<Landmark>();

                            for (int i1 = 0; i1 < stop_list.length(); i1++)

                            {
                                JSONObject friendsObj11 = stop_list.getJSONObject(i1);
                                Landmark landmarkBean = new Landmark();

                                String landmark_id = friendsObj11.getString("landmark_id");
                                landmarkBean.setLandmark_id(landmark_id);
                                Log.v("t_stop_id = ", landmark_id);


                                String landmark = friendsObj11.getString("landmark");
                                landmarkBean.setLandmark(landmark);
                                Log.v("stop_name = ", landmark);

                                stopList.add(landmarkBean);
                            }


                            //	tripList.add(parentsApprovedChildBean);

                            routesBean.setLandmark_array(stopList);
                            routeList.add(routesBean);
                        }

                    }
                    if (routeList.size() > 0) {
                        listAdapter = new ParentExpandableListAdapter(mContext, routeList);
                        // setting list adapter
                        ExpandList.setAdapter(listAdapter);

                        listAdapter.notifyDataSetChanged();
                    }
                } catch (Exception e)
                {
                    hideprogress();
                    e.printStackTrace();
                    Log.e("Home exception", e.toString());
                }
            }
        });

    }


    public static void SetAdapter()
    {
        if (routeList.size() > 0) {

            //    ExpandList.setVisibility(View.VISIBLE);
            listAdapter = new ParentExpandableListAdapter(mContext, routeList);
            // setting list adapter
            ExpandList.setAdapter(listAdapter);
            hideprogress();
            listAdapter.notifyDataSetChanged();
        }
    }
    public void GetexpandList_datafromLat_long(double latitude, double longitude) {
        Map<String, Object> params = new HashMap<String, Object>();

        params.put("loc_lat",latitude);
        params.put("loc_long",longitude);
        params.put("type","1");

        GlobalValues.getMethodManagerObj(mContext).GetRoute_list(params, new MethodManager_Listner() {
            @Override
            public void onError() {

                UtilMethod.showToast("onError ", mContext);
                hideprogress();
            }

            @Override
            public void onSuccess(String json1) {

                //UtilMethod.showToast("onSuccess ", mContext);

                try {

                    JSONObject json = new JSONObject(json1);

                    String message = json.getString("msg");
                    String status = json.getString("status");


                    if (status.equalsIgnoreCase("false")) {

                        routeList = new ArrayList<RoutesBean>();

                        listAdapter = new ParentExpandableListAdapter(mContext, routeList);
                        // setting list adapter
                        ExpandList.setAdapter(listAdapter);
                        hideprogress();

                        showDialogForConfirmEmail("Message", message);


                    } else if (status.equalsIgnoreCase("false") && message.equalsIgnoreCase("No bus route found")) {
                        hideprogress();
                        showDialogForConfirmEmail("Message", message);

                    } else if (status.equals("true"))
                    {
                           hideprogress();
                        JSONObject obj = json.getJSONObject("city_details");
                        city_lat = obj.getString("city_name");
                        city_id_lat = obj.getString("city_id");
                        DashBoardActivity.setdialogtitle(city_lat);
                        Getlandmark_auto(city_id_lat);
                        JSONArray responseJsonArray = json.getJSONArray("routes");
                        routeList = new ArrayList<RoutesBean>();

                        for (int j = 0; j < responseJsonArray.length(); j++)

                        {
                            JSONObject friendsObj = responseJsonArray.getJSONObject(j);
                            RoutesBean routesBean = new RoutesBean();

                            String bus_route_id = friendsObj.getString("bus_route_id");
                            routesBean.setBus_route_id(bus_route_id);
                            Log.v("school_id = ", bus_route_id);

                            String bus_operator_id = friendsObj.getString("bus_operator_id");
                            routesBean.setBus_operator_id(bus_operator_id);
                            Log.v("bus_operator_id  ", bus_operator_id);

                            String bus_city_id = friendsObj.getString("bus_city_id");
                            routesBean.setBus_city_id(bus_city_id);
                            Log.v("bus_city_id  ", bus_city_id);

                            String bus_route_title = friendsObj.getString("bus_route_title");
                            routesBean.setBus_route_title(bus_route_title);
                            Log.v("bus_route_title = ", bus_route_title);

                            String bus_route_from = friendsObj.getString("bus_route_from");
                            routesBean.setBus_route_from(bus_route_from);
                            Log.v("bus_route_from  ", bus_route_from);

                            String bus_route_to = friendsObj.getString("bus_route_to");
                            routesBean.setBus_route_to(bus_route_to);
                            Log.v("bus_route_to = ", bus_route_to);


                            String bus_route_start_lat = friendsObj.getString("bus_route_start_lat");
                            routesBean.setBus_route_start_lat(bus_route_start_lat);
                            Log.v("bus_route_start_lat  ", bus_route_start_lat);

                            String bus_route_start_long = friendsObj.getString("bus_route_start_long");
                            routesBean.setBus_route_start_long(bus_route_start_long);
                            Log.v("bus_route_start_long  ", bus_route_start_long);

                            String bus_route_end_lat = friendsObj.getString("bus_route_end_lat");
                            routesBean.setBus_route_end_lat(bus_route_end_lat);
                            Log.v("bus_route_end_lat = ", bus_route_end_lat);

                            String bus_route_end_long = friendsObj.getString("bus_route_end_long");
                            routesBean.setBus_route_end_long(bus_route_end_long);
                            Log.v("bus_route_end_long  ", bus_route_end_long);

                            String bus_route_status = friendsObj.getString("bus_route_status");
                            routesBean.setBus_route_status(bus_route_status);
                            Log.v("bus_route_status  ", bus_route_status);


                            JSONArray stop_list = friendsObj.getJSONArray("landmarks");
                            stopList = new ArrayList<Landmark>();

                            for (int i1 = 0; i1 < stop_list.length(); i1++)

                            {
                                JSONObject friendsObj11 = stop_list.getJSONObject(i1);
                                Landmark landmarkBean = new Landmark();

                                String landmark_id = friendsObj11.getString("landmark_id");
                                landmarkBean.setLandmark_id(landmark_id);
                                Log.v("t_stop_id = ", landmark_id);


                                String landmark = friendsObj11.getString("landmark");
                                landmarkBean.setLandmark(landmark);
                                Log.v("stop_name = ", landmark);

                                stopList.add(landmarkBean);
                            }


                            //	tripList.add(parentsApprovedChildBean);

                            routesBean.setLandmark_array(stopList);
                            routeList.add(routesBean);
                        }

                    }
                    if (routeList.size() > 0) {

                        //    ExpandList.setVisibility(View.VISIBLE);
                        listAdapter = new ParentExpandableListAdapter(mContext, routeList);
                        // setting list adapter
                        ExpandList.setAdapter(listAdapter);
                        hideprogress();
                        listAdapter.notifyDataSetChanged();
                    }
                } catch (Exception e)
                {
                    hideprogress();
                    e.printStackTrace();
                    Log.e("Home exception",e.toString());
                }
            }
        });

    }
    public  void Getlandmark_auto(String City_id)
    {
        Map<String, Object> params = new HashMap<String, Object>();

        params.put("city_id",City_id);

        GlobalValues.getMethodManagerObj(mContext).GetLandmark_list(params, new MethodManager_Listner() {
            @Override
            public void onError() {

            }

            @Override
            public void onSuccess(String json1) {
                JSONObject json = null;
                try {
                    json = new JSONObject(json1);
                    String message = json.getString("message");
                    String status = json.getString("status");
                    if (status.equals("true")) {

                        JSONArray responseJsonArray = json.getJSONArray("landmark_list");

                        landmark_arrays = new ArrayList<LandMark_bean>();
                        for (int j = 0; j < responseJsonArray.length(); j++) {
                            JSONObject landmark_obj = responseJsonArray.getJSONObject(j);
                            LandMark_bean landMark_bean = new LandMark_bean();
                            String lanmark_id = landmark_obj.getString("landmark_id");
                            landMark_bean.setLandmark_id(lanmark_id);

                            String landmark = landmark_obj.getString("landmark");
                            landMark_bean.setLandmark(landmark);

                            landmark_arrays.add(landMark_bean);

                        }
                    }

                    LandMarkList_Adapter landMarkListAdapter = new LandMarkList_Adapter(mContext, R.layout.landmark_listcustom, landmark_arrays);
                    actyourlocationid.setAdapter(landMarkListAdapter);
                    actselectdestinid.setAdapter(landMarkListAdapter);

                    actselectdestinid.setThreshold(1);
                    actyourlocationid.setThreshold(1);
                    landMarkListAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });
    }
    public static void showDialogForConfirmEmail(String title, final String message)

    {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext).setTitle(title).setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
        public void CheckGPS()
    {
        gps = new GPSTracker(mContext);

        // check if GPS enabled
        if (gps.canGetLocation()) {

            double latitude = gps.getLatitude();//22.7257742
            double longitude = gps.getLongitude();//75.87731
            GetexpandList_datafromLat_long(latitude,longitude);

        }
        else
        {
            showAlertDialog();
        }
    }
    protected void showAlertDialog() {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings");

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                mContext.startActivity(intent);


            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                GetexpandList_data(City_id);
            }
        });

        // Showing Alert Message
        alertDialog.show();

    }
    public void showprogress() {
        pdialog = new ProgressDialog(mContext);
        pdialog.setCancelable(false);
        pdialog.setCanceledOnTouchOutside(false);
        pdialog.setMessage("Loading...");
        pdialog.show();
    }

    public static void hideprogress() {
        if (pdialog != null) {
            pdialog.dismiss();
        }
    }

    public static void chqValue(){

        if (ParentExpandableListAdapter.from ==-1 ) {

            changeButtonGray();

        } else if (ParentExpandableListAdapter.to ==-1 ) {

            changeButtonGray();

        } else {

            changeButtonGreen();
        }

    }

    public static  void changeButtonGreen()
    {
        liner_button.setBackgroundResource(R.color.textGreen);
    }
    public static  void changeButtonGray()
    {
        liner_button.setBackgroundResource(R.color.hinttext_color_gray);
    }

}