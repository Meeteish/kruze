package com.journepassenger.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.journepassenger.activity.DashBoardActivity;
import com.journyapp.journyapp.R;

/**
 * Created by administrator on 22/2/16.
 */
public class ThankYou_Fragmant extends Fragment
{
    public static String tag="thankyou";
    public static Fragment fragment;
    static FragmentTransaction ft;
    static public FragmentManager fragmentmanger;
    static public Context mContext;
    View view;
    TextView btnSuggestAnotherRoute;

    public static Fragment getInstance(Context ct, FragmentManager fm,Bundle b)
    {

        mContext=ct;
        fragmentmanger=fm;
        fragment=new ThankYou_Fragmant();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view=inflater.inflate(R.layout.thank_you, container, false);
        mContext=getActivity();
        btnSuggestAnotherRoute=(TextView)view.findViewById(R.id.btnSuggestAnotherRoute);
        DashBoardActivity.hideSearchButton(mContext);
        DashBoardActivity.hideLoacationButton(mContext);
        DashBoardActivity.hideafteredit(mContext);
        DashBoardActivity.hidebeforeedit(mContext);
        DashBoardActivity.hideMenuDrawerButton(mContext);
        DashBoardActivity.showBackButton(mContext);
        DashBoardActivity.hidecityname_header(mContext);
        DashBoardActivity.updatedTitle("Thank You");
        DashBoardActivity.hidePrintIcon(mContext);
        DashBoardActivity.showhomeButton(mContext);

        btnSuggestAnotherRoute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DashBoardActivity.displayview(DashBoardActivity.home,null);
            }
        });
        return view;
    }
}
