package com.journepassenger.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.journepassenger.activity.DashBoardActivity;
import com.journepassenger.bean.Mapdata_bean;
import com.journepassenger.utilities.GlobalValues;
import com.journepassenger.utilities.MethodManager_Listner;
import com.journyapp.journyapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by administrator on 7/3/16.
 */
public class TrackRide_Fragment extends Fragment {
    public static String tag = "trackride";
    public static Fragment fragment;
    static public FragmentManager fragmentmanger;
    static public Context mContext;
    static FragmentTransaction ft;
    static ArrayList<Mapdata_bean> mapdata_array_list;
    static String booking_id;
    /*  MapView mapView;
      GoogleMap googleMap;
      Double stringLatitude;
      Double stringLongitude;*/
    Handler textHandlerChange;
    /* PolylineOptions polyLineOptions = null;*/


    MapView mMapView;
    MarkerOptions markerOptions;
    PolylineOptions polylineOptions;
    Handler handler;
    int sizeOfPoints;
    int counter = 5;
    private GoogleMap googleMap;
    TextView tvlocation;
    float zoomLevel = 14.0f;
    Runnable runnable = new Runnable() {
        @Override
        public void run() {

            //updateVechileLocation();
            GetMapData();

        }
    };

    public static Fragment getInstance(Context ct, FragmentManager fm, Bundle b) {

        mContext = ct;
        fragmentmanger = fm;
        fragment = new TrackRide_Fragment();
        booking_id = b.getString("booking_id");
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //View view=inflater.inflate(R.layout.your_ride, container, false);
        View view = inflater.inflate(R.layout.your_ride, container, false);
        mContext = getActivity();
        ((Activity) mContext).getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
        tvlocation=(TextView)view.findViewById(R.id.tvlocation);
        DashBoardActivity.hideSearchButton(mContext);
        DashBoardActivity.hideLoacationButton(mContext);
        DashBoardActivity.hideafteredit(mContext);
        DashBoardActivity.hidebeforeedit(mContext);
        DashBoardActivity.hideMenuDrawerButton(mContext);
        DashBoardActivity.showBackButton(mContext);
        DashBoardActivity.hidecityname_header(mContext);
        DashBoardActivity.updatedTitle("Your Ride");
        DashBoardActivity.hidePrintIcon(mContext);
        DashBoardActivity.showhomeButton(mContext);
        DashBoardActivity.slidemenu.setSlidingEnabled(false);
        DashBoardActivity activity = (DashBoardActivity) getActivity();
        mMapView = (MapView) view.findViewById(R.id.mapView);
        mMapView.onCreate(savedInstanceState);
        mMapView.onResume();// needed to get the map to display immediately
        try {
            MapsInitializer.initialize(mContext);
            googleMap.setOnCameraChangeListener(mOnCameraChangeListener);
        } catch (Exception e) {
            e.printStackTrace();
        }
        googleMap = mMapView.getMap();
        markerOptions = new MarkerOptions();
        handler = new Handler();
        checkGooglePlayService();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
        googleMap.clear();
        if (handler!=null)
        {
            handler.removeCallbacks(runnable);
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        googleMap.clear();
        if (handler!=null)
        {
            handler.removeCallbacks(runnable);
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        if (handler!=null)
        {
            handler.removeCallbacks(runnable);
        }
    }
    GoogleMap.OnCameraChangeListener mOnCameraChangeListener = new GoogleMap.OnCameraChangeListener() {

        @Override
        public void onCameraChange(CameraPosition position) {
            if(zoomLevel != position.zoom){
                zoomLevel = position.zoom;
                //isZooming = true;

            }
        }
    };

    private void checkGooglePlayService() {
        // Getting status
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity());
        // Showing status
        if (status == ConnectionResult.SUCCESS) {
            GetMapData();
        } else {
            int requestCode = 10;
            Dialog dialog = GooglePlayServicesUtil.getErrorDialog(status, getActivity(), requestCode);
            dialog.show();
        }
    }
    public void GetMapData() {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("b_id", booking_id);
        GlobalValues.getMethodManagerObj(mContext).GetMap_Data(params,new MethodManager_Listner() {
            @Override
            public void onError() {
                if (handler!=null)
                {
                    handler.removeCallbacks(runnable);
                }

            }

            @Override
            public void onSuccess(String json1) {
                try {
                    JSONObject json = new JSONObject(json1);
                    String message = json.getString("msg");
                    String status = json.getString("status");
                    if (status.equalsIgnoreCase("false")) {
                        mapdata_array_list = new ArrayList<Mapdata_bean>();
                        showDialogForConfirmEmail("Message", message);
                        if (handler!=null)
                        {
                            handler.removeCallbacks(runnable);
                        }

                    } else if (status.equalsIgnoreCase("false") && message.equalsIgnoreCase("No bus route found"))
                    {
                        showDialogForConfirmEmail("Message", message);
                        if (handler!=null)
                        {
                            handler.removeCallbacks(runnable);
                        }
                    }
                    else if (status.equals("true")) {
                        JSONArray jsonArray = json.getJSONArray("logs");
                        mapdata_array_list = new ArrayList<Mapdata_bean>();
                        polylineOptions = new PolylineOptions();

                        if (googleMap != null) {
                            googleMap.clear();
                        }

                        double gps_lat = 0;
                        double gps_long = 0;
                        for (int i = 0; i < jsonArray.length(); i++) {
                            Mapdata_bean databean = new Mapdata_bean();
                            gps_lat = jsonArray.getJSONObject(i).getDouble("gps_lat");
                            databean.setGps_lat(gps_lat);
                            gps_long = jsonArray.getJSONObject(i).getDouble("gps_long");
                            databean.setGps_long(gps_long);
                            String gps_location = jsonArray.getJSONObject(i).getString("gps_location");
                            databean.setGps_location(gps_location);

                            Log.e("Lat,Lng=", "" + gps_lat + "," + gps_long + ",Location" + gps_location);

                            //Reverse Location arraylist elements
                            mapdata_array_list.add(databean);
                            Collections.reverse(mapdata_array_list);
                            showMarker(gps_lat, gps_long);
                        }

                        googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(
                                new LatLng(gps_lat, gps_long), 17));
                        Log.e("Check size", mapdata_array_list.size() - 1 + "");
                        tvlocation.setText(mapdata_array_list.get(mapdata_array_list.size()).getGps_location());

                        /*for(int i=0;i<mapdata_array_list.size();i++){
                            showMarker(mapdata_array_list.get(i).getGps_lat(), mapdata_array_list.get(i).getGps_long());
                        }*/

                        handler.postDelayed(runnable, 2000);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    if (handler!=null)
                    {
                        handler.removeCallbacks(runnable);
                    }
                }

            }
        });


    }

    private void updateVechileLocation() {

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("b_id", booking_id);
        GlobalValues.getMethodManagerObj(mContext).GetMap_Data(params, new MethodManager_Listner() {
            @Override
            public void onError() {
                if (handler != null) {
                    handler.removeCallbacks(runnable);
                }
            }

            @Override
            public void onSuccess(String json1) {

                try {
                    JSONObject json = new JSONObject(json1);
                    String message = json.getString("msg");
                    String status = json.getString("status");
                    if (status.equalsIgnoreCase("false")) {
                        if (handler != null) {
                            handler.removeCallbacks(runnable);
                        }
                        mapdata_array_list = new ArrayList<Mapdata_bean>();
                        showDialogForConfirmEmail("Message", message);

                    } else if (status.equalsIgnoreCase("false") && message.equalsIgnoreCase("No bus route found"))
                    {
                        if (handler != null) {
                            handler.removeCallbacks(runnable);
                        }
                        showDialogForConfirmEmail("Message", message);
                    } else if (status.equals("true")) {
                        JSONArray jsonArray = json.getJSONArray("logs");
                        if (jsonArray != null) {

                            Log.e("thread location array",""+jsonArray.toString());

                            double gps_lat = jsonArray.getJSONObject(0).getDouble("gps_lat");
                            double gps_long = jsonArray.getJSONObject(0).getDouble("gps_long");
                            String gps_location=jsonArray.getJSONObject(0).getString("gps_location");

                            Log.e("Lat,Lng=", "" + gps_lat + "," + gps_long+",Location"+gps_location);
                            tvlocation.setText(gps_location);
                            showMarker(gps_lat,gps_long);
                            handler.postDelayed(runnable, 10000);
                        }
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                    if (handler != null) {
                        handler.removeCallbacks(runnable);
                    }
                }

            }
        });
    }

    private void showMarker(double gps_lat,double gps_long){

        markerOptions.position(new LatLng(gps_lat, gps_long));
        Bitmap icon = BitmapFactory.decodeResource(mContext.getResources(),
                R.drawable.pin_icon);
        markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon));//.fromResource(R.drawable.app_icon)
        googleMap.addMarker(markerOptions);

        polylineOptions.color(Color.BLUE);
        polylineOptions.width(4);
        polylineOptions.add(new LatLng(gps_lat, gps_long));
        googleMap.addPolyline(polylineOptions);
    }
    public static void showDialogForConfirmEmail(String title, final String message)

    {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext).setTitle(title).setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                        //DashBoardActivity.displayview(DashBoardActivity.booking_history,null);
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
