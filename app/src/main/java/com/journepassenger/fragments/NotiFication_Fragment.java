package com.journepassenger.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.Toast;

import com.journedriver.data.UtilMethod;
import com.journepassenger.activity.DashBoardActivity;
import com.journepassenger.adapter.Notification_Adapter;
import com.journepassenger.bean.Notification_bean;
import com.journepassenger.utilities.GlobalValues;
import com.journepassenger.utilities.MethodManager_Listner;
import com.journepassenger.utilities.Session_manager;
import com.journyapp.journyapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by administrator on 17/3/16.
 */
public class NotiFication_Fragment extends Fragment {
    public static String tag = "notification";
    public static Fragment fragment;
    static FragmentTransaction ft;
    static public FragmentManager fragmentmanger;
    static public Context mContext;
    View v;
    ListView notification_list;
    String p_id;
    static ArrayList<Notification_bean> noti_arrayList;
    public static Fragment getInstance(Context ct, FragmentManager fm, Bundle b) {
        {
            mContext = ct;
            fragmentmanger = fm;

            fragment = new NotiFication_Fragment();
            return fragment;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        v=inflater.inflate(R.layout.notificationlist_xml,container,false);
        notification_list=(ListView)v.findViewById(R.id.notification_list);
        mContext=getActivity();
        DashBoardActivity.hideSearchButton(mContext);
        DashBoardActivity.hideLoacationButton(mContext);
        DashBoardActivity.hideafteredit(mContext);
        DashBoardActivity.hidebeforeedit(mContext);
        DashBoardActivity.showMenuDrawerButton(mContext);
        DashBoardActivity.hideBackButton(mContext);
        DashBoardActivity.hidecityname_header(mContext);
        DashBoardActivity.updatedTitle("Notifications");
        DashBoardActivity.hidePrintIcon(mContext);
        DashBoardActivity.hidehomeButton(mContext);
        p_id= Session_manager.getPassengerid(mContext);
        GetNotificationList_Data();
        UtilMethod.showToast("helloo",mContext);
        return v;
    }

    public void GetNotificationList_Data()
    {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("passenger_id",p_id);
        Log.e("Notification parma",params.toString());
        GlobalValues.getMethodManagerObj(mContext).GetNotification_Data(params, new MethodManager_Listner() {
            @Override
            public void onError() {

            }

            @Override
            public void onSuccess(String json)
            {
                JSONObject jobject = null;
                try {
                    jobject = new JSONObject(json);
                    String msg = jobject.getString("status");

                    if (msg.equals("false"))
                    {
                        showDialogForConfirmEmail("Message", msg);

                        Toast.makeText(mContext, jobject.getString("msg"), Toast.LENGTH_LONG).show();
                    }
                    else if (msg.equals("true"))
                    {
                        JSONArray responseJsonArray = jobject.getJSONArray("notifications");
                        noti_arrayList=new ArrayList<Notification_bean>();
                        for (int j = 0; j < responseJsonArray.length(); j++)

                        {
                            JSONObject friendsObj = responseJsonArray.getJSONObject(j);
                            Notification_bean bean=new Notification_bean();

                            String noti_id=friendsObj.getString("noti_id");
                            bean.setNoti_id(noti_id);

                            String noti_message=friendsObj.getString("noti_message");
                            bean.setNoti_message(noti_message);

                            String noti_type=friendsObj.getString("noti_type");
                            bean.setNoti_type(noti_type);

                            String noti_datetime=friendsObj.getString("noti_datetime");
                            bean.setNoti_datetime(noti_datetime);

                            noti_arrayList.add(bean);

                        }
                        Notification_Adapter notification_adapter=new Notification_Adapter(mContext,noti_arrayList);
                        notification_list.setAdapter(notification_adapter);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
    public static void showDialogForConfirmEmail(String title, final String message)

    {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext).setTitle(title).setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
