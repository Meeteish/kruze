package com.journepassenger.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.journepassenger.activity.DashBoardActivity;
import com.journyapp.journyapp.R;

/**
 * Created by administrator on 25/2/16.
 */
public class RecentTransection_Fragment extends Fragment
{
    public static String tag="recentTrasection";
    public static Fragment fragment;
    static FragmentTransaction ft;
    static public FragmentManager fragmentmanger;
    static public Context mContext;
    View view;
    ListView treasec_list;
    public static Fragment getInstance(Context ct, FragmentManager fm,Bundle b) {
        {
            mContext = ct;
            fragmentmanger = fm;

            fragment = new RecentTransection_Fragment();
            return fragment;
        }
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view=inflater.inflate(R.layout.recent_transactions,null,false);
        treasec_list=(ListView)view.findViewById(R.id.treasec_list);
        mContext=getActivity();

        DashBoardActivity.hideSearchButton(mContext);
        DashBoardActivity.hideLoacationButton(mContext);
        DashBoardActivity.hideafteredit(mContext);
        DashBoardActivity.hidebeforeedit(mContext);
        DashBoardActivity.hideMenuDrawerButton(mContext);
        DashBoardActivity.showBackButton(mContext);
        DashBoardActivity.updatedTitle("Recent Transactions");
        DashBoardActivity.hidecityname_header(mContext);
        DashBoardActivity.hidePrintIcon(mContext);
        DashBoardActivity.showhomeButton(mContext);

        /*RecentTra_Adapter adapter=new RecentTra_Adapter(mContext);
        treasec_list.setAdapter(adapter);*/
        return view;
    }
}
