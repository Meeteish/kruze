package com.journepassenger.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.location.Address;
import android.location.Geocoder;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.TimePicker;
import android.widget.Toast;

import com.google.android.gms.maps.GoogleMap;
import com.journepassenger.activity.DashBoardActivity;
import com.journepassenger.utilities.GlobalValues;
import com.journepassenger.utilities.MethodManager_Listner;
import com.journepassenger.utilities.Session_manager;
import com.journyapp.journyapp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by administrator on 19/2/16.
 */
public class SuggestRoute_Fragment extends Fragment
{
    public static String tag="SuggestRoutetag";
    public static Fragment fragment;
    static FragmentTransaction ft;
    static public FragmentManager fragmentmanger;
    static public Context mContext;
    View view;
//    MapView mapView;
    GoogleMap googleMap;
    Double stringLatitude;
    Double stringLongitude;
    Handler textHandlerChange;
    Geocoder geocoder;
    List<Address> addresses;
    String address; // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()
    String city;
    String state;
    String country;
    String postalCode;
    String knownName;
    AutoCompleteTextView source;
    AutoCompleteTextView destination;
    TextView sugesst_btn;
    EditText edTravelTimes, edFromTime, edToTime;
    TimePicker tp;
    String Passenger_id;
    ArrayList<String>placesList = new ArrayList<String>();
    ArrayAdapter<String> adap;

    public static Fragment getInstance(Context ct, FragmentManager fm,Bundle b)
    {
        mContext=ct;
        fragmentmanger=fm;
        fragment=new SuggestRoute_Fragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view=inflater.inflate(R.layout.suggestroute_xml,container,false);
//        mapView=(MapView)view.findViewById(R.id.mapView);
        edTravelTimes=(EditText)view.findViewById(R.id.edTravelTimes);
        edFromTime=(EditText)view.findViewById(R.id.edFromTime);
        edToTime=(EditText)view.findViewById(R.id.edToTime);
        source=(AutoCompleteTextView)view.findViewById(R.id.source);
        destination=(AutoCompleteTextView)view.findViewById(R.id.destination);
        sugesst_btn=(TextView)view.findViewById(R.id.sugesst_btn);
        mContext=getActivity();
        DashBoardActivity.hideSearchButton(mContext);
        DashBoardActivity.hideLoacationButton(mContext);
        DashBoardActivity.hideafteredit(mContext);
        DashBoardActivity.hidebeforeedit(mContext);
        DashBoardActivity.showMenuDrawerButton(mContext);
        DashBoardActivity.hideBackButton(mContext);
        DashBoardActivity.hidecityname_header(mContext);
        DashBoardActivity.updatedTitle("Suggest Route");
        DashBoardActivity.hidePrintIcon(mContext);
        DashBoardActivity.hidehomeButton(mContext);
        Passenger_id= Session_manager.getPassengerid(mContext);

//        textHandlerChange = new Handler();
//        textHandlerChange.postDelayed(TextChangeRunnable, 1000);

        edFromTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        mContext);
                tp = new TimePicker(mContext);
                builder.setView(tp);
                builder.setPositiveButton("Done",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {

                                int hh = tp.getCurrentHour();
                                int mm = tp.getCurrentMinute();
                                String min="";
                                String s = null;

                                if (hh >= 0 && hh <= 11)
                                {
                                    s = "AM";
                                }
                                else
                                {
                                    s = "PM";
                                    if (hh != 12)
                                    {
                                        hh = hh - 12;
                                    }
                                }
                                if(mm < 10)
                                {
                                    min = "0"+mm;
                                }
                                else
                                {
                                    min= mm+"";
                                }

                                edFromTime.setText(hh + ":" + min + " " + s);

                            }
                        });
              /*  builder.setNegativeButton("Cancel",
                        new DialogInterface.OnClickListener() {

                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {

                            }
                        });*/
                builder.show();

            }
        });

        edToTime.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(
                        mContext);
                tp = new TimePicker(mContext);
                builder.setView(tp);
                builder.setPositiveButton("Done",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog,
                                                int which) {

                                int hh = tp.getCurrentHour();
                                int mm = tp.getCurrentMinute();
                                String min="";
                                String s = null;

                                if (hh >= 0 && hh <= 11)
                                {
                                    s = "AM";
                                }
                                else
                                {
                                    s = "PM";
                                    if (hh != 12)
                                    {
                                        hh = hh - 12;
                                    }
                                }
                                if(mm < 10)
                                {
                                    min = "0"+mm;
                                }
                                else
                                {
                                    min= mm+"";
                                }

                                edToTime.setText(hh + ":" + min + " " + s);

                            }
                        });

                builder.show();

            }
        });

        /*getNearByPlaces();

        adap = new ArrayAdapter<String>(mContext,android.R.layout.simple_spinner_dropdown_item,placesList);
        source.setAdapter(adap);
        destination.setAdapter(adap);

        source.setThreshold(1);
        destination.setThreshold(1);*/


        /*mapView = (MapView) view.findViewById(R.id.mapView);
        mapView.onCreate(savedInstanceState);
        mapView.onResume();// needed to get the map to display immediately

        try
        {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e)
        {
            e.printStackTrace();
        }
        googleMap = mapView.getMap();*/

        sugesst_btn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if(source.getText().toString().equals(""))
                {
                    Toast.makeText(mContext,"Please set your Source",Toast.LENGTH_LONG).show();
                }
                else if(destination.getText().toString().equals(""))
                {
                    Toast.makeText(mContext,"Please set your Destination",Toast.LENGTH_LONG).show();
                }
                else if(edTravelTimes.getText().toString().equals(""))
                {
                    Toast.makeText(mContext,"Please provide Travel times",Toast.LENGTH_LONG).show();
                }
                else if(edFromTime.getText().toString().equals(""))
                {
                    Toast.makeText(mContext,"From Time is empty",Toast.LENGTH_LONG).show();
                }
                else if(edToTime.getText().toString().equals(""))
                {
                    Toast.makeText(mContext,"To Time is empty",Toast.LENGTH_LONG).show();
                }
                else
                {
                    SendSuggestRoute();
                }
            }
        });

        return  view;
    }
    /*private Runnable TextChangeRunnable = new Runnable() {
        @Override
        public void run() {
            GPSTracker gpsTracker=new GPSTracker(getActivity());
            if (gpsTracker.canGetLocation()) {
                stringLatitude = gpsTracker.latitude;
                stringLongitude = gpsTracker.longitude;
                Toast.makeText(getActivity(),""+stringLatitude+""+stringLongitude,Toast.LENGTH_LONG).show();
                MarkerOptions marker = new MarkerOptions().position(
                        new LatLng(stringLatitude, stringLongitude)).title("");
                googleMap.addMarker(marker);
                CameraPosition cameraPosition = new CameraPosition.Builder()
                        .target(new LatLng(stringLatitude, stringLongitude)).zoom(12).build();
                googleMap.animateCamera(CameraUpdateFactory
                        .newCameraPosition(cameraPosition));
            }
            else
            {

            }
        }
    };*/

   /* @Override
    public void onResume() {
        super.onResume();
        mapView.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        mapView.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mapView.onLowMemory();
    }
*/
    /*public  void getNearByPlaces()
    {
        GlobalValues.getMethodManagerObj(mContext).GetLatLong(new MethodManager_Listner()
        {
            @Override
            public void onError()
            {
                Toast.makeText(mContext, "Error in Lat Long", Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onSuccess(String json)
            {

                try {
                    JSONObject jobj = new JSONObject(json);
                    String status = jobj.optString("status");

                    if (status.equals("OK"))
                    {

                        JSONArray jarr = jobj.getJSONArray("results");

                        for (int i = 0; i < jarr.length(); i++)
                        {
                            JSONObject jsonObj = jarr.getJSONObject(i);
                            String name = jsonObj.getString("name");
                            placesList.add(name);
                            Toast.makeText(mContext, name, Toast.LENGTH_SHORT).show();
                        }
                    } else
                    {
                        Toast.makeText(mContext, "Cannot get nearby places", Toast.LENGTH_SHORT).show();
                    }


                } catch (JSONException e)
                {
                    Toast.makeText(mContext, e.toString(), Toast.LENGTH_SHORT).show();
                }


            }
        });
    }*/

    public void SendSuggestRoute()
    {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("passenger_id",Passenger_id);
        params.put("source",source.getText().toString());
        params.put("destination",destination.getText().toString());
        params.put("travel_times",edTravelTimes.getText().toString());
        params.put("to_time",edFromTime.getText().toString());
        params.put("from_time",edToTime.getText().toString());

        GlobalValues.getMethodManagerObj(mContext).Suggest_Route(params, new MethodManager_Listner() {
            @Override
            public void onError()
            {

            }

            @Override
            public void onSuccess(String json)
            {
                JSONObject jobject = null;
                try {
                    jobject = new JSONObject(json);
                    String msg = jobject.getString("message");
                    DashBoardActivity.displayview(DashBoardActivity.thankyou, null);
                    Toast.makeText(mContext,""+msg,Toast.LENGTH_LONG).show();
                    source.setText("");
                    destination.setText("");
                    edTravelTimes.setText("");
                    edFromTime.setText("");
                    edToTime.setText("");
                    DashBoardActivity.displayview(DashBoardActivity.home, null);

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }
    @Override
    public void onResume()
    {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        super.onResume();

    }

    @Override
    public void onPause()
    {
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        super.onPause();
    }

}
