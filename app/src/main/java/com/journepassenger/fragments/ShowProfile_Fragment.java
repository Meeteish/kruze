package com.journepassenger.fragments;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.journepassenger.activity.DashBoardActivity;
import com.journepassenger.adapter.Citylist_Adapter;
import com.journepassenger.bean.City_bean;
import com.journepassenger.utilities.CircleTransform;
import com.journepassenger.utilities.GlobalValues;
import com.journepassenger.utilities.MethodManager_Listner;
import com.journepassenger.utilities.SessionClass;
import com.journepassenger.utilities.Session_manager;
import com.journyapp.journyapp.R;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by administrator on 10-Feb-16.
 */
public class ShowProfile_Fragment extends Fragment
{
    public static Fragment fragment;
    static FragmentTransaction ft;
    static public FragmentManager fragmentmanger;
    static public Context mContext;
    View view;

    EditText edNameId,edTelephoneId,edCompanyNameId;
    EditText edEmrgncyMobNoId,edWorkLocationId,edHouseLocationId,edDobId;
    EditText edOldPassword, edNewPassword, edNewConfrimPassword;
    TextView edEmailId,tvReferralCode,tvChangePswdId;
    RadioButton rbmale,rbfemale;
    String gender;
    LinearLayout lLayoutHideShowAdditionalInfo;
    RelativeLayout rLayoutAdditionalInfo;
    ImageView uparrow,downarrow,editIcon,imgprofileId;
    int check=0;
    String p_id;
    String passenger_name, passenger_mobile,passenger_email, passenger_gender,comapny_name;
    String passenger_hose_loac,passenger_work_loac, passenger_company,qr_url;
    String passenger_dob,passenger_emergency_num,passenger_referral_code, passenger_image,P_id;
    String passenger_image_mode;
    static String cityid;
    static ArrayList<City_bean> citylist;
    static Spinner city_spinner;
    Typeface font;

    public static Fragment getInstance(Context ct, FragmentManager fm,Bundle b)
    {

        mContext=ct;
        fragmentmanger=fm;

        fragment=new ShowProfile_Fragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view=inflater.inflate(R.layout.profile_show_passenger, null, false);
        mContext=getActivity();
        DashBoardActivity.hideSearchButton(mContext);
        DashBoardActivity.hideafteredit(mContext);
        DashBoardActivity.hidebeforeedit(mContext);
        DashBoardActivity.showMenuDrawerButton(mContext);
        DashBoardActivity.hideBackButton(mContext);
        DashBoardActivity.hidecityname_header(mContext);
        DashBoardActivity.updatedTitle("Profile");
        DashBoardActivity.hideLoacationButton(mContext);
        DashBoardActivity.hidehomeButton(mContext);
        DashBoardActivity.hidePrintIcon(mContext);
        citylist = new ArrayList<City_bean>();
        edNameId = (EditText)view.findViewById(R.id.edNameId);
        edTelephoneId = (EditText)view.findViewById(R.id.edTelephoneId);
        edCompanyNameId = (EditText)view.findViewById(R.id.edCompanyNameId);
        edEmrgncyMobNoId = (EditText)view.findViewById(R.id.edEmrgncyMobNoId);
        edWorkLocationId = (EditText)view.findViewById(R.id.edWorkLocationId);
        edHouseLocationId = (EditText)view.findViewById(R.id.edHouseLocationId);
        edDobId = (EditText)view.findViewById(R.id.edDobId);
        edEmailId = (TextView)view.findViewById(R.id.edEmailId);
        tvReferralCode = (TextView)view.findViewById(R.id.tvReferralCode);
        tvChangePswdId = (TextView)view.findViewById(R.id.tvChangePswdId);
        imgprofileId=(ImageView)view.findViewById(R.id.imgprofileId);
        editIcon=(ImageView)view.findViewById(R.id.editIcon);
        rbmale = (RadioButton)view.findViewById(R.id.rbmale);
        rbfemale = (RadioButton)view.findViewById(R.id.rbfemale);
        downarrow = (ImageView)view.findViewById(R.id.imgdownarrowid);
        uparrow = (ImageView)view.findViewById(R.id.imguparrowid);
        city_spinner=(Spinner)view.findViewById(R.id.city_spinner);
        lLayoutHideShowAdditionalInfo = (LinearLayout)view.findViewById(R.id.hideShowAdditionalInfo);
        rLayoutAdditionalInfo = (RelativeLayout)view.findViewById(R.id.relativeLayoutAdditionalInfo);

        p_id= Session_manager.getPassengerid(mContext);

        font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Light.ttf");
        rbfemale.setTypeface(font);
        rbmale.setTypeface(font);

        GetUser_Data();
        getCity_data();

        tvChangePswdId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                final Dialog dialog = new Dialog(mContext);
                LayoutInflater li = (LayoutInflater) mContext.getApplicationContext()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View vi = li.inflate(R.layout.change_password, null, false);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(vi);
                dialog.show();

                edOldPassword=(EditText)vi.findViewById(R.id.edOldPassword);
                edNewPassword=(EditText)vi.findViewById(R.id.edNewPassword);
                edNewConfrimPassword=(EditText)vi.findViewById(R.id.edNewConfrimPassword);
                TextView btnDone=(TextView)vi.findViewById(R.id.btnDone);
                TextView btnCancel=(TextView)vi.findViewById(R.id.btnCancel);

                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });

                btnDone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (edOldPassword.getText().toString().equals("")) {
                            Toast.makeText(mContext, "Password can not be blank", Toast.LENGTH_LONG).show();
                        } else if (edNewPassword.getText().toString().length() == 0) {
                            edNewPassword.setError("Password can not be blank");
                        } else if (!edNewConfrimPassword.getText().toString().equals(edNewPassword.getText().toString())) {
                            edNewConfrimPassword.setError("Password not matched");
                        } else {
                            changePassword(edOldPassword.getText().toString(), edNewPassword.getText().toString());
                            dialog.dismiss();
                        }

                    }
                });

            }
        });

        lLayoutHideShowAdditionalInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (check == 0) {
                    rLayoutAdditionalInfo.setVisibility(View.VISIBLE);
                    downarrow.setVisibility(View.INVISIBLE);
                    uparrow.setVisibility(View.VISIBLE);
                    check = 1;
                } else {
                    rLayoutAdditionalInfo.setVisibility(View.INVISIBLE);
                    downarrow.setVisibility(View.VISIBLE);
                    uparrow.setVisibility(View.INVISIBLE);
                    check = 0;
                }

            }
        });

        editIcon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DashBoardActivity.displayview(DashBoardActivity.editprofile,null);
            }
        });



        return view;
    }

    public void changePassword(String oldpassword, String newpassword)
    {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("passenger_id",p_id);
        params.put("old_pass",oldpassword);
        params.put("new_pass",newpassword);

        GlobalValues.getMethodManagerObj(mContext).change_password(params, new MethodManager_Listner() {
            @Override
            public void onError() {

            }

            @Override
            public void onSuccess(String json)
            {
                JSONObject jobject = null;
                try {
                    jobject = new JSONObject(json);
                    String msg = jobject.getString("status");

                    if (msg.equals("false"))
                    {
                        Toast.makeText(mContext, jobject.getString("msg"), Toast.LENGTH_LONG).show();
                    }
                    else if (msg.equals("true"))
                    {
                        Toast.makeText(mContext, jobject.getString("msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

    }


    public void GetUser_Data()
    {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("passenger_id",p_id);
        GlobalValues.getMethodManagerObj(mContext).userProfileData(params, new MethodManager_Listner() {
            @Override
            public void onError() {
            }

            @Override
            public void onSuccess(String json) {

                try {
                    JSONObject jobject = new JSONObject(json);
                    String status = jobject.getString("status");
                    if (status.equals("true")) {
                        passenger_name = jobject.optString("passenger_name");
                        passenger_mobile = jobject.optString("passenger_mobile");
                        passenger_email = jobject.optString("passenger_email");
                        passenger_gender = jobject.optString("passenger_gender");
                        passenger_hose_loac = jobject.optString("passenger_hose_loac");
                        passenger_work_loac = jobject.optString("passenger_work_loac");
                        passenger_company = jobject.optString("passenger_company");
                        passenger_dob = jobject.optString("passenger_dob");
                        passenger_emergency_num = jobject.optString("passenger_emergency_num");
                        passenger_referral_code = jobject.optString("passenger_referral_code");
                        P_id = jobject.optString("passenger_id");
                        qr_url = jobject.optString("passenger_qrcode_image");
                        rbfemale.setChecked(false);
                        rbmale.setChecked(false);
                        passenger_image = jobject.optString("passenger_image");
                        comapny_name=jobject.optString("passenger_company");
                        passenger_image_mode=jobject.optString("image_type");
                        //Toast.makeText(mContext,passenger_gender,Toast.LENGTH_SHORT).show();
                        Session_manager.saveUserInfo(mContext, passenger_name, passenger_email, P_id, passenger_image, qr_url, passenger_referral_code, passenger_mobile,comapny_name,passenger_image_mode);
                        if (passenger_gender.equals("1")) {
                            rbfemale.setChecked(false);
                            rbmale.setChecked(true);
                        } else {
                            rbfemale.setChecked(true);
                            rbmale.setChecked(false);
                        }

                        edNameId.setText(passenger_name);
                        edTelephoneId.setText(passenger_mobile);
                        edCompanyNameId.setText(passenger_company);
                        edEmrgncyMobNoId.setText(passenger_emergency_num);
                        edWorkLocationId.setText(passenger_work_loac);
                        edHouseLocationId.setText(passenger_hose_loac);
                        edDobId.setText(passenger_dob);
                        edEmailId.setText(passenger_email);
                        tvReferralCode.setText(passenger_referral_code);

//                       To set profile pic
                        if(passenger_image_mode.equals("1"))
                            Picasso.with(mContext).load(passenger_image).rotate(90).transform(new CircleTransform()).into(imgprofileId);
                        else
                            Picasso.with(mContext).load(passenger_image).transform(new CircleTransform()).into(imgprofileId);


                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    /*public static void showCountryListDialog(final ArrayList<City_bean> LanguageList) {

        try {

            final CustomPopupAdapter adapter;
            final Dialog dialog = new Dialog(mContext);
            LayoutInflater li = (LayoutInflater) mContext.getApplicationContext()
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            View vi = li.inflate(R.layout.popup_view, null, false);
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            dialog.setContentView(vi);
            dialog.setCanceledOnTouchOutside(false);
            ListView listView = (ListView) vi.findViewById(R.id.list_view);

            //ImageView cancel_img = (ImageView) vi.findViewById(R.id.cancel_img);
            TextView title_tv = (TextView) vi.findViewById(R.id.title);
            title_tv.setText("Select City");

            //final EditText Search_et = (EditText) vi.findViewById(R.id.Search_et);
            //Search_et.setVisibility(View.GONE);

            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

                @Override
                public void onItemClick(AdapterView<?> arg0, View arg1, int arg2, long arg3) {

                    dialog.dismiss();
                    cityid = citylist.get(arg2).getCity_id();
                    //Toast.makeText(mContext,""+cityid,Toast.LENGTH_LONG).show();
                    //DashBoardActivity.setdialogtitle(citylist.get(arg2).getCity_name());
                    *//*SharedPreferences pref = mContext.getSharedPreferences("City_name", Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = pref.edit();
                    editor.putString("cityname", citylist.get(arg2).getCity_name());
                    editor.putString("city_id", cityid);
                    editor.commit();*//*
                    //GetexpandList_data(cityid);
                    Session_manager.SetCityPrefrence(mContext,citylist.get(arg2).getCity_name(),cityid);
                    Toast.makeText(mContext,"City Update successfully",Toast.LENGTH_LONG).show();
                    *//*Session_manager.ClearPrefrence_city(mContext);
                    Session_manager.ClearPrefrence_tocity(mContext);*//*

                    Session_manager.setTocity(mContext, null);
                    Session_manager.setfromcity(mContext, null);
                }
            });


            Button ok_bt = (Button) vi.findViewById(R.id.ok_bt);

            ok_bt.setVisibility(View.GONE);

            adapter = new CustomPopupAdapter(LanguageList, mContext);
            listView.setAdapter(adapter);
            listView.setTextFilterEnabled(true);


            dialog.show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/
    public static void getCity_data() {
        GlobalValues.getMethodManagerObj(mContext).Getcity_list(new MethodManager_Listner() {
            @Override
            public void onError() {

            }

            @Override
            public void onSuccess(String json) {
                citylist = getcitylist(json);
                //showCountryListDialog(citylist);

                final ArrayList<String> ids=new ArrayList<String>();
                final ArrayList<String> name=new ArrayList<String>();

                ids.add("-1");
                name.add("Change City");
                for (int i = 0; i < citylist.size(); i++)
                {
                    ids.add(citylist.get(i).getCity_id());
                    name.add(citylist.get(i).getCity_name());
                }

                Citylist_Adapter adapter=new Citylist_Adapter(mContext, R.layout.popup_item_city,name);
                adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                city_spinner.setAdapter(adapter);

                city_spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
					/*int itemposition=product_subcat.getSelectedItemPosition();
					subcatogary_id=subcatogary_list.get(itemposition).getCategory_id();*/
                        String itemposition = ids.get(position);
                        String city_id = ids.get(position);
                        String city_name = name.get(position);
                        if (city_id=="-1")
                        {

                        }
                        else
                        {
                            Session_manager.SetCityPrefrence(mContext, city_name, city_id);
                            SessionClass.setCurrentLongitude(0.0);
                            SessionClass.setCurrentLatitude(0.0);
                        }
                    }


                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {
                        // TODO Auto-generated method stub

                    }
                });

            }
        });
    }

    public static ArrayList<City_bean> getcitylist(String json)
    {
        ArrayList<City_bean> getcityarray = new ArrayList<City_bean>();
        try {
            JSONObject jobj = new JSONObject(json);
            JSONArray jarray = jobj.getJSONArray("city");
            for (int i = 0; i < jarray.length(); i++) {
                City_bean cbean = new City_bean();
                cbean.setCity_id(jarray.getJSONObject(i).getString("city_id"));
                cbean.setCity_name(jarray.getJSONObject(i).getString("city_name"));

                getcityarray.add(cbean);
            }
        } catch (JSONException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }

        return getcityarray;
    }
}
