package com.journepassenger.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.journepassenger.activity.DashBoardActivity;
import com.journepassenger.utilities.GlobalValues;
import com.journepassenger.utilities.MethodManager_Listner;
import com.journepassenger.utilities.Session_manager;
import com.journyapp.journyapp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by administrator on 19/2/16.
 */
public class WalletBalance extends Fragment
{
    public static String tag="walletTag";
    public static Fragment fragment;
    static FragmentTransaction ft;
    static public FragmentManager fragmentmanger;
    static public Context mContext;
    View view;
    LinearLayout liner_cash,liner_rewardspt;
    String p_id;
    String rides;
    String points;
    String balance;
    TextView tvJourneCash,tvRewardPoints,free_ride;
    public static Fragment getInstance(Context ct, FragmentManager fm,Bundle b)
    {

        mContext=ct;
        fragmentmanger=fm;

        fragment=new WalletBalance();

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view=inflater.inflate(R.layout.wallet_screen,container,false);
        liner_cash=(LinearLayout)view.findViewById(R.id.liner_cash);
        tvJourneCash=(TextView)view.findViewById(R.id.tvJourneCash);
        tvRewardPoints=(TextView)view.findViewById(R.id.tvRewardPoints);
        free_ride=(TextView)view.findViewById(R.id.free_ride);
        liner_rewardspt=(LinearLayout)view.findViewById(R.id.liner_rewardspt);
        mContext=getActivity();
        DashBoardActivity.hideSearchButton(mContext);
        DashBoardActivity.hideLoacationButton(mContext);
        DashBoardActivity.hideafteredit(mContext);
        DashBoardActivity.hidebeforeedit(mContext);
        DashBoardActivity.showMenuDrawerButton(mContext);
        DashBoardActivity.hideBackButton(mContext);
        DashBoardActivity.hidecityname_header(mContext);
        DashBoardActivity.updatedTitle("Wallet");
        DashBoardActivity.hidehomeButton(mContext);
        DashBoardActivity.hidePrintIcon(mContext);
        p_id = Session_manager.getPassengerid(mContext);
        GetWalletData();
        liner_cash.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DashBoardActivity.displayview(DashBoardActivity.recenttrasection, null);
            }
        });
        liner_rewardspt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                Bundle bundle=new Bundle();
                bundle.putString("total_points",points);
              DashBoardActivity.displayview(DashBoardActivity.Rewards_Points,bundle);
            }
        });
        return view;
    }
    public void GetWalletData() {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("passenger_id", p_id);
        GlobalValues.getMethodManagerObj(mContext).GeTreferData(params, new MethodManager_Listner() {
            @Override
            public void onError() {

            }

            @Override
            public void onSuccess(String json) {
                try {
                    JSONObject jsonObject = new JSONObject(json);
                    String status = jsonObject.getString("status");
                    if (status.equals("false")) {
                        Toast.makeText(mContext, jsonObject.getString("msg").toString(), Toast.LENGTH_LONG).show();

                    } else if (status.equals("true"))
                    {
                        rides=jsonObject.getString("rides");
                        points=jsonObject.getString("points");
                        balance=jsonObject.getString("balance");
                        tvJourneCash.setText(balance);
                        tvRewardPoints.setText(points);
                        free_ride.setText(rides);


                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        });
    }
}
