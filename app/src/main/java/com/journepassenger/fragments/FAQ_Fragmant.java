package com.journepassenger.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.EditText;
import android.widget.TextView;

import com.journepassenger.activity.DashBoardActivity;
import com.journyapp.journyapp.R;

/**
 * Created by administrator on 31/3/16.
 */
public class FAQ_Fragmant extends Fragment {
    public static Fragment fragment;
    static FragmentTransaction ft;
    static public FragmentManager fragmentmanger;
    static public Context mContext;
    View view;
    private WebView webView;



    public static Fragment getInstance(Context ct, FragmentManager fm, Bundle b) {

        mContext = ct;
        fragmentmanger = fm;

        fragment = new FAQ_Fragmant();

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view=inflater.inflate(R.layout.faq,container,false);
        mContext = getActivity();
        webView = (WebView) view.findViewById(R.id.webView1);
        DashBoardActivity.hideSearchButton(mContext);
        DashBoardActivity.hideLoacationButton(mContext);
        DashBoardActivity.hideafteredit(mContext);
        DashBoardActivity.hidebeforeedit(mContext);
        DashBoardActivity.showMenuDrawerButton(mContext);
        DashBoardActivity.hideBackButton(mContext);
        DashBoardActivity.hidecityname_header(mContext);
        DashBoardActivity.updatedTitle("FAQ");
        DashBoardActivity.hidePrintIcon(mContext);
        DashBoardActivity.hidehomeButton(mContext);
        startWebView("http://54.67.85.189/journe/webservices/html/faq.html");
        return view;
    }
    private void startWebView(String url) {

        // Create new webview Client to show progress dialog
        // When opening a url or click on link

        webView.setWebViewClient(new WebViewClient() {


            // If you will not use this method url links are opeen in new brower
            // not in webview
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                view.loadUrl(url);
                return true;
            }

        });

        // Javascript inabled on webview
        webView.getSettings().setJavaScriptEnabled(true);

        webView.loadUrl(url);

    }
}
