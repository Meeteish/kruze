package com.journepassenger.fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.journedriver.data.UtilMethod;
import com.journepassenger.activity.DashBoardActivity;
import com.journepassenger.utilities.GlobalValues;
import com.journepassenger.utilities.MethodManager_Listner;
import com.journepassenger.utilities.Session_manager;
import com.journyapp.journyapp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by administrator on 19/2/16.
 */
public class BookCar_Bus_Fragment extends Fragment
{
    public static String tag="BookCarTag";
    public static Fragment fragment;
    static FragmentTransaction ft;
    static public FragmentManager fragmentmanger;
    static public Context mContext;
    View view;
    EditText edJourneToId;
    EditText edJourneFromId;
    EditText edDate;
    EditText edmsgid;
    TextView submitbtn;
    String passenger_id;
    Calendar calendar;
    String getdate;
    String vechicle_type;
    ImageView car_active,car_inactive;
    ImageView bus_active,bus_inactive;
    ImageView plane_active,plane_inactive;
    int vehicleCheck=0;
    int reward_point_check = 0;
    int clickCheck=0;

    public static Fragment getInstance(Context ct, FragmentManager fm,Bundle b)
    {

        mContext=ct;
        fragmentmanger=fm;

        fragment=new BookCar_Bus_Fragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view=inflater.inflate(R.layout.book_car_bus, container, false);
        mContext=getActivity();
        edJourneFromId=(EditText)view.findViewById(R.id.edJourneFromId);
        edJourneToId=(EditText)view.findViewById(R.id.edJourneToId);
        edDate=(EditText)view.findViewById(R.id.edDate);
        edmsgid=(EditText)view.findViewById(R.id.edmsgid);
        submitbtn=(TextView)view.findViewById(R.id.submitbtn);
        car_active=(ImageView)view.findViewById(R.id.car_active);
        car_inactive=(ImageView)view.findViewById(R.id.car_inactive);
        bus_active=(ImageView)view.findViewById(R.id.bus_active);
        bus_inactive=(ImageView)view.findViewById(R.id.bus_inactive);
        plane_active=(ImageView)view.findViewById(R.id.plane_active);
        plane_inactive=(ImageView)view.findViewById(R.id.plane_inactive);

        passenger_id= Session_manager.getPassengerid(mContext);

        DashBoardActivity.hideSearchButton(mContext);
        DashBoardActivity.hideLoacationButton(mContext);
        DashBoardActivity.hideafteredit(mContext);
        DashBoardActivity.hidebeforeedit(mContext);
        DashBoardActivity.showMenuDrawerButton(mContext);
        DashBoardActivity.hideBackButton(mContext);
        DashBoardActivity.hidehomeButton(mContext);
        DashBoardActivity.setdialogtitle("");
        DashBoardActivity.updatedTitle("Book A Car/Bus");
        DashBoardActivity.hidePrintIcon(mContext);
        DashBoardActivity.hidecityname_header(mContext);
        calendar = Calendar.getInstance();

        car_inactive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                car_active.setVisibility(View.VISIBLE);
                car_inactive.setVisibility(View.INVISIBLE);
                bus_active.setVisibility(View.INVISIBLE);
                bus_inactive.setVisibility(View.VISIBLE);
                plane_active.setVisibility(View.INVISIBLE);
                plane_inactive.setVisibility(View.VISIBLE);

                vehicleCheck=1;
            }
        });

        bus_inactive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                car_active.setVisibility(View.INVISIBLE);
                car_inactive.setVisibility(View.VISIBLE);
                bus_active.setVisibility(View.VISIBLE);
                bus_inactive.setVisibility(View.INVISIBLE);
                plane_active.setVisibility(View.INVISIBLE);
                plane_inactive.setVisibility(View.VISIBLE);

                vehicleCheck=2;
            }
        });

        plane_inactive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                car_active.setVisibility(View.INVISIBLE);
                car_inactive.setVisibility(View.VISIBLE);
                bus_active.setVisibility(View.INVISIBLE);
                bus_inactive.setVisibility(View.VISIBLE);
                plane_active.setVisibility(View.VISIBLE);
                plane_inactive.setVisibility(View.INVISIBLE);

                vehicleCheck=3;
                if(reward_point_check == 0)
                {
                    ShowDialog();
                    GetReward_Planepopup();
                    reward_point_check=1;
                }
                else
                {
                    vehicleCheck=0;
                    //Toast.makeText(mContext,"You have already get Rewards points",Toast.LENGTH_LONG).show();
                    ShowDialog_afterreawrd();
                }


            }
        });

        edDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDatePicker();
            }
        });

        submitbtn.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (edJourneFromId.getText().toString().length()==0)
                {
                    edJourneFromId.setError("Field From can not be blank");
                }
                else if(edJourneToId.getText().toString().length()==0)
                {
                    edJourneToId.setError("Field To can not be blank");
                }
                else if (edDate.getText().toString().length()==0)
                {
                    Toast.makeText(mContext,"Select Date",Toast.LENGTH_SHORT).show();
                }
                else if (vehicleCheck==0 || vehicleCheck == 3)
                {
                    Toast.makeText(mContext,"Please select Vehicle",Toast.LENGTH_SHORT).show();
                }
                else if (edmsgid.getText().toString().length()==0)
                {
                    edmsgid.setError("Message can not be blank");
                }
                else
                {
//                    Toast.makeText(mContext,"Done",Toast.LENGTH_SHORT).show();
                    SendBookingDetails();
                }

            }
        });
        return view;
    }

    public void ShowDialog() {
        final Dialog reward_points = new Dialog(mContext);
        reward_points.requestWindowFeature(Window.FEATURE_NO_TITLE);
        reward_points.setContentView(R.layout.popup_plane_rewardspoint);

        reward_points.setCancelable(false);
        reward_points.setCanceledOnTouchOutside(false);

        final TextView btn_done = (TextView) reward_points.findViewById(R.id.btn_done);
//        final TextView btn_submit=(TextView)reward_points.findViewById(R.id.btn_submit);
        //final EditText dealcode=(EditText)reward_points.findViewById(R.id.u_dealcode);
        //dealcode.setVisibility(View.GONE);

        btn_done.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                reward_points.dismiss();
                plane_inactive.setVisibility(View.VISIBLE);
            }
        });
        reward_points.show();
    }

    public void SendBookingDetails()
    {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("passenger_id", passenger_id);
        params.put("source", edJourneFromId.getText().toString());
        params.put("destination", edJourneToId.getText().toString());
        params.put("date", getdate);
        params.put("message",edmsgid.getText().toString());

        if(vehicleCheck==1)
        {
            vechicle_type= "Car";
        }
        if(vehicleCheck==2)
        {
            vechicle_type= "Bus";
        }
        params.put("vechicle_type", vechicle_type);
        GlobalValues.getMethodManagerObj(mContext).BookCarBus(params, new MethodManager_Listner() {
            @Override
            public void onError() {

            }

            @Override
            public void onSuccess(String json) {
                Log.e("Book Car Bus", json);
                JSONObject jobject = null;
                try {
                    jobject = new JSONObject(json);
                    String msg = jobject.getString("msg");

                    showDialogForConfirmEmail("Message", msg);

                    car_active.setVisibility(View.INVISIBLE);
                    car_inactive.setVisibility(View.VISIBLE);
                    bus_active.setVisibility(View.INVISIBLE);
                    bus_inactive.setVisibility(View.VISIBLE);
                    plane_active.setVisibility(View.INVISIBLE);
                    plane_inactive.setVisibility(View.VISIBLE);
                    vehicleCheck = 0;
                    edJourneFromId.requestFocus();
                    edDate.setText("");
                    edmsgid.setText("");
                    edJourneFromId.setText("");
                    edJourneToId.setText("");
                    DashBoardActivity.displayview(DashBoardActivity.home,null);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }
         public void GetReward_Planepopup()
         {
             Map<String, Object> params = new HashMap<String, Object>();
             params.put("passenger_id", passenger_id);
             GlobalValues.getMethodManagerObj(mContext).GeTRewardspoint_plane(params, new MethodManager_Listner() {
                 @Override
                 public void onError() {

                 }

                 @Override
                 public void onSuccess(String json) {
                     JSONObject jobject = null;
                     try {
                         jobject = new JSONObject(json);
                         String msg = jobject.getString("msg");
                         //showDialogForConfirmEmail("Message", msg);
                         if (msg.equals("Successfully submitted"))
                         {
                             //Toast.makeText(mContext,"congratulations!You Have Get 10 Credit Point",Toast.LENGTH_LONG).show();
                         }


                     } catch (JSONException e) {
                         e.printStackTrace();
                     }
                 }
             });
         }
    public void openDatePicker() {

        final Calendar c = Calendar.getInstance();
        int mYear = c.get(Calendar.YEAR);
        int mMonth = c.get(Calendar.MONTH);
        int mDay = c.get(Calendar.DAY_OF_MONTH);

        // Launch Date Picker Dialog
        DatePickerDialog dpd = new DatePickerDialog(mContext, new DatePickerDialog.OnDateSetListener() {
            @SuppressLint("SimpleDateFormat")
            @Override
            public void onDateSet(DatePicker arg0, int year, int monthOfYear, int dayOfMonth)
            {
                calendar.set(Calendar.YEAR, year);
                calendar.set(Calendar.MONTH, monthOfYear);
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
                Date date1 = null;
                try {
                    date1 = sdf.parse(year + "-" + (monthOfYear + 1) + "-" + dayOfMonth);
                } catch (java.text.ParseException e) {

                    e.printStackTrace();
                }
                Date todaydate = new Date();
                String todayDate = sdf.format(todaydate);
                Date dateToday=null;
                try
                {
                    dateToday = sdf.parse(todayDate);
                }
                catch (java.text.ParseException e1) {
                    // TODO Auto-generated catch block
                    e1.printStackTrace();
                }
                if(date1.compareTo(dateToday) < 0) {

                    UtilMethod.showToast("You can't select previous date from today.", mContext);
                }
                else
                {
                    edDate.setText("" + dayOfMonth + "-" + "" + (monthOfYear + 1) + "-" + year);
                    getdate = edDate.getText().toString();

                }
                    }
        }, mYear, mMonth, mDay);

        dpd.show();
    }
    public static void showDialogForConfirmEmail(String title, final String message)

    {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext).setTitle(title).setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
    public void ShowDialog_afterreawrd() {
        final Dialog reward_points = new Dialog(mContext);
        reward_points.requestWindowFeature(Window.FEATURE_NO_TITLE);
        reward_points.setContentView(R.layout.planereward_popup);

        reward_points.setCancelable(false);
        reward_points.setCanceledOnTouchOutside(false);

        final TextView btn_done = (TextView) reward_points.findViewById(R.id.btn_done);
//        final TextView btn_submit=(TextView)reward_points.findViewById(R.id.btn_submit);
        //final EditText dealcode=(EditText)reward_points.findViewById(R.id.u_dealcode);
        //dealcode.setVisibility(View.GONE);

        btn_done.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                reward_points.dismiss();
                plane_inactive.setVisibility(View.VISIBLE);
            }
        });
        reward_points.show();
    }

}
