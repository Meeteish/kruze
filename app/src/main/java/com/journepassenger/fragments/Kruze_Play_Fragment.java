package com.journepassenger.fragments;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.journepassenger.activity.DashBoardActivity;
import com.journyapp.journyapp.R;

/**
 * Created by administrator on 04-Mar-16.
 */
public class Kruze_Play_Fragment extends Fragment
{
    public static String tag="kruzeplay";
    public static Fragment fragment;
    static android.support.v4.app.FragmentTransaction ft;
    static public FragmentManager fragmentmanger;
    static public Context mContext;
    View view;

    ImageView active_music,inactive_music;
    ImageView active_songs,inactive_songs;
    ImageView active_youtube,inactive_youtube;
    ImageView active_news,inactive_news;
    ImageView active_ebooks,inactive_ebooks;
    RelativeLayout rltvMusic,rltvSong,rltvYouTube,rltvNews,rltvEbooks;
    TextView btnSubmit;
    int music_check = 0;
    int song_check = 0;
    int youtube_check = 0;
    int news_check = 0;
    int ebooks_check = 0;



    public static Fragment getInstance(Context ct ,FragmentManager fm , Bundle b)
    {
        mContext= ct;
        fragmentmanger=fm;
        fragment= new Kruze_Play_Fragment();
        return fragment;
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.kruze_play, null, false);
        mContext= getActivity();
        DashBoardActivity.hideSearchButton(mContext);
        DashBoardActivity.hideLoacationButton(mContext);
        DashBoardActivity.hideafteredit(mContext);
        DashBoardActivity.hidebeforeedit(mContext);
        DashBoardActivity.showMenuDrawerButton(mContext);
        DashBoardActivity.hideBackButton(mContext);
        DashBoardActivity.hidecityname_header(mContext);
        DashBoardActivity.updatedTitle("Kruze Play");
        DashBoardActivity.hidePrintIcon(mContext);
        DashBoardActivity.hidehomeButton(mContext);

        active_music =(ImageView) view.findViewById(R.id.active_music);
        inactive_music =(ImageView) view.findViewById(R.id.inactive_music);
        active_songs =(ImageView) view.findViewById(R.id.active_songs);
        inactive_songs =(ImageView) view.findViewById(R.id.inactive_songs);
        active_youtube =(ImageView) view.findViewById(R.id.active_youtube);
        inactive_youtube =(ImageView) view.findViewById(R.id.inactive_youtube);
        active_news =(ImageView) view.findViewById(R.id.active_news);
        inactive_news =(ImageView) view.findViewById(R.id.inactive_news);
        active_ebooks =(ImageView) view.findViewById(R.id.active_ebooks);
        inactive_ebooks =(ImageView) view.findViewById(R.id.inactive_ebooks);
        rltvMusic =(RelativeLayout) view.findViewById(R.id.rltvMusic);
        rltvSong =(RelativeLayout) view.findViewById(R.id.rltvSong);
        rltvYouTube =(RelativeLayout) view.findViewById(R.id.rltvYouTube);
        rltvNews =(RelativeLayout) view.findViewById(R.id.rltvNews);
        rltvEbooks =(RelativeLayout) view.findViewById(R.id.rltvEbooks);
        btnSubmit =(TextView) view.findViewById(R.id.btnSubmit);

        active_music.setVisibility(View.INVISIBLE);
        active_songs.setVisibility(View.INVISIBLE);
        active_youtube.setVisibility(View.INVISIBLE);
        active_news.setVisibility(View.INVISIBLE);
        active_ebooks.setVisibility(View.INVISIBLE);
        inactive_music.setVisibility(View.VISIBLE);
        inactive_songs.setVisibility(View.VISIBLE);
        inactive_youtube.setVisibility(View.VISIBLE);
        inactive_news.setVisibility(View.VISIBLE);
        inactive_ebooks.setVisibility(View.VISIBLE);


        rltvMusic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (music_check == 0) {
                    active_music.setVisibility(View.VISIBLE);
                    inactive_music.setVisibility(View.INVISIBLE);
                    music_check = 1;
                } else {
                    active_music.setVisibility(View.INVISIBLE);
                    inactive_music.setVisibility(View.VISIBLE);
                    music_check = 0;
                }
            }
        });

        rltvSong.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (song_check == 0)
                {
                    active_songs.setVisibility(View.VISIBLE);
                    inactive_songs.setVisibility(View.INVISIBLE);
                    song_check=1;
                }
                else
                {
                    active_songs.setVisibility(View.INVISIBLE);
                    inactive_songs.setVisibility(View.VISIBLE);
                    song_check=0;
                }
            }
        });

        rltvYouTube.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (youtube_check == 0)
                {
                    active_youtube.setVisibility(View.VISIBLE);
                    inactive_youtube.setVisibility(View.INVISIBLE);
                    youtube_check = 1;
                } else {
                    active_youtube.setVisibility(View.INVISIBLE);
                    inactive_youtube.setVisibility(View.VISIBLE);
                    youtube_check = 0;
                }
            }
        });


        rltvNews.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (news_check == 0)
                {
                    active_news.setVisibility(View.VISIBLE);
                    inactive_news.setVisibility(View.INVISIBLE);
                    news_check = 1;
                } else {
                    active_news.setVisibility(View.INVISIBLE);
                    inactive_news.setVisibility(View.VISIBLE);
                    news_check = 0;
                }
            }
        });


        rltvEbooks.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                if (ebooks_check == 0)
                {
                    active_ebooks.setVisibility(View.VISIBLE);
                    inactive_ebooks.setVisibility(View.INVISIBLE);
                    ebooks_check = 1;
                } else {
                    active_ebooks.setVisibility(View.INVISIBLE);
                    inactive_ebooks.setVisibility(View.VISIBLE);
                    ebooks_check = 0;
                }
            }
        });

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (music_check == 0 && song_check == 0 && youtube_check == 0 && news_check == 0 && ebooks_check == 0)
                {
                    Toast.makeText(mContext,"Please Select Option",Toast.LENGTH_SHORT).show();
                }
                else
                {
                    showDialogForConfirmEmail("Message","Thanks for your reply, we will get you entertained soon.");
                    DashBoardActivity.displayview(DashBoardActivity.home, null);
                }

            }
        });



        return view;
    }
    public void showDialogForConfirmEmail(String title, final String message)

    {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext).setTitle(title).setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        active_music.setVisibility(View.INVISIBLE);
                        active_songs.setVisibility(View.INVISIBLE);
                        active_youtube.setVisibility(View.INVISIBLE);
                        active_news.setVisibility(View.INVISIBLE);
                        active_ebooks.setVisibility(View.INVISIBLE);
                        inactive_music.setVisibility(View.VISIBLE);
                        inactive_songs.setVisibility(View.VISIBLE);
                        inactive_youtube.setVisibility(View.VISIBLE);
                        inactive_news.setVisibility(View.VISIBLE);
                        inactive_ebooks.setVisibility(View.VISIBLE);

                        music_check = 0;
                        song_check = 0;
                        youtube_check = 0;
                        news_check = 0;
                        ebooks_check = 0;

                        dialog.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
