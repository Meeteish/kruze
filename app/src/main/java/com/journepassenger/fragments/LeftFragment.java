package com.journepassenger.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.journepassenger.activity.DashBoardActivity;
import com.journepassenger.activity.SigninActivity;
import com.journepassenger.utilities.CircleTransform;
import com.journepassenger.utilities.GlobalValues;
import com.journepassenger.utilities.MethodManager_Listner;
import com.journepassenger.utilities.Session_manager;
import com.journyapp.journyapp.R;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


/**
 * Created by administrator on 4/2/16.
 */
public class LeftFragment extends Fragment {

    static Context mContext;
    View view;
   public static ImageView imgProfilePic;
    ImageButton imgQrCode;
    TextView user_name;
    TextView user_email;
    static TextView reward_pt;
    TextView txt_home;
    TextView txt_booking;
    TextView txt_notification;
    TextView txt_suggest;
    TextView txt_kruzplay;
    TextView txt_bookcar;
    TextView txt_perks;
    TextView txt_feedback;
    TextView txt_contactus;
    TextView txt_logout;
    static TextView tvWalletBalance;
    TextView txt_FaQ;
    String name, email, passenger_img;
    static String p_id;
    static String rides;
    static String points;
    static String balance;
    LinearLayout wallet_liner;
    String image_type;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.navigationdrawer_custom, container, false);

        mContext = getActivity();

        name = Session_manager.getPassengername(mContext);
        email = Session_manager.getPassengeremail(mContext);
        passenger_img = Session_manager.getPassengerimg(mContext);
        p_id = Session_manager.getPassengerid(mContext);
        imgProfilePic = (ImageView) view.findViewById(R.id.imgProfilePic);
        user_name = (TextView) view.findViewById(R.id.user_name);
        user_email = (TextView) view.findViewById(R.id.user_email);
        reward_pt = (TextView) view.findViewById(R.id.reward_pt);
        txt_booking = (TextView) view.findViewById(R.id.txt_booking);
        txt_notification = (TextView) view.findViewById(R.id.txt_notification);
        txt_suggest = (TextView) view.findViewById(R.id.txt_suggest);
        txt_kruzplay = (TextView) view.findViewById(R.id.txt_kruzplay);
        txt_bookcar = (TextView) view.findViewById(R.id.txt_bookcar);
        txt_perks = (TextView) view.findViewById(R.id.txt_perks);
        txt_feedback = (TextView) view.findViewById(R.id.txt_feedback);
        txt_contactus = (TextView) view.findViewById(R.id.txt_contactus);
        txt_logout = (TextView) view.findViewById(R.id.txt_logout);
        txt_home = (TextView) view.findViewById(R.id.txt_home);
        tvWalletBalance = (TextView) view.findViewById(R.id.wallet_bal);
        txt_FaQ = (TextView) view.findViewById(R.id.txt_FaQ);
        imgQrCode = (ImageButton) view.findViewById(R.id.imgQrCode);
        wallet_liner=(LinearLayout)view.findViewById(R.id.wallet_liner);
        GetWalletData();
        user_name.setText(name);
        user_email.setText(email);

        image_type = Session_manager.getPassengerImageType(mContext);
        Log.e("image_type@left",image_type);
        Log.e("passenger_img@left",passenger_img);

        //Picasso.with(mContext).load(passenger_img).transform(new CircleTransform()).into(imgProfilePic);

        if(image_type.equals("1"))
            Picasso.with(mContext).load(passenger_img).rotate(90).transform(new CircleTransform()).into(imgProfilePic);
        else
            Picasso.with(mContext).load(passenger_img).transform(new CircleTransform()).into(imgProfilePic);

        txt_logout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //open(v);
                ShowDialog();

            }
        });

        imgQrCode.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DashBoardActivity.slidemenutoggle();
                DashBoardActivity.displayview(DashBoardActivity.memberShipFragment, null);
                /*MemberShipFragment memberShipFragment = new MemberShipFragment();
                getActivity().getSupportFragmentManager().beginTransaction().replace(R.id.driver_frame, memberShipFragment, "memberShipFragment").commit();*/
            }
        });
        imgProfilePic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DashBoardActivity.slidemenutoggle();
                DashBoardActivity.displayview(DashBoardActivity.showprofilefragment, null);
            }
        });
        txt_home.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DashBoardActivity.slidemenutoggle();
                DashBoardActivity.displayview(DashBoardActivity.home, null);
            }
        });

        txt_perks.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DashBoardActivity.slidemenutoggle();
                DashBoardActivity.displayview(DashBoardActivity.referandearnfragment, null);
            }
        });

        txt_feedback.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DashBoardActivity.slidemenutoggle();
                DashBoardActivity.displayview(DashBoardActivity.feedbackfragment, null);
            }
        });

        txt_contactus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DashBoardActivity.slidemenutoggle();
                DashBoardActivity.displayview(DashBoardActivity.contactusfragment, null);

            }
        });
        wallet_liner.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DashBoardActivity.slidemenutoggle();
                DashBoardActivity.displayview(DashBoardActivity.walletTag, null);
            }
        });

        txt_bookcar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DashBoardActivity.slidemenutoggle();
                DashBoardActivity.displayview(DashBoardActivity.BookCarTag, null);
            }
        });
        txt_suggest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DashBoardActivity.slidemenutoggle();
                DashBoardActivity.displayview(DashBoardActivity.SuggestRoutetag, null);
            }
        });
        txt_booking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DashBoardActivity.slidemenutoggle();
                DashBoardActivity.displayview(DashBoardActivity.booking_history, null);
            }
        });
        txt_kruzplay.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DashBoardActivity.slidemenutoggle();
                DashBoardActivity.displayview(DashBoardActivity.kruzeplay, null);
            }
        });
        txt_notification.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DashBoardActivity.slidemenutoggle();
                DashBoardActivity.displayview(DashBoardActivity.notification, null);
            }
        });
        txt_FaQ.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DashBoardActivity.slidemenutoggle();
                DashBoardActivity.displayview(DashBoardActivity.faq, null);
            }
        });
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(image_type.equals("1"))
            Picasso.with(mContext).load(passenger_img).rotate(90).transform(new CircleTransform()).into(imgProfilePic);
        else
            Picasso.with(mContext).load(passenger_img).transform(new CircleTransform()).into(imgProfilePic);
    }

    public static void GetWalletData() {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("passenger_id",p_id);
        GlobalValues.getMethodManagerObj(mContext).GeTreferData(params, new MethodManager_Listner() {
            @Override
            public void onError() {

            }

            @Override
            public void onSuccess(String json) {
                try {
                    JSONObject jsonObject = new JSONObject(json);
                    String status = jsonObject.getString("status");
                    if (status.equals("false")) {
                        Toast.makeText(mContext, jsonObject.getString("msg").toString(), Toast.LENGTH_LONG).show();

                    } else if (status.equals("true")) {
                        rides = jsonObject.getString("rides");
                        points = jsonObject.getString("points");
                        balance = jsonObject.getString("balance");
                        tvWalletBalance.setText("" + balance);
                        reward_pt.setText("" + points + " pts");
                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                }

            }
        });
    }

    public void open(View view) {
        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(mContext);
        alertDialogBuilder.setMessage("Are you sure you want to Logout?");

        alertDialogBuilder.setPositiveButton(Html.fromHtml("<b><i>" + getString(R.string.logout) + "</i><b>"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface arg0, int arg1) {

                Session_manager.ClearPrefrence(mContext);
                Session_manager.clearpref_login(mContext);
                DashBoardActivity.logoutGoogle();
                SigninActivity.check = 0;
                Intent intent = new Intent(mContext, SigninActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                mContext.startActivity(intent);
            }
        });

        alertDialogBuilder.setNegativeButton(Html.fromHtml("<b><i>" + getString(R.string.cancel) + "</i><b>"), new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
   public void ShowDialog() {
       final Dialog user_info = new Dialog(mContext);
       LayoutInflater li = (LayoutInflater) mContext.getApplicationContext()
               .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
       View vi = li.inflate(R.layout.logout_popup, null, false);
       user_info.requestWindowFeature(Window.FEATURE_NO_TITLE);


       final TextView btn_cancel=(TextView)vi.findViewById(R.id.btn_cancel);
       final TextView btn_submit=(TextView)vi.findViewById(R.id.btn_logout);
       //final EditText dealcode=(EditText)user_info.findViewById(R.id.u_dealcode);
       //dealcode.setVisibility(View.GONE);

       btn_cancel.setOnClickListener(new View.OnClickListener() {

           @Override
           public void onClick(View v) {
               // TODO Auto-generated method stub
               user_info.dismiss();
           }
       });
       btn_submit.setOnClickListener(new View.OnClickListener() {

           @Override
           public void onClick(View v) {
               Session_manager.ClearPrefrence(mContext);
               Session_manager.clearpref_login(mContext);
               DashBoardActivity.logoutGoogle();
               SigninActivity.check = 0;
               Intent intent = new Intent(mContext, SigninActivity.class);
               intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
               mContext.startActivity(intent);
               user_info.dismiss();
               }

       });
       user_info.setContentView(vi);
       user_info.setCanceledOnTouchOutside(false);
       user_info.show();

   }

    @Override
    public void onStart() {
        super.onStart();
    }
}
//builder.setPositiveButton(Html.fromHtml("<b><i>" + getString(R.string.ok_button) + "</i><b>"), null);