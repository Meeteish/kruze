package com.journepassenger.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.journepassenger.activity.DashBoardActivity;
import com.journepassenger.utilities.ConnectionDetector;
import com.journepassenger.utilities.GlobalValues;
import com.journepassenger.utilities.MethodManager_Listner;
import com.journepassenger.utilities.Session_manager;
import com.journyapp.journyapp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by administrator on 11-Feb-16.
 */
public class BookingDetail_Fragment extends Fragment
{
    public static Fragment fragment;
    static FragmentTransaction ft;
    static public FragmentManager fragmentmanger;
    static public Context mContext;

    TextView tvAddressSource;
    TextView tvAddressDestination;
    TextView tvSeatType;
    TextView tvSeatCost;
    TextView tvwalletbal;
    TextView tvTotalRs,tvSeatNo,tvBusNo,tvTimeLeaveAt,tvTimeArriveAt,tvSeatNoBellow;
    static String  fromcity_landmark;
    static String tocity_landmark;
    static String price;
    static String seats;
    static ArrayList<String> getSeatData;
    String booked_seats="";
    static String bus_no;
    static String fromS_Time;
    static String toS_Time;
    static String pessenger_id;
    static String trip_id;
    static String bus_id;
    static int seat_price;
    TextView btnConfrim;
    static String fromstop_Id;
    static String tostop_Id;

    ProgressDialog pdialog;
    ImageView imgOnlineActive,imgOnlineInactive;
    ImageView imgCashActive,imgCashInactive;
    RelativeLayout rltvCashPayment, rltvOnlinePayment;
    int payment_check = 1;
    String payment_type;
    static String ac_type;
    static String fromstop_time,tostop_time;
    View view;
    //    RadioButton rbOnlinePayment,rbCashPayment;
    public static Fragment getInstance(Context ct, FragmentManager fm,Bundle b)
    {

        mContext=ct;
        fragmentmanger=fm;

        fragment=new BookingDetail_Fragment();
        fromcity_landmark=b.getString("fromcity_landmark");
        tocity_landmark=b.getString("tocity_landmark");
        price=b.getString("price");
        seats=b.getString("seats");
        bus_no=b.getString("bus_no");
        fromS_Time=b.getString("fromS_Time");
        toS_Time=b.getString("toS_Time");
        getSeatData=b.getStringArrayList("seats");
        trip_id=b.getString("trip_id");
        bus_id=b.getString("bus_id");
        seat_price=b.getInt("seat_price");
        fromstop_Id=b.getString("fromstop_Id");
        tostop_Id=b.getString("tostop_Id");
        ac_type=b.getString("ac_type");
        fromstop_time=b.getString("fromstop_time");
        tostop_time=b.getString("tostop_time");
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.booking_details_new, container,false);
        mContext=getActivity();
        DashBoardActivity.hideSearchButton(mContext);
        DashBoardActivity.hideLoacationButton(mContext);
        DashBoardActivity.hideafteredit(mContext);
        DashBoardActivity.hidebeforeedit(mContext);
        DashBoardActivity.hideMenuDrawerButton(mContext);
        DashBoardActivity.showBackButton(mContext);
        DashBoardActivity.setdialogtitle("");
        DashBoardActivity.updatedTitle("Booking Details");
        DashBoardActivity.hidePrintIcon(mContext);
        DashBoardActivity.showhomeButton(mContext);
        DashBoardActivity.hidecityname_header(mContext);

        tvAddressSource=(TextView)view.findViewById(R.id.tvAddressSource);
        tvAddressDestination=(TextView)view.findViewById(R.id.tvAddressDestination);
        tvSeatType=(TextView)view.findViewById(R.id.tvSeatType);
        tvSeatCost=(TextView)view.findViewById(R.id.tvSeatCost);
        tvwalletbal=(TextView)view.findViewById(R.id.tvwalletbal);
        tvTotalRs=(TextView)view.findViewById(R.id.tvTotalRs);
        tvSeatNo=(TextView)view.findViewById(R.id.tvSeatNo);
        tvBusNo=(TextView)view.findViewById(R.id.tvBusNo);
        tvTimeLeaveAt=(TextView)view.findViewById(R.id.tvTimeLeaveAt);
        tvTimeArriveAt=(TextView)view.findViewById(R.id.tvTimeArriveAt);
        btnConfrim=(TextView)view.findViewById(R.id.btnConfrim);
        imgCashActive=(ImageView)view.findViewById(R.id.imgCashActive);
        imgCashInactive=(ImageView)view.findViewById(R.id.imgCashInactive);
        imgOnlineActive=(ImageView)view.findViewById(R.id.imgOnlineActive);
        imgOnlineInactive=(ImageView)view.findViewById(R.id.imgOnlineInactive);
        rltvOnlinePayment=(RelativeLayout)view.findViewById(R.id.rltvOnlinePayment);
        rltvCashPayment=(RelativeLayout)view.findViewById(R.id.rltvCashPayment);

//        tvSeatNoBellow=(TextView)view.findViewById(R.id.tvSeatNoBellow);//Remaining
//         rbOnlinePayment=(RadioButton)view.findViewById(R.id.rbOnlinePayment);
//         rbCashPayment=(RadioButton)view.findViewById(R.id.rbCashPayment);

        imgCashActive.setVisibility(View.VISIBLE);
        imgCashInactive.setVisibility(View.INVISIBLE);
        imgOnlineActive.setVisibility(View.INVISIBLE);
        imgOnlineInactive.setVisibility(View.VISIBLE);

        rltvCashPayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (payment_check == 2) {
                    imgCashActive.setVisibility(View.VISIBLE);
                    imgCashInactive.setVisibility(View.INVISIBLE);
                    imgOnlineActive.setVisibility(View.INVISIBLE);
                    imgOnlineInactive.setVisibility(View.VISIBLE);
                    payment_check = 1;
//                    Toast.makeText(mContext,"Cash 1",Toast.LENGTH_SHORT).show();
                }


            }
        });
        rltvOnlinePayment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(payment_check == 1 )
                {
                    imgOnlineActive.setVisibility(View.VISIBLE);
                    imgOnlineInactive.setVisibility(View.INVISIBLE);
                    imgCashActive.setVisibility(View.INVISIBLE);
                    imgCashInactive.setVisibility(View.VISIBLE);
                    payment_check=2;
//                    Toast.makeText(mContext,"Online 2",Toast.LENGTH_SHORT).show();
                }

            }
        });

        pessenger_id= Session_manager.getPassengerid(mContext);

        tvAddressSource.setText(fromcity_landmark);
        tvAddressDestination.setText(tocity_landmark);
        tvSeatCost.setText("Rs."+seat_price);
        tvwalletbal.setText("Rs.100");
        tvTotalRs.setText("Rs."+price);
        //tvRideFare.setText("Rs."+price);
        tvBusNo.setText(bus_no);
        tvSeatType.setText(ac_type);
        tvTimeLeaveAt.setText(fromstop_time);
        tvTimeArriveAt.setText(tostop_time);
//        rbCashPayment.setChecked(true);
        for (int i=0;i<getSeatData.size();i++)
        {
            if(i<getSeatData.size()-1)
            {
                booked_seats=booked_seats+getSeatData.get(i)+",";
            }
            else {
                booked_seats = booked_seats + getSeatData.get(i);
            }
        }

        tvSeatNo.setText(booked_seats);
        //tvSeatNoBellow.setText(booked_seats);

        //passenger_id*, trip_id*, bus_id*, from_stop*, to_stop*, amount*, seats*, payment_type*(1-Cash/2-Online), payment_status*(1-Paid/2-Unpaid)
        btnConfrim.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectionDetector.isNetworkAvailable(mContext)) {
                    SendBooking_Details();
                    showprogress();

                } else {
                    Toast.makeText(mContext, "You don't have Internet Connection", Toast.LENGTH_LONG).show();

                }
            }
        });
        return view;
    }
    public  void SendBooking_Details()
    {
        payment_type = payment_check+"";
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("passenger_id",pessenger_id);
        params.put("trip_id",trip_id);
        params.put("bus_id",bus_id);
        params.put("from_stop",fromstop_Id);
        params.put("to_stop",tostop_Id);
        params.put("amount",price);
        params.put("seats",booked_seats);
        params.put("payment_type",payment_type);
        params.put("payment_status",2);
        Log.e("booking parma",""+params);
        GlobalValues.getMethodManagerObj(mContext).SendBookingData(params, new MethodManager_Listner() {
            @Override
            public void onError() {
                hideprogress();
            }

            @Override
            public void onSuccess(String json)
            {
                try
                {
                    JSONObject jobject=new JSONObject(json);
                    String message=jobject.getString("msg");
                    String booking_id=jobject.getString("b_id");
                    hideprogress();
                    Bundle b=new Bundle();
                    b.putString("fromcity_landmark",fromcity_landmark);
                    b.putString("tocity_landmark", tocity_landmark);
                    b.putString("b_id",booking_id);

                    if (message.equals("Successfully booked"))
                    {
                        DashBoardActivity.displayview(DashBoardActivity.sucess_booking, b);
                        //Toast.makeText(mContext,""+message,Toast.LENGTH_LONG).show();
                        Snackbar.make(view, ""+message, Snackbar.LENGTH_LONG).show();
                    }
                    else
                    {
                        Toast.makeText(mContext,""+message,Toast.LENGTH_LONG).show();
                    }

                }
                catch (JSONException e) {
                    e.printStackTrace();
                    hideprogress();
                }
            }
        });
    }
    public void showprogress() {
        pdialog = new ProgressDialog(mContext);
        pdialog.setCancelable(false);
        pdialog.setCanceledOnTouchOutside(false);
        pdialog.setMessage("Loading...");
        pdialog.show();
    }

    public  void hideprogress() {
        if (pdialog != null) {
            pdialog.dismiss();
        }
    }
}
