package com.journepassenger.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.journepassenger.activity.DashBoardActivity;
import com.journepassenger.utilities.Session_manager;
import com.journyapp.journyapp.R;

/**
 * Created by administrator on 1/3/16.
 */
public class HistoryBookignDetail_Fragment extends Fragment
{
    public static String tag="historybooking";
    public static Fragment fragment;
    static FragmentTransaction ft;
    static public FragmentManager fragmentmanger;
    static public Context mContext;
    View view;
    TextView fromlocation_booking;
    TextView tv_timesrc_booking;
    TextView text_tolocation_booking;
    TextView tv_timedesti_booking;
    TextView bookingid;
    TextView tvseats;
    TextView tvtotalcost;
    TextView feedback_btn;
    TextView tvBusNo;
    static String source;
    static String destination;
    static String source_time;
    static String desti_time;
    public static String booking_id;
    static String seats;
    static String coast;
    static String bus_no;
    static String pessenger_id;
    static String feedback_status;
    static String status;
    public static Fragment getInstance(Context ct, FragmentManager fm,Bundle b)
    {

        mContext=ct;
        fragmentmanger=fm;

        fragment=new HistoryBookignDetail_Fragment();
        source=b.getString("source");
        destination=b.getString("destination");
        source_time=b.getString("source_time");
        desti_time=b.getString("desti_time");
        booking_id=b.getString("booking_id");
        seats=b.getString("seats");
        coast=b.getString("coast");
        bus_no=b.getString("bus_no");
        feedback_status=b.getString("feedback_status");
        status=b.getString("status");

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view=inflater.inflate(R.layout.history_bookingdetails,container,false);
        fromlocation_booking=(TextView)view.findViewById(R.id.fromlocation_booking);
        tv_timesrc_booking=(TextView)view.findViewById(R.id.tv_timesrc_booking);
        text_tolocation_booking=(TextView)view.findViewById(R.id.text_tolocation_booking);
        tv_timedesti_booking=(TextView)view.findViewById(R.id.tv_timedesti_booking);
        bookingid=(TextView)view.findViewById(R.id.bookingid);
        tvseats=(TextView)view.findViewById(R.id.tvseats);
        tvtotalcost=(TextView)view.findViewById(R.id.tvtotalcost);
        tvBusNo=(TextView)view.findViewById(R.id.tvBusNo);
        feedback_btn=(TextView)view.findViewById(R.id.feedback_btn);
        mContext=getActivity();
        DashBoardActivity.hideSearchButton(mContext);
        DashBoardActivity.hideLoacationButton(mContext);
        DashBoardActivity.hideafteredit(mContext);
        DashBoardActivity.hidebeforeedit(mContext);
        DashBoardActivity.hideMenuDrawerButton(mContext);
        DashBoardActivity.showBackButton(mContext);
        DashBoardActivity.hidecityname_header(mContext);
        DashBoardActivity.updatedTitle("Bookings");
        DashBoardActivity.hidePrintIcon(mContext);
        DashBoardActivity.showhomeButton(mContext);
        DashBoardActivity.hideMenuDrawerButton(mContext);
        pessenger_id= Session_manager.getPassengerid(mContext);
        fromlocation_booking.setText(source);
        tv_timesrc_booking.setText(source_time);
        text_tolocation_booking.setText(destination);
        tv_timedesti_booking.setText(desti_time);
        bookingid.setText(booking_id);
        tvseats.setText(seats);
        tvtotalcost.setText("Rs."+coast);
        tvBusNo.setText(bus_no);
        Session_manager.setbookingid_feedback(mContext,""+booking_id);
        if (status.equals("Booked"))
        {

            feedback_btn.setClickable(false);
            feedback_btn.setEnabled(false);
            feedback_btn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.feeback_inactive, 0, 0, 0);
            feedback_btn.setTextColor(getResources().getColor(R.color.light_gray));
        }
        else
        {
            if (feedback_status.equals("1"))
            {

                feedback_btn.setClickable(false);
                feedback_btn.setEnabled(false);
                feedback_btn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.feeback_inactive, 0, 0, 0);
                feedback_btn.setTextColor(getResources().getColor(R.color.light_gray));

            }
            else
            {
                feedback_btn.setClickable(true);
                feedback_btn.setEnabled(true);
                feedback_btn.setCompoundDrawablesWithIntrinsicBounds(R.drawable.feeback_active, 0, 0, 0);
                feedback_btn.setTextColor(getResources().getColor(R.color.white));
            }
        }
        feedback_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                DashBoardActivity.displayview(DashBoardActivity.feedbackfragment,null);
            }
        });
        return view;
    }


}
