package com.journepassenger.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.journepassenger.activity.DashBoardActivity;
import com.journepassenger.utilities.ConnectionDetector;
import com.journepassenger.utilities.GlobalValues;
import com.journepassenger.utilities.MethodManager_Listner;
import com.journepassenger.utilities.Session_manager;
import com.journyapp.journyapp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by administrator on 12-Feb-16.
 */
public class ContactUs_Fragment extends Fragment {

    public static Fragment fragment;
    static FragmentTransaction ft;
    static public FragmentManager fragmentmanger;
    static public Context mContext;
    View view;

    EditText edNameId, edEmailId, edPhoneId, edMsg;
    TextView btnSend;
    String p_id;
    static TextView tvLocationPoint;
    static  TextView tvEmail;
    static TextView tvContactNo;
    String name,email,phone;

    public static Fragment getInstance(Context ct, FragmentManager fm,Bundle b)
    {

        mContext=ct;
        fragmentmanger=fm;

        fragment=new ContactUs_Fragment();

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.contact_us, null, false);
        mContext = getActivity();
        tvLocationPoint=(TextView)view.findViewById(R.id.tvLocationPoint);
        tvEmail=(TextView)view.findViewById(R.id.tvEmail);
        tvContactNo=(TextView)view.findViewById(R.id.tvContactNo);
        edNameId = (EditText)view.findViewById(R.id.edNameId);
        edEmailId = (EditText)view.findViewById(R.id.edEmailId);
        edPhoneId = (EditText)view.findViewById(R.id.edPhoneId);
        edMsg = (EditText)view.findViewById(R.id.edMsg);
        btnSend = (TextView)view.findViewById(R.id.btnSend);

        DashBoardActivity.hideSearchButton(mContext);
        DashBoardActivity.hideLoacationButton(mContext);
        DashBoardActivity.hideafteredit(mContext);
        DashBoardActivity.hidebeforeedit(mContext);
        DashBoardActivity.showMenuDrawerButton(mContext);
        DashBoardActivity.hideBackButton(mContext);
        DashBoardActivity.hidecityname_header(mContext);
        DashBoardActivity.updatedTitle("Contact Us");
        DashBoardActivity.hidePrintIcon(mContext);
        DashBoardActivity.hidehomeButton(mContext);

        p_id= Session_manager.getPassengerid(mContext);
        name=Session_manager.getPassengername(mContext);
        email=Session_manager.getPassengeremail(mContext);
        phone=Session_manager.getPassengermob(mContext);

        edNameId.setText(name);
        edEmailId.setText(email);
        edPhoneId.setText(phone);
        GetContactInfo();
        btnSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(edNameId.getText().toString().length()==0)
                {
                    edNameId.setError("Name can not be blank");
                }
                else if (!isValidEmail(edEmailId.getText().toString()))
                {
                    edEmailId.setError("Invalid email");
                }
                else if (edPhoneId.getText().toString().length() < 8 || edPhoneId.getText().toString().length() > 13)
                {
                    edPhoneId.setError("Check PhoneNo");
                }
                else if(edMsg.getText().toString().length()==0)
                {
                    edMsg.setError("Message can not be blank");
                }
                else if (ConnectionDetector.isNetworkAvailable(mContext))

                {
                    SendUserData();
                }
                else
                {
                    Toast.makeText(mContext, "You don't have Internet Connection", Toast.LENGTH_LONG).show();

                }
            }
        });
        return  view;
    }

    public static void GetContactInfo()
   {
       GlobalValues.getMethodManagerObj(mContext).contactUsDetails(new MethodManager_Listner() {
           @Override
           public void onError() {

           }

           @Override
           public void onSuccess(String json) {
               try {
                           JSONObject jsonObject = new JSONObject(json);
                           JSONObject jobject=jsonObject.getJSONObject("details");

                               String c_detail_id=jobject.optString("c_detail_id");
                               String c_detail_email=jobject.optString("c_detail_email");
                               String c_detail_website=jobject.optString("c_detail_website");
                               String c_detail_helpline_no=jobject.optString("c_detail_helpline_no");
                               String c_detail_location=jobject.optString("c_detail_location");
                               tvContactNo.setText(c_detail_helpline_no);
                               tvLocationPoint.setText(c_detail_location);
                               tvEmail.setText(c_detail_email);


               } catch (JSONException e) {
                   e.printStackTrace();
               }
           }
       });
    }
    public void SendUserData()
    {
        Map<String, Object> params = new HashMap<String, Object>();

        params.put("passenger_id", p_id);
        params.put("name", edNameId.getText().toString());
        params.put("email", edEmailId.getText().toString());
        params.put("phone",edPhoneId.getText().toString());
        params.put("message",edMsg.getText().toString());


        GlobalValues.getMethodManagerObj(mContext).contactUsData(params, new MethodManager_Listner() {
            @Override
            public void onError() {
                Log.e("Contact us error", "error");
            }

            @Override
            public void onSuccess(String json) {

                Log.e("contactUs", json.toString());

                try {
                    JSONObject jsonObject = new JSONObject(json);
                    String msg = jsonObject.getString("status");

                    if (msg.equals("false"))
                    {
                        Toast.makeText(mContext, "Not Submitted", Toast.LENGTH_LONG).show();
                    }
                    else if (msg.equals("true"))
                    {
                        Toast.makeText(mContext, "Submitted Successfully", Toast.LENGTH_LONG).show();
                        DashBoardActivity.displayview(DashBoardActivity.home, null);

                    }
                    edMsg.setText("");

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }

    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }



}
