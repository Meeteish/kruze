package com.journepassenger.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.journedriver.data.UtilMethod;
import com.journepassenger.activity.DashBoardActivity;
import com.journepassenger.utilities.GlobalValues;
import com.journepassenger.utilities.MethodManager_Listner;
import com.journepassenger.utilities.Session_manager;
import com.journyapp.journyapp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by administrator on 09-Feb-16.
 */
public class Feedback_Fragment extends Fragment implements View.OnClickListener {
    public static Fragment fragment;
    static FragmentTransaction ft;
    static public FragmentManager fragmentmanger;
    static public Context mContext;
    View view;

    TextView tvdriverid,tvnotsafeid,tvacnotworking,tvnotclean,tvnotontime,tvsomeotherissue,tvjustawesome;
    String p_id,feedback;
    public  static String booking_id;

    public static Fragment getInstance(Context ct, FragmentManager fm,Bundle b)
    {

        mContext=ct;
        fragmentmanger=fm;
        fragment=new Feedback_Fragment();

        //
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View view = inflater.inflate(R.layout.feedback, container,false);
        mContext=getActivity();
        DashBoardActivity.hideSearchButton(mContext);
        DashBoardActivity.hideLoacationButton(mContext);
        DashBoardActivity.hideafteredit(mContext);
        DashBoardActivity.hidebeforeedit(mContext);
        DashBoardActivity.showMenuDrawerButton(mContext);
        DashBoardActivity.hideBackButton(mContext);
        DashBoardActivity.hidecityname_header(mContext);
        DashBoardActivity.updatedTitle("Feedback");
        DashBoardActivity.hidePrintIcon(mContext);
        DashBoardActivity.hidehomeButton(mContext);
       /* booking_id=Session_manager.getBookinigid_feedback(mContext);
        Log.e("Feedback booking_id",""+booking_id);*/
        booking_id=HistoryBookignDetail_Fragment.booking_id;
        Log.e("Feedback booking_id",""+booking_id);
        p_id = Session_manager.getPassengerid(mContext);

        tvdriverid = (TextView)view.findViewById(R.id.tvdriverid);
        tvnotsafeid = (TextView)view.findViewById(R.id.tvnotsafeid);
        tvacnotworking = (TextView)view.findViewById(R.id.tvacnotworking);
        tvnotclean = (TextView)view.findViewById(R.id.tvnotclean);
        tvnotontime = (TextView)view.findViewById(R.id.tvnotontime);
        tvsomeotherissue = (TextView)view.findViewById(R.id.tvsomeotherissue);
        tvjustawesome = (TextView)view.findViewById(R.id.tvjustawesome);

        tvdriverid.setOnClickListener(this);
        tvnotsafeid.setOnClickListener(this);
        tvacnotworking.setOnClickListener(this);
        tvnotclean.setOnClickListener(this);
        tvnotontime.setOnClickListener(this);
        tvsomeotherissue.setOnClickListener(this);
        tvjustawesome.setOnClickListener(this);


        return view;
    }

    @Override
    public void onClick(View v)
    {
        switch (v.getId()) {
            case R.id.tvdriverid:
                feedback = tvdriverid.getText().toString();
                //Toast.makeText(mContext,feedback,Toast.LENGTH_SHORT).show();
                feedbackPassenger(feedback,booking_id);
                break;

            case R.id.tvnotsafeid:
                feedback = tvnotsafeid.getText().toString();
                //Toast.makeText(mContext,feedback,Toast.LENGTH_SHORT).show();
                feedbackPassenger(feedback,booking_id);
                break;

            case R.id.tvacnotworking:
                feedback = tvacnotworking.getText().toString();
                //Toast.makeText(mContext,feedback,Toast.LENGTH_SHORT).show();
                feedbackPassenger(feedback,booking_id);
                break;

            case R.id.tvnotclean:

                //Toast.makeText(mContext,feedback,Toast.LENGTH_SHORT).show();
                feedbackPassenger(feedback,booking_id);
                break;

            case R.id.tvnotontime:
                feedback = tvnotontime.getText().toString();
                //Toast.makeText(mContext,feedback,Toast.LENGTH_SHORT).show();
                feedbackPassenger(feedback,booking_id);
                break;

            case R.id.tvsomeotherissue:
                feedback = tvsomeotherissue.getText().toString();
                //Toast.makeText(mContext,feedback,Toast.LENGTH_SHORT).show();
                Sendotherisues();

                break;

            case R.id.tvjustawesome:
                feedback = tvjustawesome.getText().toString();
                //Toast.makeText(mContext,feedback,Toast.LENGTH_SHORT).show();
                feedbackPassenger(feedback,booking_id);
                break;
        }

    }

    public void feedbackPassenger(String feedback,String booking_id)
    {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("passenger_id",p_id);
        params.put("feedback",feedback);
        params.put("b_id",booking_id);
        params.put("feedback_type","1");
        GlobalValues.getMethodManagerObj(mContext).feedback_passenger(params, new MethodManager_Listner() {
            @Override
            public void onError() {

            }

            @Override
            public void onSuccess(String json) {
                JSONObject jobject = null;
                try {
                    jobject = new JSONObject(json);
                    String msg = jobject.getString("status");

                    if (msg.equals("false")) {
                        Toast.makeText(mContext, jobject.getString("msg"), Toast.LENGTH_LONG).show();
                    } else if (msg.equals("true")) {
                        Toast.makeText(mContext, jobject.getString("msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

    }
    public void Sendotherisues()
    {
        final Dialog user_info = new Dialog(mContext);
        LayoutInflater li = (LayoutInflater) mContext.getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View vi = li.inflate(R.layout.popup_otherisuues, null, false);
        user_info.requestWindowFeature(Window.FEATURE_NO_TITLE);


        final TextView submit = (TextView) vi.findViewById(R.id.submit);
        final TextView btnCancel = (TextView) vi.findViewById(R.id.btnCancel);
        final EditText edissue=(EditText)vi.findViewById(R.id.edissue);

        //final EditText dealcode=(EditText)user_info.findViewById(R.id.u_dealcode);
        //dealcode.setVisibility(View.GONE);

        btnCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                user_info.dismiss();
            }
        });
        submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                feedback = edissue.getText().toString();
                feedbackPassenger(feedback,booking_id);
                user_info.dismiss();
            }
        });

        user_info.setContentView(vi);
        user_info.setCanceledOnTouchOutside(false);
        user_info.show();

    }

}


