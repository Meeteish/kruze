package com.journepassenger.fragments;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.journepassenger.activity.DashBoardActivity;
import com.journepassenger.utilities.ConnectionDetector;
import com.journepassenger.utilities.Session_manager;
import com.journyapp.journyapp.R;

/**
 * Created by administrator on 11-Feb-16.
 */
public class ReferAndEarn_Fragment extends Fragment
{
    public static Fragment fragment;
    static FragmentTransaction ft;
    static public FragmentManager fragmentmanger;
    static public Context mContext;
    View view;
    String referralCode;
    TextView tvReferralCode;
    TextView btninvitefriends;
    String refrealText;
    public static Fragment getInstance(Context ct, FragmentManager fm,Bundle b)
    {

        mContext=ct;
        fragmentmanger=fm;
        fragment=new ReferAndEarn_Fragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view = inflater.inflate(R.layout.refer_earn_perks, null, false);
        mContext = getActivity();
        tvReferralCode=(TextView)view.findViewById(R.id.tvReferralCode);
        btninvitefriends=(TextView)view.findViewById(R.id.btninvitefriends);
        DashBoardActivity.hideSearchButton(mContext);
        DashBoardActivity.hideLoacationButton(mContext);
        DashBoardActivity.hideafteredit(mContext);
        DashBoardActivity.hidebeforeedit(mContext);
        DashBoardActivity.showMenuDrawerButton(mContext);
        DashBoardActivity.hideBackButton(mContext);
        DashBoardActivity.hidecityname_header(mContext);
        DashBoardActivity.updatedTitle("Invite & Win");
        DashBoardActivity.hidePrintIcon(mContext);
        DashBoardActivity.hidehomeButton(mContext);
        referralCode= Session_manager.getPassenget_refercode(mContext);
        //DashBoardActivity.slidemenu.setSlidingEnabled(false);
        //Toast.makeText(mContext,""+referralCode,Toast.LENGTH_LONG).show();
        tvReferralCode.setText("" + referralCode);
        refrealText="Hey! Try KRUZE with my referral code "+referralCode+" and your first trip is FREE.With WiFi and AC bus service in Mumbai, KRUZE is the new way to travel. Check it out.";
        btninvitefriends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if (ConnectionDetector.isNetworkAvailable(mContext))

                {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, refrealText);
                    sendIntent.setType("text/plain");
                    startActivity(Intent.createChooser(sendIntent, getResources().getText(R.string.send_to)));
                }
                else
                {
                    Toast.makeText(mContext, "You don't have Internet Connection", Toast.LENGTH_LONG).show();

                }
            }
        });
        return view;
    }



}
