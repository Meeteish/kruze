package com.journepassenger.fragments;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AutoCompleteTextView;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.Toast;

import com.journepassenger.activity.DashBoardActivity;
import com.journepassenger.adapter.LandMarkList_Adapter;
import com.journepassenger.adapter.SearchBusAdapter;
import com.journepassenger.bean.FromDetailsBean;
import com.journepassenger.bean.LandMark_bean;
import com.journepassenger.bean.ToDetailsBean;
import com.journepassenger.bean.TripIdBean;
import com.journepassenger.utilities.ConnectionDetector;
import com.journepassenger.utilities.GlobalValues;
import com.journepassenger.utilities.MethodManager_Listner;
import com.journyapp.journyapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;



/**
 * Created by administrator on 5/2/16.
 */
public class SearchedBus_Fragment extends Fragment
{
    public static String tag="searchbus";
    public static Fragment fragment;
    static FragmentTransaction ft;
    static public FragmentManager fragmentmanger;
    static public Context mContext;
    public static String getfromlocation;
    public static String gettolocation;

    static ArrayList<TripIdBean> routetriparray;

    static SearchBusAdapter busAdapter;
    static ListView bus_list;
    public static String getcityfromedit;
    public static String gettocityedit,route_id,type;
    AutoCompleteTextView search_edyourlocationid;
    AutoCompleteTextView search_edselectdestinid;
    ImageButton btn_search;
    ArrayList<LandMark_bean> landmark_arrays;
    static ProgressDialog pdialog;
   static String b_id;
    public static Fragment getInstance(Context context,FragmentManager fm,Bundle b)
    {
        mContext = context;
        fragmentmanger = fm;
        fragment = new SearchedBus_Fragment();
        getfromlocation=b.getString("fromcity");
        gettolocation=b.getString("tocity");
        route_id=b.getString("route_id");
        type=b.getString("type");
        b_id=b.getString("b_id");
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View v=inflater.inflate(R.layout.search_bus, container, false);
        bus_list=(ListView)v.findViewById(R.id.bus_list);
        search_edyourlocationid=(AutoCompleteTextView)v.findViewById(R.id.search_edyourlocationid);
        search_edselectdestinid=(AutoCompleteTextView)v.findViewById(R.id.search_edselectdestinid);
        btn_search=(ImageButton)v.findViewById(R.id.btn_search);
        mContext=getActivity();
         search_edyourlocationid.setText(getfromlocation);
         search_edselectdestinid.setText(gettolocation);
        DashBoardActivity.updatedTitle("Search Result");
        DashBoardActivity.hidecityname_header(mContext);
        DashBoardActivity.hideSearchButton(mContext);
        DashBoardActivity.hideLoacationButton(mContext);
        DashBoardActivity.hideafteredit(mContext);
        DashBoardActivity.hidebeforeedit(mContext);
        DashBoardActivity.showBackButton(mContext);
        DashBoardActivity.hideMenuDrawerButton(mContext);
        DashBoardActivity.hidePrintIcon(mContext);
        DashBoardActivity.showhomeButton(mContext);
        hideKeyboar(mContext);
        getcityfromedit=search_edyourlocationid.getText().toString();
        gettocityedit=search_edselectdestinid.getText().toString();
        //showprogress();

        if (type.equals("2"))
        {
            Getsearchfrom_landmark(getfromlocation, gettolocation);
            Log.e("type",type+"");
            showprogress();
        }
        else if(type.equals("3"))
        {
            GetSearchData_reverse(b_id);
            showprogress();
        }
        else
        {
            GetSearchData();
            showprogress();
        }
        btn_search.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (search_edyourlocationid.getText().toString().equals("")) {
                    Toast.makeText(mContext, "Please Select location", Toast.LENGTH_LONG).show();
                } else if (search_edselectdestinid.getText().toString().equals("")) {
                    Toast.makeText(mContext, "Please Select Destination", Toast.LENGTH_LONG).show();
                } else if (ConnectionDetector.isNetworkAvailable(mContext))

                {
                    Getsearchfrom_landmark(getfromlocation, gettolocation);
                    //showprogress();
                } else {
                    Toast.makeText(mContext, "You don't have Internet Connection", Toast.LENGTH_LONG).show();
                }
            }
        });

        return v;
    }

    public static void GetSearchData()
    {
        Map<String, Object> params = new HashMap<String, Object>();
        //params.put("from_stop",from);
        //params.put("to_stop", to);
        params.put("route_id",route_id);
        params.put("type","1");
        Log.e("search params",params.toString());
        GlobalValues.getMethodManagerObj(mContext).BusLists(params, new MethodManager_Listner() {
            @Override
            public void onError() {
                hideprogress();
            }

            @Override
            public void onSuccess(String json1) {
                //UtilMethod.showToast("onSuccess ", mContext);
                  hideprogress();
                try {

                    JSONObject json = new JSONObject(json1);

                    String message = json.getString("msg");
                    String status = json.getString("status");

                    if (status.equalsIgnoreCase("false")) {
                        showDialogForConfirmEmail("Message", "No Buses Found..");
                        //hideprogress();

                    } else if (status.equalsIgnoreCase("false") && message.equalsIgnoreCase("Invalid request!")) {
                        //hideprogress();
                    } else if (status.equals("true")) {
                        //hideprogress();

                        JSONArray responseJsonArray = json.getJSONArray("result");

                        routetriparray = new ArrayList<TripIdBean>();

                        for (int j = 0; j < responseJsonArray.length(); j++)

                        {
                            JSONObject friendsObj = responseJsonArray.getJSONObject(j);
                            TripIdBean routesBean = new TripIdBean();

                            String trip_id = friendsObj.getString("trip_id");
                            routesBean.setTrip_id(trip_id);
                            Log.v("trip_id = ", trip_id);

                            String bus_no = friendsObj.getString("bus_no");
                            routesBean.setBus_no(bus_no);

                            String available_seats = friendsObj.optString("available_seats");
                            routesBean.setAvailable_seats(available_seats);

                            String total_seats = friendsObj.optString("total_seats");
                            routesBean.setTotal_seats(total_seats);

                            String price = friendsObj.optString("price");
                            routesBean.setPrice(price);

                            String via = friendsObj.optString("via");
                            routesBean.setVia(via);

                            String ac_type = friendsObj.optString("ac_type");
                            routesBean.setAc_type(ac_type);

                            JSONObject friendsObj1 = friendsObj.getJSONObject("from_details");
                            FromDetailsBean fromDetailsBean = new FromDetailsBean();

                            String bus_stop_name = friendsObj1.getString("bus_stop_name");
                            fromDetailsBean.setBus_stop_name(bus_stop_name);
                            Log.v("bus_stop_name = ", bus_stop_name);

                            String t_stop_time = friendsObj1.getString("t_stop_time");
                            fromDetailsBean.setT_stop_time(t_stop_time);
                            Log.v("t_stop_time  ", t_stop_time);

                            //String t_stop_id = friendsObj1.getString("t_stop");
                            fromDetailsBean.setT_stop("0");
                            routesBean.setFromDetailsBean(fromDetailsBean);

                            JSONObject friendsObj11 = friendsObj.getJSONObject("to_details");
                            ToDetailsBean toDetailsBean = new ToDetailsBean();

                            String bus_stop_nm = friendsObj11.getString("bus_stop_name");
                            toDetailsBean.setBus_stop_name(bus_stop_nm);
                            Log.v("bus_stop_name = ", bus_stop_nm);

                            String t_stop_tm = friendsObj11.getString("t_stop_time");
                            toDetailsBean.setT_stop_time(t_stop_tm);
                            Log.v("t_stop_time  ", t_stop_tm);

                            //String tostopId = friendsObj11.getString("t_stop");
                            toDetailsBean.setT_stop("0");

                            routesBean.setToDetailsBean(toDetailsBean);

                            routetriparray.add(routesBean);
                        }

                        busAdapter = new SearchBusAdapter(mContext, routetriparray);
                        bus_list.setAdapter(busAdapter);
                        hideKeyboard(mContext);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("exception",e.toString());
                    hideprogress();
                }
            }

        });

    }
    public void Getsearchfrom_landmark(String from_landmark,String to_landmark)
    {
        {
            Map<String, Object> params = new HashMap<String, Object>();
            params.put("from_stop",from_landmark);
            params.put("to_stop", to_landmark);
            params.put("type","2");
            Log.e("search params",params.toString());
            GlobalValues.getMethodManagerObj(mContext).BusLists(params, new MethodManager_Listner() {
                @Override
                public void onError() {
                    hideprogress();
                }

                @Override
                public void onSuccess(String json1) {
                    //UtilMethod.showToast("onSuccess ", mContext);
                          hideprogress();
                    try {

                        JSONObject json = new JSONObject(json1);

                        String message = json.getString("msg");
                        String status = json.getString("status");

                        if (status.equalsIgnoreCase("false")) {
                            showDialogForConfirmEmail("Message", "No Buses Found..");
                            //hideprogress();

                        } else if (status.equalsIgnoreCase("false") && message.equalsIgnoreCase("Invalid request!")) {
                            //hideprogress();
                        } else if (status.equals("true")) {
                            //hideprogress();

                            JSONArray responseJsonArray = json.getJSONArray("result");

                            routetriparray = new ArrayList<TripIdBean>();

                            for (int j = 0; j < responseJsonArray.length(); j++)

                            {
                                JSONObject friendsObj = responseJsonArray.getJSONObject(j);
                                TripIdBean routesBean = new TripIdBean();

                                String trip_id = friendsObj.getString("trip_id");
                                routesBean.setTrip_id(trip_id);
                                Log.v("trip_id = ", trip_id);

                                String bus_no = friendsObj.getString("bus_no");
                                routesBean.setBus_no(bus_no);

                                String available_seats = friendsObj.optString("available_seats");
                                routesBean.setAvailable_seats(available_seats);

                                String total_seats = friendsObj.optString("total_seats");
                                routesBean.setTotal_seats(total_seats);

                                String price = friendsObj.optString("price");
                                routesBean.setPrice(price);

                                String via = friendsObj.optString("via");
                                routesBean.setVia(via);

                                String ac_type = friendsObj.optString("ac_type");
                                routesBean.setAc_type(ac_type);

                                JSONObject friendsObj1 = friendsObj.getJSONObject("from_details");
                                FromDetailsBean fromDetailsBean = new FromDetailsBean();

                                String bus_stop_name = friendsObj1.getString("bus_stop_name");
                                fromDetailsBean.setBus_stop_name(bus_stop_name);
                                Log.v("bus_stop_name = ", bus_stop_name);

                                String t_stop_time = friendsObj1.getString("t_stop_time");
                                fromDetailsBean.setT_stop_time(t_stop_time);
                                Log.v("t_stop_time  ", t_stop_time);

                                //String t_stop_id = friendsObj1.getString("t_stop");
                                fromDetailsBean.setT_stop("0");
                                routesBean.setFromDetailsBean(fromDetailsBean);

                                JSONObject friendsObj11 = friendsObj.getJSONObject("to_details");
                                ToDetailsBean toDetailsBean = new ToDetailsBean();

                                String bus_stop_nm = friendsObj11.getString("bus_stop_name");
                                toDetailsBean.setBus_stop_name(bus_stop_nm);
                                Log.v("bus_stop_name = ", bus_stop_nm);

                                String t_stop_tm = friendsObj11.getString("t_stop_time");
                                toDetailsBean.setT_stop_time(t_stop_tm);
                                Log.v("t_stop_time  ", t_stop_tm);

                                //String tostopId = friendsObj11.getString("t_stop");
                                toDetailsBean.setT_stop("0");

                                routesBean.setToDetailsBean(toDetailsBean);

                                routetriparray.add(routesBean);
                            }

                            busAdapter = new SearchBusAdapter(mContext, routetriparray);
                            bus_list.setAdapter(busAdapter);
                            hideKeyboard(mContext);
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        Log.e("exception",e.toString());
                        hideprogress();
                    }
                }

            });

        }
    }
    public void GetSearchData_reverse(String bookig_id)
    {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("b_id",bookig_id);

        Log.e(" return search params", params.toString());
        GlobalValues.getMethodManagerObj(mContext).ReturnBusLists(params, new MethodManager_Listner() {
            @Override
            public void onError() {
                hideprogress();
            }

            @Override
            public void onSuccess(String json1) {
                //UtilMethod.showToast("onSuccess ", mContext);
                hideprogress();
                try {

                    JSONObject json = new JSONObject(json1);

                    String message = json.getString("msg");
                    String status = json.getString("status");

                    if (status.equalsIgnoreCase("false")) {
                        showDialogForConfirmEmail("Message", "No Buses Found..");
                        //hideprogress();

                    } else if (status.equalsIgnoreCase("false") && message.equalsIgnoreCase("Invalid request!")) {
                        //hideprogress();
                    } else if (status.equals("true")) {
                        //hideprogress();

                        JSONArray responseJsonArray = json.getJSONArray("result");

                        routetriparray = new ArrayList<TripIdBean>();

                        for (int j = 0; j < responseJsonArray.length(); j++)

                        {
                            JSONObject friendsObj = responseJsonArray.getJSONObject(j);
                            TripIdBean routesBean = new TripIdBean();

                            String trip_id = friendsObj.getString("trip_id");
                            routesBean.setTrip_id(trip_id);
                            Log.v("trip_id = ", trip_id);

                            String bus_no = friendsObj.getString("bus_no");
                            routesBean.setBus_no(bus_no);

                            String available_seats = friendsObj.optString("available_seats");
                            routesBean.setAvailable_seats(available_seats);

                            String total_seats = friendsObj.optString("total_seats");
                            routesBean.setTotal_seats(total_seats);

                            String price = friendsObj.optString("price");
                            routesBean.setPrice(price);

                            String via = friendsObj.optString("via");
                            routesBean.setVia(via);

                            String ac_type = friendsObj.optString("ac_type");
                            routesBean.setAc_type(ac_type);

                            JSONObject friendsObj1 = friendsObj.getJSONObject("from_details");
                            FromDetailsBean fromDetailsBean = new FromDetailsBean();

                            String bus_stop_name = friendsObj1.getString("bus_stop_name");
                            fromDetailsBean.setBus_stop_name(bus_stop_name);
                            Log.v("bus_stop_name = ", bus_stop_name);

                            String t_stop_time = friendsObj1.getString("t_stop_time");
                            fromDetailsBean.setT_stop_time(t_stop_time);
                            Log.v("t_stop_time  ", t_stop_time);

                            //String t_stop_id = friendsObj1.getString("t_stop");
                            fromDetailsBean.setT_stop("0");
                            routesBean.setFromDetailsBean(fromDetailsBean);

                            JSONObject friendsObj11 = friendsObj.getJSONObject("to_details");
                            ToDetailsBean toDetailsBean = new ToDetailsBean();

                            String bus_stop_nm = friendsObj11.getString("bus_stop_name");
                            toDetailsBean.setBus_stop_name(bus_stop_nm);
                            Log.v("bus_stop_name = ", bus_stop_nm);

                            String t_stop_tm = friendsObj11.getString("t_stop_time");
                            toDetailsBean.setT_stop_time(t_stop_tm);
                            Log.v("t_stop_time  ", t_stop_tm);

                            //String tostopId = friendsObj11.getString("t_stop");
                            toDetailsBean.setT_stop("0");

                            routesBean.setToDetailsBean(toDetailsBean);

                            routetriparray.add(routesBean);
                        }

                        busAdapter = new SearchBusAdapter(mContext, routetriparray);
                        bus_list.setAdapter(busAdapter);
                        hideKeyboard(mContext);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    Log.e("exception", e.toString());
                    hideprogress();
                }
            }

        });

    }
    public static void showDialogForConfirmEmail(String title, final String message)

    {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext).setTitle(title).setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public  void Getlandmark_auto(String City_id) {
        Map<String, Object> params = new HashMap<String, Object>();

        params.put("city_id", City_id);

        GlobalValues.getMethodManagerObj(mContext).GetLandmark_list(params, new MethodManager_Listner() {
            @Override
            public void onError() {

            }

            @Override
            public void onSuccess(String json1) {
                JSONObject json = null;
                try {
                    json = new JSONObject(json1);
                    String message = json.getString("msg");
                    String status = json.getString("status");
                    if (status.equals("true")) {

                        JSONArray responseJsonArray = json.getJSONArray("landmark_list");

                        landmark_arrays = new ArrayList<LandMark_bean>();
                        for (int j = 0; j < responseJsonArray.length(); j++) {
                            JSONObject landmark_obj = responseJsonArray.getJSONObject(j);
                            LandMark_bean landMark_bean = new LandMark_bean();
                            String lanmark_id = landmark_obj.getString("landmark_id");
                            landMark_bean.setLandmark_id(lanmark_id);

                            String landmark = landmark_obj.getString("landmark");
                            landMark_bean.setLandmark(landmark);

                            landmark_arrays.add(landMark_bean);

                        }
                    }

                    LandMarkList_Adapter landMarkListAdapter = new LandMarkList_Adapter(mContext, R.layout.landmark_listcustom, landmark_arrays);
                    search_edyourlocationid.setAdapter(landMarkListAdapter);
                    search_edselectdestinid.setAdapter(landMarkListAdapter);

                    search_edselectdestinid.setThreshold(1);
                    search_edyourlocationid.setThreshold(1);
                    landMarkListAdapter.notifyDataSetChanged();
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });
    }
    public void showprogress() {
        pdialog = new ProgressDialog(mContext);
        pdialog.setCancelable(false);
        pdialog.setCanceledOnTouchOutside(false);
        pdialog.setMessage("Loading...");
        pdialog.show();
    }

    public static void hideprogress() {
        if (pdialog != null) {
            pdialog.dismiss();
        }
    }
    public static void hideKeyboard(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View v = ((Activity) ctx).getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }
    public static void hideKeyboar(Context ctx) {
        InputMethodManager inputManager = (InputMethodManager) ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE);

        // check if no view has focus:
        View v = ((Activity) ctx).getCurrentFocus();
        if (v == null)
            return;

        inputManager.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }
}
