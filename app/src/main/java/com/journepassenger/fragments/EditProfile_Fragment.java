package com.journepassenger.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.journepassenger.activity.DashBoardActivity;
import com.journepassenger.activity.SDDir;
import com.journepassenger.utilities.CircleTransform;
import com.journepassenger.utilities.GlobalValues;
import com.journepassenger.utilities.ImageDecoder;
import com.journepassenger.utilities.MethodManager_Listner;
import com.journepassenger.utilities.RoundedImage;
import com.journepassenger.utilities.Session_manager;
import com.journyapp.journyapp.R;
import com.squareup.picasso.Picasso;

import org.apache.http.entity.mime.content.FileBody;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by administrator on 06-Feb-16.
 */
public class EditProfile_Fragment extends Fragment

{
    public static String tag="EditProfile";
    public static Fragment fragment;
    static FragmentTransaction ft;
    static public FragmentManager fragmentmanger;
    static public Context mContext;
    View view;
    String absPath;
    private static final int CAMERA_REQUEST = 11;
    private static final int SELECT_FILE = 12;
    static String fileName = null;
    String picturePath;
    DatePicker dp;

    Bitmap image;
    public static Bitmap imageBitmap;
    String filePath = null;
    EditText edNameId,edTelephoneId,edCompanyNameId;
    EditText edEmrgncyMobNoId,edWorkLocationId,edHouseLocationId,edDobId;
    TextView tvEmailId,tvReferralCode,tvChangePswdId;
    RadioButton rbmale,rbfemale;
//    RadioGroup genderRadioGroup;
    String gender;
    LinearLayout lLayoutHideShowAdditionalInfo;
    RelativeLayout rLayoutAdditionalInfo;
    ImageView uparrow,downarrow,imgcameraId,imgprofileId;
    int check=0;
    Uri cameraImagePath;
    File f;
    int currentVersion = Build.VERSION.SDK_INT;
    int rotate;
    String p_id;

    String passenger_name;
    String passenger_mobile;
    String passenger_email;
    String passenger_gender;
    String passenger_hose_loac;
    String passenger_work_loac;
    String passenger_company;
    String passenger_dob,passenger_emergency_num,passenger_referral_code, passenger_image;
    String passenger_image_mode;
    String passenger_image_mode_edited;

    ProgressDialog pdialog;

    String P_name;
    String P_email;
    String P_id,P_imgurl,qr_url,referral_code;
    Typeface font;

    public static Fragment getInstance(Context ct, FragmentManager fm)
    {

        mContext=ct;
        fragmentmanger=fm;

        fragment=new EditProfile_Fragment();

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view=inflater.inflate(R.layout.profile_edit_passenger, null, false);
        mContext=getActivity();
        DashBoardActivity.hideSearchButton(mContext);
        DashBoardActivity.hideLoacationButton(mContext);
        DashBoardActivity.showafteredit(mContext);
        DashBoardActivity.hidebeforeedit(mContext);
        DashBoardActivity.hideMenuDrawerButton(mContext);
        DashBoardActivity.showBackButton(mContext);
        DashBoardActivity.hidecityname_header(mContext);
        DashBoardActivity.updatedTitle("Edit Profile");
        DashBoardActivity.hidehomeButton(mContext);
        DashBoardActivity.hidePrintIcon(mContext);
        edNameId = (EditText)view.findViewById(R.id.edNameId);
        edTelephoneId = (EditText)view.findViewById(R.id.edTelephoneId);
        edCompanyNameId = (EditText)view.findViewById(R.id.edCompanyNameId);
        edEmrgncyMobNoId = (EditText)view.findViewById(R.id.edEmrgncyMobNoId);
        edWorkLocationId = (EditText)view.findViewById(R.id.edWorkLocationId);
        edHouseLocationId = (EditText)view.findViewById(R.id.edHouseLocationId);
        edDobId = (EditText)view.findViewById(R.id.edDobId);
        tvEmailId = (TextView)view.findViewById(R.id.edEmailId);
        tvReferralCode = (TextView)view.findViewById(R.id.tvReferralCode);
        tvChangePswdId = (TextView)view.findViewById(R.id.tvChangePswdId);
        rbmale = (RadioButton)view.findViewById(R.id.rbmale);
        rbfemale = (RadioButton)view.findViewById(R.id.rbfemale);
//        genderRadioGroup = (RadioGroup)view.findViewById(R.id.genderRadioGroup);

        lLayoutHideShowAdditionalInfo = (LinearLayout)view.findViewById(R.id.hideShowAdditionalInfo);
        rLayoutAdditionalInfo = (RelativeLayout)view.findViewById(R.id.relativeLayoutAdditionalInfo);
        downarrow = (ImageView)view.findViewById(R.id.imgdownarrowid);
        uparrow = (ImageView)view.findViewById(R.id.imguparrowid);
        imgcameraId = (ImageView)view.findViewById(R.id.cameraicon);
        imgprofileId=(ImageView)view.findViewById(R.id.imgprofileId);

        font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/Roboto-Light.ttf");
        rbmale.setTypeface(font);
        rbfemale.setTypeface(font);

        p_id = Session_manager.getPassengerid(mContext);
         GetUser_Data();
        imgcameraId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                try {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showDialogForImageSelect();
                        }
                    });
                } catch (Exception e) {

                }
            }
        });



        lLayoutHideShowAdditionalInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(check==0)
                {
                    rLayoutAdditionalInfo.setVisibility(View.VISIBLE);
                    downarrow.setVisibility(View.INVISIBLE);
                    uparrow.setVisibility(View.VISIBLE);
                    check=1;
                }
                else
                {
                    rLayoutAdditionalInfo.setVisibility(View.INVISIBLE);
                    downarrow.setVisibility(View.VISIBLE);
                    uparrow.setVisibility(View.INVISIBLE);
                    check=0;
                }

            }
        });

        edDobId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dp = new DatePicker(mContext);

                final AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                builder.setView(dp);
                builder.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        int day = dp.getDayOfMonth();
                        int month = dp.getMonth() + 1;
                        int year = dp.getYear();

                        edDobId.setText(day + "-" + month + "-" + year);

                    }
                });
                builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                    }
                });
                builder.show();
            }
        });

        DashBoardActivity.after_edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                showprogress();
                SendUserData();
               /* DashBoardActivity.before_edit.setVisibility(View.INVISIBLE);
                DashBoardActivity.after_edit.setVisibility(View.VISIBLE);*/

            }
        });

        return view;
    }
     public void GetUser_Data()
     {
         Map<String, Object> params = new HashMap<String, Object>();
         params.put("passenger_id",p_id);
         GlobalValues.getMethodManagerObj(mContext).userProfileData(params, new MethodManager_Listner() {
             @Override
             public void onError() {
             }

             @Override
             public void onSuccess(String json) {

                 try {
                     JSONObject jobject = new JSONObject(json);
                     String status = jobject.getString("status");
                     if (status.equals("true"))
                     {
                         passenger_name=jobject.optString("passenger_name");
                         passenger_mobile=jobject.optString("passenger_mobile");
                         passenger_email=jobject.optString("passenger_email");
                         passenger_gender=jobject.optString("passenger_gender");
                         passenger_hose_loac=jobject.optString("passenger_hose_loac");
                         passenger_work_loac=jobject.optString("passenger_work_loac");
                         passenger_company=jobject.optString("passenger_company");
                         passenger_dob=jobject.optString("passenger_dob");
                         passenger_emergency_num=jobject.optString("passenger_emergency_num");
                         passenger_referral_code= jobject.optString("passenger_referral_code");
                         passenger_image_mode= jobject.optString("image_type");
                         rbfemale.setChecked(false);
                         rbmale.setChecked(false);
                         passenger_image= jobject.optString("passenger_image");
                         //Toast.makeText(mContext,passenger_gender,Toast.LENGTH_SHORT).show();

                         if (passenger_gender.equals("1"))
                         {
                             rbfemale.setChecked(false);
                             rbmale.setChecked(true);
                         }
                         else
                         {
                             rbfemale.setChecked(true);
                             rbmale.setChecked(false);
                         }

                         edNameId.setText(passenger_name);
                         edTelephoneId.setText(passenger_mobile);
                         edCompanyNameId.setText(passenger_company);
                         edEmrgncyMobNoId.setText(passenger_emergency_num);
                         edWorkLocationId.setText(passenger_work_loac);
                         edHouseLocationId.setText(passenger_hose_loac);
                         edDobId.setText(passenger_dob);
                         tvEmailId.setText(passenger_email);
                         tvReferralCode.setText(passenger_referral_code);

//                       To set profile pic
                         if(passenger_image_mode.equals("1"))
                            Picasso.with(mContext).load(passenger_image).rotate(90).transform(new CircleTransform()).into(imgprofileId);
                         else
                             Picasso.with(mContext).load(passenger_image).transform(new CircleTransform()).into(imgprofileId);

                     }
                 }
                 catch (JSONException e) {
                     e.printStackTrace();
                 }
             }
         });
     }

    public void SendUserData()
    {

        if (rbmale.isChecked())
        {
            gender = "1";
        }
        if (rbfemale.isChecked())
        {
            gender = "2";
        }

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("passenger_id", p_id);
        params.put("passenger_mobile",edTelephoneId.getText().toString());
        params.put("passenger_company", edCompanyNameId.getText().toString());
        params.put("passenger_gender",gender);
        params.put("passenger_hose_loac",edHouseLocationId.getText().toString());
        params.put("passenger_work_loac",edWorkLocationId.getText().toString());
        params.put("passenger_dob",edDobId.getText().toString());
        params.put("passenger_emergency_num",edEmrgncyMobNoId.getText().toString());
        params.put("passenger_name", edNameId.getText().toString());
        params.put("image_type", passenger_image_mode_edited);


        if(filePath==null) {


                // multipart.addPart("user_image", filebody);
            }
        else {
            File f = new File(filePath);
            if (f != null) {
                FileBody filebody = new FileBody(f, "image/jpeg");

                params.put("passenger_image", f);

            }
        }

        Log.e("image_type",passenger_image_mode_edited);

        GlobalValues.getMethodManagerObj(mContext).userSaveProfileData(params, new MethodManager_Listner() {
            @Override
            public void onError()
            {
                hideprogress();
                Log.e("userSaveProfile error", "error");
            }

            @Override
            public void onSuccess(String json) {

                Log.e("userSaveProfile", json.toString());

                try
                {
                    JSONObject jsonObject = new JSONObject(json);
                    String msg = jsonObject.getString("msg");
                     hideprogress();
                    if (msg.equals("Invalid Request Parameter")) {
                        Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();
                    } else if (msg.equals("Profile updated successfully"))
                    {
                        Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();
                        P_name=jsonObject.optString("passenger_name");
                        P_email=jsonObject.optString("passenger_email");
                        P_id=jsonObject.optString("passenger_id");
                        P_imgurl = jsonObject.optString("passenger_image");
                        qr_url = jsonObject.optString("passenger_qrcode_image");
                        referral_code=jsonObject.optString("passenger_referral_code");
                        passenger_mobile=jsonObject.optString("passenger_mobile");
                        Session_manager.saveUserInfo(mContext, P_name, P_email, P_id, P_imgurl, qr_url, referral_code,passenger_mobile,passenger_company,passenger_image_mode);
                        DashBoardActivity.displayview(DashBoardActivity.showprofilefragment,null);
                        //Session_manager.saveUserInfo(mContext, P_name, P_email, P_id, P_imgurl, qr_url, passenger_referral_code)
                    }

                }
                catch (JSONException e)
                {
                    hideprogress();
                    e.printStackTrace();
                }
            }
        });

    }
    public void showDialogForImageSelect()
    {
        final CharSequence[] items = { "Take Photo", "Choose from Gallery", "Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int position) {
                try {
                    if (items[position].equals("Take Photo")) {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        fileName = System.currentTimeMillis() + "-" + ".jpg";
                        File sdCardDir = SDDir.createSDCardDir();
                        File imageDir = SDDir.createImageSubDir(mContext, sdCardDir);
                        File profileImage = new File(imageDir, fileName);
                        absPath = profileImage.getAbsolutePath();
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(profileImage));
                        savePref(absPath);
                        startActivityForResult(intent, CAMERA_REQUEST);
                    } else if (items[position].equals("Choose from Gallery")) {
                        Intent intent = new Intent(Intent.ACTION_PICK,
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(intent, SELECT_FILE);
                    } else {
                        dialog.dismiss();
                    }
                } catch (Exception e) {

                }
            }
        });
        builder.setCancelable(false);
        builder.show();
    }

    private void savePref(String str)
    {
        SharedPreferences sf = mContext.getSharedPreferences("profileImage", 1);
        SharedPreferences.Editor editor = sf.edit();
        editor.putString("imageURI", str);
        editor.commit();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == SELECT_FILE && resultCode == getActivity().RESULT_OK) {
                if (data != null) {
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = { MediaStore.Images.Media.DATA };
                    Cursor cursor = mContext.getContentResolver().query(selectedImage, filePathColumn, null, null,
                            null);
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    picturePath = cursor.getString(columnIndex);
                    image = ImageDecoder.decodeFile(picturePath);
                    if (image != null) {

                        try {
                            int width = image.getWidth() / 2;
                            int height = image.getHeight() / 2;
                            image = Bitmap.createScaledBitmap(image, width, height, false);
                            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                            image.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                            Bitmap bm = BitmapFactory.decodeStream(new ByteArrayInputStream(bytes.toByteArray()));
                            imageBitmap = bm;
                            imgprofileId.setImageBitmap(RoundedImage.GetBitmapClippedCircle(imageBitmap));
                            filePath = picturePath;
                            passenger_image_mode_edited="2";

                        } catch (OutOfMemoryError e) {

                        } catch (Exception e) {

                        }

                    }
                    cursor.close();
                }
            }
            if (requestCode == CAMERA_REQUEST && resultCode == getActivity().RESULT_OK) {
                try {
                    String URI = getImageURI();
                    try {
                        File file = new File(URI);
                        ExifInterface exif = new ExifInterface(file.getAbsolutePath());
                        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                                ExifInterface.ORIENTATION_NORMAL);
                        switch (orientation) {
                            case ExifInterface.ORIENTATION_ROTATE_270:
                                rotate = 270;
                                ImageOrientation(file, rotate);
                                break;
                            case ExifInterface.ORIENTATION_ROTATE_180:
                                rotate = 180;
                                ImageOrientation(file, rotate);
                                break;
                            case ExifInterface.ORIENTATION_ROTATE_90:
                                rotate = 90;
                                ImageOrientation(file, rotate);
                                break;
                            case 1:
                                rotate = 90;
                                ImageOrientation(file, rotate);
                                break;

                            case 2:
                                rotate = 0;
                                ImageOrientation(file, rotate);
                                break;
                            case 4:
                                rotate = 180;
                                ImageOrientation(file, rotate);
                                break;

                            case 0:
                                rotate = 90;
                                ImageOrientation(file, rotate);
                                break;
                        }
                    } catch (Exception e) {

                    }
                } catch (Exception e) {

                }
            }
            if (requestCode == CAMERA_REQUEST && resultCode == getActivity().RESULT_OK) {
                setImageFromSDCard();
                passenger_image_mode_edited="1";
            }
        } catch (Exception e) {

        }


    }
    private String getImageURI()
    {
        SharedPreferences sf = mContext.getSharedPreferences("profileImage", 1);
        String uri = sf.getString("imageURI", null);
        return uri;
    }
    private void ImageOrientation(File file, int rotate)
    {
        try {
            FileInputStream fis = new FileInputStream(file);
            filePath = file.getAbsolutePath();
            Bitmap photo = BitmapFactory.decodeStream(fis);
            Matrix matrix = new Matrix();
            matrix.preRotate(rotate); // clockwise by 90 degrees
            photo = Bitmap.createBitmap(photo, 0, 0, photo.getWidth() / 3, photo.getHeight() / 3, matrix, true);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            photo.compress(Bitmap.CompressFormat.JPEG, 100, out);
            Bitmap bm = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
            photo = bm;
            imgprofileId.setImageBitmap(RoundedImage.GetBitmapClippedCircle(photo));
            imageBitmap = photo;
        } catch (FileNotFoundException e) {

        } catch (OutOfMemoryError e) {

        } catch (Exception e) {

        }
    }

    private void setImageFromSDCard()
    {
        if (currentVersion > 15) {
            String[] filePathColumn = { MediaStore.Images.Media.DATA };
            if (cameraImagePath != null) {
                Cursor imageCursor = mContext.getContentResolver().query(cameraImagePath, filePathColumn, null, null,
                        null);
                if (imageCursor != null && imageCursor.moveToFirst()) {
                    int columnIndex = imageCursor.getColumnIndex(filePathColumn[0]);
                    filePath = imageCursor.getString(columnIndex);

                    imageCursor.close();
                }
            }
        } else {
            String struri = getImageURI();
            filePath = struri;
        }
        Bitmap bitmap = ImageDecoder.decodeFile(filePath);
        if (bitmap != null) {

            try {
                int widht = bitmap.getWidth() / 3;
                int height = bitmap.getHeight() / 3;
                bitmap = Bitmap.createScaledBitmap(bitmap, widht, height, false);
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                imageBitmap = BitmapFactory.decodeStream(new ByteArrayInputStream(bytes.toByteArray()));
                imgprofileId.setImageBitmap(RoundedImage.GetBitmapClippedCircle(imageBitmap));
                bitmap.recycle();
            } catch (OutOfMemoryError e) {

            } catch (Exception e) {

            }

        }
    }

    public void showprogress() {
        pdialog = new ProgressDialog(mContext);
        pdialog.setCancelable(false);
        pdialog.setCanceledOnTouchOutside(false);
        pdialog.setMessage("Updating...");
        pdialog.show();
    }

    public void hideprogress() {
        if (pdialog != null) {
            pdialog.dismiss();
        }
    }

}

//passenger_id*, passenger_mobile, company_name, passenger_gender(1for male,2 for Female), passenger_hose_loac, passenger_work_loac, passenger_dob, passenger_emergency_num, passenger_image, passenger_name
