package com.journepassenger.fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.journepassenger.activity.DashBoardActivity;
import com.journepassenger.utilities.ConnectionDetector;
import com.journepassenger.utilities.GlobalValues;
import com.journepassenger.utilities.MethodManager_Listner;
import com.journepassenger.utilities.Session_manager;
import com.journyapp.journyapp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by administrator on 29/2/16.
 */
public class CurrentBookingDetail_Fragment extends Fragment
{
    public static String tag="curruntbooking";
    public static Fragment fragment;
    static FragmentTransaction ft;
    static public FragmentManager fragmentmanger;
    static public Context mContext;
    View view;
    TextView fromlocation_booking;
    TextView tv_timesrc_booking;
    TextView text_tolocation_booking;
    TextView tv_timedesti_booking;
    TextView bookingid;
    TextView tvseats;
    TextView tvtotalcost;
    TextView cancel_btn;
    TextView tvBusNo;
    TextView tvTrackRide;
    static String source;
    static String destination;
    static String source_time;
    static String desti_time;
    static String booking_id;
    static String seats;
    static String coast;
    static String bus_no;
    static String pessenger_id,bus_id;
    public static Fragment getInstance(Context ct, FragmentManager fm,Bundle b)
    {

        mContext=ct;
        fragmentmanger=fm;

        fragment=new CurrentBookingDetail_Fragment();
        source=b.getString("source");
        destination=b.getString("destination");
        source_time=b.getString("source_time");
        desti_time=b.getString("desti_time");
        booking_id=b.getString("booking_id");
        seats=b.getString("seats");
        coast=b.getString("coast");
        bus_no=b.getString("bus_no");
        //bus_id=b.getString("bus_id");
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view=inflater.inflate(R.layout.currentbookingdetail,container,false);

        fromlocation_booking=(TextView)view.findViewById(R.id.fromlocation_booking);
        tv_timesrc_booking=(TextView)view.findViewById(R.id.tv_timesrc_booking);
        text_tolocation_booking=(TextView)view.findViewById(R.id.text_tolocation_booking);
        tv_timedesti_booking=(TextView)view.findViewById(R.id.tv_timedesti_booking);
        bookingid=(TextView)view.findViewById(R.id.bookingid);
        tvseats=(TextView)view.findViewById(R.id.tvseats);
        tvtotalcost=(TextView)view.findViewById(R.id.tvtotalcost);
        cancel_btn=(TextView)view.findViewById(R.id.cancel_btn);
        tvBusNo=(TextView)view.findViewById(R.id.tvBusNo);
        tvTrackRide=(TextView)view.findViewById(R.id.tvTrackRide);
        mContext=getActivity();
        DashBoardActivity.hideSearchButton(mContext);
        DashBoardActivity.hideLoacationButton(mContext);
        DashBoardActivity.hideafteredit(mContext);
        DashBoardActivity.hidebeforeedit(mContext);
        DashBoardActivity.hideMenuDrawerButton(mContext);
        DashBoardActivity.showBackButton(mContext);
        DashBoardActivity.hidecityname_header(mContext);
        DashBoardActivity.updatedTitle("Current Bookings Details");
        DashBoardActivity.hidePrintIcon(mContext);
        DashBoardActivity.hidehomeButton(mContext);
        pessenger_id= Session_manager.getPassengerid(mContext);
        fromlocation_booking.setText(source);
        tv_timesrc_booking.setText(source_time);
        text_tolocation_booking.setText(destination);
        tv_timedesti_booking.setText(desti_time);
        bookingid.setText(booking_id);
        tvseats.setText(seats);
        tvtotalcost.setText("Rs."+coast);
        tvBusNo.setText(bus_no);


        cancel_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectionDetector.isNetworkAvailable(mContext))

                {
                    ShowDialog();
                } else {
                    Toast.makeText(mContext, "You don't have Internet Connection", Toast.LENGTH_LONG).show();

                }
            }
        });
        tvTrackRide.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle b=new Bundle();
                b.putString("booking_id", booking_id);
                DashBoardActivity.displayview(DashBoardActivity.trackride,b);
                //showDialogForConfirmEmail("Message", "Your Trip is Not Started Yet.");
            }
        });
        return view;
    }
    public static void cancel_Boolking()
    {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("passenger_id",pessenger_id);
        params.put("b_id",booking_id);
        params.put("reason","Nothing");
        Log.e("parma",""+params);
        GlobalValues.getMethodManagerObj(mContext).Cancel_Booking(params, new MethodManager_Listner() {
            @Override
            public void onError()
            {

            }

            @Override
            public void onSuccess(String json1)
            {
                Log.e("Cancel json",json1.toString());
                JSONObject json = null;
                try {
                    json = new JSONObject(json1);
                    String message = json.getString("msg");
                    String status = json.getString("status");


                    if(message.equals("Booking cancelled successfully"))
                    {
                        DashBoardActivity.displayview(DashBoardActivity.booking_history,null);
                        Toast.makeText(mContext, "" + message, Toast.LENGTH_LONG).show();
                    }
                    else
                    {
                        Toast.makeText(mContext,""+message,Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });
    }

    public void ShowDialog() {
        final Dialog user_info = new Dialog(mContext);
        LayoutInflater li = (LayoutInflater) mContext.getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View vi = li.inflate(R.layout.pop_upreason, null, false);
        user_info.requestWindowFeature(Window.FEATURE_NO_TITLE);

        final EditText reason = (EditText) vi.findViewById(R.id.edreason);
        final TextView btn_cancel=(TextView)vi.findViewById(R.id.btn_cancel);
        final TextView btn_submit=(TextView)vi.findViewById(R.id.btn_submit);
        //final EditText dealcode=(EditText)user_info.findViewById(R.id.u_dealcode);
        //dealcode.setVisibility(View.GONE);

        btn_cancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                user_info.dismiss();
            }
        });
        btn_submit.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                String reasontext = reason.getText().toString();

                if (reasontext.equals("")) {
                    Toast.makeText(mContext, "Please Fill all Information", Toast.LENGTH_LONG).show();
                } else {
                    cancel_Boolking();
                    user_info.dismiss();
                }
            }
        });
        user_info.setContentView(vi);
        user_info.setCanceledOnTouchOutside(false);
        user_info.show();

    }
    public static void showDialogForConfirmEmail(String title, final String message)

    {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext).setTitle(title).setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                        //DashBoardActivity.displayview(DashBoardActivity.booking_history,null);
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }
}
