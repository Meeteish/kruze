package com.journepassenger.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.journepassenger.activity.DashBoardActivity;
import com.journepassenger.utilities.Session_manager;
import com.journyapp.journyapp.R;

/**
 * Created by administrator on 26/2/16.
 */
public class BookingSucess_Fragmant extends Fragment
{
    public static String tag="sucess_booking";
    public static Fragment fragment;
    static FragmentTransaction ft;
    static public FragmentManager fragmentmanger;
    static public Context mContext;
    View view;
    ImageView imgReturnTicket;
    ImageView imgMonthlyPass;
    ImageView imgNotNow,imgContactUs;
    static String fromcity_landmark;
    static String tocity_landmark;
    static String b_id;
    public static Fragment getInstance(Context ct, FragmentManager fm,Bundle b)
    {

        mContext=ct;
        fragmentmanger=fm;
        fragment=new BookingSucess_Fragmant();
        fromcity_landmark=b.getString("fromcity_landmark");
        tocity_landmark=b.getString("tocity_landmark");
        b_id=b.getString("b_id");
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view=inflater.inflate(R.layout.successful,container,false);
         mContext=getActivity();
        imgReturnTicket=(ImageView)view.findViewById(R.id.imgReturnTicket);
        imgMonthlyPass=(ImageView)view.findViewById(R.id.imgMonthlyPass);
        imgNotNow=(ImageView)view.findViewById(R.id.imgNotNow);
        imgContactUs=(ImageView)view.findViewById(R.id.imgContactUs);
        DashBoardActivity.hideSearchButton(mContext);
        DashBoardActivity.hideLoacationButton(mContext);
        DashBoardActivity.hideafteredit(mContext);
        DashBoardActivity.hidebeforeedit(mContext);
        DashBoardActivity.hideMenuDrawerButton(mContext);
        DashBoardActivity.hideBackButton(mContext);
        DashBoardActivity.updatedTitle("");
        DashBoardActivity.hidecityname_header(mContext);
        DashBoardActivity.hidePrintIcon(mContext);
        DashBoardActivity.showhomeButton(mContext);


        fromcity_landmark= Session_manager.getfromcity_reverse(mContext);
        tocity_landmark=Session_manager.gettocity_reverse(mContext);
        imgReturnTicket.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle b = new Bundle();
                b.putString("fromcity", fromcity_landmark);
                b.putString("tocity", tocity_landmark);
                b.putString("b_id",b_id);
                b.putString("type","3");
                DashBoardActivity.displayview(DashBoardActivity.searchbus, b);
            }
        });

        imgNotNow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle b=new Bundle();
                b.putString("fromcity",fromcity_landmark);
                b.putString("tocity",tocity_landmark);
                DashBoardActivity.displayview(DashBoardActivity.booking_history, b);

            }
        });

        imgContactUs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DashBoardActivity.displayview(DashBoardActivity.contactusfragment,null);
            }
        });
        return view;
    }
}
