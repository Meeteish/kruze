package com.journepassenger.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.journepassenger.activity.DashBoardActivity;
import com.journepassenger.bean.SeatRows_bean;
import com.journepassenger.bean.SeatSecond_bean;
import com.journepassenger.bean.Seatno_bean;
import com.journepassenger.utilities.ConnectionDetector;
import com.journepassenger.utilities.GlobalValues;
import com.journepassenger.utilities.MethodManager_Listner;
import com.journepassenger.utilities.Session_manager;
import com.journepassenger.utilities.Static_Data;
import com.journyapp.journyapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by administrator on 8/2/16.
 */
public class SeatAvliablity_Fragment extends Fragment
{
    public static Fragment fragment;
    static FragmentTransaction ft;
    static public FragmentManager fragmentmanger;
    static public Context mContext;
    View v;
    ArrayList<SeatRows_bean> seatrows;
    ArrayList<SeatSecond_bean> seatsecondrow;
    ArrayList<Seatno_bean> seatno_Arraylist;
    static String tripid;
    GridView seat_grid;
    LinearLayout seat_liner;
    Button btn_done;
     public static TextView seattext;
    LinearLayout mLinearListView;
    public static TextView seat_price;
    static String fromcity_landmark;
    static String tocity_landmark;
    public static ArrayList<String> bookedseat_arraylist;
    //public static ArrayList<String> selected_seatstatus;
    TextView textView_time,tvBusNo;
    static String bus_no;
    static String fromS_Time;
    static String toS_Time;
    static String fromstop_Id;
    static String tostop_Id;
    public static int price;
    String bus_id;
    static ProgressDialog pdialog;
    String seat_totalprice,seat_total;
    static int con_seat_totalprice;
    static int con_seat_total;
    static String ac_type;
    static String fromstop_time,tostop_time;
    public static Fragment getInstance(Context context,FragmentManager fm,Bundle b)
    {
        mContext=context;
        fragmentmanger=fm;
        fragment=new SeatAvliablity_Fragment();
        tripid=b.getString("trip_id");
        fromcity_landmark=b.getString("fromlandmark");
        tocity_landmark=b.getString("tolandmark");
        bus_no=b.getString("bus_no");
        fromS_Time=b.getString("fromS_Time");
        toS_Time=b.getString("toS_Time");
        fromstop_Id=b.getString("fromstop_Id");
        tostop_Id=b.getString("tostop_Id");
        ac_type=b.getString("ac_type");
        fromstop_time=b.getString("fromstop_time");
        tostop_time=b.getString("tostop_time");

        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        //Static_Data.selected_seatstatus=new ArrayList<>();

        if(!Static_Data.is_done)
        {
            Static_Data.selected_seatstatus.clear();
            Session_manager.setseattotal(mContext, "0","0");

        }
        v=inflater.inflate(R.layout.seatview_xml, null, false);
        mContext=getActivity();
        seattext=(TextView)v.findViewById(R.id.seattext);
        seat_price=(TextView)v.findViewById(R.id.seat_price);
        textView_time=(TextView)v.findViewById(R.id.textView_time);
        tvBusNo=(TextView)v.findViewById(R.id.tvBusNo);
        btn_done=(Button)v.findViewById(R.id.btn_done);

        mLinearListView  =(LinearLayout)v.findViewById(R.id.mLinearListView);

        DashBoardActivity.hidecityname_header(mContext);
        DashBoardActivity.updatedTitle("Select Seat");
        DashBoardActivity.hideLoacationButton(mContext);
        DashBoardActivity.hidePrintIcon(mContext);
        DashBoardActivity.showBackButton();
        DashBoardActivity.hideafteredit(mContext);
        DashBoardActivity.hidebeforeedit(mContext);
        DashBoardActivity.showhomeButton(mContext);
        tvBusNo.setText(bus_no);
        textView_time.setText(fromS_Time + " - " + toS_Time);
        seat_totalprice=Session_manager.getseat_totalprice(mContext);
        seat_total=Session_manager.getseat_total(mContext);


        con_seat_totalprice=Integer.parseInt(seat_totalprice);
        con_seat_total=Integer.parseInt(seat_total);

        seattext.setText("" + con_seat_total);
        seat_price.setText("" + con_seat_totalprice);
        GetseatsInformation();
        showprogress();

        btn_done.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(seattext.getText().toString().equals("0"))
                {
                    Toast.makeText(mContext,"Please Select Seat First",Toast.LENGTH_LONG).show();
                }
                else if (ConnectionDetector.isNetworkAvailable(mContext))
                {
                    //Toast.makeText(mContext,""+seattext.getText().toString(),Toast.LENGTH_LONG).show();
                    Bundle b=new Bundle();
                    b.putString("fromcity_landmark",fromcity_landmark);
                    b.putString("tocity_landmark",tocity_landmark);
                    b.putString("price",seat_price.getText().toString());
                    b.putString("bus_no",bus_no);
                    b.putString("fromS_Time",fromS_Time);
                    b.putString("toS_Time",toS_Time);
                    b.putInt("seat_price", price);
                    b.putString("trip_id", tripid);
                    b.putString("bus_id", bus_id);
                    b.putString("fromstop_Id", fromstop_Id);
                    b.putString("tostop_Id",tostop_Id);
                    b.putString("ac_type",ac_type);
                    b.putString("fromstop_time",fromstop_time);
                    b.putString("tostop_time",tostop_time);
                    b.putStringArrayList("seats", Static_Data.selected_seatstatus);
                    Static_Data.is_done=true;
                    Session_manager.setseattotal(mContext,seattext.getText().toString(),seat_price.getText().toString());
                    DashBoardActivity.displayview(DashBoardActivity.bookingdetailfragment,b);
                }
                else
                {
                    Toast.makeText(mContext, "You don't have Internet Connection", Toast.LENGTH_LONG).show();

                }

            }
        });
        return v;
    }

    public void GetseatsInformation()

    {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("trip_id",tripid);
        params.put("fromstop_Id",fromstop_Id);
        params.put("tostop_Id",tostop_Id);
        Log.e("parma",""+params);

        GlobalValues.getMethodManagerObj(mContext).GetseatsAvliability(params, new MethodManager_Listner() {
            @Override
            public void onError()
            {
                hideprogress();
            }

            @Override
            public void onSuccess(String json1)
            {
                hideprogress();
                {
                    JSONObject json = null;
                    try {
                        json = new JSONObject(json1);
                        price=json.optInt("price");
                        bus_id=json.optString("bus_id");
                        JSONArray responseJsonArray = json.getJSONArray("rows_array");
                        seatrows = new ArrayList<SeatRows_bean>();


                        JSONArray seat_nos = null;
                        for (int j = 0; j < responseJsonArray.length(); j++)

                        {
                            JSONObject friendsObj = responseJsonArray.getJSONObject(j);
                            SeatRows_bean bean = new SeatRows_bean();
                            String row = friendsObj.getString("row");
                            bean.setRow(row);
                            seat_nos = friendsObj.getJSONArray("seats");

                            seatno_Arraylist = new ArrayList<Seatno_bean>();

                            for (int j1 = 0; j1 < seat_nos.length(); j1++) {
                                JSONObject seatsno_object = seat_nos.getJSONObject(j1);
                                Seatno_bean seatno_bean = new Seatno_bean();
                                String Seatnos = seatsno_object.getString("seat_no");
                                seatno_bean.setSeat_no(Seatnos);

                                seatno_Arraylist.add(seatno_bean);
                            }

                            bean.setSeatno_array(seatno_Arraylist);
                            seatrows.add(bean);

                        }
                        JSONArray booked_responceArray = json.getJSONArray("booked_seats");
                        bookedseat_arraylist = new ArrayList<String>();
                        for (int i = 0; i < booked_responceArray.length(); i++) {

                            String no = (String) booked_responceArray.get(i);
                            bookedseat_arraylist.add(no);

                        }
                        for (SeatRows_bean s : seatrows) {
                            LinearLayout.LayoutParams lp = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                            LinearLayout layout = new LinearLayout(mContext);
                            layout.setLayoutParams(lp);

                            for (Seatno_bean seat : s.getSeatno_array()) {
                                LinearLayout.LayoutParams lp_iv = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                                lp_iv.weight = 1;
                                FrameLayout framelayout = new FrameLayout(mContext);
                                framelayout.setLayoutParams(new FrameLayout.LayoutParams(FrameLayout.LayoutParams.WRAP_CONTENT, FrameLayout.LayoutParams.WRAP_CONTENT));
                                final TextView tv=new TextView(mContext);
                                 tv.setGravity(Gravity.CENTER);
                                final ImageView iv = new ImageView(mContext);

                                framelayout.addView(iv);
                                framelayout.addView(tv);
                                framelayout.setLayoutParams(lp_iv);

                                final String sno = seat.getSeat_no();
                                if (sno.equals("0"))
                                {
                                    iv.setImageResource(R.drawable.white_bg);
                                } else
                                {
                                    if (bookedseat_arraylist.contains(sno))
                                    {
                                        tv.setText(""+sno);
                                        iv.setImageResource(R.drawable.reserved_seat);
                                    }


                                    else
                                    {
                                        if (Static_Data.selected_seatstatus.contains(sno))
                                        {
                                            tv.setText(""+sno);
                                        iv.setImageResource(R.drawable.selected_seat);
                                    }
                                        else
                                        {     tv.setText(""+sno);
                                            iv.setImageResource(R.drawable.available_seat);
                                        }

                                        iv.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                if (Static_Data.selected_seatstatus.contains(sno)) {
                                                    iv.setImageResource(R.drawable.available_seat);
                                                    Static_Data.selected_seatstatus.remove(sno);
                                                    if (Static_Data.selected_seatstatus.size()==0)
                                                    {
                                                        btn_done.setBackgroundColor(getResources().getColor(R.color.black));
                                                    }
                                                    seattext.setText("" + Static_Data.selected_seatstatus.size());
                                                    seat_price.setText("" +price * (Static_Data.selected_seatstatus.size()));
                                                } else {
                                                    iv.setImageResource(R.drawable.selected_seat);
                                                    Static_Data.selected_seatstatus.add(sno);
                                                    seattext.setText("" + Static_Data.selected_seatstatus.size());
                                                    seat_price.setText("" + price * (Static_Data.selected_seatstatus.size()));
                                                    btn_done.setBackgroundColor(getResources().getColor(R.color.textGreen));
                                                }
                                                Log.e("selected_seatstatus", Static_Data.selected_seatstatus.toString());
                                            }
                                        });
                                    }
                                }
                                layout.addView(framelayout);
                            }
                            mLinearListView.addView(layout);
                        }




                    } catch (JSONException e)
                    {
                        e.printStackTrace();
                        hideprogress();
                    }

                }
            }
        });
    }

    public void showprogress() {
        pdialog = new ProgressDialog(mContext);
        pdialog.setCancelable(false);
        pdialog.setCanceledOnTouchOutside(false);
        pdialog.setMessage("Loading...");
        pdialog.show();
    }

    public static void hideprogress() {
        if (pdialog != null) {
            pdialog.dismiss();
        }
    }
}