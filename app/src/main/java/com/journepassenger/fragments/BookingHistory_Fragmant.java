package com.journepassenger.fragments;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.journepassenger.activity.DashBoardActivity;
import com.journepassenger.adapter.BookingCurrunt_Adapter;
import com.journepassenger.adapter.BookingHistory_Adapter;
import com.journepassenger.bean.Bookingdata_bean;
import com.journepassenger.utilities.ConnectionDetector;
import com.journepassenger.utilities.GlobalValues;
import com.journepassenger.utilities.MethodManager_Listner;
import com.journepassenger.utilities.Session_manager;
import com.journyapp.journyapp.R;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by administrator on 26/2/16.
 */
public class BookingHistory_Fragmant extends Fragment
{
    public static String tag="bookinghistory";
    public static Fragment fragment;
    static FragmentTransaction ft;
    static public FragmentManager fragmentmanger;
    static public Context mContext;
    View view;
    String pessenger_id;
    ListView booking_deatail_list;
    ArrayList<Bookingdata_bean> GetBookingArraycurrunt_json;
    BookingCurrunt_Adapter booking_apater_curunt;
    ArrayList<Bookingdata_bean> GetBookingArrayhistory_json;
    BookingHistory_Adapter booking_apater_history;
    LinearLayout tv_currunt_booking;
    LinearLayout tv_history_booking;
    static ProgressDialog pdialog;
    TextView currunt_text,histroy_txt;
    TextView tv_today;
    ArrayList<Bookingdata_bean> DummyGetBookingArraycurrunt_json=new ArrayList<Bookingdata_bean>();
    public static Fragment getInstance(Context ct, FragmentManager fm,Bundle b)
    {

        mContext=ct;
        fragmentmanger=fm;
        fragment=new BookingHistory_Fragmant();
        /*fromcity=b.getString("fromcity");
        tocity=b.getString("tocity");*/
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view=inflater.inflate(R.layout.history_bookings, container, false);

        tv_currunt_booking=(LinearLayout)view.findViewById(R.id.tv_currunt_booking);
        tv_history_booking=(LinearLayout)view.findViewById(R.id.tv_history_booking);
        booking_deatail_list=(ListView)view.findViewById(R.id.booking_deatail_list);
        currunt_text=(TextView)view.findViewById(R.id.currunt_text);
        histroy_txt=(TextView)view.findViewById(R.id.histroy_txt);
        tv_today=(TextView)view.findViewById(R.id.tv_today);
        mContext=getActivity();
        DashBoardActivity.hideSearchButton(mContext);
        DashBoardActivity.hideLoacationButton(mContext);
        DashBoardActivity.hideafteredit(mContext);
        DashBoardActivity.hidebeforeedit(mContext);
        DashBoardActivity.showMenuDrawerButton(mContext);
        DashBoardActivity.hideBackButton(mContext);
        DashBoardActivity.updatedTitle("Bookings");
        DashBoardActivity.hidecityname_header(mContext);
        DashBoardActivity.hidePrintIcon(mContext);
        DashBoardActivity.hidehomeButton(mContext);
        pessenger_id= Session_manager.getPassengerid(mContext);
        GetBooking_data_Currunt();
        tv_currunt_booking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                booking_apater_history = new BookingHistory_Adapter(mContext, DummyGetBookingArraycurrunt_json);
                booking_deatail_list.setAdapter(booking_apater_history);
                booking_apater_history.notifyDataSetChanged();

                if (ConnectionDetector.isNetworkAvailable(mContext))
                {
                    tv_today.setVisibility(View.VISIBLE);
                    GetBooking_data_Currunt();
                    tv_currunt_booking.setBackgroundColor(getResources().getColor(R.color.Active_tab_bg));
                    tv_history_booking.setBackgroundColor(getResources().getColor(R.color.Inactive_tab_bg_color));
                    currunt_text.setTextColor(getResources().getColor(R.color.Active_tab_text));
                    histroy_txt.setTextColor(getResources().getColor(R.color.Inactive_tab_text));
                    showprogress();
                }
                else
                {
                    Toast.makeText(mContext, "You don't have Internet Connection", Toast.LENGTH_LONG).show();

                }
            }
        });
        tv_history_booking.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tv_today.setVisibility(View.GONE);
                booking_apater_history = new BookingHistory_Adapter(mContext, DummyGetBookingArraycurrunt_json);
                booking_deatail_list.setAdapter(booking_apater_history);
                booking_apater_history.notifyDataSetChanged();

                if (ConnectionDetector.isNetworkAvailable(mContext)) {
                    GetBooking_data_History();
                    tv_currunt_booking.setBackgroundColor(getResources().getColor(R.color.Inactive_tab_bg_color));
                    tv_history_booking.setBackgroundColor(getResources().getColor(R.color.Active_tab_bg));
                    currunt_text.setTextColor(getResources().getColor(R.color.Inactive_tab_text));
                    histroy_txt.setTextColor(getResources().getColor(R.color.Active_tab_text));
                    showprogress();
                }
                else
                {
                    Toast.makeText(mContext, "You don't have Internet Connection", Toast.LENGTH_LONG).show();

                }
            }
        });
        return view;
    }

    public void GetBooking_data_Currunt()
    {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("passenger_id",pessenger_id);
        Log.e("booking Data",""+params);
        GlobalValues.getMethodManagerObj(mContext).GetBooking_Details(params, new MethodManager_Listner() {
            @Override
            public void onError()
            {
              hideprogress();
            }

            @Override
            public void onSuccess(String json1)
            {
                try {
                     hideprogress();
                    JSONObject json = new JSONObject(json1);

                    String message = json.getString("msg");
                    String status = json.getString("status");

                    if (status.equalsIgnoreCase("false")) {

                        GetBookingArraycurrunt_json = new ArrayList<Bookingdata_bean>();
                        booking_apater_curunt = new BookingCurrunt_Adapter(mContext, GetBookingArraycurrunt_json);
                        booking_deatail_list.setAdapter(booking_apater_curunt);
                        hideprogress();

                        showDialogForConfirmEmail("Message", "No Bookings Found");
                    } else if (status.equals("true")) {
                        hideprogress();

                        JSONArray responseJsonArray = json.getJSONArray("booking_array");

                        GetBookingArraycurrunt_json = new ArrayList<Bookingdata_bean>();

                        for (int j = 0; j < responseJsonArray.length(); j++)

                        {
                            JSONObject friendsObj = responseJsonArray.getJSONObject(j);
                            Bookingdata_bean booking_dataBean = new Bookingdata_bean();

                            String b_id = friendsObj.getString("b_id");
                            booking_dataBean.setB_id(b_id);


                            String bus_no = friendsObj.getString("bus_no");
                            booking_dataBean.setBus_no(bus_no);

                            String source = friendsObj.optString("source");
                            booking_dataBean.setSource(source);

                            String destination = friendsObj.optString("destination");
                            booking_dataBean.setDestination(destination);

                            String source_time = friendsObj.optString("source_time");
                            booking_dataBean.setSource_time(source_time);

                            String destination_time = friendsObj.optString("destination_time");
                            booking_dataBean.setDestination_time(destination_time);

                            String seats = friendsObj.optString("seats");
                            booking_dataBean.setSeats(seats);

                            String price = friendsObj.optString("price");
                            booking_dataBean.setPrice(price);




                            GetBookingArraycurrunt_json.add(booking_dataBean);
                        }

                        booking_apater_curunt = new BookingCurrunt_Adapter(mContext, GetBookingArraycurrunt_json);
                        booking_deatail_list.setAdapter(booking_apater_curunt);
                        //booking_apater_curunt.notifyDataSetChanged();
                        //hideKeyboard(mContext);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    hideprogress();
                }
            }

        });

    }
    public void GetBooking_data_History()
    {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("passenger_id",pessenger_id);
        GlobalValues.getMethodManagerObj(mContext).GetBooking_Details_History(params, new MethodManager_Listner() {
            @Override
            public void onError() {
                hideprogress();
            }

            @Override
            public void onSuccess(String json1) {

                try {
                    hideprogress();
                    JSONObject json = new JSONObject(json1);

                    String message = json.getString("msg");
                    String status = json.getString("status");

                    if (status.equalsIgnoreCase("false")) {
                        showDialogForConfirmEmail("Message", "No Data Found");
                        //hideprogress();


                    } else if (status.equals("true")) {
                        //hideprogress();

                        JSONArray responseJsonArray = json.getJSONArray("booking_array");

                        GetBookingArrayhistory_json = new ArrayList<Bookingdata_bean>();

                        for (int j = 0; j < responseJsonArray.length(); j++)

                        {
                            JSONObject friendsObj = responseJsonArray.getJSONObject(j);
                            Bookingdata_bean booking_dataBean = new Bookingdata_bean();

                            String b_id = friendsObj.getString("b_id");
                            booking_dataBean.setB_id(b_id);


                            String bus_no = friendsObj.getString("bus_no");
                            booking_dataBean.setBus_no(bus_no);

                            String source = friendsObj.optString("source");
                            booking_dataBean.setSource(source);

                            String destination = friendsObj.optString("destination");
                            booking_dataBean.setDestination(destination);

                            String source_time = friendsObj.optString("source_time");
                            booking_dataBean.setSource_time(source_time);

                            String destination_time = friendsObj.optString("destination_time");
                            booking_dataBean.setDestination_time(destination_time);

                            String seats = friendsObj.optString("seats");
                            booking_dataBean.setSeats(seats);

                            String price = friendsObj.optString("price");
                            booking_dataBean.setPrice(price);


                            String bokstatus = friendsObj.optString("status");
                            booking_dataBean.setBooking_status(bokstatus);

                            String date = friendsObj.optString("date");
                            booking_dataBean.setDate(date);

                            String feedback_status=friendsObj.getString("feedback_status");
                            booking_dataBean.setFeedback_status(feedback_status);

                            GetBookingArrayhistory_json.add(booking_dataBean);
                        }

                        booking_apater_history = new BookingHistory_Adapter(mContext, GetBookingArrayhistory_json);
                        booking_deatail_list.setAdapter(booking_apater_history);
                        booking_apater_history.notifyDataSetChanged();
                        //hideKeyboard(mContext);
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                    hideprogress();
                }
            }

        });

    }

    public static void showDialogForConfirmEmail(String title, final String message)

    {
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext).setTitle(title).setMessage(message)
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {

                        dialog.dismiss();
                    }
                });
        AlertDialog dialog = builder.create();
        dialog.show();
    }

    public void showprogress() {
        pdialog = new ProgressDialog(mContext);
        pdialog.setCancelable(false);
        pdialog.setCanceledOnTouchOutside(false);
        pdialog.setMessage("Loading...");
        pdialog.show();
    }

    public static void hideprogress() {
        if (pdialog != null) {
            pdialog.dismiss();
        }
    }
}
