package com.journepassenger.fragments;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.journepassenger.activity.DashBoardActivity;
import com.journepassenger.adapter.StopList_Adapter;
import com.journepassenger.bean.Stop_bean;
import com.journepassenger.utilities.ConnectionDetector;
import com.journepassenger.utilities.GlobalValues;
import com.journepassenger.utilities.MethodManager_Listner;
import com.journepassenger.utilities.Session_manager;
import com.journepassenger.utilities.Static_Data;
import com.journyapp.journyapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by administrator on 12-Feb-16.
 */
public class StopDetails_Fragment extends Fragment
{
    public static Fragment fragment;
    static FragmentTransaction ft;
    static public FragmentManager fragmentmanger;
    static public Context mContext;
    View v;
    TextView from;
    TextView to;
    TextView select_seat;
    static ArrayList<Stop_bean> stoplistArray;
    static String selform_city,selto_city,trip_id;
    ListView stop_list;
    StopList_Adapter stopList_adapter;
    static String fromland_id,toland_id;
    static LinearLayout liner_select;
    String fromlandmark;
    String tolandmark;
    static String bus_no;
    static String fromS_Time;
    static String toS_Time;
    TextView tvBusNo;
    static String fromstop_Id;
    static String tostop_Id;
    static String ac_type;
    static ProgressDialog pdialog;
    public static Fragment getInstance(Context context,FragmentManager fm,Bundle b)
    {
        mContext=context;

        fragmentmanger = fm;

        fragment=new StopDetails_Fragment();
        selform_city=b.getString("fromcity");
        selto_city=b.getString("tocity");
        trip_id=b.getString("trip_id");
        fromland_id=b.getString("from_id");
        toland_id=b.getString("to_id");
        bus_no=b.getString("busno");
        fromS_Time=b.getString("fromS_Time");
        toS_Time=b.getString("toS_Time");
        fromstop_Id=b.getString("fromstop_Id");
        tostop_Id=b.getString("tostop_Id");
        ac_type=b.getString("ac_type");
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        v=inflater.inflate(R.layout.stop_detilsxml,null,false);
        from=(TextView)v.findViewById(R.id.from);
        to=(TextView)v.findViewById(R.id.to);
        select_seat=(TextView)v.findViewById(R.id.select_seat);
        stop_list=(ListView)v.findViewById(R.id.stop_list);
        tvBusNo=(TextView)v.findViewById(R.id.tvBusNo);
        liner_select=(LinearLayout)v.findViewById(R.id.liner_select);
        DashBoardActivity.hideSearchButton(mContext);
        DashBoardActivity.hideLoacationButton(mContext);
        DashBoardActivity.hideafteredit(mContext);
        DashBoardActivity.hidebeforeedit(mContext);
        DashBoardActivity.hideMenuDrawerButton(mContext);
        DashBoardActivity.showBackButton(mContext);
        DashBoardActivity.hidecityname_header(mContext);
        DashBoardActivity.updatedTitle("Stop Details");
        DashBoardActivity.hidePrintIcon(mContext);
        DashBoardActivity.showhomeButton(mContext);
        from.setText(selform_city);
        to.setText(selto_city);
        tvBusNo.setText(bus_no);


        GetStop_list();

        select_seat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                fromlandmark = Session_manager.getfromlandmark(mContext);
                tolandmark = Session_manager.gettolandmark(mContext);
                if (fromlandmark == null)
                {

                    Toast.makeText(mContext, "Select Your Source", Toast.LENGTH_LONG).show();

                } else if (tolandmark == null) {

                    Toast.makeText(mContext, "Select Your Destination", Toast.LENGTH_LONG).show();

                }
                else if (ConnectionDetector.isNetworkAvailable(mContext))
                {
                    Bundle b=new Bundle();
                    b.putString("fromlandmark", fromlandmark);
                    b.putString("tolandmark", tolandmark);
                    b.putString("trip_id", trip_id);
                    b.putString("bus_no",bus_no);
                    b.putString("fromS_Time",fromS_Time);
                    b.putString("toS_Time",toS_Time);
                    b.putString("fromstop_Id",StopList_Adapter.from_stopid);
                    b.putString("tostop_Id",StopList_Adapter.to_stopid);
                    b.putString("ac_type",ac_type);
                    b.putString("fromstop_time",StopList_Adapter.fromstop_time);
                    b.putString("tostop_time",StopList_Adapter.tostop_time);
                    StopList_Adapter.from=-1;
                    StopList_Adapter.to=-1;
                    Session_manager.setToLanmark(mContext, null,StopList_Adapter.from_stopid);
                    Session_manager.setfromlandmark(mContext, null, StopList_Adapter.to_stopid);
                    Log.e("check ids", "from :" + StopList_Adapter.from_stopid);
                    Log.e("check ids", "To :" + StopList_Adapter.to_stopid);
                    Static_Data.is_done=false;
                    DashBoardActivity.displayview(DashBoardActivity.seatavliability, b);
                }
                else
                {
                    Toast.makeText(mContext, "You don't have Internet Connection", Toast.LENGTH_LONG).show();

                }
                /*Bundle b=new Bundle();
                b.putString("bus_id",trip_id);
                DashBoardActivity.displayview(DashBoardActivity.seatavliability,b);*/
            }
        });
        return v;
    }

    public  void GetStop_list()
    {
        showprogress();
        Map<String, Object> params = new HashMap<String, Object>();
        /*params.put("from_stop",fromland_id);
        params.put("to_stop",toland_id);*/
        params.put("trip_id",trip_id);
        Log.e("Stop Parma", "" + params);
        GlobalValues.getMethodManagerObj(mContext).GetStop_list(params, new MethodManager_Listner() {
            @Override
            public void onError() {
            hideprogress();
            }

            @Override
            public void onSuccess(String json1)
            {
                hideprogress();
                JSONObject json = null;
                Log.e("stop json:", json1.toString());
                try {
                    json = new JSONObject(json1);
                    String message = json.getString("msg");
                    String status = json.getString("status");
                    if (status.equals("true")) {

                        JSONArray responseJsonArray = json.getJSONArray("stop_list");
                        stoplistArray = new ArrayList<Stop_bean>();

                        for (int j = 0; j < responseJsonArray.length(); j++)

                        {
                            JSONObject friendsObj = responseJsonArray.getJSONObject(j);
                            Stop_bean stop_bean = new Stop_bean();
                            String t_stop_id = friendsObj.optString("t_stop");
                            stop_bean.setT_stop_id(t_stop_id);

                            String t_stop_trip_id = friendsObj.optString("t_stop_trip_id");
                            stop_bean.setT_stop_trip_id(t_stop_trip_id);

                            String t_stop_time = friendsObj.optString("t_stop_time");
                            stop_bean.setT_stop_time(t_stop_time);

                            String bus_stop_name = friendsObj.optString("bus_stop_name");
                            stop_bean.setBus_stop_name(bus_stop_name);

                            String reach_status=friendsObj.optString("reach_status");
                            stop_bean.setReach_status(reach_status);

                            stoplistArray.add(stop_bean);
                        }
                    }
                    stopList_adapter = new StopList_Adapter(mContext, stoplistArray);
                    stop_list.setAdapter(stopList_adapter);

                } catch (JSONException e) {
                    e.printStackTrace();
                    hideprogress();
                }


            }
        });
    }

    public static void chqValue(){

        if (StopList_Adapter.from ==-1 ) {

            changeButtonGray();

        } else if (StopList_Adapter.to ==-1 ) {

            changeButtonGray();

        } else {

            changeButtonGreen();
        }

    }
    public void showprogress() {
        pdialog = new ProgressDialog(mContext);
        pdialog.setCancelable(false);
        pdialog.setCanceledOnTouchOutside(false);
        pdialog.setMessage("Loading...");
        pdialog.show();
    }

    public static void hideprogress() {
        if (pdialog != null) {
            pdialog.dismiss();
        }
    }
    public static  void changeButtonGreen()
    {
        liner_select.setBackgroundResource(R.color.textGreen);
    }
    public static  void changeButtonGray()
    {
        liner_select.setBackgroundResource(R.color.membership_bg_uper);
    }
}
