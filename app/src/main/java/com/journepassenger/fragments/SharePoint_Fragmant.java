package com.journepassenger.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.journepassenger.activity.DashBoardActivity;
import com.journepassenger.utilities.GlobalValues;
import com.journepassenger.utilities.MethodManager_Listner;
import com.journepassenger.utilities.Session_manager;
import com.journyapp.journyapp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by administrator on 29/3/16.
 */
public class SharePoint_Fragmant extends Fragment {
    public static String tag = "Share_Points";
    public static Fragment fragment;
    static FragmentTransaction ft;
    static public FragmentManager fragmentmanger;
    static public Context mContext;
    View view;
    String p_id;
    TextView tvPoints;
    EditText edEmailIdorMail;
    EditText edSharePoints;
    EditText edOptional;
    TextView btnSendPints;
    static String total_points;
    public static Fragment getInstance(Context ct, FragmentManager fm, Bundle b) {
        {
            mContext = ct;
            fragmentmanger = fm;
            fragment = new SharePoint_Fragmant();
            total_points=b.getString("total_points");
            return fragment;
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view=inflater.inflate(R.layout.share_pointxml, container, false);
        tvPoints=(TextView)view.findViewById(R.id.tvPoints);
        edEmailIdorMail=(EditText)view.findViewById(R.id.edEmailIdorMail);
        edSharePoints=(EditText)view.findViewById(R.id.edSharePoints);
        edOptional=(EditText)view.findViewById(R.id.edOptional);
        btnSendPints=(TextView)view.findViewById(R.id.btnSendPints);
        mContext=getActivity();
        p_id = Session_manager.getPassengerid(mContext);
        DashBoardActivity.hideSearchButton(mContext);
        DashBoardActivity.hideLoacationButton(mContext);
        DashBoardActivity.hideafteredit(mContext);
        DashBoardActivity.hidebeforeedit(mContext);
        DashBoardActivity.hideMenuDrawerButton(mContext);
        DashBoardActivity.showBackButton(mContext);
        DashBoardActivity.updatedTitle("Share Points");
        DashBoardActivity.hidecityname_header(mContext);
        DashBoardActivity.hidePrintIcon(mContext);
        DashBoardActivity.showhomeButton(mContext);
        tvPoints.setText("" + total_points);
        btnSendPints.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (edEmailIdorMail.getText().toString().equals("")) {
                    Toast.makeText(mContext, "Please Fill Phone no or email", Toast.LENGTH_LONG);
                } else if (edSharePoints.getText().toString().equals("")) {
                    Toast.makeText(mContext, "Points Can not be blank", Toast.LENGTH_LONG);
                } else {
                    SendSharePoint();
                }
            }
        });

        return view;
    }

    public void SendSharePoint()
    {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("passenger_id",p_id);
        params.put("friend_detail",edEmailIdorMail.getText().toString());
        params.put("points",""+edSharePoints.getText().toString());

        GlobalValues.getMethodManagerObj(mContext).SharePoint(params, new MethodManager_Listner() {
            @Override
            public void onError() {

            }

            @Override
            public void onSuccess(String json) {
                try {
                    JSONObject jobject = new JSONObject(json);
                    String message = jobject.getString("msg");
                    if (message.equals("Points shared successfully")) {
                        Toast.makeText(mContext, "" + message, Toast.LENGTH_LONG).show();
                        DashBoardActivity.displayview(DashBoardActivity.home, null);
                    } else {
                        Toast.makeText(mContext, "" + message, Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }

        });
    }
}