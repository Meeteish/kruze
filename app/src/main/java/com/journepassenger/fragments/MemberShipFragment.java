package com.journepassenger.fragments;

import android.accounts.Account;
import android.accounts.AccountManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import com.journepassenger.activity.DashBoardActivity;
import com.journepassenger.activity.SDDir;
import com.journepassenger.utilities.CircleTransform;
import com.journepassenger.utilities.Session_manager;
import com.journyapp.journyapp.R;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.FileOutputStream;

/**
 * Created by administrator on 06-Feb-16.
 */
public class MemberShipFragment extends Fragment
{
    public static Fragment fragment;
    static FragmentTransaction ft;
    static public FragmentManager fragmentmanger;
    static public Context mContext;
    View view;
    TextView tvnameid;
    TextView tvemail,tvcompany;
    String name, email, passenger_img, qr_img,Passengercompany;
    ImageView imgprofileicon, imgqrcode;
    ProgressBar progress;
    static String useremail;
    public static Fragment getInstance(Context ct, FragmentManager fm)
    {
        mContext=ct;
        fragmentmanger = fm;

        fragment=new MemberShipFragment();

        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view=inflater.inflate(R.layout.membership_identification,null,false);
        tvnameid=(TextView)view.findViewById(R.id.tvnameid);
        tvemail=(TextView)view.findViewById(R.id.tvemail);
        imgprofileicon= (ImageView)view.findViewById(R.id.imgprofileicon);
        imgqrcode= (ImageView)view.findViewById(R.id.imgqrcode);
        progress=(ProgressBar)view.findViewById(R.id.progress);
        tvcompany=(TextView)view.findViewById(R.id.tvcompany);
         mContext=getActivity();

        DashBoardActivity.hideSearchButton(mContext);
        DashBoardActivity.hideLoacationButton(mContext);
        DashBoardActivity.hideafteredit(mContext);
        DashBoardActivity.hidebeforeedit(mContext);
        DashBoardActivity.showMenuDrawerButton(mContext);
        DashBoardActivity.hideBackButton(mContext);
        DashBoardActivity.updatedTitle("Membership Identification");
        DashBoardActivity.hidecityname_header(mContext);
        DashBoardActivity.showPrintIcon(mContext);
        DashBoardActivity.hidehomeButton(mContext);
        name= Session_manager.getPassengername(mContext);
        email=Session_manager.getPassengeremail(mContext);
        passenger_img=Session_manager.getPassengerimg(mContext);
        qr_img=Session_manager.getqrimg(mContext);
        if(Passengercompany==null)
        {
            Passengercompany=Session_manager.getPassengercompany(mContext);
        }
        

        tvnameid.setText(name);
        tvemail.setText(email);
        tvcompany.setText(Passengercompany);
        Picasso.with(mContext).load(passenger_img).placeholder(R.drawable.profileicon).transform(new CircleTransform()).into(imgprofileicon);
        Picasso.with(mContext).load(qr_img).into(imgqrcode, new Callback() {
            @Override
            public void onSuccess() {
                progress.setVisibility(View.GONE);
            }

            @Override
            public void onError() {

            }
        });
             DashBoardActivity.print_icon.setOnClickListener(new View.OnClickListener() {
                 @Override
                 public void onClick(View v) {
                     takeScreenshot();
                 }
             });
      /*  File myDir=new File("/sdcard/QrCode_images");
        myDir.mkdirs();

        String str=""+System.currentTimeMillis();
        String fname = "Image_"+ str +".pdf";
        File file = new File (myDir, fname);

        Document document = new Document();

        try {
            FileOutputStream fos = new FileOutputStream(file);
            PdfWriter writer = PdfWriter.getInstance(document, fos);
            writer.open();
            document.open();
            document.add(Image.getInstance(new java.net.URL(qr_img)));
            document.close();
            writer.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }*/






        /*new AQuery(mContext).download(qr_img,file, new AjaxCallback<File>(){

            public void callback(String url, File file, AjaxStatus status) {


            }
        });*/



        return view;
    }

    public void takeScreenshot() {
        View u = getActivity().findViewById(R.id.member_scroll);
        u.setDrawingCacheEnabled(true);
        ScrollView z = (ScrollView) getActivity().findViewById(R.id.member_scroll);
        int totalHeight = z.getChildAt(0).getHeight();
        int totalWidth = z.getChildAt(0).getWidth();

        u.layout(0, 0, totalWidth, totalHeight);
        u.buildDrawingCache(true);
        Bitmap b = Bitmap.createBitmap(u.getDrawingCache());
        u.setDrawingCacheEnabled(false);

        //Save bitmap
        try {
            String path = System.currentTimeMillis() + "-downloadImage" + ".jpg";
            File dirPath = new File(path);
            String fileName = dirPath.getName();
            File rootDir = SDDir.createSDCardDir();
            File newPath = SDDir.createDownloadSubDir(mContext, rootDir);
            File file = new File(newPath, fileName);
            FileOutputStream out = new FileOutputStream(file);
            b.compress(Bitmap.CompressFormat.JPEG, 100, out);
            out.flush();
            out.close();
            startEmailActivity(mContext, Uri.fromFile(file));
        } catch (Exception e) {
            Log.e("TAG", "" + e.toString());
        }
    }
    static String getEmail(Context context) {
        AccountManager accountManager = AccountManager.get(context);
        Account account = getAccount(accountManager);

        if (account == null) {
            return null;
        } else {
            return account.name;
        }
    }

    private static Account getAccount(AccountManager accountManager) {
        Account[] accounts = accountManager.getAccountsByType("com.google");
        Account account;
        if (accounts.length > 0) {
            account = accounts[0];
        } else {
            account = null;
        }
        return account;
    }
    public static void startEmailActivity(Context context, Uri attachment) {
        Intent sendEmailIntent = new Intent(Intent.ACTION_SEND);
        sendEmailIntent.setType("message/rfc822");
        useremail=getEmail(mContext);
        sendEmailIntent.putExtra(Intent.EXTRA_EMAIL, new String[]{useremail});
        sendEmailIntent.putExtra(Intent.EXTRA_SUBJECT, "MemberShip Image");
        sendEmailIntent.putExtra(Intent.EXTRA_STREAM, attachment);
        context.startActivity(Intent.createChooser(sendEmailIntent, "Send email"));
    }
}
/*
new AQuery(mcontext).download(imagepath,target, new AjaxCallback<File>(){

public void callback(String url, File file, AjaxStatus status) {
*/
