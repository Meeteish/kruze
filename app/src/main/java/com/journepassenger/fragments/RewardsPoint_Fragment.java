package com.journepassenger.fragments;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.journedriver.data.UtilMethod;
import com.journepassenger.activity.DashBoardActivity;
import com.journepassenger.adapter.RewardPoint_detail_Adapter;
import com.journepassenger.bean.Rewards_bean;
import com.journepassenger.utilities.GlobalValues;
import com.journepassenger.utilities.MethodManager_Listner;
import com.journepassenger.utilities.Session_manager;
import com.journyapp.journyapp.R;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by administrator on 22/3/16.
 */
public class RewardsPoint_Fragment extends Fragment
{
    public static String tag="Rewards_Points";
    public static Fragment fragment;
    static FragmentTransaction ft;
    static public FragmentManager fragmentmanger;
    static public Context mContext;
    View view;
    ListView rewards_list;
    String p_id;
    ArrayList<Rewards_bean> rewards_beans_Array;
    TextView btn_reedempoints,btn_sharepoint;
    static String total_points;
    int i,result;
    String reedem_text;
    public static Fragment getInstance(Context ct, FragmentManager fm,Bundle b) {
        {
            mContext = ct;
            fragmentmanger = fm;
           fragment = new RewardsPoint_Fragment();
            total_points=b.getString("total_points");
            return fragment;
        }
    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        view=inflater.inflate(R.layout.rewardpoint_xml,null,false);
        rewards_list=(ListView)view.findViewById(R.id.rewards_list);
        btn_reedempoints=(TextView)view.findViewById(R.id.btn_reedempoints);
        btn_sharepoint=(TextView)view.findViewById(R.id.btn_sharepoint);
        mContext=getActivity();
        p_id = Session_manager.getPassengerid(mContext);
        DashBoardActivity.hideSearchButton(mContext);
        DashBoardActivity.hideLoacationButton(mContext);
        DashBoardActivity.hideafteredit(mContext);
        DashBoardActivity.hidebeforeedit(mContext);
        DashBoardActivity.hideMenuDrawerButton(mContext);
        DashBoardActivity.showBackButton(mContext);
        DashBoardActivity.updatedTitle("Rewards Points");
        DashBoardActivity.hidecityname_header(mContext);
        DashBoardActivity.hidePrintIcon(mContext);
        DashBoardActivity.showhomeButton(mContext);

        i = Integer.parseInt(total_points);
        result = i/1000;
        reedem_text="You Can Reddem Upto "+result+ " Rides Maximum. Enter Number Of Rides You Want To Redeem";
        //UtilMethod.showToast(""+result,mContext);
       btn_sharepoint.setOnClickListener(new View.OnClickListener() {
           @Override
           public void onClick(View v) {
               Bundle bundle = new Bundle();
               bundle.putString("total_points", total_points);
               DashBoardActivity.displayview(DashBoardActivity.Share_Points, bundle);
           }
       });
        btn_reedempoints.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                reedem_popup();
            }
        });
        GetRewardsData();
        return view;
    }
    public void GetRewardsData()
    {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("passenger_id", p_id);
        GlobalValues.getMethodManagerObj(mContext).GeTRewardspoint(params, new MethodManager_Listner() {
            @Override
            public void onError() {

            }

            @Override
            public void onSuccess(String json) {
                try {
                    JSONObject jsonObject = new JSONObject(json);
                    String status = jsonObject.getString("status");
                    if (status.equals("false")) {
                        Toast.makeText(mContext, jsonObject.getString("msg").toString(), Toast.LENGTH_LONG).show();
                        btn_sharepoint.setVisibility(View.INVISIBLE);
                        btn_reedempoints.setVisibility(View.INVISIBLE);

                    } else if (status.equals("true"))
                    {
                        btn_sharepoint.setVisibility(View.VISIBLE);
                        btn_reedempoints.setVisibility(View.VISIBLE);
                        JSONArray responseJsonArray = jsonObject.getJSONArray("reward_points");
                        rewards_beans_Array=new ArrayList<Rewards_bean>();
                        for (int i=0;i<responseJsonArray.length();i++)
                        {
                            Rewards_bean bean=new Rewards_bean();
                            String ep_points=responseJsonArray.getJSONObject(i).getString("ep_points");
                            bean.setEp_points(ep_points);
                            String ep_remark=responseJsonArray.getJSONObject(i).getString("ep_remark");
                            bean.setEp_remark(ep_remark);
                            String ep_date=responseJsonArray.getJSONObject(i).getString("ep_date");
                            bean.setEp_datetime(ep_date);
                            String ep_time=responseJsonArray.getJSONObject(i).getString("ep_time");
                            bean.setEp_time(ep_time);

                            rewards_beans_Array.add(bean);

                        }
                        RewardPoint_detail_Adapter adpter=new RewardPoint_detail_Adapter(mContext,rewards_beans_Array);
                        rewards_list.setAdapter(adpter);

                    }

                } catch (JSONException e) {
                    e.printStackTrace();

                }
            }
        });
    }
    public void reedem_popup()
    {
        final Dialog user_info = new Dialog(mContext);
        LayoutInflater li = (LayoutInflater) mContext.getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View vi = li.inflate(R.layout.popup_reedempoint, null, false);
        user_info.requestWindowFeature(Window.FEATURE_NO_TITLE);

        final TextView next_ride = (TextView) vi.findViewById(R.id.next_ride);
        final TextView monthly_pass = (TextView) vi.findViewById(R.id.monthly_pass);
        final TextView cancel = (TextView) vi.findViewById(R.id.cancel);
        //final EditText dealcode=(EditText)user_info.findViewById(R.id.u_dealcode);
        //dealcode.setVisibility(View.GONE);

        cancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                user_info.dismiss();
            }
        });
        next_ride.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getFreeRide();
                user_info.dismiss();
            }
        });

        user_info.setContentView(vi);
        user_info.setCanceledOnTouchOutside(false);
        user_info.show();

    }

    public void  Reddem_points(String rides)
    {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("rides", rides);
        params.put("passenger_id", p_id);

        GlobalValues.getMethodManagerObj(mContext).ReedemPoint(params, new MethodManager_Listner() {
            @Override
            public void onError() {

            }

            @Override
            public void onSuccess(String json1) {
                JSONObject json = null;
                try {
                    json = new JSONObject(json1);
                    String message = json.getString("msg");
                    String status = json.getString("status");
                    UtilMethod.showToast(message,mContext);

                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        });

    }
    public void getFreeRide()
    {
        final Dialog user_info = new Dialog(mContext);
        LayoutInflater li = (LayoutInflater) mContext.getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View vi = li.inflate(R.layout.popup_reedempoints, null, false);
        user_info.requestWindowFeature(Window.FEATURE_NO_TITLE);

        final TextView hedertext = (TextView) vi.findViewById(R.id.hedertext);
        final TextView btnreedem = (TextView) vi.findViewById(R.id.btnreedem);
        final TextView btnCancel = (TextView) vi.findViewById(R.id.btnCancel);
        final EditText edreddem_point=(EditText)vi.findViewById(R.id.edreddem_point);
         hedertext.setText(""+reedem_text);
        //final EditText dealcode=(EditText)user_info.findViewById(R.id.u_dealcode);
        //dealcode.setVisibility(View.GONE);

        btnCancel.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub
                user_info.dismiss();
            }
        });
        btnreedem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                String check_rides=edreddem_point.getText().toString();
                int j=Integer.parseInt(check_rides);
                    if(j<=result)
                {
                    Reddem_points(edreddem_point.getText().toString());
                    user_info.dismiss();
                }
                else
                {
                    UtilMethod.showToast("you have not suffciant point to get maximam rides",mContext);
                }

            }
        });

        user_info.setContentView(vi);
        user_info.setCanceledOnTouchOutside(false);
        user_info.show();

    }
}
/*
        i = Integer.parseInt(ed.getText().toString());
        result = i/1000;
        tv.setText(result+"");*/
