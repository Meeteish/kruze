package com.journepassenger.bean;

/**
 * Created by administrator on 3/2/16.
 */
public class Routes_bean
{
    String bus_route_id;

    String bus_route_title;

    public String getBus_route_id() {
        return bus_route_id;
    }

    public void setBus_route_id(String bus_route_id) {
        this.bus_route_id = bus_route_id;
    }

    public String getBus_route_title() {
        return bus_route_title;
    }

    public void setBus_route_title(String bus_route_title) {
        this.bus_route_title = bus_route_title;
    }
}
