package com.journepassenger.bean;

import java.io.Serializable;

/**
 * Created by administrator on 7/3/16.
 */
public class Mapdata_bean implements Serializable
{
    String log_id;
    String gps_device_id;
    double gps_lat;
    double gps_long;
    String gps_location;

    public String getLog_id() {
        return log_id;
    }

    public void setLog_id(String log_id) {
        this.log_id = log_id;
    }

    public String getGps_device_id() {
        return gps_device_id;
    }

    public void setGps_device_id(String gps_device_id) {
        this.gps_device_id = gps_device_id;
    }

    public double getGps_lat() {
        return gps_lat;
    }

    public void setGps_lat(double gps_lat) {
        this.gps_lat = gps_lat;
    }

    public double getGps_long() {
        return gps_long;
    }

    public void setGps_long(double gps_long) {
        this.gps_long = gps_long;
    }

    public String getGps_location() {
        return gps_location;
    }

    public void setGps_location(String gps_location) {
        this.gps_location = gps_location;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    String datetime;

}
