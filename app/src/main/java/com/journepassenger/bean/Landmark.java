package com.journepassenger.bean;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Landmark implements Serializable {

	String landmark_id,landmark;

	public Landmark() {
		super();
	}

	public String getLandmark_id() {
		return landmark_id;
	}

	public void setLandmark_id(String landmark_id) {
		this.landmark_id = landmark_id;
	}

	public String getLandmark() {
		return landmark;
	}

	public void setLandmark(String landmark) {
		this.landmark = landmark;
	}
}