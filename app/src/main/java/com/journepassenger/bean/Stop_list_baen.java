package com.journepassenger.bean;

/**
 * Created by administrator on 3/2/16.
 */
public class Stop_list_baen
{
    String t_stop_id;
    String stop_name;

    public String getT_stop_id() {
        return t_stop_id;
    }

    public void setT_stop_id(String t_stop_id) {
        this.t_stop_id = t_stop_id;
    }

    public String getStop_name() {
        return stop_name;
    }

    public void setStop_name(String stop_name) {
        this.stop_name = stop_name;
    }
}
