package com.journepassenger.bean;

/**
 * Created by administrator on 5/2/16.
 */
public class FromDetailsBean
{
    String Bus_stop_name;
    String T_stop_time;
    String t_stop;

    public String getT_stop() {
        return t_stop;
    }

    public void setT_stop(String t_stop) {
        this.t_stop = t_stop;
    }



    public String getBus_stop_name() {
        return Bus_stop_name;
    }

    public void setBus_stop_name(String bus_stop_name) {
        Bus_stop_name = bus_stop_name;
    }

    public String getT_stop_time() {
        return T_stop_time;
    }

    public void setT_stop_time(String t_stop_time) {
        T_stop_time = t_stop_time;
    }
}
