package com.journepassenger.bean;

import java.util.ArrayList;

/**
 * Created by administrator on 3/2/16.
 */
public class Trip_bean
{
    String trip_id;
    String trip_start_time;

    public String getTrip_id() {
        return trip_id;
    }

    public void setTrip_id(String trip_id) {
        this.trip_id = trip_id;
    }

    public String getTrip_start_time() {
        return trip_start_time;
    }

    public void setTrip_start_time(String trip_start_time) {
        this.trip_start_time = trip_start_time;
    }

    public String getTrip_end_time() {
        return trip_end_time;
    }

    public void setTrip_end_time(String trip_end_time) {
        this.trip_end_time = trip_end_time;
    }

    public ArrayList<Stop_list_baen> getStoplist() {
        return stoplist;
    }

    public void setStoplist(ArrayList<Stop_list_baen> stoplist) {
        this.stoplist = stoplist;
    }

    String trip_end_time;

    ArrayList<Stop_list_baen> stoplist;
}
