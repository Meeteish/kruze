package com.journepassenger.bean;

/**
 * Created by administrator on 5/2/16.
 */
public class ToDetailsBean
{
    String bus_stop_name;
    String t_stop_time;
    String  t_stop;

    public String getBus_stop_name() {
        return bus_stop_name;
    }

    public void setBus_stop_name(String bus_stop_name) {
        this.bus_stop_name = bus_stop_name;
    }

    public String getT_stop_time() {
        return t_stop_time;
    }

    public void setT_stop_time(String t_stop_time) {
        this.t_stop_time = t_stop_time;
    }

    public String getT_stop() {
        return t_stop;
    }

    public void setT_stop(String t_stop) {
        this.t_stop = t_stop;
    }
}
