package com.journepassenger.bean;

import java.io.Serializable;

/**
 * Created by administrator on 17/3/16.
 */
public class Notification_bean implements Serializable
{
      String noti_message;
      String noti_datetime;
      String noti_id;
      String noti_type;

    public String getNoti_id() {
        return noti_id;
    }

    public void setNoti_id(String noti_id) {
        this.noti_id = noti_id;
    }

    public String getNoti_type() {
        return noti_type;
    }

    public void setNoti_type(String noti_type) {
        this.noti_type = noti_type;
    }

    public String getNoti_message() {
        return noti_message;
    }

    public void setNoti_message(String noti_message) {
        this.noti_message = noti_message;
    }

    public String getNoti_datetime() {
        return noti_datetime;
    }

    public void setNoti_datetime(String noti_datetime) {
        this.noti_datetime = noti_datetime;
    }
}
