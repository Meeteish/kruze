package com.journepassenger.bean;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class RoutesBean implements Serializable {

	String bus_route_id, bus_operator_id, bus_city_id, bus_route_title, bus_route_from, bus_route_from_title,bus_route_to,bus_route_to_title,
			bus_route_start_lat, bus_route_start_long, bus_route_end_lat, bus_route_end_long, bus_route_status;
	//TripBean tripBean;
	ArrayList<Landmark> landmark_array;

	public RoutesBean() {
		super();
	}

	public String getBus_route_id() {
		return bus_route_id;
	}

	public void setBus_route_id(String bus_route_id) {
		this.bus_route_id = bus_route_id;
	}

	public String getBus_operator_id() {
		return bus_operator_id;
	}

	public void setBus_operator_id(String bus_operator_id) {
		this.bus_operator_id = bus_operator_id;
	}

	public String getBus_city_id() {
		return bus_city_id;
	}

	public void setBus_city_id(String bus_city_id) {
		this.bus_city_id = bus_city_id;
	}

	public String getBus_route_title() {
		return bus_route_title;
	}

	public void setBus_route_title(String bus_route_title) {
		this.bus_route_title = bus_route_title;
	}

	public String getBus_route_from() {
		return bus_route_from;
	}

	public void setBus_route_from(String bus_route_from) {
		this.bus_route_from = bus_route_from;
	}

	public String getBus_route_to() {
		return bus_route_to;
	}

	public void setBus_route_to(String bus_route_to) {
		this.bus_route_to = bus_route_to;
	}

	public String getBus_route_start_lat() {
		return bus_route_start_lat;
	}

	public void setBus_route_start_lat(String bus_route_start_lat) {
		this.bus_route_start_lat = bus_route_start_lat;
	}

	public String getBus_route_start_long() {
		return bus_route_start_long;
	}

	public void setBus_route_start_long(String bus_route_start_long) {
		this.bus_route_start_long = bus_route_start_long;
	}

	public String getBus_route_end_lat() {
		return bus_route_end_lat;
	}

	public void setBus_route_end_lat(String bus_route_end_lat) {
		this.bus_route_end_lat = bus_route_end_lat;
	}

	public String getBus_route_end_long() {
		return bus_route_end_long;
	}

	public void setBus_route_end_long(String bus_route_end_long) {
		this.bus_route_end_long = bus_route_end_long;
	}

	public String getBus_route_status() {
		return bus_route_status;
	}

	public void setBus_route_status(String bus_route_status) {
		this.bus_route_status = bus_route_status;
	}

	public ArrayList<Landmark> getLandmark_array() {
		return landmark_array;
	}

	public void setLandmark_array(ArrayList<Landmark> landmark_array) {
		this.landmark_array = landmark_array;
	}

	public String getBus_route_from_title() {
		return bus_route_from_title;
	}

	public void setBus_route_from_title(String bus_route_from_title) {
		this.bus_route_from_title = bus_route_from_title;
	}

	public String getBus_route_to_title() {
		return bus_route_to_title;
	}

	public void setBus_route_to_title(String bus_route_to_title) {
		this.bus_route_to_title = bus_route_to_title;
	}
	/*public TripBean getTripBean() {
		return tripBean;
	}

	public void setTripBean(TripBean tripBean) {
		this.tripBean = tripBean;
	}*/


}