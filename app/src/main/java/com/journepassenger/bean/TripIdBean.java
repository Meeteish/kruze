package com.journepassenger.bean;

/**
 * Created by administrator on 5/2/16.
 */
public class TripIdBean
{
    String trip_id;
    String bus_no;

    String available_seats;
    String total_seats;
    String price;

    String ac_type;

    String via;
    FromDetailsBean fromDetailsBean;
    ToDetailsBean toDetailsBean;

    public String getTrip_id() {
        return trip_id;
    }

    public void setTrip_id(String trip_id) {
        this.trip_id = trip_id;
    }

    public FromDetailsBean getFromDetailsBean() {
        return fromDetailsBean;
    }

    public void setFromDetailsBean(FromDetailsBean fromDetailsBean) {
        this.fromDetailsBean = fromDetailsBean;
    }

    public ToDetailsBean getToDetailsBean() {
        return toDetailsBean;
    }

    public void setToDetailsBean(ToDetailsBean toDetailsBean) {
        this.toDetailsBean = toDetailsBean;
    }
    public String getBus_no() {
        return bus_no;
    }

    public void setBus_no(String bus_no) {
        this.bus_no = bus_no;
    }

    public String getAvailable_seats() {
        return available_seats;
    }

    public void setAvailable_seats(String available_seats) {
        this.available_seats = available_seats;
    }

    public String getTotal_seats() {
        return total_seats;
    }

    public String getAc_type() {
        return ac_type;
    }

    public void setAc_type(String ac_type) {
        this.ac_type = ac_type;
    }

    public void setTotal_seats(String total_seats) {
        this.total_seats = total_seats;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getVia() {
        return via;
    }

    public void setVia(String via) {
        this.via = via;
    }
}
