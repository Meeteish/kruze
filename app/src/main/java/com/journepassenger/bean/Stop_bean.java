package com.journepassenger.bean;

import java.io.Serializable;

/**
 * Created by administrator on 16/2/16.
 */
public class Stop_bean implements Serializable
{
    String t_stop_id;
    String t_stop_trip_id;
    String t_stop;
    String t_stop_time;
    String  t_stop_status;
    String  bus_stop_name;
    String reach_status;
    public String getT_stop_id() {
        return t_stop_id;
    }

    public void setT_stop_id(String t_stop_id) {
        this.t_stop_id = t_stop_id;
    }

    public String getT_stop_trip_id() {
        return t_stop_trip_id;
    }

    public void setT_stop_trip_id(String t_stop_trip_id) {
        this.t_stop_trip_id = t_stop_trip_id;
    }

    public String getT_stop() {
        return t_stop;
    }

    public void setT_stop(String t_stop) {
        this.t_stop = t_stop;
    }

    public String getT_stop_time() {
        return t_stop_time;
    }

    public void setT_stop_time(String t_stop_time) {
        this.t_stop_time = t_stop_time;
    }

    public String getT_stop_status() {
        return t_stop_status;
    }

    public void setT_stop_status(String t_stop_status) {
        this.t_stop_status = t_stop_status;
    }

    public String getBus_stop_name() {
        return bus_stop_name;
    }

    public String getReach_status() {
        return reach_status;
    }

    public void setReach_status(String reach_status) {
        this.reach_status = reach_status;
    }

    public void setBus_stop_name(String bus_stop_name) {
        this.bus_stop_name = bus_stop_name;
    }
}
