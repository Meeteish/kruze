package com.journepassenger.bean;

import java.io.Serializable;
import java.util.ArrayList;

/**
 * Created by administrator on 8/2/16.
 */
public class SeatRows_bean implements Serializable
{
    String row;
    ArrayList<Seatno_bean> seatno_array;

    public String getRow() {
        return row;
    }

    public void setRow(String row) {
        this.row = row;
    }

    public ArrayList<Seatno_bean> getSeatno_array() {
        return seatno_array;
    }

    public void setSeatno_array(ArrayList<Seatno_bean> seatno_array) {
        this.seatno_array = seatno_array;
    }
}
