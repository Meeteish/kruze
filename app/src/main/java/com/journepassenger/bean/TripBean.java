package com.journepassenger.bean;

import java.io.Serializable;
import java.util.ArrayList;

@SuppressWarnings("serial")
public class TripBean implements Serializable {

	String trip_id, trip_route_id, trip_start_time, trip_end_time, trip_bus_id, trip_operator_id, trip_create_datetime,
			trip_status;
	
	//ArrayList<StopListBean> stop_list;

	public TripBean() {
		super();
	}

	public String getTrip_id() {
		return trip_id;
	}

	public void setTrip_id(String trip_id) {
		this.trip_id = trip_id;
	}

	public String getTrip_route_id() {
		return trip_route_id;
	}

	public void setTrip_route_id(String trip_route_id) {
		this.trip_route_id = trip_route_id;
	}

	public String getTrip_start_time() {
		return trip_start_time;
	}

	public void setTrip_start_time(String trip_start_time) {
		this.trip_start_time = trip_start_time;
	}

	public String getTrip_end_time() {
		return trip_end_time;
	}

	public void setTrip_end_time(String trip_end_time) {
		this.trip_end_time = trip_end_time;
	}

	public String getTrip_bus_id() {
		return trip_bus_id;
	}

	public void setTrip_bus_id(String trip_bus_id) {
		this.trip_bus_id = trip_bus_id;
	}

	public String getTrip_operator_id() {
		return trip_operator_id;
	}

	public void setTrip_operator_id(String trip_operator_id) {
		this.trip_operator_id = trip_operator_id;
	}

	public String getTrip_create_datetime() {
		return trip_create_datetime;
	}

	public void setTrip_create_datetime(String trip_create_datetime) {
		this.trip_create_datetime = trip_create_datetime;
	}

	public String getTrip_status() {
		return trip_status;
	}

	public void setTrip_status(String trip_status) {
		this.trip_status = trip_status;
	}

	/*public ArrayList<StopListBean> getStop_list() {
		return stop_list;
	}

	public void setStop_list(ArrayList<StopListBean> stop_list) {
		this.stop_list = stop_list;
	}*/

}