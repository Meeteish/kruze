package com.journepassenger.bean;

import java.io.Serializable;

/**
 * Created by administrator on 23/2/16.
 */
public class Seatno_bean implements Serializable
{
    String seat_no;

    public String getSeat_no() {
        return seat_no;
    }

    public void setSeat_no(String seat_no) {
        this.seat_no = seat_no;
    }
}
