package com.journepassenger.bean;

import java.io.Serializable;

/**
 * Created by administrator on 22/3/16.
 */
public class Rewards_bean implements Serializable
{
        String ep_id;
        String ep_points;
        String ep_passenger_id;
        String ep_type;
        String ep_remark;
        String ep_datetime;
        String ep_date;
        String ep_time;

    public String getEp_id() {
        return ep_id;
    }

    public void setEp_id(String ep_id) {
        this.ep_id = ep_id;
    }

    public String getEp_points() {
        return ep_points;
    }

    public void setEp_points(String ep_points) {
        this.ep_points = ep_points;
    }

    public String getEp_passenger_id() {
        return ep_passenger_id;
    }

    public void setEp_passenger_id(String ep_passenger_id) {
        this.ep_passenger_id = ep_passenger_id;
    }

    public String getEp_type() {
        return ep_type;
    }

    public void setEp_type(String ep_type) {
        this.ep_type = ep_type;
    }

    public String getEp_remark() {
        return ep_remark;
    }

    public void setEp_remark(String ep_remark) {
        this.ep_remark = ep_remark;
    }

    public String getEp_datetime() {
        return ep_datetime;
    }

    public void setEp_datetime(String ep_datetime) {
        this.ep_datetime = ep_datetime;
    }

    public String getEp_date() {
        return ep_date;
    }

    public void setEp_date(String ep_date) {
        this.ep_date = ep_date;
    }

    public String getEp_time() {
        return ep_time;
    }

    public void setEp_time(String ep_time) {
        this.ep_time = ep_time;
    }
}
