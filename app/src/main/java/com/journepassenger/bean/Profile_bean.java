package com.journepassenger.bean;

import java.io.Serializable;

/**
 * Created by administrator on 06-Feb-16.
 */
public class Profile_bean implements Serializable
{
            String passenger_name;
            String passenger_mobile;
            String passenger_email;
            String passenger_gender;
            String passenger_hose_loac;
            String passenger_work_loac;
            String company_name;
            String passenger_dob;

    public String getPassenger_name() {
        return passenger_name;
    }

    public void setPassenger_name(String passenger_name) {
        this.passenger_name = passenger_name;
    }

    public String getPassenger_mobile() {
        return passenger_mobile;
    }

    public void setPassenger_mobile(String passenger_mobile) {
        this.passenger_mobile = passenger_mobile;
    }

    public String getPassenger_email() {
        return passenger_email;
    }

    public void setPassenger_email(String passenger_email) {
        this.passenger_email = passenger_email;
    }

    public String getPassenger_hose_loac() {
        return passenger_hose_loac;
    }

    public void setPassenger_hose_loac(String passenger_hose_loac) {
        this.passenger_hose_loac = passenger_hose_loac;
    }

    public String getPassenger_gender() {
        return passenger_gender;
    }

    public void setPassenger_gender(String passenger_gender) {
        this.passenger_gender = passenger_gender;
    }

    public String getPassenger_work_loac() {
        return passenger_work_loac;
    }

    public void setPassenger_work_loac(String passenger_work_loac) {
        this.passenger_work_loac = passenger_work_loac;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getPassenger_dob() {
        return passenger_dob;
    }

    public void setPassenger_dob(String passenger_dob) {
        this.passenger_dob = passenger_dob;
    }

    public String getPassenger_emergency_num() {
        return passenger_emergency_num;
    }

    public void setPassenger_emergency_num(String passenger_emergency_num) {
        this.passenger_emergency_num = passenger_emergency_num;
    }

    public String getPassenger_image() {
        return passenger_image;
    }

    public void setPassenger_image(String passenger_image) {
        this.passenger_image = passenger_image;
    }

    public String getPassenger_referral_code() {
        return passenger_referral_code;
    }

    public void setPassenger_referral_code(String passenger_referral_code) {
        this.passenger_referral_code = passenger_referral_code;
    }

    String passenger_emergency_num;
            String passenger_referral_code;
            String passenger_image;
}
