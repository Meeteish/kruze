package com.journepassenger.bean;

import java.io.Serializable;

/**
 * Created by administrator on 11/2/16.
 */
public class LandMark_bean implements Serializable
{
    String landmark;
    String landmark_id;

    public String getLandmark() {
        return landmark;
    }

    public void setLandmark(String landmark) {
        this.landmark = landmark;
    }

    public String getLandmark_id() {
        return landmark_id;
    }

    public void setLandmark_id(String landmark_id) {
        this.landmark_id = landmark_id;
    }
}
