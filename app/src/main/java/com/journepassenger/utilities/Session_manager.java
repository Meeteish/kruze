package com.journepassenger.utilities;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by administrator on 27/1/16.
 */
public class Session_manager
{
    public static void saveDeviceToen(Context mContext,String token){
        SharedPreferences pref = mContext.getSharedPreferences("DeviceToken", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("device_token",token);
        editor.commit();
    }

    public  static  String getdeviceToken(Context mContext)
    {
        SharedPreferences pref = mContext.getSharedPreferences("DeviceToken", Context.MODE_PRIVATE);
        String D_token=pref.getString("device_token", null);
        return D_token;
    }

    public  static void saveloginData(Context mContext,String email,String status)
    {
        SharedPreferences pref=mContext.getSharedPreferences("login_data", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit=pref.edit();
        edit.putString("email",email);
        edit.putString("status",status);
        edit.commit();
    }
    public static String getemail_pref(Context mContext)
    {
        SharedPreferences pref=mContext.getSharedPreferences("login_data", Context.MODE_PRIVATE);
        return pref.getString("email",null);
    }
    public static String getstatus(Context mContext)
    {
        SharedPreferences pref=mContext.getSharedPreferences("login_data", Context.MODE_PRIVATE);
        return pref.getString("status",null);
    }
    public static void ClearPrefrence(Context mContext)
    {
        SharedPreferences pref=mContext.getSharedPreferences("login_data", Context.MODE_PRIVATE);
        SharedPreferences prefstatus=mContext.getSharedPreferences("LOGINSTATUS", Context.MODE_PRIVATE);
        pref.edit().clear().commit();
        prefstatus.edit().clear().commit();
    }

    public  static void saveUserInfo(Context mContext,String p_name, String p_mail, String p_id, String p_imgurl, String qr_url,String passenger_referral_code,String passenger_mobile,String comapny_name,String passenger_image_mode)
    {
        SharedPreferences pref=mContext.getSharedPreferences("UserInfo", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit=pref.edit();
        edit.putString("p_name",p_name);
        edit.putString("p_mail",p_mail);
        edit.putString("p_id",p_id);
        edit.putString("p_imgurl",p_imgurl);
        edit.putString("qr_url", qr_url);
        edit.putString("passenger_mobile",passenger_mobile);
        edit.putString("passenger_referral_code", passenger_referral_code);
        edit.putString("comapny_name",comapny_name);
        edit.putString("passenger_image_mode", passenger_image_mode);
        edit.commit();
    }

    public static String getPassengerImageType(Context mContext)
    {
        SharedPreferences pref=mContext.getSharedPreferences("UserInfo", Context.MODE_PRIVATE);
        return pref.getString("passenger_image_mode",null);
    }

    public static String getPassengername(Context mContext)
    {
        SharedPreferences pref=mContext.getSharedPreferences("UserInfo", Context.MODE_PRIVATE);
        return pref.getString("p_name",null);
    }
    public static String getPassengeremail(Context mContext)
    {
        SharedPreferences pref=mContext.getSharedPreferences("UserInfo", Context.MODE_PRIVATE);
        return pref.getString("p_mail",null);
    }
    public static String getPassengerid(Context mContext)
    {
        SharedPreferences pref=mContext.getSharedPreferences("UserInfo", Context.MODE_PRIVATE);
        return pref.getString("p_id",null);
    }
    public static String getPassenget_refercode(Context mContext)
    {
        SharedPreferences pref=mContext.getSharedPreferences("UserInfo", Context.MODE_PRIVATE);
        return pref.getString("passenger_referral_code",null);
    }
    public static String getPassengerimg(Context mContext)
    {
        SharedPreferences pref=mContext.getSharedPreferences("UserInfo", Context.MODE_PRIVATE);
        return pref.getString("p_imgurl",null);
    }
    public static String getqrimg(Context mContext)
    {
        SharedPreferences pref=mContext.getSharedPreferences("UserInfo", Context.MODE_PRIVATE);
        return pref.getString("qr_url",null);
    }
    public static String getPassengermob(Context mContext)
    {
        SharedPreferences pref=mContext.getSharedPreferences("UserInfo", Context.MODE_PRIVATE);
        return pref.getString("passenger_mobile",null);
    }
    public static String getPassengercompany(Context mContext)
{
    SharedPreferences pref=mContext.getSharedPreferences("UserInfo", Context.MODE_PRIVATE);
    return pref.getString("comapny_name",null);
}
    public static void setfromcity(Context mContext,String city)
    {
        SharedPreferences pref=mContext.getSharedPreferences("ClickedFROMPrefrence", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit=pref.edit();

        edit.putString("city",city);
        edit.commit();

    }

    public static void setTocity(Context mContext,String city)
    {
        SharedPreferences pref=mContext.getSharedPreferences("ClickedToPrefrence", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit=pref.edit();

        edit.putString("city",city);
        edit.commit();

    }

    public static String getfromcity(Context mContext)
    {
        SharedPreferences pref=mContext.getSharedPreferences("ClickedFROMPrefrence", Context.MODE_PRIVATE);
        return pref.getString("city",null);
    }
    public static String gettocity(Context mContext)
    {
        SharedPreferences pref=mContext.getSharedPreferences("ClickedToPrefrence", Context.MODE_PRIVATE);
        return pref.getString("city",null);
    }
    public static void ClearPrefrence_city(Context mContext)
    {
        SharedPreferences pref=mContext.getSharedPreferences("ClickedFROMPrefrence", Context.MODE_PRIVATE);


        pref.edit().clear().commit();
        //prefto.edit().clear().commit();
    }
    public static void ClearPrefrence_tocity(Context mContext)
    {
        SharedPreferences pref=mContext.getSharedPreferences("ClickedToPrefrence", Context.MODE_PRIVATE);


        pref.edit().clear().commit();
        //prefto.edit().clear().commit();
    }



    public static void setlogin_status(Context mContext,int status)
    {
        SharedPreferences pref=mContext.getSharedPreferences("LOGINSTATUS", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit=pref.edit();
        edit.putInt("status", status);
        edit.commit();
    }

    public static int getloginstatus(Context mContext)
    {
        SharedPreferences pref=mContext.getSharedPreferences("LOGINSTATUS", Context.MODE_PRIVATE);
        return pref.getInt("status", 0);
    }
    public static void setfromlandmark(Context mContext,String city,int position)
    {
        SharedPreferences pref=mContext.getSharedPreferences("fromLandmarkClick", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit=pref.edit();

        edit.putString("city",city);
        edit.putInt("position",position);
        edit.commit();

    }

    public static void settolandmark(Context mContext,String city,int position)
    {
        SharedPreferences pref=mContext.getSharedPreferences("toLandmarkClick", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit=pref.edit();

        edit.putString("city",city);
        edit.putInt("position",position);
        edit.commit();

    }
    public static int getfromclicked_pos (Context mContext)
    {
        SharedPreferences pref=mContext.getSharedPreferences("fromLandmarkClick", Context.MODE_PRIVATE);
        return pref.getInt("position",-1);
    }

    public static int gettoclicked_pos (Context mContext)
    {
        SharedPreferences pref=mContext.getSharedPreferences("toLandmarkClick", Context.MODE_PRIVATE);
        return pref.getInt("position",-1);
    }

    public static  void SetCityPrefrence(Context mContext,String cityname,String city_id)
    {
        SharedPreferences pref = mContext.getSharedPreferences("City_name", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = pref.edit();
        editor.putString("cityname",cityname);
        editor.putString("city_id", city_id);
        editor.commit();
    }

    public static String GetcityName(Context mContext)
    {
        SharedPreferences pref = mContext.getSharedPreferences("City_name", Context.MODE_PRIVATE);
        return  pref.getString("cityname", null);
    }
    public static String GetcityId(Context mContext)
    {
        SharedPreferences pref = mContext.getSharedPreferences("City_name", Context.MODE_PRIVATE);
        return  pref.getString("city_id", null);
    }
       public  static void clearpref_login(Context mContext)
       {
           SharedPreferences pref=mContext.getSharedPreferences("LOGINSTATUS", Context.MODE_PRIVATE);
           pref.edit().clear().commit();
       }

    public static void setfromlandmark(Context mContext,String landmark,String fromstop_id)
    {
        SharedPreferences pref=mContext.getSharedPreferences("FromLandMarkPrefrence", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit=pref.edit();

        edit.putString("landmark",landmark);
        edit.putString("fromstop_id",fromstop_id);

        edit.commit();

    }

    public static void setToLanmark(Context mContext,String landmark,String tostop_id)
    {
        SharedPreferences pref=mContext.getSharedPreferences("ToLandMarkPrefrence", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit=pref.edit();

        edit.putString("landmark",landmark);
        edit.putString("tostop_id",tostop_id);
        edit.commit();

    }

    public static String getfromlandmark(Context mContext)
    {
        SharedPreferences pref=mContext.getSharedPreferences("FromLandMarkPrefrence", Context.MODE_PRIVATE);
        return pref.getString("landmark",null);
    }
    public static String gettolandmark(Context mContext)
    {
        SharedPreferences pref=mContext.getSharedPreferences("ToLandMarkPrefrence", Context.MODE_PRIVATE);
        return pref.getString("landmark",null);
    }

    public static void setseattotal(Context mContext,String total_seat,String total_price)
    {
        SharedPreferences pref=mContext.getSharedPreferences("SeatInfo", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit=pref.edit();

        edit.putString("total_seat", total_seat);
        edit.putString("total_price", total_price);
        edit.commit();

    }

    public static String  getseat_total(Context mContext)
    {
        SharedPreferences pref=mContext.getSharedPreferences("SeatInfo", Context.MODE_PRIVATE);
        return pref.getString("total_seat",null);
    }

    public static String  getseat_totalprice(Context mContext)
    {
        SharedPreferences pref=mContext.getSharedPreferences("SeatInfo", Context.MODE_PRIVATE);
        return pref.getString("total_price",null);
    }

    public static void setReverceCity(Context mContext,String from_stop,String to_stop)
    {
        SharedPreferences pref=mContext.getSharedPreferences("Reverse_city", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit=pref.edit();

        edit.putString("from_stop", from_stop);
        edit.putString("to_stop", to_stop);
        edit.commit();

    }
    public static String  getfromcity_reverse(Context mContext)
    {
        SharedPreferences pref=mContext.getSharedPreferences("Reverse_city", Context.MODE_PRIVATE);
        return pref.getString("from_stop",null);
    }
    public static String  gettocity_reverse(Context mContext)
    {
        SharedPreferences pref=mContext.getSharedPreferences("Reverse_city", Context.MODE_PRIVATE);
        return pref.getString("to_stop",null);
    }
    public static void setbookingid_feedback(Context mContext,String bookingid)
    {
        SharedPreferences pref=mContext.getSharedPreferences("BOOKINGID_PREF", Context.MODE_PRIVATE);
        SharedPreferences.Editor edit=pref.edit();

        edit.putString("city",bookingid);
        edit.commit();

    }

    public static String getBookinigid_feedback(Context mContext)
    {
        SharedPreferences pref=mContext.getSharedPreferences("BOOKINGID_PREF", Context.MODE_PRIVATE);
        return pref.getString("bookingid",null);
    }
}
