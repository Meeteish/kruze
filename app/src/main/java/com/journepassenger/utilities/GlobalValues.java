package com.journepassenger.utilities;

import android.content.Context;

/**
 * Created by administrator on 25/1/16.
 */
public class GlobalValues
{
    private static MethodManager methodManager;

    public static MethodManager getMethodManagerObj(Context mContext) {
        if(methodManager!=null)
            return methodManager;
        else
            return new MethodManager(mContext);
    }
}
