package com.journepassenger.utilities;

/**
 * Created by administrator on 25/1/16.
 */
public class Webservices
{
    public static  String HOST_URL="http://54.67.85.189//journe/webservices/passenger/";

    public  static  String SIGNUP_URL=HOST_URL+"passenger_signup.php";

    public  static  String Varification=HOST_URL+"passenger_verification.php";

    public  static  String SIGNIN_URL=HOST_URL+"passenger_login.php";

    public  static  String Driver_SIGNIN_URL=HOST_URL+"passenger_login.php";

    public  static  String City_List=HOST_URL+"city_list.php";

    public  static  String Route_List=HOST_URL+"route_list.php";

    public  static  String bus_List=HOST_URL+"search_2.php";

    public  static  String user_profile=HOST_URL+"passenger_profile.php";

    public  static  String saveuser_profile=HOST_URL+"edit_profile_passenger.php";

    public static   String seats_details=HOST_URL+"get_trip_bus.php";

    public  static  String landmark_listing=HOST_URL+"landmark_list.php";

    public static   String forgot_Password=HOST_URL+"forgot_passowrd";

    public static   String contact_us=HOST_URL+"contact_us";

    public static   String stop_list=HOST_URL+"get_stops.php";

    public static String change_Password=HOST_URL+"change_password";

    public static String feedback_Passenger=HOST_URL+"passenger_feedback";

    public static String suggest_route=HOST_URL+"suggest_route.php";

    public static String bookCar=HOST_URL+"bookcar.php";

    public static String company_list="http://54.67.85.189//journe/webservices/driver/company_list.php";

    public static String booking_seat=HOST_URL+"book_seats.php";

    public static String currunt_booking=HOST_URL+"current_bookings.php";

    public static String cancel_booking=HOST_URL+"cancel_booking.php";

    public static String history_booking=HOST_URL+"booking_history.php";

    public static String contact_usdetails=HOST_URL+"company_details.php";

    public static String ACCEPT_TERMS_AND_CONDITIONS=HOST_URL+"privacy-container.html";

    public static String map_data=HOST_URL+"gps_location.php";

    public static String social_data=HOST_URL+"social_login.php";

    public static String social_password=HOST_URL+"check_password.php";

    public static String returnbus_list=HOST_URL+"return_trip";

    public static String getreferdata=HOST_URL+"rewards.php";

    public static String notificationdata=HOST_URL+"notification_list.php";

    public static String rewardshistory=HOST_URL+"rewards_history.php";

    public static String plane_reward=HOST_URL+"plane_booking.php";

    public static String share_point=HOST_URL+"share_points.php";

    public static String reedem_point=HOST_URL+"redeem_ride.php";

}
