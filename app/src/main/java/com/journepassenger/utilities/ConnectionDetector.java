package com.journepassenger.utilities;

/**
 * Created by administrator on 01-Mar-16.
 */
import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

public class ConnectionDetector {


    public static boolean isNetworkAvailable(Context context) {
        NetworkInfo localNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity"))
                .getActiveNetworkInfo();
        return (localNetworkInfo != null) && (localNetworkInfo.isConnected());
    }
}