package com.journepassenger.utilities;

import java.io.File;
import java.util.List;

import android.app.ProgressDialog;
import android.app.Service;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

public class UtilMethod {

	public static ProgressDialog showLoading(ProgressDialog progress, Context context) {
		try {
			if (progress == null) {
				progress = new ProgressDialog(context);
				progress.setMessage("Please wait..");
				progress.setCancelable(true);
			}
			progress.show();
		} catch (Exception e) {

			e.printStackTrace();
		}
		return progress;
	}

	public static void showServerError(Context ctx) {
		try {
			if (ctx != null) {
				String message = "server_error_message";
				Toast.makeText(ctx, message, Toast.LENGTH_LONG).show();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void showNetworkError(Context ctx) {
		try {
			if (ctx != null) {
				String message = "network_error_message";
				
				Toast.makeText(ctx, message, Toast.LENGTH_LONG).show();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void hideLoading(ProgressDialog progress) {
		try {
			if (progress != null) {

				if (progress.isShowing()) {
					progress.dismiss();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void showDownloading(ProgressBar progress) {
		if (progress != null) {
			progress.setVisibility(View.VISIBLE);
		}
	}

	public static void hideDownloading(ProgressBar progress) {
		if (progress != null) {
			progress.setVisibility(View.GONE);
		}
	}

	public static boolean isNetworkAvailable(Context context) {

		NetworkInfo localNetworkInfo = ((ConnectivityManager) context.getSystemService("connectivity"))
				.getActiveNetworkInfo();
		return (localNetworkInfo != null) && (localNetworkInfo.isConnected());
	}

	public static boolean isStringNullOrBlank(String str) {
		if (str == null) {
			return true;
		} else if (str.equals("null") || str.equals("")) {
			return true;
		}
		return false;
	}

	public static void showToast(String message, Context ctx) {
		try {
			if (ctx != null)
				Toast.makeText(ctx, message, Toast.LENGTH_LONG).show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}


	public static void printToLog(String message) {
		Log.i("Emper App Log ", message);
	}

	public static void hideKeyBoard(Context ct, EditText ed) {
		InputMethodManager imm = (InputMethodManager) ct.getSystemService(Service.INPUT_METHOD_SERVICE);
		imm.hideSoftInputFromWindow(ed.getWindowToken(), 0);
	}




	private static final String GooglePlayStorePackageNameOld = "com.google.market";
	private static final String GooglePlayStorePackageNameNew = "com.android.vending";

	public static boolean isGooglePlayInstalled(Context context) {
		boolean googlePlayStoreInstalled = false;
		PackageManager packageManager = context.getPackageManager();
		List<PackageInfo> packages = packageManager.getInstalledPackages(PackageManager.GET_UNINSTALLED_PACKAGES);
		for (PackageInfo packageInfo : packages) {
			if (packageInfo.packageName.equals(GooglePlayStorePackageNameOld)
					|| packageInfo.packageName.equals(GooglePlayStorePackageNameNew)) {
				googlePlayStoreInstalled = true;
				break;
			}
		}
		return googlePlayStoreInstalled;
	}


	public static String getDevicePath() {
		File directory = new File(Environment.getExternalStorageDirectory(), "Emper");
		if (!directory.exists()) {
			directory.mkdirs();
		}

		String path = Environment.getExternalStorageDirectory() + File.separator + "Emper" + File.separator;
		return path;
	}

}