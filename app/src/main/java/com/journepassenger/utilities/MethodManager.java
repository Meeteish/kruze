package com.journepassenger.utilities;

import android.content.Context;
import android.util.Log;
import android.widget.Toast;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;

import org.json.JSONObject;

import java.util.Map;

/**
 * Created by administrator on 25/1/16.
 */
public class MethodManager {
    private String TAG = "MethodManager";

    private Context mContext;

    private AQuery aquary;

    public MethodManager(Context ct) {
        aquary = new AQuery(ct);
        mContext = ct;

    }

    public void usersignup_data(Map<String, Object> params, final MethodManager_Listner listner) {
        aquary.ajax(Webservices.SIGNUP_URL, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                int code = status.getCode();

                if (code == AjaxStatus.NETWORK_ERROR) {
                    listner.onError();
                    return;
                } else if (code == AjaxStatus.TRANSFORM_ERROR) {
                    listner.onError();
                    return;
                } else if (code == 200) {

                }
                String result = object.toString();
                Log.e("result:", "" + result);
                if (result.equals("")) {
                    listner.onError();
                } else {
                    listner.onSuccess(result);

                }

            }
        });
    }

    public void usersignin(Map<String, Object> params, final MethodManager_Listner listner) {
        aquary.ajax(Webservices.SIGNIN_URL, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status)
            {
                super.callback(url, object, status);
                if(object==null)
                {
                    UtilMethod.showToast("Invalid User Name or Password",mContext);
                }
                int code = status.getCode();

                if (code == AjaxStatus.NETWORK_ERROR) {
                    listner.onError();
                    return;
                } else if (code == AjaxStatus.TRANSFORM_ERROR) {
                    listner.onError();
                    return;
                } else if (code == 200) {

                }
                String result = object.toString();
                Log.e("result:", "" + result);
                if (result.equals("")) {
                    listner.onError();
                } else {
                    listner.onSuccess(result);

                }

            }
        });
    }

    public void userProfileData(Map<String, Object> params, final MethodManager_Listner listner) {
        aquary.ajax(Webservices.user_profile, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                int code = status.getCode();

                if (code == AjaxStatus.NETWORK_ERROR) {
                    listner.onError();
                    return;
                } else if (code == AjaxStatus.TRANSFORM_ERROR) {
                    listner.onError();
                    return;
                } else if (code == 200) {

                }
                String result = object.toString();
                Log.e("result:", "" + result);
                if (result.equals("")) {
                    listner.onError();
                } else {
                    listner.onSuccess(result);

                }

            }
        });
    }



    public void userSaveProfileData(Map<String, Object> params, final MethodManager_Listner listner) {
        aquary.ajax(Webservices.saveuser_profile, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                int code = status.getCode();

                if (code == AjaxStatus.NETWORK_ERROR) {
                    listner.onError();
                    return;
                } else if (code == AjaxStatus.TRANSFORM_ERROR) {
                    listner.onError();
                    return;
                } else if (code == 200) {

                }
                String result = object.toString();
                Log.e("result:", "" + result);
                if (result.equals("")) {
                    listner.onError();
                } else {
                    listner.onSuccess(result);

                }

            }
        });
    }

    public void contactUsData(Map<String, Object> params, final MethodManager_Listner listner) {
        aquary.ajax(Webservices.contact_us, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                int code = status.getCode();

                if (code == AjaxStatus.NETWORK_ERROR) {
                    listner.onError();
                    return;
                } else if (code == AjaxStatus.TRANSFORM_ERROR) {
                    listner.onError();
                    return;
                } else if (code == 200) {

                }
                String result = object.toString();
                Log.e("result:", "" + result);
                if (result.equals("")) {
                    listner.onError();
                } else {
                    listner.onSuccess(result);

                }

            }
        });
    }

    public void driversignin(Map<String, Object> params, final MethodManager_Listner listner) {
        aquary.ajax("http://expertteam.in/journe/webservices/driver/driver_login.php", params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                int code = status.getCode();

                if (code == AjaxStatus.NETWORK_ERROR) {
                    listner.onError();
                    return;
                } else if (code == AjaxStatus.TRANSFORM_ERROR) {
                    listner.onError();
                    return;
                } else if (code == 200) {

                }
                String result = object.toString();
                Log.e("result:", "" + result);
                if (result.equals("")) {
                    listner.onError();
                } else {
                    listner.onSuccess(result);

                }

            }
        });
    }

    public void uservarification(Map<String, Object> params, final MethodManager_Listner listner) {
        aquary.ajax(Webservices.Varification, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                int code = status.getCode();

                if (code == AjaxStatus.NETWORK_ERROR) {
                    listner.onError();
                    return;
                } else if (code == AjaxStatus.TRANSFORM_ERROR) {
                    listner.onError();
                    return;
                } else if (code == 200) {

                }
                String result = object.toString();
                Log.e("result:", "" + result);
                if (result.equals("")) {
                    listner.onError();
                } else {
                    listner.onSuccess(result);

                }

            }
        });
    }

    public void Getcity_list(final MethodManager_Listner listner) {
        aquary.ajax(Webservices.City_List, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                int code = status.getCode();

                if (code == AjaxStatus.NETWORK_ERROR) {
                    listner.onError();
                    return;
                } else if (code == AjaxStatus.TRANSFORM_ERROR) {
                    listner.onError();
                    return;
                } else if (code == 200) {

                }
                String result = object.toString();
                Log.e("result:", "" + result);
                if (result.equals("")) {
                    listner.onError();
                } else {
                    listner.onSuccess(result);

                }

            }
        });
    }

    public void GetRoute_list(Map<String, Object> params, final MethodManager_Listner listner) {
        aquary.ajax(Webservices.Route_List, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                int code = status.getCode();

                if (code == AjaxStatus.NETWORK_ERROR)
                {
                    Toast.makeText(mContext,"No Network Found",Toast.LENGTH_LONG).show();
                    listner.onError();
                    return;
                } else if (code == AjaxStatus.TRANSFORM_ERROR) {
                    listner.onError();
                    return;
                } else if (code == 200) {

                }
                String result = object.toString();
                Log.e("result:", "" + result);
                if (result.equals("")) {
                    listner.onError();
                } else {
                    listner.onSuccess(result);

                }

            }
        });
    }


    public void UserSignup(Map<String, Object> params, final MethodManager_Listner listner)
    {
        aquary.ajax(Webservices.SIGNUP_URL,params,JSONObject.class,new AjaxCallback<JSONObject>()
        {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                int code = status.getCode();

                if (code == AjaxStatus.NETWORK_ERROR) {
                    listner.onError();
                    return;
                } else if (code == AjaxStatus.TRANSFORM_ERROR) {
                    listner.onError();
                    return;
                } else if (code == 200) {

                }
                String result = object.toString();
                Log.e("result:", "" + result);
                if (result.equals("")) {
                    listner.onError();
                } else {
                    listner.onSuccess(result);

                }

            }
        });
            }

    public void BusLists(Map<String, Object> params, final MethodManager_Listner listner)
    {
        aquary.ajax(Webservices.bus_List,params,JSONObject.class,new AjaxCallback<JSONObject>()
        {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                int code = status.getCode();

                if (code == AjaxStatus.NETWORK_ERROR) {
                    listner.onError();
                    return;
                } else if (code == AjaxStatus.TRANSFORM_ERROR) {
                    listner.onError();
                    return;
                } else if (code == 200) {

                }
                String result = object.toString();
                Log.e("result:", "" + result);
                if (result.equals("")) {
                    listner.onError();
                } else {
                    listner.onSuccess(result);

                }

            }
        });
    }
    public void GetseatsAvliability(Map<String, Object> params,final MethodManager_Listner listner) {
        aquary.ajax(Webservices.seats_details,params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                int code = status.getCode();

                if (code == AjaxStatus.NETWORK_ERROR) {
                    listner.onError();
                    return;
                } else if (code == AjaxStatus.TRANSFORM_ERROR) {
                    listner.onError();
                    return;
                } else if (code == 200) {

                }
                String result = object.toString();
                Log.e("result:", "" + result);
                if (result.equals("")) {
                    listner.onError();
                } else {
                    listner.onSuccess(result);

                }

            }
        });
    }
    public void GetLandmark_list(Map<String, Object> params, final MethodManager_Listner listner) {
        aquary.ajax(Webservices.landmark_listing, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                int code = status.getCode();

                if (code == AjaxStatus.NETWORK_ERROR) {
                    listner.onError();
                    return;
                } else if (code == AjaxStatus.TRANSFORM_ERROR) {
                    listner.onError();
                    return;
                } else if (code == 200) {

                }
                String result = object.toString();
                Log.e("result:", "" + result);
                if (result.equals("")) {
                    listner.onError();
                } else {
                    listner.onSuccess(result);

                }

            }
        });
    }

    public void Forgot_password(Map<String, Object> params, final MethodManager_Listner listner) {
        aquary.ajax(Webservices.forgot_Password, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                int code = status.getCode();

                if (code == AjaxStatus.NETWORK_ERROR) {
                    listner.onError();
                    return;
                } else if (code == AjaxStatus.TRANSFORM_ERROR) {
                    listner.onError();
                    return;
                } else if (code == 200) {

                }
                String result = object.toString();
                Log.e("result:", "" + result);
                if (result.equals("")) {
                    listner.onError();
                } else {
                    listner.onSuccess(result);

                }

            }
        });
    }

    public void GetStop_list(Map<String, Object> params, final MethodManager_Listner listner) {
        aquary.ajax(Webservices.stop_list, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                int code = status.getCode();

                if (code == AjaxStatus.NETWORK_ERROR) {
                    listner.onError();
                    return;
                } else if (code == AjaxStatus.TRANSFORM_ERROR) {
                    listner.onError();
                    return;
                } else if (code == 200) {

                }
                String result = object.toString();
                Log.e("result:", "" + result);
                if (result.equals("")) {
                    listner.onError();
                } else {
                    listner.onSuccess(result);

                }

            }
        });
    }
    public void change_password(Map<String, Object> params, final MethodManager_Listner listner) {
        aquary.ajax(Webservices.change_Password, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                int code = status.getCode();

                if (code == AjaxStatus.NETWORK_ERROR) {
                    listner.onError();
                    return;
                } else if (code == AjaxStatus.TRANSFORM_ERROR) {
                    listner.onError();
                    return;
                } else if (code == 200) {

                }
                String result = object.toString();
                Log.e("result:", "" + result);
                if (result.equals("")) {
                    listner.onError();
                } else {
                    listner.onSuccess(result);

                }

            }
        });
    }

    public void feedback_passenger(Map<String, Object> params, final MethodManager_Listner listner) {
        aquary.ajax(Webservices.feedback_Passenger, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                int code = status.getCode();

                if (code == AjaxStatus.NETWORK_ERROR) {
                    listner.onError();
                    return;
                } else if (code == AjaxStatus.TRANSFORM_ERROR) {
                    listner.onError();
                    return;
                } else if (code == 200) {

                }
                String result = object.toString();
                Log.e("result:", "" + result);
                if (result.equals("")) {
                    listner.onError();
                } else {
                    listner.onSuccess(result);

                }

            }
        });
    }

    public void Suggest_Route(Map<String, Object> params, final MethodManager_Listner listner) {
        aquary.ajax(Webservices.suggest_route, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                int code = status.getCode();

                if (code == AjaxStatus.NETWORK_ERROR) {
                    listner.onError();
                    return;
                } else if (code == AjaxStatus.TRANSFORM_ERROR) {
                    listner.onError();
                    return;
                } else if (code == 200) {

                }
                String result = object.toString();
                Log.e("result:", "" + result);
                if (result.equals("")) {
                    listner.onError();
                } else {
                    listner.onSuccess(result);

                }

            }
        });
    }

    public void BookCarBus(Map<String, Object> params, final MethodManager_Listner listner) {
        aquary.ajax(Webservices.bookCar, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                int code = status.getCode();

                if (code == AjaxStatus.NETWORK_ERROR) {
                    listner.onError();
                    return;
                } else if (code == AjaxStatus.TRANSFORM_ERROR) {
                    listner.onError();
                    return;
                } else if (code == 200) {

                }
                String result = object.toString();
                Log.e("result:", "" + result);
                if (result.equals("")) {
                    listner.onError();
                } else {
                    listner.onSuccess(result);

                }

            }
        });
    }

    public void Getcompany_list(final MethodManager_Listner listner) {
        aquary.ajax(Webservices.company_list, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                int code = status.getCode();

                if (code == AjaxStatus.NETWORK_ERROR) {
                    listner.onError();
                    return;
                } else if (code == AjaxStatus.TRANSFORM_ERROR) {
                    listner.onError();
                    return;
                } else if (code == 200) {

                }
                String result = object.toString();
                Log.e("result:", "" + result);
                if (result.equals("")) {
                    listner.onError();
                } else {
                    listner.onSuccess(result);

                }

            }
        });
    }
    public void SendBookingData(Map<String, Object> params,final MethodManager_Listner listner) {
        aquary.ajax(Webservices.booking_seat, params,JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                int code = status.getCode();

                if (code == AjaxStatus.NETWORK_ERROR) {
                    listner.onError();
                    return;
                } else if (code == AjaxStatus.TRANSFORM_ERROR) {
                    listner.onError();
                    return;
                } else if (code == 200) {

                }
                String result = object.toString();
                Log.e("result:", "" + result);
                if (result.equals("")) {
                    listner.onError();
                } else {
                    listner.onSuccess(result);

                }

            }
        });
    }
    public void GetBooking_Details(Map<String, Object> params,final MethodManager_Listner listner) {
        aquary.ajax(Webservices.currunt_booking, params,JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                int code = status.getCode();

                if (code == AjaxStatus.NETWORK_ERROR) {
                    listner.onError();
                    return;
                } else if (code == AjaxStatus.TRANSFORM_ERROR) {
                    listner.onError();
                    return;
                } else if (code == 200) {

                }
                String result = object.toString();
                Log.e("result:", "" + result);
                if (result.equals("")) {
                    listner.onError();
                } else {
                    listner.onSuccess(result);

                }
            }
        });
    }
    public void Cancel_Booking(Map<String, Object> params,final MethodManager_Listner listner) {
        aquary.ajax(Webservices.cancel_booking, params,JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                int code = status.getCode();

                if (code == AjaxStatus.NETWORK_ERROR) {
                    listner.onError();
                    return;
                } else if (code == AjaxStatus.TRANSFORM_ERROR) {
                    listner.onError();
                    return;
                } else if (code == 200) {

                }
                String result = object.toString();
                Log.e("result:", "" + result);
                if (result.equals("")) {
                    listner.onError();
                } else {
                    listner.onSuccess(result);

                }
            }
        });
    }

    public void GetBooking_Details_History(Map<String, Object> params,final MethodManager_Listner listner) {
        aquary.ajax(Webservices.history_booking, params,JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                int code = status.getCode();

                if (code == AjaxStatus.NETWORK_ERROR) {
                    listner.onError();
                    return;
                } else if (code == AjaxStatus.TRANSFORM_ERROR) {
                    listner.onError();
                    return;
                } else if (code == 200) {

                }
                String result = object.toString();
                Log.e("result:", "" + result);
                if (result.equals("")) {
                    listner.onError();
                } else {
                    listner.onSuccess(result);

                }
            }
        });
    }
    public void contactUsDetails(final MethodManager_Listner listner) {
        aquary.ajax(Webservices.contact_usdetails,JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                int code = status.getCode();

                if (code == AjaxStatus.NETWORK_ERROR) {
                    listner.onError();
                    return;
                } else if (code == AjaxStatus.TRANSFORM_ERROR) {
                    listner.onError();
                    return;
                } else if (code == 200) {

                }
                String result = object.toString();
                Log.e("result:", "" + result);
                if (result.equals("")) {
                    listner.onError();
                } else {
                    listner.onSuccess(result);

                }

            }
        });
    }
    public void GetMap_Data(Map<String, Object> params,final MethodManager_Listner listner) {
        aquary.ajax(Webservices.map_data, params,JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                int code = status.getCode();

                if (code == AjaxStatus.NETWORK_ERROR) {
                    listner.onError();
                    return;
                } else if (code == AjaxStatus.TRANSFORM_ERROR) {
                    listner.onError();
                    return;
                } else if (code == 200) {

                }
                String result = object.toString();
                Log.e("result:", "" + result);
                if (result.equals("")) {
                    listner.onError();
                } else {
                    listner.onSuccess(result);

                }
            }
        });
    }
    public void SendSocial_data(Map<String, Object> params,final MethodManager_Listner listner) {
        aquary.ajax(Webservices.social_data, params,JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                int code = status.getCode();

                if (code == AjaxStatus.NETWORK_ERROR) {
                    listner.onError();
                    return;
                } else if (code == AjaxStatus.TRANSFORM_ERROR) {
                    listner.onError();
                    return;
                } else if (code == 200) {

                }
                String result = object.toString();
                Log.e("result:", "" + result);
                if (result.equals("")) {
                    listner.onError();
                } else {
                    listner.onSuccess(result);

                }
            }
        });
    }
    public void CheckSocial_Password(Map<String, Object> params,final MethodManager_Listner listner) {
        aquary.ajax(Webservices.social_password, params,JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                int code = status.getCode();

                if (code == AjaxStatus.NETWORK_ERROR) {
                    listner.onError();
                    return;
                } else if (code == AjaxStatus.TRANSFORM_ERROR) {
                    listner.onError();
                    return;
                } else if (code == 200) {

                }
                String result = object.toString();
                Log.e("result:", "" + result);
                if (result.equals("")) {
                    listner.onError();
                } else {
                    listner.onSuccess(result);

                }
            }
        });
    }
    public void ReturnBusLists(Map<String, Object> params, final MethodManager_Listner listner)
    {
        aquary.ajax(Webservices.returnbus_list,params,JSONObject.class,new AjaxCallback<JSONObject>()
        {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                int code = status.getCode();

                if (code == AjaxStatus.NETWORK_ERROR) {
                    listner.onError();
                    return;
                } else if (code == AjaxStatus.TRANSFORM_ERROR) {
                    listner.onError();
                    return;
                } else if (code == 200) {

                }
                String result = object.toString();
                Log.e("result:", "" + result);
                if (result.equals("")) {
                    listner.onError();
                } else {
                    listner.onSuccess(result);

                }

            }
        });
    }
    public void GeTreferData(Map<String, Object> params, final MethodManager_Listner listner)
    {
        aquary.ajax(Webservices.getreferdata,params,JSONObject.class,new AjaxCallback<JSONObject>()
        {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                int code = status.getCode();

                if (code == AjaxStatus.NETWORK_ERROR) {
                    listner.onError();
                    return;
                }
                else if (code == AjaxStatus.TRANSFORM_ERROR) {
                    listner.onError();
                    return;
                } else if (code == 200) {

                }
                String result = object.toString();
                Log.e("result:", "" + result);
                if (result.equals("")) {
                    listner.onError();
                } else {
                    listner.onSuccess(result);

                }

            }
        });
    }
    public void GetNotification_Data(Map<String, Object> params, final MethodManager_Listner listner) {
        aquary.ajax(Webservices.notificationdata, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                int code = status.getCode();

                if (code == AjaxStatus.NETWORK_ERROR) {
                    listner.onError();
                    return;
                } else if (code == AjaxStatus.TRANSFORM_ERROR) {
                    listner.onError();
                    return;
                } else if (code == 200) {

                }
                String result = object.toString();
                Log.e("result:", "" + result);
                if (result.equals("")) {
                    listner.onError();
                } else {
                    listner.onSuccess(result);

                }

            }
        });
    }
    public void GeTRewardspoint(Map<String, Object> params, final MethodManager_Listner listner)
    {
        aquary.ajax(Webservices.rewardshistory,params,JSONObject.class,new AjaxCallback<JSONObject>()
        {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                int code = status.getCode();

                if (code == AjaxStatus.NETWORK_ERROR) {
                    listner.onError();
                    return;
                }
                else if (code == AjaxStatus.TRANSFORM_ERROR) {
                    listner.onError();
                    return;
                } else if (code == 200) {

                }
                String result = object.toString();
                Log.e("result:", "" + result);
                if (result.equals("")) {
                    listner.onError();
                } else {
                    listner.onSuccess(result);

                }

            }
        });
    }
    public void GeTRewardspoint_plane(Map<String, Object> params, final MethodManager_Listner listner)
    {
        aquary.ajax(Webservices.plane_reward,params,JSONObject.class,new AjaxCallback<JSONObject>()
        {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                int code = status.getCode();

                if (code == AjaxStatus.NETWORK_ERROR) {
                    listner.onError();
                    return;
                }
                else if (code == AjaxStatus.TRANSFORM_ERROR) {
                    listner.onError();
                    return;
                } else if (code == 200) {

                }
                String result = object.toString();
                Log.e("result:", "" + result);
                if (result.equals("")) {
                    listner.onError();
                } else {
                    listner.onSuccess(result);

                }

            }
        });
    }
    public void SharePoint(Map<String, Object> params, final MethodManager_Listner listner) {
        aquary.ajax(Webservices.share_point, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                int code = status.getCode();

                if (code == AjaxStatus.NETWORK_ERROR) {
                    listner.onError();
                    return;
                } else if (code == AjaxStatus.TRANSFORM_ERROR) {
                    listner.onError();
                    return;
                } else if (code == 200) {

                }
                String result = object.toString();
                Log.e("result:", "" + result);
                if (result.equals("")) {
                    listner.onError();
                } else {
                    listner.onSuccess(result);

                }

            }
        });
    }
    public void ReedemPoint(Map<String, Object> params, final MethodManager_Listner listner) {
        aquary.ajax(Webservices.reedem_point, params, JSONObject.class, new AjaxCallback<JSONObject>() {
            @Override
            public void callback(String url, JSONObject object, AjaxStatus status) {
                super.callback(url, object, status);
                int code = status.getCode();

                if (code == AjaxStatus.NETWORK_ERROR) {
                    listner.onError();
                    return;
                } else if (code == AjaxStatus.TRANSFORM_ERROR) {
                    listner.onError();
                    return;
                } else if (code == 200) {

                }
                String result = object.toString();
                Log.e("result:", "" + result);
                if (result.equals("")) {
                    listner.onError();
                } else {
                    listner.onSuccess(result);

                }

            }
        });
    }
}

