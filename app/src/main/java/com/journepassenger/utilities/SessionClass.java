package com.journepassenger.utilities;

/**
 * Created by administrator on 17/2/16.
 */

    public class SessionClass
    {
        private static double currentLatitude,currentLongitude;

        private static String GCM_red_id;

        public static double getCurrentLatitude() {
            return currentLatitude;
        }

        public static double getCurrentLongitude() {
            return currentLongitude;
        }

        public static void setCurrentLatitude(double currentLatitude) {
            SessionClass.currentLatitude = currentLatitude;
        }

        public static void setCurrentLongitude(double currentLongitude) {
            SessionClass.currentLongitude = currentLongitude;
        }

        public static String getGCM_red_id() {
            return GCM_red_id;
        }

        public static void setGCM_red_id(String GCM_red_id) {
            SessionClass.GCM_red_id = GCM_red_id;
        }
    }

