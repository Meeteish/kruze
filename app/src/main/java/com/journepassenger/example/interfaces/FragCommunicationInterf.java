package com.journepassenger.example.interfaces;




public interface FragCommunicationInterf {

   void LeftMenuContentClickListner(String var1, int var2);

   void otherAppClick(String var1, String var2, String var3);

   void restartFetchingAllData();

   void slideMenu();

   void switchToDetailContentListner();
}
