package com.journepassenger.example.interfaces;

public interface AddReminderInterface {
	
	public void onAddReminder();

}
