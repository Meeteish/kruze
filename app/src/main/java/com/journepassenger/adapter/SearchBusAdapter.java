package com.journepassenger.adapter;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.journepassenger.activity.DashBoardActivity;
import com.journepassenger.bean.TripIdBean;
import com.journepassenger.utilities.ConnectionDetector;
import com.journyapp.journyapp.R;

import java.util.ArrayList;



/**
 * Created by administrator on 29/1/16.
 */
public class SearchBusAdapter extends BaseAdapter {
    Context mContext;
    FragmentManager mFrgManager;
    ViewHolder holder;
    ArrayList<TripIdBean> arrayList;
    public SearchBusAdapter(Context mContext, ArrayList<TripIdBean> array) {
        this.mContext = mContext;
        arrayList=array;
    }

    @Override
    public int getCount() {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.searchbuscustom_new_one_xml, parent, false);
            holder=new ViewHolder();
            holder.from_city=(TextView)convertView.findViewById(R.id.from_city);
            holder.to_city=(TextView)convertView.findViewById(R.id.to_city);
            holder.leave_tm=(TextView)convertView.findViewById(R.id.leave_tm);
            holder.arrive_tm=(TextView)convertView.findViewById(R.id.arrive_tm);
            holder.btn_book=(TextView)convertView.findViewById(R.id.btn_book);
            holder.s_avliable=(TextView)convertView.findViewById(R.id.s_avliable);
            holder.s_cost=(TextView)convertView.findViewById(R.id.s_cost);
            holder.tvBusNo=(TextView)convertView.findViewById(R.id.tvBusNo);
            holder.seatType=(TextView)convertView.findViewById(R.id.seatType);
            holder.txtvia=(TextView)convertView.findViewById(R.id.txtvia);
            convertView.setTag(holder);
        }
        else
        {
            holder= (ViewHolder) convertView.getTag();
        }
        String total_seat=arrayList.get(position).getTotal_seats();
        holder.from_city.setText(arrayList.get(position).getFromDetailsBean().getBus_stop_name());

        holder.to_city.setText(arrayList.get(position).getToDetailsBean().getBus_stop_name());
        holder.leave_tm.setText(arrayList.get(position).getFromDetailsBean().getT_stop_time());
        holder.arrive_tm.setText(arrayList.get(position).getToDetailsBean().getT_stop_time());
        holder.s_avliable.setText(arrayList.get(position).getAvailable_seats()+" Out of "+total_seat);
        holder.s_cost.setText("Rs "+arrayList.get(position).getPrice());
        holder.tvBusNo.setText(arrayList.get(position).getBus_no());
        holder.seatType.setText(arrayList.get(position).getAc_type());
        holder.txtvia.setText(arrayList.get(position).getVia());
        holder.btn_book.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ConnectionDetector.isNetworkAvailable(mContext))

                {
                    Bundle b = new Bundle();
                    b.putString("trip_id", arrayList.get(position).getTrip_id());
                    b.putString("fromcity", arrayList.get(position).getFromDetailsBean().getBus_stop_name());
                    b.putString("tocity", arrayList.get(position).getToDetailsBean().getBus_stop_name());
                    b.putString("from_id", arrayList.get(position).getFromDetailsBean().getT_stop());
                    b.putString("to_id", arrayList.get(position).getToDetailsBean().getT_stop());
                    b.putString("price", arrayList.get(position).getPrice());
                    b.putString("busno", arrayList.get(position).getBus_no());
                    b.putString("fromS_Time", arrayList.get(position).getFromDetailsBean().getT_stop_time());
                    b.putString("toS_Time", arrayList.get(position).getToDetailsBean().getT_stop_time());
                    b.putString("fromstop_Id", arrayList.get(position).getFromDetailsBean().getT_stop());
                    b.putString("tostop_Id", arrayList.get(position).getToDetailsBean().getT_stop());
                    b.putString("ac_type",arrayList.get(position).getAc_type());
                    DashBoardActivity.displayview(DashBoardActivity.stopdetils, b);
                }
                else
                {
                    Toast.makeText(mContext, "You don't have Internet Connection", Toast.LENGTH_LONG).show();

                }
            }
        });

        return convertView;
    }
    public  class ViewHolder
    {
         TextView from_city;
         TextView to_city;
         TextView leave_tm;
         TextView arrive_tm;
         TextView btn_book;
         TextView s_avliable;
         TextView s_cost;
         TextView tvBusNo;
         TextView seatType;
         TextView txtvia;
    }
}