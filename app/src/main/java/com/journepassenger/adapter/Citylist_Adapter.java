package com.journepassenger.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.journyapp.journyapp.R;

import java.util.ArrayList;
import java.util.List;


public class Citylist_Adapter extends ArrayAdapter<String>
{
	private LayoutInflater layoutInflater;
    List<String> city;

	public Citylist_Adapter(Context context, int resource, List<String> citys)
	{
		super(context, resource, citys);
		   city = new ArrayList<String>(citys.size());
	        city.addAll(citys);
	        layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	
	public View getCustomView(int position, View convertView, ViewGroup parent) {
		View view = convertView;
		if (view == null) {
            view = layoutInflater.inflate(R.layout.popup_item, parent,false);
        }

		String customer = getItem(position);

        TextView name = (TextView) view.findViewById(R.id.sp_text);
        name.setText(customer);

        return view;
	}
	@Override
	public View getDropDownView(int position, View convertView, ViewGroup parent) {
		
		return getCustomView(position, convertView, parent);
	}
	@Override
	  public View getView(int position, View convertView, ViewGroup parent) {
	   // TODO Auto-generated method stub
	   return getCustomView(position, convertView, parent);
	  }
}
