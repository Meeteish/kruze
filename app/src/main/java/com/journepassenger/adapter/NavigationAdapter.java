package com.journepassenger.adapter;

import android.content.Context;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.journyapp.journyapp.R;

/**
 * Created by administrator on 29/1/16.
 */
public class NavigationAdapter extends BaseAdapter {
    Context mContext;
    FragmentManager mFrgManager;
    ViewHolder holder;
    public NavigationAdapter(Context mContext, FragmentManager mFragmentManager) {
        this.mContext = mContext;
        this.mFrgManager = mFragmentManager;
    }

    @Override
    public int getCount() {
        return 1;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.navigationdrawer_custom, parent, false);

             holder=new ViewHolder();
            convertView.setTag(holder);
        }
        else
        {
            holder= (ViewHolder) convertView.getTag();
        }
        return convertView;
    }
    public  class ViewHolder
    {

    }
}