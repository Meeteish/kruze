package com.journepassenger.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.journepassenger.bean.Stop_bean;
import com.journepassenger.fragments.StopDetails_Fragment;
import com.journepassenger.utilities.Session_manager;
import com.journyapp.journyapp.R;

import java.util.ArrayList;

/**
 * Created by administrator on 16/2/16.
 */
public class StopList_Adapter extends BaseAdapter
{
    Context mContext;
    ArrayList<Stop_bean> arrayList;
    ViewHolder holder;
    public static int to=-1;
    public static int from=-1;
    boolean b_to=false,b_from=false;
    String from_landmark;
    String to_landmark;
    public static String fromstop_time;
    public static String tostop_time;
    public static String from_stopid;
    public static String to_stopid;
    public StopList_Adapter(Context mContext,ArrayList<Stop_bean> arrayList)
    {
        this.mContext=mContext;
        this.arrayList=arrayList;
    }
    @Override
    public int getCount()
    {
        return arrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)

    {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.stoplist_custom, parent, false);
            holder=new ViewHolder();
            holder.pointerImage=(ImageView)convertView.findViewById(R.id.pointerImage);
            holder.tvstop_name=(TextView)convertView.findViewById(R.id.tvstop_name);
            holder.tv_time=(TextView)convertView.findViewById(R.id.tv_time);
            convertView.setTag(holder);
        }
        else
        {
            holder= (ViewHolder) convertView.getTag();
        }
        holder.tvstop_name.setText(arrayList.get(position).getBus_stop_name());
        holder.tv_time.setText(arrayList.get(position).getT_stop_time());

        if(arrayList.get(position).getReach_status().equals("1")) {

            holder.pointerImage.setClickable(false);
            holder.pointerImage.setFocusable(false);
            holder.pointerImage.setFocusableInTouchMode(false);
        }

        if(from==position)
            holder.pointerImage.setImageResource(R.drawable.location_green);
        else if(to==position)
            holder.pointerImage.setImageResource(R.drawable.location_red);
        else
            holder.pointerImage.setImageResource(R.drawable.location_white);


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(arrayList.get(position).getReach_status().equals("1")) {

                    Toast.makeText(mContext,"Bus Reached to this Stop",Toast.LENGTH_LONG).show();
                } else
                {


                if(from==position)
                {
                    from=-1;
                }
                else if(to==position)
                {
                    to=-1;
                }
                else if(from==-1&&to==-1)
                {
                    from=position;
                }
                else if(from==-1 && to!=-1)
                {
                    if(position>to)
                    {
                        from=to;
                        to=position;
                    }
                    else
                    {
                        from=position;
                    }
                }
                else if(position>from)
                {
                    if(to==-1)
                        to=position;
                    else if(position>(to-from-1)/2)
                        to=position;
                    else
                        from=position;
                }
                else
                {
                    if(to==-1) {
                        to = from;
                        from = position;
                    }
                    else
                    {
                        from=position;
                    }
                }

                Log.e("---pos",+position+"");
                Log.e("---from",+from+"");
                Log.e("---to",+to+"");
                notifyDataSetChanged();

                StopDetails_Fragment.chqValue();

                //Toast.makeText(_context,"to :: "+to+" , from :: "+from,Toast.LENGTH_LONG).show();

                if (to != -1){

                         to_landmark = arrayList.get(to).getBus_stop_name();
                         to_stopid = arrayList.get(to).getT_stop_id();
                         tostop_time=arrayList.get(to).getT_stop_time();
                         Session_manager.setToLanmark(mContext, to_landmark, to_stopid);
                     }


                else
                {
                    Session_manager.setToLanmark(mContext, null,null);
                }

                if (from != -1){

                    from_landmark=arrayList.get(from).getBus_stop_name();
                    from_stopid=arrayList.get(from).getT_stop_id();
                    fromstop_time=arrayList.get(from).getT_stop_time();
                    Session_manager.setfromlandmark(mContext, from_landmark,from_stopid);
                    }

                else
                {

                    Session_manager.setfromlandmark(mContext,null,null);
                }

            }}
        });


        return convertView;
    }
    public class ViewHolder
    {
        ImageView pointerImage;
        TextView tvstop_name;
        TextView tv_time;
    }

}
