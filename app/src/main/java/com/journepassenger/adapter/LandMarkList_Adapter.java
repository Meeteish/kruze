package com.journepassenger.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import com.journepassenger.bean.LandMark_bean;
import com.journyapp.journyapp.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by administrator on 11/2/16.
 */
public class LandMarkList_Adapter extends ArrayAdapter<LandMark_bean> {
    private LayoutInflater layoutInflater;
    List<LandMark_bean> landmark;

    private Filter mfilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            return ((LandMark_bean) resultValue).getLandmark();
        }

        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {

            clear();
            if (results != null && results.count > 0) {
                // we have filtered results
                addAll((ArrayList<LandMark_bean>) results.values);
            } else {
                // no filter, add entire original list back in
                addAll(landmark);
            }
            notifyDataSetChanged();
        }

        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();

            if (constraint != null) {
                ArrayList<LandMark_bean> suggestions = new ArrayList<LandMark_bean>();
                for (LandMark_bean customer : landmark) {
                    // Note: change the "contains" to "startsWith" if you only want starting matches
//	                    if (customer.getCity_name().toLowerCase().contains(constraint.toString().toLowerCase())) {
                    if (customer.getLandmark().toLowerCase().startsWith(constraint.toString().toLowerCase())) {
                        suggestions.add(customer);
                    }
                }

                results.values = suggestions;
                results.count = suggestions.size();
            }

            return results;
        }
    };


    public LandMarkList_Adapter(Context context, int resource, List<LandMark_bean> objects) {
        super(context, resource, objects);

        landmark = new ArrayList<LandMark_bean>(objects.size());
        landmark.addAll(objects);
        layoutInflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = convertView;
        if (view == null) {
            view = layoutInflater.inflate(R.layout.landmark_listcustom, null);
        }
            LandMark_bean customer = getItem(position);

            TextView name = (TextView) view.findViewById(R.id.lanamark_name);
            name.setText(customer.getLandmark());

            return view;
        }

    @Override
    public Filter getFilter() {
        return mfilter;
    }
    }

