package com.journepassenger.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.journyapp.journyapp.R;

/**
 * Created by administrator on 25/2/16.
 */
public class RecentTra_Adapter extends BaseAdapter
{
    Context mContext;
    View view;
    LayoutInflater inflator;
    ViewHolder holder;
    public RecentTra_Adapter(Context mContext)
    {
        this.mContext=mContext;
        inflator = LayoutInflater.from(mContext);
    }
    @Override
    public int getCount()
    {
        return 0;
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        view = convertView;

        if (view == null) {
            view = inflator.inflate(R.layout.recent_transactions, parent, false);
            holder = new ViewHolder();


            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }



        return view;
    }
        public class ViewHolder
        {

        }
    }

