package com.journepassenger.adapter;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.journepassenger.activity.DashBoardActivity;
import com.journepassenger.bean.Landmark;
import com.journepassenger.bean.RoutesBean;
import com.journepassenger.fragments.HomeScreenFragment;
import com.journepassenger.utilities.Session_manager;
import com.journyapp.journyapp.R;

import java.util.ArrayList;
import java.util.List;



public class ParentExpandableListAdapter extends BaseExpandableListAdapter {

	private Context _context;
	private List<RoutesBean> _listDataHeader;
	public static boolean selectstatus[];
	String cityfrom;
	String cityto;

	public static int to=-1;
    public static int from=-1;
	boolean b_to=false,b_from=false;
    int grouppositionclick;
	// header titles
	// child data in format of header title, child title

	public ParentExpandableListAdapter(Context context, List<RoutesBean> listDataHeader) {
		this._context = context;
		this._listDataHeader = listDataHeader;

		selectstatus = new boolean[listDataHeader.size()];

		for (int i = 0; i < selectstatus.length; i++) {

			selectstatus[i] = false;

		}
	}

	@Override
	public Object getChild(int groupPosition, int childPosititon) {
	 return this._listDataHeader.get(groupPosition).getLandmark_array();
	//	return this._listDataHeader.get(groupPosition).getTrip_list().get(groupPosition).getStop_list();
	}

	@Override
	public long getChildId(int groupPosition, int childPosition) {
		return childPosition;
	}

	@Override
	public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView,
			ViewGroup parent) {

		final ArrayList<Landmark> mlList = _listDataHeader.get(groupPosition).getLandmark_array();

		final String Invited_name = mlList.get(childPosition).getLandmark();

		final String lanmark_id=mlList.get(childPosition).getLandmark_id();


	
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.dashboard_child_item, null);
		}

		TextView name_tv = (TextView) convertView.findViewById(R.id.tvlocationid);

		ImageView imageView=(ImageView)convertView.findViewById(R.id.imgchildlistid);
		name_tv.setText(Invited_name);

		final View finalConvertView = convertView;

		if(from==childPosition)
        {
			imageView.setImageResource(R.drawable.location_icon);

		}
		else if(to==childPosition) {
			imageView.setImageResource(R.drawable.destination_icon);
			//HomeScreenFragment.changeButtonGreen(_context);
		}
		else {
			imageView.setImageResource(R.drawable.circle);
			//HomeScreenFragment.changeButtonGray(_context);
		}
		convertView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v)
			{

				if(from==childPosition)
				{
					from=-1;

				}
				else if(to==childPosition)
				{
					to=-1;

				}
				else if(from==-1&&to==-1)
				{
					from=childPosition;
				}
				else if(childPosition>from)
				{
					if(to==-1)
						to=childPosition;
					else if(childPosition>(to-from-1)/2)
						to=childPosition;
					else
						from=childPosition;
				}
				else
				{
					if(to==-1) {
						to = from;
						from = childPosition;
					}
					else
					{
						from=childPosition;
					}
				}

				Log.e("---pos",+childPosition+"");
				Log.e("---from",+from+"");
				Log.e("---to", +to + "");
				notifyDataSetChanged();

                HomeScreenFragment.chqValue();

                //Toast.makeText(_context,"to :: "+to+" , from :: "+from,Toast.LENGTH_LONG).show();

                if (to != -1){

                    cityto= mlList.get(to).getLandmark();
                    Session_manager.setTocity(_context,cityto);

                }
                else
                {
                    Session_manager.setTocity(_context,null);
                }

                if (from != -1){

                    cityfrom=mlList.get(from).getLandmark();
                    Session_manager.setfromcity(_context, cityfrom);
                }
                else
                {
                    Session_manager.setfromcity(_context,null);
                }

                //Toast.makeText(_context,""+cityfrom+" , "+cityto,Toast.LENGTH_LONG).show();


			}
		});

		return convertView;
	}

	@Override
	public int getChildrenCount(int groupPosition) {
		Log.e("groupPosition", groupPosition + "");
		
		//Log.e("size = ", _listDataHeader.get(1).getLandmark_array() + "");

		return this._listDataHeader.get(groupPosition).getLandmark_array().size();
		
	//	return this._listDataHeader.get(groupPosition).getTrip_list().get(groupPosition).getStop_list().size();
		
	//	return this._listDataHeader.get(groupPosition).getTrip_list().size();

	}

	@Override
	public Object getGroup(int groupPosition) {
		return this._listDataHeader.get(groupPosition);
	}

	@Override
	public int getGroupCount() {
		return this._listDataHeader.size();
	}

	@Override
	public long getGroupId(int groupPosition) {
		return groupPosition;
	}

	@Override
	public View getGroupView(final int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {

		// String headerTitle = (String) getGroup(groupPosition);

		String headerTitle = _listDataHeader.get(groupPosition).getBus_route_title();
		if (convertView == null) {
			LayoutInflater infalInflater = (LayoutInflater) this._context
					.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
			convertView = infalInflater.inflate(R.layout.dashboard_group_item, null);
		}

		TextView lblListHeader = (TextView) convertView.findViewById(R.id.routetitleid);
		//TextView tvtimeid=(TextView)convertView.findViewById(R.id.tvtimeid);
		ImageView iconExpand = (ImageView) convertView.findViewById(R.id.imguparrowid);
		ImageView iconCollapse = (ImageView) convertView
				.findViewById(R.id.imgdownarrowid);

		//tvtimeid.setText(""+s_time+ "-"+e_time);
		lblListHeader.setTypeface(null, Typeface.BOLD);
		lblListHeader.setText(headerTitle);
		if (isExpanded) {
			iconExpand.setVisibility(View.VISIBLE);
			iconCollapse.setVisibility(View.GONE);

		} else {
			iconExpand.setVisibility(View.GONE);
			iconCollapse.setVisibility(View.VISIBLE);
		}


		return convertView;
	}

	@Override
	public boolean hasStableIds() {
		return false;
	}

	@Override
	public boolean isChildSelectable(int groupPosition, int childPosition) {
		return true;
	}

	public void open(View view, final String fromcity, final String tocity){
		AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(_context);
		alertDialogBuilder.setMessage("Are you sure,want to search buses accordingly");

		alertDialogBuilder.setPositiveButton("yes", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface arg0, int arg1) {
				Bundle b=new Bundle();
				Session_manager.setTocity(_context, null);
				Session_manager.setfromcity(_context, null);
				b.putString("fromcity",fromcity);
				b.putString("tocity",tocity);
				DashBoardActivity.displayview(DashBoardActivity.searchbus, b);
			}
		});

		alertDialogBuilder.setNegativeButton("No",new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dialog.dismiss();
				Session_manager.setTocity(_context, null);
				Session_manager.setfromcity(_context, null);
				HomeScreenFragment.SetAdapter();
			}
		});

		AlertDialog alertDialog = alertDialogBuilder.create();
		alertDialog.show();
	}


}
