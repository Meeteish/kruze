package com.journepassenger.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.journepassenger.bean.Rewards_bean;
import com.journyapp.journyapp.R;

import java.util.ArrayList;

/**
 * Created by administrator on 22/3/16.
 */
public class RewardPoint_detail_Adapter extends BaseAdapter
{
    Context mContext;
    ArrayList<Rewards_bean> rewardsarray;
    ViewHolder holder;
    View view;
    LayoutInflater inflator;
    public RewardPoint_detail_Adapter(Context mContext,ArrayList<Rewards_bean> rewardsarray)
    {
        this.mContext=mContext;
        this.rewardsarray=rewardsarray;
        inflator = LayoutInflater.from(mContext);
    }
    @Override
    public int getCount() {
        return rewardsarray.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        view = convertView;
        if (view == null) {
            view = inflator.inflate(R.layout.rewadspoint_custom, parent, false);
            holder = new ViewHolder();
            //holder.tvDate=(TextView)view.findViewById(R.id.tvDate);
            holder.tvTime=(TextView)view.findViewById(R.id.tvTime);
            holder.tvCash=(TextView)view.findViewById(R.id.tvCash);
            holder.tvStatus=(TextView)view.findViewById(R.id.tvStatus);
            view.setTag(holder);
        }
        else
        {
            holder = (ViewHolder) view.getTag();
        }
        //holder.tvDate.setText(rewardsarray.get(position).getEp_date());
        holder.tvCash.setText(rewardsarray.get(position).getEp_points()+" pts");
        holder.tvTime.setText(rewardsarray.get(position).getEp_datetime());
        holder.tvStatus.setText(rewardsarray.get(position).getEp_remark());
        return view;
    }
    public class ViewHolder
    {
        TextView tvDate;
        TextView tvTime;
        TextView tvCash;
        TextView tvStatus;
    }
}
