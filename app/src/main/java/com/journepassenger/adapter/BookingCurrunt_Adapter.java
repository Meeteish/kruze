package com.journepassenger.adapter;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.journepassenger.activity.DashBoardActivity;
import com.journepassenger.bean.Bookingdata_bean;
import com.journyapp.journyapp.R;

import java.util.ArrayList;

/**
 * Created by administrator on 29/2/16.
 */
public class BookingCurrunt_Adapter extends BaseAdapter
{
    Context mcontext;
    ArrayList<Bookingdata_bean> booking_array;
    ViewHolder holder;
    LayoutInflater inflator;
    View view;
    public BookingCurrunt_Adapter(Context mcContext,ArrayList<Bookingdata_bean> booking_array)
    {
        this.mcontext=mcContext;
        this.booking_array=booking_array;
        inflator = LayoutInflater.from(mcontext);
    }
    @Override
    public int getCount()
    {
        return booking_array.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        view = convertView;

        if (view == null) {
            view = inflator.inflate(R.layout.booking_history, parent, false);
            holder = new ViewHolder();
            holder.fromlocation=(TextView)view.findViewById(R.id.fromlocation);
            holder.text_tolocation=(TextView)view.findViewById(R.id.text_tolocation);
            holder.tvBusNo=(TextView)view.findViewById(R.id.tvBusNo);
            holder.tv_timesrc=(TextView)view.findViewById(R.id.tv_timesrc);
            holder.tv_timedesti=(TextView)view.findViewById(R.id.tv_timedesti);
            holder.status=(TextView)view.findViewById(R.id.status);
            holder.date=(TextView)view.findViewById(R.id.date);

            view.setTag(holder);
        } else
        {
            holder = (ViewHolder) view.getTag();
        }

        holder.fromlocation.setText(booking_array.get(position).getSource());
        holder.text_tolocation.setText(booking_array.get(position).getDestination());
        holder.tvBusNo.setText(booking_array.get(position).getBus_no());
        holder.tv_timesrc.setText(booking_array.get(position).getSource_time());
        holder.tv_timedesti.setText(booking_array.get(position).getDestination_time());
        holder.status.setVisibility(View.GONE);
        holder.date.setVisibility(View.GONE);
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle b=new Bundle();
                b.putString("source",booking_array.get(position).getSource());
                b.putString("destination",booking_array.get(position).getDestination());
                b.putString("source_time",booking_array.get(position).getSource_time());
                b.putString("desti_time",booking_array.get(position).getDestination_time());
                b.putString("booking_id",booking_array.get(position).getB_id());
                b.putString("seats",booking_array.get(position).getSeats());
                b.putString("coast",booking_array.get(position).getPrice());
                b.putString("bus_no",booking_array.get(position).getBus_no());
                //b.putString("bus_id",booking_array.get(position).getB_id());
                DashBoardActivity.displayview(DashBoardActivity.curruntbooking,b);

            }
        });
        return view;
    }
    public class  ViewHolder
    {
        TextView fromlocation;
        TextView text_tolocation;
        TextView tvBusNo;
        TextView tv_timesrc;
        TextView tv_timedesti;
        TextView status;
        TextView date;
    }
}
