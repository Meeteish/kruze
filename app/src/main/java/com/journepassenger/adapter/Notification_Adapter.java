package com.journepassenger.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.journepassenger.bean.Notification_bean;
import com.journyapp.journyapp.R;

import java.util.ArrayList;

/**
 * Created by administrator on 17/3/16.
 */
public class Notification_Adapter extends BaseAdapter
{
    Context mContext;
    //FragmentManager mFrgManager;
    ArrayList<Notification_bean> notification_beanArrayList;
    Holder holder;
    public Notification_Adapter(Context mContext, ArrayList<Notification_bean> notification_beanArrayList) {
        this.mContext = mContext;
        this.notification_beanArrayList = notification_beanArrayList;
    }

    @Override
    public int getCount() {
        return notification_beanArrayList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.notification_customxml, parent, false);
            holder=new Holder();
            holder.tvnoti_type=(TextView)convertView.findViewById(R.id.tvnoti_type);
            holder.tvtime=(TextView)convertView.findViewById(R.id.tvtime);
            holder.tvnoti_text=(TextView)convertView.findViewById(R.id.tvnoti_text);
            convertView.setTag(holder);
        }
        else
        {
            holder= (Holder) convertView.getTag();

        }
        holder.tvnoti_type.setText(notification_beanArrayList.get(position).getNoti_type());
        holder.tvnoti_text.setText(notification_beanArrayList.get(position).getNoti_message());
        holder.tvtime.setText(notification_beanArrayList.get(position).getNoti_datetime());
        return convertView;
    }
    public  class Holder
    {
        TextView tvnoti_type;
        TextView tvtime;
        TextView tvnoti_text;
    }
}
