package com.journepassenger.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.journepassenger.bean.SeatSecond_bean;
import com.journyapp.journyapp.R;

import java.util.ArrayList;

/**
 * Created by administrator on 8/2/16.
 */
public class SecondSeatRow_Adapter extends BaseAdapter
{
    Context mContext;
    ArrayList<SeatSecond_bean> secondseatarray;
    ViewHolder holder;

    public SecondSeatRow_Adapter(Context mcContext, ArrayList<SeatSecond_bean> secondseatarray)
    {
        this.mContext=mcContext;
        this.secondseatarray=secondseatarray;

    }
    @Override
    public int getCount()
    {
        return secondseatarray.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.seat_custom, parent, false);
            holder = new ViewHolder();
            //holder.seatiamge=(ImageView)convertView.findViewById(R.id.seatimage);

            convertView.setTag(holder);

        }
        else {
            holder= (ViewHolder) convertView.getTag();
        }

        return convertView;
    }
    public  class ViewHolder
    {
        ImageView seatiamge;
    }

}
