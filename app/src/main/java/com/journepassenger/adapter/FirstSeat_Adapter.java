package com.journepassenger.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.journepassenger.bean.SeatRows_bean;
import com.journepassenger.fragments.SeatAvliablity_Fragment;
import com.journyapp.journyapp.R;

import java.util.ArrayList;

/**
 * Created by administrator on 8/2/16.
 */
public class FirstSeat_Adapter extends BaseAdapter
{
    Context mContext;
    ArrayList<SeatRows_bean> fristseatarray;
    ViewHolder holder;
    //int index;
    public static ArrayList<String> selected_seatstatus;
    int price=200;
    public FirstSeat_Adapter(Context mcContext, ArrayList<SeatRows_bean> seatarraylist)
    {
        this.mContext=mcContext;
        this.fristseatarray=seatarraylist;
        selected_seatstatus=new ArrayList<>();
    }
    @Override
    public int getCount() {
        return fristseatarray.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent)
    {
        if (convertView == null) {
            LayoutInflater inflater = (LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.seat_custom, parent, false);
            holder = new ViewHolder();
            holder.liner_seatimages=(LinearLayout)convertView.findViewById(R.id.liner_seatimages);
            convertView.setTag(holder);

        }
        else {
            holder= (ViewHolder) convertView.getTag();

        }

        int size=fristseatarray.get(position).getSeatno_array().size();
        for (int i=0;i<size;i++)
        {
            final  int index=i;

            final ImageView img=new ImageView(mContext);
            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(90,90);
        //    img.setScaleType(ImageView.ScaleType.CENTER_CROP);
            img.setLayoutParams(layoutParams);

            String sta=fristseatarray.get(position).getSeatno_array().get(i).getSeat_no();
            if(sta.equals("0"))
            {
                img.setImageResource(R.drawable.white_bg);
            }
            else
            {
                img.setImageResource(R.drawable.available_seat);
                img.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v)
                    {
                        boolean boking_status=false;
                        String seat_no=fristseatarray.get(position).getSeatno_array().get(index).getSeat_no();
                        //Toast.makeText(mContext,""+fristseatarray.get(position).getSeatno_array().get(index).getSeat_no(),Toast.LENGTH_LONG).show();

                        for (String s:selected_seatstatus)
                        {
                            if(s.equals(seat_no))
                            {
                                boking_status=true;
                            }
                        }
                        if(boking_status)
                        {
                            img.setImageResource(R.drawable.available_seat);
                            selected_seatstatus.remove(seat_no);
                            SeatAvliablity_Fragment.seattext.setText("" + selected_seatstatus.size());
                            SeatAvliablity_Fragment.seat_price.setText(""+200*(selected_seatstatus.size()));
                        }
                        else
                        {
                            img.setImageResource(R.drawable.selected_seat);
                            selected_seatstatus.add(seat_no);
                            SeatAvliablity_Fragment.seattext.setText("" + selected_seatstatus.size());
                            SeatAvliablity_Fragment.seat_price.setText(""+200*(selected_seatstatus.size()));
                        }
                        Log.e("selected_seatstatus",selected_seatstatus.toString());


                    }
                });
            }



            holder.liner_seatimages.addView(img);

        }
        return convertView;
    }
    public class ViewHolder
    {
       LinearLayout liner_seatimages;
    }
}
