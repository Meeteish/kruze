package com.journepassenger.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.journepassenger.bean.RoutesBean;
import com.journyapp.journyapp.R;

import java.util.ArrayList;

/**
 * Created by administrator on 4/3/16.
 */
public class HomeFragment_Adapter extends BaseAdapter
{
    Context mContext;
    ArrayList<RoutesBean> routeslist_array;
    View view;
    ViewHolder holder;
    LayoutInflater inflator;
    public  HomeFragment_Adapter( Context mContext,ArrayList<RoutesBean> routeslist_array)
    {
        this.mContext=mContext;
        this.routeslist_array=routeslist_array;
        inflator = LayoutInflater.from(mContext);
    }
    @Override
    public int getCount() {
        return routeslist_array.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        view = convertView;
        if (view == null) {
            view = inflator.inflate(R.layout.homefragment_custom, parent, false);
            holder = new ViewHolder();
            holder.stop_name=(TextView)view.findViewById(R.id.stop_name);
            view.setTag(holder);
        } else
        {
            holder = (ViewHolder) view.getTag();
        }
        holder.stop_name.setText(routeslist_array.get(position).getBus_route_title());
        return view;
    }
    public class  ViewHolder
    {
        TextView stop_name;
    }

}
