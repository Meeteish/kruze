package com.journepassenger.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.journepassenger.bean.City_bean;
import com.journyapp.journyapp.R;

import java.util.ArrayList;
import java.util.List;



public class CustomPopupAdapter extends BaseAdapter {

	ViewHolder mHolder;
	Context mContext;
	ArrayList<City_bean> mList;
	private List<City_bean> worldpopulationlist = null;
	private ArrayList<City_bean> arraylist;
	LayoutInflater inflator;
	View view;

	public CustomPopupAdapter(ArrayList<City_bean> list, Context ctx) {
		mList = list;
		mContext = ctx;
		inflator = LayoutInflater.from(mContext);
		worldpopulationlist = list;
		this.arraylist = new ArrayList<City_bean>();
		this.arraylist.addAll(list);
	}

	@Override
	public int getCount() {
		return mList.size();
	}

	@Override
	public Object getItem(int position) {
		return position;
	}

	@Override
	public long getItemId(int position) {
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		view = convertView;

		if (view == null) {
			view = inflator.inflate(R.layout.popup_item, parent, false);
			mHolder = new ViewHolder();
			mHolder.sp_text = (TextView) view.findViewById(R.id.sp_text);

			view.setTag(mHolder);
		} else {
			mHolder = (ViewHolder) view.getTag();
		}

		mHolder.sp_text.setText(mList.get(position).getCity_name());

		return view;
	}

	public class ViewHolder {

		public TextView sp_text;

	}
	/*public void filter(String charText) {
		charText = charText.toLowerCase(Locale.getDefault());
		worldpopulationlist.clear();
		if (charText.length() == 0) {
			worldpopulationlist.addAll(arraylist);
		} else {
			for (Citybean wp : arraylist) {
				if (wp.getCity_name().toLowerCase(Locale.getDefault())
						.contains(charText)) {
					worldpopulationlist.add(wp);
				}
			}
		}
		notifyDataSetChanged();
	}*/
}