package com.journepassenger.activity;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageButton;
import android.widget.TextView;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.journepassenger.example.interfaces.FragCommunicationInterf;
import com.journepassenger.example.jeremyfeinstein.slidingmenu.lib.SlidingMenu;
import com.journepassenger.example.jeremyfeinstein.slidingmenu.lib.app.SlidingFragmentActivity;
import com.journepassenger.fragments.BookCar_Bus_Fragment;
import com.journepassenger.fragments.BookingDetail_Fragment;
import com.journepassenger.fragments.BookingHistory_Fragmant;
import com.journepassenger.fragments.BookingSucess_Fragmant;
import com.journepassenger.fragments.ContactUs_Fragment;
import com.journepassenger.fragments.CurrentBookingDetail_Fragment;
import com.journepassenger.fragments.DashFragment;
import com.journepassenger.fragments.EditProfile_Fragment;
import com.journepassenger.fragments.FAQ_Fragmant;
import com.journepassenger.fragments.Feedback_Fragment;
import com.journepassenger.fragments.HistoryBookignDetail_Fragment;
import com.journepassenger.fragments.HomeScreenFragment;
import com.journepassenger.fragments.Kruze_Play_Fragment;
import com.journepassenger.fragments.LeftFragment;
import com.journepassenger.fragments.MemberShipFragment;
import com.journepassenger.fragments.NotiFication_Fragment;
import com.journepassenger.fragments.RecentTransection_Fragment;
import com.journepassenger.fragments.ReferAndEarn_Fragment;
import com.journepassenger.fragments.RewardsPoint_Fragment;
import com.journepassenger.fragments.SearchedBus_Fragment;
import com.journepassenger.fragments.SeatAvliablity_Fragment;
import com.journepassenger.fragments.SharePoint_Fragmant;
import com.journepassenger.fragments.ShowProfile_Fragment;
import com.journepassenger.fragments.StopDetails_Fragment;
import com.journepassenger.fragments.SuggestRoute_Fragment;
import com.journepassenger.fragments.ThankYou_Fragmant;
import com.journepassenger.fragments.TrackRide_Fragment;
import com.journepassenger.fragments.WalletBalance;
import com.journepassenger.utilities.CircleTransform;
import com.journepassenger.utilities.Session_manager;
import com.journyapp.journyapp.R;
import com.squareup.picasso.Picasso;


/**
 * Created by administrator on 29/1/16.
 */
public class DashBoardActivity extends SlidingFragmentActivity implements FragCommunicationInterf,GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    public static SlidingMenu slidemenu;
    static ActionBar actionBar;
    static Context context;
    private static DrawerLayout mDrawerLayout;
    public static TextView header_title;
    static FragmentTransaction ft;
    static FragmentManager fm;
    public static int home = 0;
    public static int searchbus = 1;
    public static int memberShipFragment = 2;
    public static int editprofile = 3;
    public static int seatavliability = 4;
    public static int feedbackfragment = 5;
    public static int showprofilefragment = 6;
    public static int referandearnfragment = 7;
    public static int bookingdetailfragment = 8;
    public static int contactusfragment = 9;
    public static int stopdetils = 10;
    public static int walletTag=11;
    public static int BookCarTag=12;
    public static int SuggestRoutetag=13;
    public static int thankyou=14;
    public static int recenttrasection=15;
    public static int sucess_booking=16;
    public static int booking_history=17;
    public static int curruntbooking=18;
    public static int historybooking=19;
    public static int kruzeplay=20;
    public static int trackride=21;
    public static int notification=22;
    public static int Rewards_Points=23;
    public static int Share_Points=24;
    public static int faq=25;
    static ActionBarDrawerToggle mDrawerToggle;
    public static boolean isStatusOfDrawer = true;
    static FragmentManager frgManager;
    static Fragment fragment;
    private CharSequence mDrawerTitle;
    private static CharSequence mTitle;
    public static ImageButton menu_btn, adminbackbtn;
    static TextView doalogtext;
    static ImageButton home_icon;
    static ImageButton ho_icon;
    public static ImageButton print_icon;
    public static ImageButton search_icon, before_edit, after_edit;
    String from_city;
    String to_city;
    private static final int SIGN_IN_REQUEST_CODE = 10;
    private static final int ERROR_DIALOG_REQUEST_CODE = 11;
    private static GoogleApiClient mGoogleApiClient;
    String image_type;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard);

        fm = getSupportFragmentManager();
        context = this;

        actionBar = getSupportActionBar();
        //actionBar.hide();
        setActionBar();
        updatedTitle("");
        mGoogleApiClient = buildGoogleAPIClient();
        if (this.findViewById(R.layout.menu_frame) == null) {
            this.setBehindContentView(R.layout.menu_frame);
            this.getSlidingMenu().setSlidingEnabled(true);
            this.getSlidingMenu().setTouchModeAbove(1);
        }
        this.startLeftMenu();
        //this.startRightMenu();
        slidemenu = this.getSlidingMenu();
        slidemenu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        slidemenu.setShadowDrawable(R.drawable.shadow);
        slidemenu.setBehindScrollScale(0.25F);
        slidemenu.setFadeDegree(0.25F);
        slidemenu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        Display display = getWindowManager().getDefaultDisplay();
        int screenWidth = display.getWidth();
        int screenHeight = display.getHeight();
        slidemenu.setBehindOffset((int) (screenWidth / 3.2));
        slidemenu.setBehindScrollScale(0.25F);
        slidemenu.setFadeDegree(0.25F);
        slidemenu.setOnOpenedListener(new SlidingMenu.OnOpenedListener() {

            @Override
            public void onOpened() {
                hideKeyboard(context);
                String passenger_img = Session_manager.getPassengerimg(context);
                LeftFragment.GetWalletData();
                if (passenger_img != null && !passenger_img.equals(""))
                {
                    if(image_type.equals("1"))
                        Picasso.with(context).load(passenger_img).rotate(90).transform(new CircleTransform()).into(LeftFragment.imgProfilePic);
                    else
                        Picasso.with(context).load(passenger_img).transform(new CircleTransform()).into(LeftFragment.imgProfilePic);
                }
                //ivMenuIcon.setImageResource(R.drawable.menu_icon_black);
            }

        });
        slidemenu.setOnClosedListener(new SlidingMenu.OnClosedListener() {

            @Override
            public void onClosed() {
                hideKeyboard(context);
                //	openDrawer
                //ivMenuIcon.setImageResource(R.drawable.menu_icon);

            }
        });
        displayview(home, null);
       /* HomePageFragment homePageFragment = new HomePageFragment();
        getSupportFragmentManager().beginTransaction().add(R.id.content_frame, homePageFragment,"homePageFragment").addToBackStack(null) .commit();*/
        frgManager = getSupportFragmentManager();
        mTitle = mDrawerTitle = getTitle();
    }

    private GoogleApiClient buildGoogleAPIClient() {
        return new GoogleApiClient.Builder(this).addConnectionCallbacks(this).addOnConnectionFailedListener(this)
                .addApi(Plus.API, Plus.PlusOptions.builder().build()).addScope(Plus.SCOPE_PLUS_LOGIN).build();
    }

    private void setActionBar() {
        actionBar.setDisplayShowCustomEnabled(true);
        actionBar.setDisplayUseLogoEnabled(false);
        actionBar.setDisplayShowHomeEnabled(false);
        actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
       /* actionBar.setCustomView(((Activity) context).getLayoutInflater()
                        .inflate(R.layout.navdesign_xml, null),
                new ActionBar.LayoutParams(ActionBar.LayoutParams.MATCH_PARENT,
                        ActionBar.LayoutParams.MATCH_PARENT, Gravity.CENTER));
*/
        View view = ((Activity) context).getLayoutInflater().inflate(R.layout.navdesign_xml, null);
        actionBar.setCustomView(view);
        Toolbar toolbar = (Toolbar) view.getParent();
        toolbar.setContentInsetsAbsolute(0, 0);

        menu_btn = (ImageButton) actionBar.getCustomView().findViewById(R.id.menu_icon);
        adminbackbtn = (ImageButton) actionBar.getCustomView().findViewById(R.id.ic_back);
        header_title = (TextView) actionBar.getCustomView().findViewById(R.id.title_text);
        doalogtext = (TextView) actionBar.getCustomView().findViewById(R.id.doalogtext);
        home_icon = (ImageButton) actionBar.getCustomView().findViewById(R.id.home_icon);
        search_icon = (ImageButton) actionBar.getCustomView().findViewById(R.id.search_icon);
        before_edit = (ImageButton) actionBar.getCustomView().findViewById(R.id.before_edit);
        after_edit = (ImageButton) actionBar.getCustomView().findViewById(R.id.after_edit);
        ho_icon= (ImageButton) actionBar.getCustomView().findViewById(R.id.ho_icon);
        print_icon=(ImageButton)actionBar.getCustomView().findViewById(R.id.print_icon);
        ho_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                displayview(home, null);
            }
        });
        home_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowProfile_Fragment.getCity_data();

            }
        });
        search_icon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        //String title = getResources().getString("Home");
        updatedTitle("Home");
        // LinearLayout sliding_layout = (LinearLayout) actionBar.getCustomView().findViewById(R.id.sliding_layout);


        menu_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.e("clickcheck", "click");
                slidemenu.toggle();
                image_type=Session_manager.getPassengerImageType(context);
                String passenger_img = Session_manager.getPassengerimg(context);
                if (passenger_img != null && !passenger_img.equals("")) {
                    if(image_type.equals("1"))
                        Picasso.with(context).load(passenger_img).rotate(90).transform(new CircleTransform()).into(LeftFragment.imgProfilePic);
                    else
                        Picasso.with(context).load(passenger_img).transform(new CircleTransform()).into(LeftFragment.imgProfilePic);
                }
            }
        });
        adminbackbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideBackButton();
                //updatedTitle("SELECT SERVICE");

                // Selectservicefragment selectservicefragment = new Selectservicefragment();
                try {

                    Fragment myFragment = frgManager.findFragmentByTag(HomeScreenFragment.HomeTag);
                    Fragment editprofile=frgManager.findFragmentByTag(EditProfile_Fragment.tag);
                    Fragment sucessbooking=frgManager.findFragmentByTag(BookingSucess_Fragmant.tag);
                    Fragment searchscrren=frgManager.findFragmentByTag(SearchedBus_Fragment.tag);

                    if (myFragment != null && myFragment.isVisible()) {

                        finish();
                        return;
                    }
                    else if(editprofile != null && editprofile.isVisible())
                    {
                        displayview(home, null);
                        return;
                    }
                    else if(sucessbooking != null && sucessbooking.isVisible())
                    {
                        displayview(home, null);
                        return;
                    }
                    else if(searchscrren != null && searchscrren.isVisible())
                    {
                        displayview(home, null);
                        return;
                    }
                    else
                    {
                        getSupportFragmentManager().popBackStack();
                    }

                } catch (Exception e) {
                    e.printStackTrace();
                }

//                Toast.makeText(context,"hii",Toast.LENGTH_LONG).show();

            }
        });
    }
    public static void showcityname_header(Context context) {
        doalogtext.setVisibility(View.VISIBLE);
    }

    public static void hidecityname_header(Context context) {
        doalogtext.setVisibility(View.GONE);
    }
    public static void showMenuDrawerButton(Context context) {
        menu_btn.setVisibility(View.VISIBLE);
    }

    public static void hideMenuDrawerButton(Context context) {
        menu_btn.setVisibility(View.INVISIBLE);
    }

    public static void showBackButton(Context context) {
        adminbackbtn.setVisibility(View.VISIBLE);
    }

    public static void hideBackButton(Context context) {
        adminbackbtn.setVisibility(View.INVISIBLE);
    }

    public static void showSearchButton(Context context) {
        search_icon.setVisibility(View.VISIBLE);
    }

    public static void hideSearchButton(Context context) {
        search_icon.setVisibility(View.INVISIBLE);
    }

    public static void showbeforeedit(Context context) {
        before_edit.setVisibility(View.VISIBLE);
    }

    public static void hidebeforeedit(Context context) {
        before_edit.setVisibility(View.INVISIBLE);
    }

    public static void showafteredit(Context context) {
        after_edit.setVisibility(View.VISIBLE);
    }

    public static void hideafteredit(Context context) {
        after_edit.setVisibility(View.INVISIBLE);
    }

    public static void slidemenutoggle() {
        slidemenu.toggle();
    }

    public static void showlocationButton(Context context) {
        home_icon.setVisibility(View.VISIBLE);
    }

    public static void hideLoacationButton(Context context) {
        home_icon.setVisibility(View.INVISIBLE);
    }
    public static void showPrintIcon(Context context) {
        print_icon.setVisibility(View.VISIBLE);
    }

    public static void hidePrintIcon(Context context) {
        print_icon.setVisibility(View.INVISIBLE);
    }

    public static void setdialogtitle(String city) {

        doalogtext.setText(city);

    }
    public static void showhomeButton(Context context) {
        ho_icon.setVisibility(View.VISIBLE);
    }

    public static void hidehomeButton(Context context) {
        ho_icon.setVisibility(View.INVISIBLE);
    }
    public static String getdialogtitle() {

        String getcity = doalogtext.getText().toString();

        return getcity;
    }

    public static void updatedTitle(String title) {
        header_title.setText(title);
    }

    public static void hideBackButton() {
        if (adminbackbtn != null) {
            menu_btn.setVisibility(View.VISIBLE);
            adminbackbtn.setVisibility(View.INVISIBLE);

        }
    }

    public static void showBackButton() {
        if (adminbackbtn != null) {

            adminbackbtn.setVisibility(View.VISIBLE);
            menu_btn.setVisibility(View.INVISIBLE);
        }

    }

    private void startLeftMenu() {

        try {
            LeftFragment frame = new LeftFragment();
            FragmentTransaction fragmenttranjaction = getSupportFragmentManager().beginTransaction();
            fragmenttranjaction.replace(R.id.menu_frame, frame, "ContentFrame");
            fragmenttranjaction.commit();
        } catch (Exception e) {
            //android.util.Log.e("Earror  ",e.getMessage());
        }

    }

    @Override
    public void LeftMenuContentClickListner(String var1, int var2) {

    }

    @Override
    public void otherAppClick(String var1, String var2, String var3) {

    }

    @Override
    public void restartFetchingAllData() {

    }

    @Override
    public void slideMenu() {

    }

    @Override
    public void switchToDetailContentListner() {

    }

    public static void displayview(int id, Bundle b)
    {
        fragment = null;
        ft = fm.beginTransaction();
        switch (id)
        {

            case 0:


                fragment = DashFragment.getInstance(context, fm);
                if (fragment != null) {
                    ft.replace(R.id.driver_frame, fragment, DashFragment.HomeTag);
                    ft.addToBackStack(null);
                }
                ft.commit();


                break;
            case 1:

                fragment = SearchedBus_Fragment.getInstance(context, fm, b);

                if (fragment != null) {
                    ft.replace(R.id.driver_frame, fragment, "searchbus");
                    ft.addToBackStack(null);

                    ft.commit();
                }

                break;

            case 2:

                fragment = MemberShipFragment.getInstance(context, fm);

                if (fragment != null) {
                    ft.replace(R.id.driver_frame, fragment, "memberShipFragment");
                    ft.addToBackStack(null);
                    //                if (currentState != id) {
                    //                    currentState = id;
                    //                }
                    ft.commit();
                }

                break;

            case 3:

                fragment = EditProfile_Fragment.getInstance(context, fm);

                if (fragment != null) {
                    ft.replace(R.id.driver_frame, fragment, "editprofile");
                    ft.addToBackStack(null);
                    //                if (currentState != id) {
                    //                    currentState = id;
                    //                }
                    ft.commit();
                }

                break;
            case 4:

                fragment = SeatAvliablity_Fragment.getInstance(context, fm, b);

                if (fragment != null) {
                    ft.replace(R.id.driver_frame, fragment, "seatavliability");
                    ft.addToBackStack(null);
                    //                if (currentState != id) {
                    //                    currentState = id;
                    //                }
                    ft.commit();
                }
                break;


            case 5:

                fragment = Feedback_Fragment.getInstance(context, fm, b);

                if (fragment != null) {
                    ft.replace(R.id.driver_frame, fragment, "feedbackfragment");
                    ft.addToBackStack(null);
                    //                if (currentState != id) {
                    //                    currentState = id;
                    //                }
                    ft.commit();
                }
                break;

            case 6:

                fragment = ShowProfile_Fragment.getInstance(context, fm, b);

                if (fragment != null) {
                    ft.replace(R.id.driver_frame, fragment, "showprofilefragment");
                    ft.addToBackStack(null);
                    //                if (currentState != id) {
                    //                    currentState = id;
                    //                }
                    ft.commit();
                }
                break;

            case 7:

                fragment = ReferAndEarn_Fragment.getInstance(context, fm, b);

                if (fragment != null) {
                    ft.replace(R.id.driver_frame, fragment, "referandearnfragment");
                    ft.addToBackStack(null);
                    //                if (currentState != id) {
                    //                    currentState = id;
                    //                }
                    ft.commit();
                }
                break;

            case 8:

                fragment = BookingDetail_Fragment.getInstance(context, fm, b);

                if (fragment != null) {
                    ft.replace(R.id.driver_frame, fragment, "bookingdetailfragment");
                    ft.addToBackStack(null);
                    //                if (currentState != id) {
                    //                    currentState = id;
                    //                }
                    ft.commit();
                }
                break;

            case 9:

                fragment = ContactUs_Fragment.getInstance(context, fm, b);

                if (fragment != null) {
                    ft.replace(R.id.driver_frame, fragment, "contactusfragment");
                    ft.addToBackStack(null);
                    //                if (currentState != id) {
                    //                    currentState = id;
                    //                }
                    ft.commit();
                }
                break;
            case 10:

                fragment = StopDetails_Fragment.getInstance(context, fm, b);

                if (fragment != null) {
                    ft.replace(R.id.driver_frame, fragment, "stopdetils");
                    ft.addToBackStack(null);
                    //                if (currentState != id) {
                    //                    currentState = id;
                    //                }
                    ft.commit();
                }
                break;
            case 11:
                fragment = null;
                ft = fm.beginTransaction();
                fragment = WalletBalance.getInstance(context, fm, b);

                if (fragment != null) {
                    ft.replace(R.id.driver_frame, fragment, "walletTag");
                    ft.addToBackStack(null);
                    //                if (currentState != id) {
                    //                    currentState = id;
                    //                }
                    ft.commit();
                }
                break;

            case 12:

                fragment = BookCar_Bus_Fragment.getInstance(context, fm, b);

                if (fragment != null) {
                    ft.replace(R.id.driver_frame, fragment,"BookCarTag");
                    ft.addToBackStack(null);
                    //                if (currentState != id) {
                    //                    currentState = id;
                    //                }
                    ft.commit();
                }
                break;
            //
            case 13:
                fragment = SuggestRoute_Fragment.getInstance(context, fm, b);

                if (fragment != null) {
                    ft.replace(R.id.driver_frame, fragment,"SuggestRoutetag");
                    ft.addToBackStack(null);
                    //                if (currentState != id) {
                    //                    currentState = id;
                    //                }
                    ft.commit();
                }
                break;
            case 14:

                fragment = ThankYou_Fragmant.getInstance(context, fm, b);

                if (fragment != null) {
                    ft.replace(R.id.driver_frame, fragment,"thankyou");
                    ft.addToBackStack(null);
                    //                if (currentState != id) {
                    //                    currentState = id;
                    //                }
                    ft.commit();
                }
                break;

            case 15:

                fragment = RecentTransection_Fragment.getInstance(context, fm, b);

                if (fragment != null) {
                    ft.replace(R.id.driver_frame, fragment,"recenttrasection");
                    ft.addToBackStack(null);
                    //                if (currentState != id) {
                    //                    currentState = id;
                    //                }
                    ft.commit();
                }
                break;

            case 16:

                fragment = BookingSucess_Fragmant.getInstance(context, fm, b);

                if (fragment != null) {
                    ft.replace(R.id.driver_frame, fragment,"sucess_booking");
                    ft.addToBackStack(null);
                    //                if (currentState != id) {
                    //                    currentState = id;
                    //                }
                    ft.commit();
                }
                break;

            case 17:

                fragment = BookingHistory_Fragmant.getInstance(context, fm, b);

                if (fragment != null) {
                    ft.replace(R.id.driver_frame, fragment,"bookinghistory");
                    ft.addToBackStack(null);
                    //                if (currentState != id) {
                    //                    currentState = id;
                    //                }
                    ft.commit();
                }
                break;
            case 18:

                fragment = CurrentBookingDetail_Fragment.getInstance(context, fm, b);

                if (fragment != null) {
                    ft.replace(R.id.driver_frame, fragment,"curruntbooking");
                    ft.addToBackStack(null);
                    //                if (currentState != id) {
                    //                    currentState = id;
                    //                }
                    ft.commit();
                }
                break;

            case 19:

            fragment = HistoryBookignDetail_Fragment.getInstance(context, fm, b);

            if (fragment != null) {
                ft.replace(R.id.driver_frame, fragment,"historybooking");
                ft.addToBackStack(null);

                ft.commit();
            }
            break;

            case 20:

                fragment = Kruze_Play_Fragment.getInstance(context, fm, b);

                if (fragment != null) {
                    ft.replace(R.id.driver_frame, fragment,"kruzeplay");
                    ft.addToBackStack(null);

                    ft.commit();
                }
                break;//trackride

            case 21:

            fragment = TrackRide_Fragment.getInstance(context, fm, b);

            if (fragment != null) {
                ft.replace(R.id.driver_frame, fragment,"trackride");
                ft.addToBackStack(null);

                ft.commit();
            }
            break;
            //notification
            case 22:

                fragment = NotiFication_Fragment.getInstance(context, fm, b);

                if (fragment != null) {
                    ft.replace(R.id.driver_frame, fragment,"notification");
                    ft.addToBackStack(null);

                    ft.commit();
                }
                break;
            case 23:

                fragment = RewardsPoint_Fragment.getInstance(context, fm, b);

                if (fragment != null) {
                    ft.replace(R.id.driver_frame, fragment,"Rewards_Points");
                    ft.addToBackStack(null);

                    ft.commit();
                }
                break;//
            case 24:

                fragment = SharePoint_Fragmant.getInstance(context, fm, b);

                if (fragment != null) {
                    ft.replace(R.id.driver_frame, fragment,"Share_Points");
                    ft.addToBackStack(null);

                    ft.commit();
                }
                break;//faq
            case 25:

                fragment = FAQ_Fragmant.getInstance(context, fm, b);

                if (fragment != null) {
                    ft.replace(R.id.driver_frame, fragment,"faq");
                    ft.addToBackStack(null);

                    ft.commit();
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        try {

            Fragment myFragment = frgManager.findFragmentByTag(HomeScreenFragment.HomeTag);
            Fragment editprofile=frgManager.findFragmentByTag(EditProfile_Fragment.tag);
            Fragment sucessbooking=frgManager.findFragmentByTag(BookingSucess_Fragmant.tag);
            Fragment searchscrren=frgManager.findFragmentByTag(SearchedBus_Fragment.tag);
            Fragment booking_history=frgManager.findFragmentByTag(BookingHistory_Fragmant.tag);

            if (myFragment != null && myFragment.isVisible()) {

                finish();
                return;
            }
            else if(editprofile != null && editprofile.isVisible())
            {
                displayview(home, null);
                return;
            }
            else if(sucessbooking != null && sucessbooking.isVisible())
            {
                displayview(home, null);
                return;
            }
            else if(searchscrren != null && searchscrren.isVisible())
            {
                displayview(home, null);
                return;
            }
            else if(booking_history != null && booking_history.isVisible())
            {
                displayview(home, null);
                return;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        super.onBackPressed();
    }

    @Override
    public void onConnected(Bundle bundle) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {
        if (!connectionResult.hasResolution()) {
            GooglePlayServicesUtil.getErrorDialog(connectionResult.getErrorCode(), this, ERROR_DIALOG_REQUEST_CODE).show();
            return;
        }
    }

    @Override
    protected void onStart() {

        super.onStart();
        mGoogleApiClient.connect();
    }

    public static void logoutGoogle() {
        if (mGoogleApiClient.isConnected()) {
            Plus.AccountApi.clearDefaultAccount(mGoogleApiClient);

            mGoogleApiClient.disconnect();
            SigninActivity.signOutFromGplus();


        }

    }
    protected void onNewIntent(Intent intent) {

        String userId = Session_manager.getemail_pref(context);
        int extraString = intent.getIntExtra("notificationId", 0);
        Bundle extra = intent.getExtras();
        if (extra != null) {
            int notificationId = extra.getInt("notificationId");

            if (userId != null) {
                if (notificationId != 0) {
                    displayview(notification,null);
                } else {

                }
            } else {

                Intent intent1 = new Intent(context, SigninActivity.class);
                startActivity(intent1);
                finish();
            }
        }
        super.onNewIntent(intent);
    }

    @Override
    protected void onResume() {
        String userId = Session_manager.getemail_pref(context);
        int extraString = getIntent().getIntExtra("notificationId", 1);
        Bundle extra = getIntent().getExtras();
        if (extra != null) {
            int notificationId = extra.getInt("notificationId");

            if (userId != null)
            {
                if (notificationId != 0) {
                    displayview(notification, null);
                }

            } else {
                finish();
                Intent intent1 = new Intent(context, SigninActivity.class);
                startActivity(intent1);
            }
        }
        super.onResume();
    }
    public void hideKeyboard(Context mContext){
      /*  InputMethodManager inputMethodManager = (InputMethodManager) context
                .getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);*/

        InputMethodManager inputManager = (InputMethodManager) context.getSystemService(
                Context.INPUT_METHOD_SERVICE);
        View focusedView = DashBoardActivity.this.getCurrentFocus();
        if (focusedView != null) {
            inputManager.hideSoftInputFromWindow(focusedView.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
}