package com.journepassenger.activity;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.IntentSender;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.journedriver.activity.SigninDriverActivity;
import com.journepassenger.utilities.ConnectionDetector;
import com.journepassenger.utilities.GlobalValues;
import com.journepassenger.utilities.MethodManager_Listner;
import com.journepassenger.utilities.PlayServicesHelper;
import com.journepassenger.utilities.Session_manager;
import com.journyapp.journyapp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by administrator on 22/1/16.
 */

//AIzaSyCUvYjHCPXHqdOo4_u-pB59-V2Hv6GvecE
public class SigninActivity extends Activity implements View.OnClickListener,GoogleApiClient.ConnectionCallbacks,GoogleApiClient.OnConnectionFailedListener

{

    private CallbackManager callbackManager;
    EditText edEmailId_signin, edEmailId_forgotpassword;
    EditText edPasswordId_signin;
    ProgressDialog pdialog;
    TextView forgotpass_signin;
    TextView login_button_signin;
    TextView signup_button_signin;
    TextView facebook_btn;
    TextView google_btn;
    private boolean mIntentInProgress;
    private static final int RC_SIGN_IN = 0;
    public static boolean mSignInClicked;
    private static final int SIGN_IN_REQUEST_CODE = 10;
    public static GoogleApiClient mGoogleApiClient;
    private RadioGroup radioGroup;
    private RadioButton radioButton;
    TextView driverlogin;
    Context mContext;
    String device_token;
    String P_name, P_email, P_id, P_imgurl, qr_url,passenger_referral_code,passenger_mobile,comapny_name;
    public static boolean signedInUser;
    private static final int ERROR_DIALOG_REQUEST_CODE = 11;
    private ConnectionResult mConnectionResult;
    static LoginManager mLoginManager;
    int login_status;
    public static int check;
    String passenger_image_mode;


    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        requestWindowFeature(Window.FEATURE_NO_TITLE);
       // getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(this.getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        setContentView(R.layout.signin_passenger_xml);


        mGoogleApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this).addApi(Plus.API, Plus.PlusOptions.builder().build())
                .addScope(Plus.SCOPE_PLUS_LOGIN).build();
                 mGoogleApiClient.connect();

        mContext = this;
        edEmailId_signin = (EditText) findViewById(R.id.edEmailId_signin);
        edPasswordId_signin = (EditText) findViewById(R.id.edPasswordId_signin);
        forgotpass_signin = (TextView) findViewById(R.id.forgotpass_signin);
        login_button_signin = (TextView) findViewById(R.id.submit_button_signin);
        signup_button_signin = (TextView) findViewById(R.id.signup_button_signin);
        facebook_btn = (TextView) findViewById(R.id.facebook_btn);
        google_btn = (TextView) findViewById(R.id.google_btn);
        //radioGroup=(RadioGroup)findViewById(R.id.radiogroup);
        driverlogin = (TextView) findViewById(R.id.driverlogin);
        PlayServicesHelper playServiceHelper = new PlayServicesHelper(SigninActivity.this);
        device_token = playServiceHelper.getRegistrationId();
        Log.e("device_token",""+device_token);
        google_btn.setOnClickListener(this);
        driverlogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent in = new Intent(SigninActivity.this, SigninDriverActivity.class);
                startActivity(in);
            }
        });
        login_button_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {

                if (!isValidEmail(edEmailId_signin.getText().toString()))
                {
                    edEmailId_signin.setError("Invalid email");
                }
                else if (edPasswordId_signin.getText().toString().equals("")) {
                    Toast.makeText(mContext, "Password can't Blank", Toast.LENGTH_LONG).show();
                }
                else if (ConnectionDetector.isNetworkAvailable(mContext))
                {
                    //Toast.makeText(mContext,"Internet is Ok",Toast.LENGTH_LONG).show();
                    showprogress();
                    CheckUserLogin();
                }
                else
                {
                    Toast.makeText(mContext,"You don't have Internet Connection",Toast.LENGTH_LONG).show();

                }
            }
        });

        forgotpass_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                final Dialog dialog = new Dialog(mContext);
                LayoutInflater li = (LayoutInflater) mContext.getApplicationContext()
                        .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                View vi = li.inflate(R.layout.forgot_password, null, false);
                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                edEmailId_forgotpassword=(EditText)vi.findViewById(R.id.edEmailId_forgotpassword);
                TextView btnDone=(TextView)vi.findViewById(R.id.btnDone);
                TextView btnCancel=(TextView)vi.findViewById(R.id.btnCancel);

                btnCancel.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        dialog.dismiss();
                    }
                });
                btnDone.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (edEmailId_forgotpassword.getText().toString().equals("")) {
                            Toast.makeText(mContext, "Email Id Can't be Blank", Toast.LENGTH_LONG).show();
                        }
                         else
                        {
                            PasswordRequest();
                            dialog.dismiss();
                        }

                    }
                });
//                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                dialog.setContentView(vi);
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
            }
        });

        signup_button_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (ConnectionDetector.isNetworkAvailable(mContext))
                {
                    Intent intent = new Intent(SigninActivity.this, SignUpActivity.class);
                    startActivity(intent);
                    finish();
                }

                else
                {
                    Toast.makeText(mContext,"You don't have Internet Connection",Toast.LENGTH_LONG).show();

                }

            }
        });


        /*google_btn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                googlePlusLogin();
            }
        });*/

        facebook_btn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {


                LoginManager.getInstance().logInWithReadPermissions(SigninActivity.this, Arrays.asList("public_profile", "user_friends","email"));
                //Toast.makeText(getApplicationContext(),"hello",Toast.LENGTH_LONG).show();
                LoginManager.getInstance().registerCallback(callbackManager,
                        new FacebookCallback<LoginResult>() {
                            @Override
                            public void onSuccess(LoginResult loginResult) {
                                RequestData();


                            }

                            @Override
                            public void onCancel() {
                                // App code
                            }

                            @Override
                            public void onError(FacebookException exception) {
                                // App code
                            }
                        });
            }


        });
    }



    @Override
    public void onConnected(Bundle bundle)
    {

        if (!mGoogleApiClient.isConnecting()) {
            mSignInClicked = true;
            Toast.makeText(getApplicationContext(), "Signed In Successfully", Toast.LENGTH_LONG).show();

            processUserInfoAndUpdateUI();


        }
    }



    @Override
    public void onConnectionSuspended(int i)
    {
        mGoogleApiClient.connect();
    }

    @Override
    public void onClick(View v)
    {
        processSignIn();
    }
    @Override
    protected void onStart() {

        super.onStart();
        mGoogleApiClient.connect();
    }

    private void processSignIn() {

        if (!mGoogleApiClient.isConnecting()) {
            processSignInError();
            mSignInClicked = true;
        }

    }
    @Override
    public void onConnectionFailed(ConnectionResult connectionResult)
    {
        if (!connectionResult.hasResolution()) {
            GooglePlayServicesUtil.getErrorDialog(connectionResult.getErrorCode(), this, ERROR_DIALOG_REQUEST_CODE).show();
            return;
        }
        if (!mIntentInProgress) {
            mConnectionResult = connectionResult;

            if (mSignInClicked) {
                processSignInError();
            }
        }

    }
    /*private void googlePlusLogin() {
        if (!mGoogleApiClient.isConnecting()) {
            signedInUser = true;

            resolveSignInError();
            //check=1;
        }
    }*/

    private void processUserInfoAndUpdateUI() {
        Person signedInUser = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
        if (signedInUser != null) {

            if (signedInUser.hasDisplayName()) {
                String userName = signedInUser.getDisplayName();
                String G_socialID = signedInUser.getId();
                Log.e("G_socialID",G_socialID+"");
                String email = Plus.AccountApi.getAccountName(mGoogleApiClient);

                login_status=2;
                //Session_manager.setlogin_status(mContext, login_status);
                //sendUserSocial_info(userName, email, 3, G_socialID);
                Intent intent=new Intent(SigninActivity.this,SignUpActivity.class);
                intent.putExtra("name",userName);
                intent.putExtra("email", email);
                intent.putExtra("login_status",login_status);
                startActivity(intent);
                finish();

            }

            }
    }
    /*private void getProfileInformation() {
        try {
            if (Plus.PeopleApi.getCurrentPerson(mGoogleApiClient) != null) {
                Person currentPerson = Plus.PeopleApi.getCurrentPerson(mGoogleApiClient);
                Log.e("currentPerson...", "" + currentPerson);
                String personName = currentPerson.getDisplayName();
                String personPhotoUrl = currentPerson.getImage().getUrl();
                Log.e("personPhotoUrl...", "" + personPhotoUrl);
                String email = Plus.AccountApi.getAccountName(mGoogleApiClient);
                Log.e("email...", "" + email);
                login_status=1;
                Session_manager.setlogin_status(mContext,login_status);
                Intent intent=new Intent(SigninActivity.this,SignUpActivity.class);
                intent.putExtra("name",personName);
                intent.putExtra("email", email);
                startActivity(intent);
                finish();

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/
    @Override
    protected void onStop() {
        super.onStop();
        if (mGoogleApiClient.isConnected()) {
           // mGoogleApiClient.disconnect();
        }
    }
    /*private void resolveSignInError() {
        if (mConnectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                mConnectionResult.startResolutionForResult(this, RC_SIGN_IN);
            } catch (IntentSender.SendIntentException e) {
                mIntentInProgress = false;
                mGoogleApiClient.connect();
            }
        }
    }*/
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);

        if (requestCode == SIGN_IN_REQUEST_CODE) {
            if (resultCode != RESULT_OK) {
                mSignInClicked = false;
            }

            mIntentInProgress = false;

            if (!mGoogleApiClient.isConnecting()) {
                mGoogleApiClient.connect();
            }
        }
    }
    public void CheckUserLogin() {
        PlayServicesHelper playServiceHelper = new PlayServicesHelper(SigninActivity.this);
        device_token = playServiceHelper.getRegistrationId();

        Map<String, Object> params = new HashMap<String, Object>();
        params.put("passenger_email", edEmailId_signin.getText().toString());
        params.put("passenger_password", edPasswordId_signin.getText().toString());
        params.put("device_token", device_token);

        GlobalValues.getMethodManagerObj(mContext).usersignin(params, new MethodManager_Listner() {
            @Override
            public void onError() {
                hideprogress();
            }

            //passenger_name//passenger_email
            @Override
            public void onSuccess(String json) {
                Log.e("signin json", json.toString());
                try {
                    JSONObject jobject = new JSONObject(json);
                    String msg = jobject.getString("status");

                    if (msg.equals("false")) {
                        Toast.makeText(mContext, jobject.getString("msg"), Toast.LENGTH_LONG).show();
                        hideprogress();
                    } else if (msg.equals("true")) {
                        Toast.makeText(mContext, jobject.getString("msg"), Toast.LENGTH_LONG).show();

                        P_name = jobject.optString("passenger_name");
                        P_email = jobject.optString("passenger_email");
                        P_id = jobject.optString("passenger_id");
                        P_imgurl = jobject.optString("passenger_image");
                        qr_url = jobject.optString("passenger_qrcode_image");
                        passenger_referral_code = jobject.optString("passenger_referral_code");
                        passenger_mobile = jobject.optString("passenger_mobile");
                        comapny_name=jobject.optString("comapny_name");
                        passenger_image_mode=jobject.optString("image_type");
                        hideprogress();
                        Session_manager.saveUserInfo(mContext, P_name, P_email, P_id, P_imgurl, qr_url, passenger_referral_code, passenger_mobile,comapny_name,passenger_image_mode);
                        Session_manager.saveloginData(mContext, P_email  /*edEmailId_signin.getText().toString()*/, "Passenger");
                        Intent intent = new Intent(mContext, DashBoardActivity.class);
                        startActivity(intent);
                        finish();
                    }


                } catch (JSONException e) {
                    hideprogress();
                    e.printStackTrace();
                }

            }
        });
    }
    public void RequestData() {
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {

                JSONObject json = response.getJSONObject();
                Log.e("fbjson",json.toString());
                try {
                    if (json != null) {
                        String text = "<b>Name :</b> " + json.getString("name") + "<br><br><b>Email :</b> " + json.getString("email") + "<br><br><b>Profile link :</b> " + json.getString("link");
                        //Toast.makeText(getApplicationContext(),""+text,Toast.LENGTH_LONG).show();
                        String name = json.getString("name");
                        String id=json.getString("id");
                        String email = json.getString("email");
                        login_status=1;

                        //sendUserSocial_info(name,email,2,id);
                        Intent intent = new Intent(SigninActivity.this, SignUpActivity.class);
                        intent.putExtra("name", name);
                        intent.putExtra("email", email);
                        intent.putExtra("login_status",login_status);
                        startActivity(intent);
                        finish();

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "id,name,link,email,picture");
        request.setParameters(parameters);
        request.executeAsync();
    }
    public void PasswordRequest()
    {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("email", edEmailId_forgotpassword.getText().toString());
        GlobalValues.getMethodManagerObj(mContext).Forgot_password(params, new MethodManager_Listner() {
            @Override
            public void onError() {

            }

            @Override
            public void onSuccess(String json) {
                JSONObject jobject = null;
                try {
                    jobject = new JSONObject(json);
                    String msg = jobject.getString("msg");
                    Toast.makeText(mContext, "" + msg, Toast.LENGTH_LONG).show();
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });

    }
    public static void signOutFromGplus() {

        mSignInClicked = false;

    }
    public void showprogress() {
        pdialog = new ProgressDialog(mContext);
        pdialog.setCancelable(false);
        pdialog.setCanceledOnTouchOutside(false);
        pdialog.setMessage("Loading...");
        pdialog.show();
    }

    public void hideprogress() {
        if (pdialog != null) {
            pdialog.dismiss();
        }
    }
    private void processSignInError() {
        if (mConnectionResult.hasResolution()) {
            try {
                mIntentInProgress = true;
                mConnectionResult.startResolutionForResult(this, SIGN_IN_REQUEST_CODE);
            } catch (IntentSender.SendIntentException e) {
                mIntentInProgress = false;
                mGoogleApiClient.connect();
            }
        }

    }
    public void sendUserSocial_info(final String name, final String email, int social_type, String id)
    {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("email",email);
        params.put("social_type",social_type);
        params.put("social_id",id);

        GlobalValues.getMethodManagerObj(mContext).SendSocial_data(params, new MethodManager_Listner() {
            @Override
            public void onError() {

            }

            @Override
            public void onSuccess(String json) {
              Log.e("Social json responce",json.toString());

                try {
                    JSONObject jsonObject=new JSONObject(json);
                    String social_status=jsonObject.getString("social_status");
                    if(social_status.equals("false"))
                    {

                    }
                    else
                    {
                        if(social_status.equals("true"))
                        {
                            final Dialog dialog = new Dialog(mContext);
                            LayoutInflater li = (LayoutInflater) mContext.getApplicationContext()
                                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            View vi = li.inflate(R.layout.checksocialpassword, null, false);
                            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                           final EditText ed_checkPassword=(EditText)vi.findViewById(R.id.ed_checkPassword);
                            TextView btnDone=(TextView)vi.findViewById(R.id.btnDone);
                            TextView btnCancel=(TextView)vi.findViewById(R.id.btnCancel);

                            btnCancel.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    dialog.dismiss();
                                }
                            });
                            btnDone.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    if (ed_checkPassword.getText().toString().equals("")) {
                                        Toast.makeText(mContext, "Password Can't be Blank", Toast.LENGTH_LONG).show();
                                    }
                                    else
                                    {
                                        PasswordRequest();
                                        dialog.dismiss();
                                    }

                                }
                            });
//                dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
                            dialog.setContentView(vi);
                            dialog.setCanceledOnTouchOutside(false);
                            dialog.show();
                        }
                        }

                }
                catch (JSONException e)
                {
                    e.printStackTrace();
                }
            }
        });
    }
    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

}
