package com.journepassenger.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.journepassenger.utilities.GlobalValues;
import com.journepassenger.utilities.MethodManager_Listner;
import com.journepassenger.utilities.Session_manager;
import com.journyapp.journyapp.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;



/**
 * Created by administrator on 27/1/16.
 */
public class OtpActivity extends Activity
{
    Button backbtn;
    TextView mob_no;
    EditText otp_edit;

    TextView submit_button_otp;
    TextView resend_button_otp;
    String user_mobno;
    String varification_code="123456";
    Context mContext;
    ProgressDialog pdialog;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.otpactivity);
        mContext=this;
        //backbtn=(Button)findViewById(R.id.backbtn);
        mob_no=(TextView)findViewById(R.id.mob_no);
        otp_edit=(EditText)findViewById(R.id.otp_edit);
        submit_button_otp=(TextView)findViewById(R.id.submit_button_otp);
        resend_button_otp=(TextView)findViewById(R.id.resend_button_otp);
         //DashBoardActivity.updatedTitle("Create Account");
        Intent intent=getIntent();
        user_mobno=intent.getStringExtra("mob_no");

        mob_no.setText("to +"+user_mobno);

        submit_button_otp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String getno=mob_no.getText().toString();
                if (getno.length()==0)
                {
                    Toast.makeText(getApplicationContext(),"Please Enter Varification Code",Toast.LENGTH_LONG).show();
                }
                else
                {
                    showprogress();
                    getvarificationResponce();
                }
            }
        });

    }
    public  void getvarificationResponce()
    {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("passenger_mobile",user_mobno);
        params.put("passenger_verification_code",varification_code);

        GlobalValues.getMethodManagerObj(mContext).uservarification(params, new MethodManager_Listner() {
            @Override
            public void onError() {

            }

            @Override
            public void onSuccess(String json)
            {
                try {
                    JSONObject object=new JSONObject(json);
                    String token=object.getString("device_token");
                    String msg=object.getString("msg");
                    Session_manager.saveDeviceToen(mContext, token);
                    Log.e("Otp responce", json.toString());
                    hideprogress();
                    if (msg.equals("verification successfully"))
                    {
                        Toast.makeText(mContext,"Verification code is sent to your registered mobile",Toast.LENGTH_LONG).show();
                         Intent intent=new Intent(mContext,DashBoardActivity.class);
                         startActivity(intent);
                         finish();
                    }
                    else
                    {
                        Log.e("Otp responce", json.toString());
                        Toast.makeText(mContext,""+msg,Toast.LENGTH_LONG).show();
                    }
                }
                catch (JSONException e)
                {
                    hideprogress();
                    e.printStackTrace();
                }

            }
        });
    }
    public void showprogress() {
        pdialog = new ProgressDialog(mContext);
        pdialog.setCancelable(false);
        pdialog.setCanceledOnTouchOutside(false);
        pdialog.setMessage("Loading...");
        pdialog.show();
    }

    public void hideprogress() {
        if (pdialog != null) {
            pdialog.dismiss();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
}
