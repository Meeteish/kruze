package com.journepassenger.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.graphics.Typeface;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.webkit.WebView;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.journepassenger.adapter.Citylist_Adapter;
import com.journepassenger.bean.Company_Bean;
import com.journepassenger.utilities.GlobalValues;
import com.journepassenger.utilities.ImageDecoder;
import com.journepassenger.utilities.MethodManager_Listner;
import com.journepassenger.utilities.PlayServicesHelper;
import com.journepassenger.utilities.RoundedImage;
import com.journepassenger.utilities.Session_manager;
import com.journepassenger.utilities.Webservices;
import com.journyapp.journyapp.R;

import org.apache.http.entity.mime.content.FileBody;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * Created by administrator on 22/1/16.
 */
public class SignUpActivity extends Activity {
    private static final int CAMERA_REQUEST = 11;
    private static final int SELECT_FILE = 12;
    static public Context mContext;
    public static Bitmap imageBitmap;
    static Spinner spinnerCompany;
    static String fileName = null;
    static ArrayList<Company_Bean> companylist;
    static String company_id_position = "";
    String user_name;
    String email_id;
    int login_status;
    String passenger_type = "1";
    EditText edFullNameId, edEmailId, edPasswordId, edPasswordConfrimId, edTelephoneId, edEmployeeId,edrefrel_code;
    TextView signup_button;
    String regId;
    PlayServicesHelper playservicehelper;
    TextView terms_textView;
    TextView login_text;
    ImageView imgCheckboxActive, imgCheckboxInactive, imgprofileId, imgcameraId;
    ImageView correctPasswordTick, confirmPasswordTick;
    RadioButton rbCompany, rbIndividual;
    int checkTermsCondition = 0;
    String absPath;
    String picturePath;
    Bitmap image;
    String filePath = null;
    int rotate;
    int currentVersion = Build.VERSION.SDK_INT;
    Uri cameraImagePath;
    File f;
    String P_name;
    String P_email;
    String P_id, P_imgurl, qr_url, passenger_referral_code, passenger_mobile,comapny_name;
    ProgressDialog pdialog;
    LinearLayout changeCompany;
    View viewEmpId;
    Typeface font;
    String passenger_image_mode;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        //getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.signup_passanger);
        mContext = this;

        edFullNameId = (EditText) findViewById(R.id.edFullNameId);
        edEmailId = (EditText) findViewById(R.id.edEmailId);
        edPasswordId = (EditText) findViewById(R.id.edPasswordId);
        edPasswordConfrimId = (EditText) findViewById(R.id.edPasswordConfrimId);
        edTelephoneId = (EditText) findViewById(R.id.edTelephoneId);
        spinnerCompany = (Spinner) findViewById(R.id.spinnerCompany);
        edEmployeeId = (EditText) findViewById(R.id.edEmployeeId);
        rbIndividual = (RadioButton) findViewById(R.id.rbIndividual);
        rbCompany = (RadioButton) findViewById(R.id.rbCompany);
        changeCompany = (LinearLayout) findViewById(R.id.changeCompany);
        viewEmpId = (View) findViewById(R.id.viewEmpId);
        signup_button = (TextView) findViewById(R.id.signup_button);
        terms_textView = (TextView) findViewById(R.id.terms_textView);
        login_text = (TextView) findViewById(R.id.login_text);
        imgCheckboxInactive = (ImageView) findViewById(R.id.imgCheckboxInactiveId);
        imgCheckboxActive = (ImageView) findViewById(R.id.imgCheckboxActiveId);
        imgprofileId = (ImageView) findViewById(R.id.imgprofileId);
        imgcameraId = (ImageView) findViewById(R.id.cameraicon);
        correctPasswordTick = (ImageView) findViewById(R.id.correctPasswordTick);
        confirmPasswordTick = (ImageView) findViewById(R.id.confirmPasswordTick);
        edrefrel_code= (EditText) findViewById(R.id.edrefrel_code);
        companylist = new ArrayList<Company_Bean>();
        Intent intent = getIntent();

        getCompany_data();

        font = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Light.ttf");
        rbCompany.setTypeface(font);
        rbIndividual.setTypeface(font);

        user_name = intent.getStringExtra("name");
        email_id = intent.getStringExtra("email");
        login_status=intent.getIntExtra("login_status",0);
        edFullNameId.setText(user_name);
        edEmailId.setText(email_id);

        rbIndividual.setChecked(true);
        edEmployeeId.setVisibility(View.GONE);
        changeCompany.setVisibility(View.GONE);
        viewEmpId.setVisibility(View.GONE);


        spinnerCompany.setEnabled(false);
        spinnerCompany.setFocusable(false);
        spinnerCompany.setFocusableInTouchMode(false);
        spinnerCompany.setClickable(false);

        edEmployeeId.setFocusable(false);
        edEmployeeId.setFocusableInTouchMode(false);
        edEmployeeId.setClickable(false);

        rbIndividual.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                passenger_type = "1";

                edEmployeeId.setVisibility(View.GONE);
                changeCompany.setVisibility(View.GONE);
                viewEmpId.setVisibility(View.GONE);

                spinnerCompany.setSelection(0);
                company_id_position = "";
                edEmployeeId.setText("");

                spinnerCompany.setEnabled(false);
                spinnerCompany.setFocusable(false);
                spinnerCompany.setFocusableInTouchMode(false);
                spinnerCompany.setClickable(false);

                edEmployeeId.setFocusable(false);
                edEmployeeId.setFocusableInTouchMode(false);
                edEmployeeId.setClickable(false);


                //Toast.makeText(mContext,passenger_type,Toast.LENGTH_SHORT).show();

            }
        });

        rbCompany.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                edEmployeeId.setVisibility(View.VISIBLE);
                changeCompany.setVisibility(View.VISIBLE);
                viewEmpId.setVisibility(View.VISIBLE);
                passenger_type = "2";

                spinnerCompany.setEnabled(true);
                spinnerCompany.setFocusable(true);
                spinnerCompany.setFocusableInTouchMode(true);
                spinnerCompany.setClickable(true);

                edEmployeeId.setFocusable(true);
                edEmployeeId.setFocusableInTouchMode(true);
                edEmployeeId.setClickable(true);

                //Toast.makeText(mContext,passenger_type,Toast.LENGTH_SHORT).show();


            }
        });

        PlayServicesHelper playServiceHelper = new PlayServicesHelper(SignUpActivity.this);
        regId = playServiceHelper.getRegistrationId();


        imgcameraId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                try {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showDialogForImageSelect();
                        }
                    });
                } catch (Exception e) {

                }
            }
        });


        imgCheckboxInactive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgCheckboxActive.setVisibility(View.VISIBLE);
                imgCheckboxInactive.setVisibility(View.INVISIBLE);
                checkTermsCondition = 1;
//                Toast.makeText(mContext,""+checkTermsCondition,Toast.LENGTH_LONG).show();
            }
        });

        imgCheckboxActive.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgCheckboxInactive.setVisibility(View.VISIBLE);
                imgCheckboxActive.setVisibility(View.INVISIBLE);
                checkTermsCondition = 0;
//                Toast.makeText(mContext,""+checkTermsCondition,Toast.LENGTH_LONG).show();
            }
        });

        terms_textView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ShowTerms_Dialog();
            }
        });
        login_text.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getApplicationContext(), SigninActivity.class);
                intent.putExtra("mob_no", edTelephoneId.getText().toString());
                startActivity(intent);
                finish();
            }
        });

        edPasswordId.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                if (edPasswordId.getText().toString().equals(edPasswordConfrimId.getText().toString()) && edPasswordId.getText().toString().length() != 0) {
                    correctPasswordTick.setVisibility(View.VISIBLE);
                    confirmPasswordTick.setVisibility(View.VISIBLE);
                } else {
                    correctPasswordTick.setVisibility(View.INVISIBLE);
                    confirmPasswordTick.setVisibility(View.INVISIBLE);
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });

        edPasswordConfrimId.addTextChangedListener(new TextWatcher() {
            public void afterTextChanged(Editable s) {
                if (edPasswordId.getText().toString().equals(edPasswordConfrimId.getText().toString()) && edPasswordId.getText().toString().length() != 0) {
                    correctPasswordTick.setVisibility(View.VISIBLE);
                    confirmPasswordTick.setVisibility(View.VISIBLE);
                } else {
                    correctPasswordTick.setVisibility(View.INVISIBLE);
                    confirmPasswordTick.setVisibility(View.INVISIBLE);
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void onTextChanged(CharSequence s, int start, int before, int count) {
            }
        });


        signup_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (edFullNameId.getText().toString().length() == 0) {
                    edFullNameId.setError("Name can not be blank");

                } else if (!isValidEmail(edEmailId.getText().toString())) {
                    edEmailId.setError("Invalid email");
                } else if (edPasswordId.getText().toString().length() == 0) {
                    edPasswordId.setError("Password can not be blank");
                } else if (!edPasswordId.getText().toString().equals(edPasswordConfrimId.getText().toString())) {
                    edPasswordConfrimId.setError("Password not matched");
                } else if (edTelephoneId.getText().toString().length() < 8 || edTelephoneId.getText().toString().length() > 13) {
                    edTelephoneId.setError("Check PhoneNo");
                } else if (passenger_type == "2" && company_id_position == "-1") {
                    Toast.makeText(mContext, "Please select Company", Toast.LENGTH_SHORT).show();
                } else if (passenger_type == "2" && edEmployeeId.getText().toString().length() == 0) {
                    edEmployeeId.setError("Employee Id can not be blank");
                } else if (checkTermsCondition == 0) {
                    Toast.makeText(mContext, "Accept terms & Conditions", Toast.LENGTH_SHORT).show();
                } else {
                    // Toast.makeText(mContext,"Done",Toast.LENGTH_LONG).show();
                    showprogress();
                    sendSignup_data();
                }
            }
        });


    }

    public void checkPasswords() {


    }

    public void check() {
        if (spinnerCompany.getSelectedItem().toString().length() == 0) {
//            spinnerCompany.setError("Company can not be blank");
        } else if (edEmployeeId.getText().toString().length() == 0) {
            edEmployeeId.setError("Employee Id can not be blank");
        }

    }

    public void sendSignup_data() {
        Map<String, Object> params = new HashMap<String, Object>();

        params.put("passenger_name", edFullNameId.getText().toString());
        params.put("passenger_email", edEmailId.getText().toString());
        params.put("passenger_password", edPasswordId.getText().toString());
        params.put("passenger_mobile", edTelephoneId.getText().toString());
        params.put("device_token", regId);
        params.put("device_type", "1");
        params.put("passenger_type", passenger_type);
        params.put("referal_code",edrefrel_code.getText().toString());
        if (company_id_position == "-1") {
            params.put("passenger_company", "");
        } else {
            params.put("passenger_company", company_id_position);
        }

        params.put("passenger_emp_id", edEmployeeId.getText().toString());

      /*  f = new File(filePath);
        if (f!=null) {
            params.put("passenger_image", f);
        }*/

        if (filePath != null) {

            File f = new File(filePath);
            if (f != null) {
                FileBody filebody = new FileBody(f, "image/jpeg");

                params.put("passenger_image", f);

                // multipart.addPart("user_image", filebody);
            }
        }
        GlobalValues.getMethodManagerObj(mContext).UserSignup(params, new MethodManager_Listner() {
            @Override
            public void onSuccess(String json) {
                Log.e("signup json", json.toString());

                try {
                    JSONObject jsonObject = new JSONObject(json);
                    String status = jsonObject.getString("status");

                    //passenger_name//passenger_email

                    if (status.equals("false")) {
                        Toast.makeText(mContext, jsonObject.getString("msg").toString(), Toast.LENGTH_LONG).show();
                        hideprogress();
                    } else if (status.equals("true")) {
                        hideprogress();
                        P_name = jsonObject.optString("passenger_name");
                        P_email = jsonObject.optString("passenger_email");
                        P_id = jsonObject.optString("passenger_id");
                        P_imgurl = jsonObject.optString("passenger_image");
                        qr_url = jsonObject.optString("passenger_qrcode_image");
                        passenger_referral_code = jsonObject.optString("passenger_referral_code");
                        passenger_mobile = jsonObject.optString("passenger_mobile");
                        comapny_name=jsonObject.optString("passenger_company");
                        passenger_image_mode=jsonObject.optString("image_type");

                        Session_manager.saveUserInfo(mContext, P_name, P_email, P_id, P_imgurl, qr_url, passenger_referral_code, passenger_mobile,comapny_name,passenger_image_mode);
                        Session_manager.saveloginData(mContext, P_email, "Passenger");
                        Session_manager.setlogin_status(mContext, login_status);
                        Intent intent = new Intent(getApplicationContext(), OtpActivity.class);
                        intent.putExtra("mob_no", edTelephoneId.getText().toString());
                        startActivity(intent);
                        finish();
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                    hideprogress();
                }

            }

            @Override
            public void onError() {
                Log.e("signup error", "error");
                hideprogress();
            }

        });

    }
    public static void getCompany_data() {
        GlobalValues.getMethodManagerObj(mContext).Getcompany_list(new MethodManager_Listner() {
            @Override
            public void onError() {

            }

            @Override
            public void onSuccess(String json) {
                companylist = getcompanylist(json);
                //showCountryListDialog(citylist);
                final ArrayList<String> ids = new ArrayList<String>();
                final ArrayList<String> names = new ArrayList<String>();

                ids.add("-1");
                names.add("Company Name");

                for (int i = 0; i < companylist.size(); i++) {
                    ids.add(companylist.get(i).getCompany_id());
                    names.add(companylist.get(i).getCompany_name());
                }

                Citylist_Adapter adapter = new Citylist_Adapter(mContext, R.layout.popup_item_company, names);
                adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
                spinnerCompany.setAdapter(adapter);

                spinnerCompany.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

                    @Override
                    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                        company_id_position = ids.get(position);
//                        String company_id = ids.get(position);
                        String company_name = names.get(position);
//                        Toast.makeText(mContext, company_name+" "+company_id, Toast.LENGTH_SHORT).show();
                    }


                    @Override
                    public void onNothingSelected(AdapterView<?> parent) {

                    }
                });

            }
        });
    }

    public static ArrayList<Company_Bean> getcompanylist(String json) {
        ArrayList<Company_Bean> getcompanyarray = new ArrayList<Company_Bean>();
        try {
            JSONObject jobj = new JSONObject(json);
            JSONArray jarray = jobj.getJSONArray("companies");
            for (int i = 0; i < jarray.length(); i++) {
                Company_Bean companybean = new Company_Bean();
                companybean.setCompany_id(jarray.getJSONObject(i).getString("company_id"));
                companybean.setCompany_name(jarray.getJSONObject(i).getString("company_name"));

                getcompanyarray.add(companybean);
            }
        } catch (JSONException e) {
            Log.e("Company Details Error", e.toString());
        }

        return getcompanyarray;
    }
    public void ShowTerms_Dialog() {
        final Dialog dialog = new Dialog(mContext);
        LayoutInflater li = (LayoutInflater) mContext.getApplicationContext()
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View vi = li.inflate(R.layout.terms_dialog, null, false);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(vi);
        dialog.setCanceledOnTouchOutside(false);

        ImageView cancel_img = (ImageView) vi.findViewById(R.id.cancel_img);
        WebView htmlweb = (WebView) vi.findViewById(R.id.terms_web);


        htmlweb.setScrollbarFadingEnabled(false);
        //Disable the horizontal scroll bar
        htmlweb.setHorizontalScrollBarEnabled(false);
        //Enable JavaScript
        htmlweb.getSettings().setJavaScriptEnabled(true);
        //Set the user agent
        htmlweb.getSettings().setUserAgentString("AndroidWebView");
        //Clear the cache
        htmlweb.clearCache(true);
        //Make the webview load the specified URL
        htmlweb.loadUrl(Webservices.ACCEPT_TERMS_AND_CONDITIONS);
        cancel_img.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });


        dialog.show();
    }

    private boolean isValidEmail(String email) {
        String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Pattern pattern = Pattern.compile(EMAIL_PATTERN);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    // validating password with retype password
    private boolean isValidPassword(String pass) {
        if (pass != null && pass.length() > 6) {
            return true;
        }
        return false;
    }


    public void showDialogForImageSelect() {
        final CharSequence[] items = {"Take Photo", "Choose from Gallery", "Cancel"};
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int position) {
                try {
                    if (items[position].equals("Take Photo")) {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        fileName = System.currentTimeMillis() + "-" + ".jpg";
                        File sdCardDir = SDDir.createSDCardDir();
                        File imageDir = SDDir.createImageSubDir(mContext, sdCardDir);
                        File profileImage = new File(imageDir, fileName);
                        absPath = profileImage.getAbsolutePath();
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(profileImage));
                        savePref(absPath);
                        startActivityForResult(intent, CAMERA_REQUEST);
                    } else if (items[position].equals("Choose from Gallery")) {
                        Intent intent = new Intent(Intent.ACTION_PICK,
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(intent, SELECT_FILE);
                    } else {
                        dialog.dismiss();
                    }
                } catch (Exception e) {

                }
            }
        });
        builder.setCancelable(false);
        builder.show();
    }

    private void savePref(String str) {
        SharedPreferences sf = mContext.getSharedPreferences("profileImage", 1);
        SharedPreferences.Editor editor = sf.edit();
        editor.putString("imageURI", str);
        editor.commit();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == SELECT_FILE && resultCode == RESULT_OK) {
                if (data != null) {
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = {MediaStore.Images.Media.DATA};
                    Cursor cursor = mContext.getContentResolver().query(selectedImage, filePathColumn, null, null,
                            null);
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    picturePath = cursor.getString(columnIndex);
                    image = ImageDecoder.decodeFile(picturePath);
                    if (image != null) {

                        try {
                            int width = image.getWidth() / 2;
                            int height = image.getHeight() / 2;
                            image = Bitmap.createScaledBitmap(image, width, height, false);
                            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                            image.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                            Bitmap bm = BitmapFactory.decodeStream(new ByteArrayInputStream(bytes.toByteArray()));
                            imageBitmap = bm;
                            imgprofileId.setImageBitmap(RoundedImage.GetBitmapClippedCircle(imageBitmap));
                            filePath = picturePath;

                            Log.e("picturePath :-", picturePath);
                            Log.e("filePath :-", filePath);

                        } catch (OutOfMemoryError e) {

                        } catch (Exception e) {

                        }

                    }
                    cursor.close();
                }
            }
            if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
                try {
                    String URI = getImageURI();
                    try {
                        File file = new File(URI);
                        ExifInterface exif = new ExifInterface(file.getAbsolutePath());
                        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                                ExifInterface.ORIENTATION_NORMAL);
                        switch (orientation) {
                            case ExifInterface.ORIENTATION_ROTATE_270:
                                rotate = 270;
                                ImageOrientation(file, rotate);
                                break;
                            case ExifInterface.ORIENTATION_ROTATE_180:
                                rotate = 180;
                                ImageOrientation(file, rotate);
                                break;
                            case ExifInterface.ORIENTATION_ROTATE_90:
                                rotate = 90;
                                ImageOrientation(file, rotate);
                                break;
                            case 1:
                                rotate = 90;
                                ImageOrientation(file, rotate);
                                break;

                            case 2:
                                rotate = 0;
                                ImageOrientation(file, rotate);
                                break;
                            case 4:
                                rotate = 180;
                                ImageOrientation(file, rotate);
                                break;

                            case 0:
                                rotate = 90;
                                ImageOrientation(file, rotate);
                                break;
                        }
                    } catch (Exception e) {

                    }
                } catch (Exception e) {

                }
            }
            if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
                setImageFromSDCard();
            }
        } catch (Exception e) {

        }


    }

    private String getImageURI() {
        SharedPreferences sf = mContext.getSharedPreferences("profileImage", 1);
        String uri = sf.getString("imageURI", null);
        return uri;
    }

    private void ImageOrientation(File file, int rotate) {
        try {
            FileInputStream fis = new FileInputStream(file);
            filePath = file.getAbsolutePath();
            Bitmap photo = BitmapFactory.decodeStream(fis);
            Matrix matrix = new Matrix();
            matrix.preRotate(rotate); // clockwise by 90 degrees
            photo = Bitmap.createBitmap(photo, 0, 0, photo.getWidth() / 3, photo.getHeight() / 3, matrix, true);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            photo.compress(Bitmap.CompressFormat.JPEG, 100, out);
            Bitmap bm = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
            photo = bm;
            imgprofileId.setImageBitmap(RoundedImage.GetBitmapClippedCircle(photo));
            imageBitmap = photo;
        } catch (FileNotFoundException e) {

        } catch (OutOfMemoryError e) {

        } catch (Exception e) {

        }
    }

    private void setImageFromSDCard() {
        if (currentVersion > 15) {
            String[] filePathColumn = {MediaStore.Images.Media.DATA};
            if (cameraImagePath != null) {
                Cursor imageCursor = mContext.getContentResolver().query(cameraImagePath, filePathColumn, null, null,
                        null);
                if (imageCursor != null && imageCursor.moveToFirst()) {
                    int columnIndex = imageCursor.getColumnIndex(filePathColumn[0]);
                    filePath = imageCursor.getString(columnIndex);

                    imageCursor.close();
                }
            }
        } else {
            String struri = getImageURI();
            filePath = struri;
        }
        Bitmap bitmap = ImageDecoder.decodeFile(filePath);
        if (bitmap != null) {

            try {
                int widht = bitmap.getWidth() / 3;
                int height = bitmap.getHeight() / 3;
                bitmap = Bitmap.createScaledBitmap(bitmap, widht, height, false);
                ;
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                imageBitmap = BitmapFactory.decodeStream(new ByteArrayInputStream(bytes.toByteArray()));
                imgprofileId.setImageBitmap(RoundedImage.GetBitmapClippedCircle(imageBitmap));
                bitmap.recycle();
            } catch (OutOfMemoryError e) {

            } catch (Exception e) {

            }

        }
    }

    public void showprogress() {
        pdialog = new ProgressDialog(mContext);
        pdialog.setCancelable(false);
        pdialog.setCanceledOnTouchOutside(false);
        pdialog.setMessage("Loading...");
        pdialog.show();
    }

    public void hideprogress() {
        if (pdialog != null) {
            pdialog.dismiss();
        }
    }

}
