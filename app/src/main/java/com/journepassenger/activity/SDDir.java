package com.journepassenger.activity;

import android.content.Context;
import android.os.Environment;

import java.io.File;

/**
 * Created by administrator on 28/1/16.
 */
public class SDDir {
    public static File createSDCardDir() {
        File folder = new File(Environment.getExternalStorageDirectory()
                + "/Kruze");
        if (!folder.exists()) {
            folder.mkdir();
        }
        return folder;
    }


    public static File createImageSubDir(Context mContext, File dirPath) {
        File audioDir = new File(dirPath, "Image");
        if (!audioDir.exists()) {
            audioDir.mkdirs();
        }
        return audioDir;
    }
    //createDownloadSubDir

    public static File createDownloadSubDir(Context mContext, File dirPath) {
        File audioDir = new File(dirPath, "MemberShip_Image");
        if (!audioDir.exists()) {
            audioDir.mkdirs();
        }
        return audioDir;
    }
}