package com.journepassenger.activity;


import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.provider.Settings;
import android.view.Window;
import android.view.WindowManager;

import com.journedriver.activity.HomeActivity;
import com.journepassenger.utilities.SessionClass;
import com.journepassenger.utilities.Session_manager;
import com.journyapp.journyapp.R;


public class SplashActivity extends Activity {
	public int Thread_TIME=3;
	Context mContext;
	GPSTracker gps;
	@Override
	protected void onCreate(Bundle savedInstanceState)
	{
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().addFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN);

		super.onCreate(savedInstanceState);
		//setContentView(R.layout.splash);
		setContentView(R.layout.activity_splash);
		mContext=this;


	}

	@Override
	protected void onResume() {
		super.onResume();
		/*SplashThread sthread=new SplashThread();
		sthread.start();*/

		new Handler().postDelayed(new Runnable() {

			/*
			 * Showing splash screen with a timer. This will be useful when you
			 * want to show case your app logo / company
			 */

			@Override
			public void run() {
				gps = new GPSTracker(SplashActivity.this);

				// check if GPS enabled


					if (gps.canGetLocation()) {

						double latitude = gps.getLatitude();//22.7257742
						double longitude = gps.getLongitude();//75.87731
						SessionClass.setCurrentLatitude(latitude);
						SessionClass.setCurrentLongitude(longitude);
						//Toast.makeText(mContext,"lat"+latitude+"longi"+longitude,Toast.LENGTH_LONG).show();
						StartActivities();


				} else {
					showAlertDialog();

				}
				// close this activity

			}
		}, Thread_TIME*1000);
	}



			/*gps = new GPSTracker(SplashActivity.this);

			// check if GPS enabled
			if (gps.canGetLocation()) {

				double latitude = gps.getLatitude();//22.7257742
				double longitude = gps.getLongitude();//75.87731
				StartActivities();
			}
			else {
				showAlertDialog();
			}
			*//*if(getSharedPreferences("User_login", MODE_PRIVATE).getString("user_id", null)!=null)
			{
//				Intent intent=new Intent(SplashActivity.this,UserInterestActivity.class);
//				startActivity(intent);
//				finish();
			}
			else{
				Intent intent=new Intent(SplashActivity.this,SigninDriverActivity.class);
				startActivity(intent);
				finish();
			}*//*
		}
	}*/
public void StartActivities()
{
	if(Session_manager.getemail_pref(mContext)!=null)
	{

		if (Session_manager.getstatus(mContext).equals("Passenger"))
		{

			Intent intent = new Intent(SplashActivity.this, DashBoardActivity.class);
			startActivity(intent);
			finish();
		}

		else
		{
			if (Session_manager.getstatus(mContext).equals("driver"))
			{
				Intent intent=new Intent(SplashActivity.this,HomeActivity.class);
				startActivity(intent);
				finish();
			}
		}
	}
	else if(Session_manager.getloginstatus(mContext)==1)
	{
		Intent intent = new Intent(SplashActivity.this, DashBoardActivity.class);
		startActivity(intent);
		finish();
	}
	else if(Session_manager.getloginstatus(mContext)==2)
	{
		Intent intent = new Intent(SplashActivity.this, DashBoardActivity.class);
		startActivity(intent);
		finish();
	}

	else {
		Intent intent=new Intent(SplashActivity.this,SigninActivity.class);
		startActivity(intent);
		finish();
	}
}
	protected void showAlertDialog() {

		AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

		// Setting Dialog Title
		alertDialog.setTitle("GPS is settings");

		// Setting Dialog Message
		alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

		// On pressing Settings button
		alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
				mContext.startActivity(intent);
			}
		});

		// on pressing cancel button
		alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int which) {
				dialog.cancel();
				Session_manager.SetCityPrefrence(mContext, "Mumbai", "8");
				SessionClass.setCurrentLatitude(0.0);
				SessionClass.setCurrentLongitude(0.0);
				StartActivities();
			}
		});

		// Showing Alert Message
		if (! ((Activity) mContext).isFinishing()) {
			alertDialog.show();
		}
	}

	@Override
	public void onBackPressed() {

//		super.onBackPressed();
	}
}
