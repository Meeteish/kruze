package com.journepassenger.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.journepassenger.utilities.ImageDecoder;
import com.journepassenger.utilities.RoundedImage;
import com.journyapp.journyapp.R;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;

public class EditProfileActivity extends Activity {

    String absPath;
    private static final int CAMERA_REQUEST = 11;
    private static final int SELECT_FILE = 12;
    static String fileName = null;
    String picturePath;
    Context mContext;
    Bitmap image;
    public static Bitmap imageBitmap;
    String filePath = "null";
    EditText edname,edemail,edtelephone,edcompanyname;
    LinearLayout lLayoutHideShowAdditionalInfo;
    RelativeLayout rLayoutAdditionalInfo;
    ImageView uparrow,downarrow,imgcameraId,imgprofileId;
    int check=0;
    Uri cameraImagePath;
    File f;
    int currentVersion = Build.VERSION.SDK_INT;
    int rotate;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_passenger);
        lLayoutHideShowAdditionalInfo = (LinearLayout)findViewById(R.id.hideShowAdditionalInfo);
        rLayoutAdditionalInfo = (RelativeLayout)findViewById(R.id.relativeLayoutAdditionalInfo);
        downarrow = (ImageView)findViewById(R.id.imgdownarrowid);
        uparrow = (ImageView)findViewById(R.id.imguparrowid);
        imgcameraId = (ImageView)findViewById(R.id.cameraicon);
        imgprofileId=(ImageView)findViewById(R.id.imgprofileId);


        imgcameraId.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                try {
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            showDialogForImageSelect();
                        }
                    });
                } catch (Exception e) {

                }
            }
        });

        lLayoutHideShowAdditionalInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(check==0)
                {
                rLayoutAdditionalInfo.setVisibility(View.VISIBLE);
                downarrow.setVisibility(View.INVISIBLE);
                uparrow.setVisibility(View.VISIBLE);
                    check=1;
                }
                else
                {
                    rLayoutAdditionalInfo.setVisibility(View.GONE);
                    downarrow.setVisibility(View.VISIBLE);
                    uparrow.setVisibility(View.INVISIBLE);
                    check=0;
                }

            }
        });


    }

    public void showDialogForImageSelect() {
        final CharSequence[] items = { "Take Photo", "Choose from Gallery", "Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
        builder.setTitle("Add Photo!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int position) {
                try {
                    if (items[position].equals("Take Photo")) {
                        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                        fileName = System.currentTimeMillis() + "-" + ".jpg";
                        File sdCardDir = SDDir.createSDCardDir();
                        File imageDir = SDDir.createImageSubDir(mContext, sdCardDir);
                        File profileImage = new File(imageDir, fileName);
                        absPath = profileImage.getAbsolutePath();
                        intent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(profileImage));
                        savePref(absPath);
                        startActivityForResult(intent, CAMERA_REQUEST);
                    } else if (items[position].equals("Choose from Gallery")) {
                        Intent intent = new Intent(Intent.ACTION_PICK,
                                MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                        startActivityForResult(intent, SELECT_FILE);
                    } else {
                        dialog.dismiss();
                    }
                } catch (Exception e) {

                }
            }
        });
        builder.setCancelable(false);
        builder.show();
    }

    private void savePref(String str) {
        SharedPreferences sf = mContext.getSharedPreferences("profileImage", 1);
        SharedPreferences.Editor editor = sf.edit();
        editor.putString("imageURI", str);
        editor.commit();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            if (requestCode == SELECT_FILE && resultCode == RESULT_OK) {
                if (data != null) {
                    Uri selectedImage = data.getData();
                    String[] filePathColumn = { MediaStore.Images.Media.DATA };
                    Cursor cursor = mContext.getContentResolver().query(selectedImage, filePathColumn, null, null,
                            null);
                    cursor.moveToFirst();
                    int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
                    picturePath = cursor.getString(columnIndex);
                    image = ImageDecoder.decodeFile(picturePath);
                    if (image != null) {

                        try {
                            int width = image.getWidth() / 2;
                            int height = image.getHeight() / 2;
                            image = Bitmap.createScaledBitmap(image, width, height, false);
                            ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                            image.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                            Bitmap bm = BitmapFactory.decodeStream(new ByteArrayInputStream(bytes.toByteArray()));
                            imageBitmap = bm;
                            imgprofileId.setImageBitmap(RoundedImage.GetBitmapClippedCircle(imageBitmap));
                            filePath = picturePath;

                        } catch (OutOfMemoryError e) {

                        } catch (Exception e) {

                        }

                    }
                    cursor.close();
                }
            }
            if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
                try {
                    String URI = getImageURI();
                    try {
                        File file = new File(URI);
                        ExifInterface exif = new ExifInterface(file.getAbsolutePath());
                        int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                                ExifInterface.ORIENTATION_NORMAL);
                        switch (orientation) {
                            case ExifInterface.ORIENTATION_ROTATE_270:
                                rotate = 270;
                                ImageOrientation(file, rotate);
                                break;
                            case ExifInterface.ORIENTATION_ROTATE_180:
                                rotate = 180;
                                ImageOrientation(file, rotate);
                                break;
                            case ExifInterface.ORIENTATION_ROTATE_90:
                                rotate = 90;
                                ImageOrientation(file, rotate);
                                break;
                            case 1:
                                rotate = 90;
                                ImageOrientation(file, rotate);
                                break;

                            case 2:
                                rotate = 0;
                                ImageOrientation(file, rotate);
                                break;
                            case 4:
                                rotate = 180;
                                ImageOrientation(file, rotate);
                                break;

                            case 0:
                                rotate = 90;
                                ImageOrientation(file, rotate);
                                break;
                        }
                    } catch (Exception e) {

                    }
                } catch (Exception e) {

                }
            }
            if (requestCode == CAMERA_REQUEST && resultCode == RESULT_OK) {
                setImageFromSDCard();
            }
        } catch (Exception e) {

        }


    }
    private String getImageURI() {
        SharedPreferences sf = mContext.getSharedPreferences("profileImage", 1);
        String uri = sf.getString("imageURI", null);
        return uri;
    }
    private void ImageOrientation(File file, int rotate) {
        try {
            FileInputStream fis = new FileInputStream(file);
            filePath = file.getAbsolutePath();
            Bitmap photo = BitmapFactory.decodeStream(fis);
            Matrix matrix = new Matrix();
            matrix.preRotate(rotate); // clockwise by 90 degrees
            photo = Bitmap.createBitmap(photo, 0, 0, photo.getWidth() / 3, photo.getHeight() / 3, matrix, true);
            ByteArrayOutputStream out = new ByteArrayOutputStream();
            photo.compress(Bitmap.CompressFormat.JPEG, 100, out);
            Bitmap bm = BitmapFactory.decodeStream(new ByteArrayInputStream(out.toByteArray()));
            photo = bm;
            imgprofileId.setImageBitmap(RoundedImage.GetBitmapClippedCircle(photo));
            imageBitmap = photo;
        } catch (FileNotFoundException e) {

        } catch (OutOfMemoryError e) {

        } catch (Exception e) {

        }
    }

    private void setImageFromSDCard() {
        if (currentVersion > 15) {
            String[] filePathColumn = { MediaStore.Images.Media.DATA };
            if (cameraImagePath != null) {
                Cursor imageCursor = mContext.getContentResolver().query(cameraImagePath, filePathColumn, null, null,
                        null);
                if (imageCursor != null && imageCursor.moveToFirst()) {
                    int columnIndex = imageCursor.getColumnIndex(filePathColumn[0]);
                    filePath = imageCursor.getString(columnIndex);

                    imageCursor.close();
                }
            }
        } else {
            String struri = getImageURI();
            filePath = struri;
        }
        Bitmap bitmap = ImageDecoder.decodeFile(filePath);
        if (bitmap != null) {

            try {
                int widht = bitmap.getWidth() / 3;
                int height = bitmap.getHeight() / 3;
                bitmap = Bitmap.createScaledBitmap(bitmap, widht, height, false);
                ;
                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
                imageBitmap = BitmapFactory.decodeStream(new ByteArrayInputStream(bytes.toByteArray()));
                imgprofileId.setImageBitmap(RoundedImage.GetBitmapClippedCircle(imageBitmap));
                bitmap.recycle();
            } catch (OutOfMemoryError e) {

            } catch (Exception e) {

            }

        }
    }

}
