package com.journedriver.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.DatePicker;
import android.widget.ExpandableListView;
import android.widget.FrameLayout;
import android.widget.ImageView;

import com.journedriver.bean.CashBean;
import com.journedriver.bean.TripCashBean;
import com.journedriver.data.StaticData;
import com.journedriver.data.UtilMethod;
import com.journedriver.task.GetTripCashTask;
import com.journedriver.task.TaskListener;
import com.journedriver.view.TextViewB;
import com.journedriver.view.TextViewR;
import com.journyapp.journyapp.R;
import com.journedriver.expandable.ExpandableListDataPump_Cash;
import com.journedriver.expandable.ExpandableList_Cash_Adapter;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.TreeSet;

public class CashActivity extends Activity implements OnClickListener{
	TextViewR tv_total_received_amount;
	TextViewR tv_date;
	TextViewB tv_trip_date;
	ImageView iv_back;
	FrameLayout frame_calendar;
	ExpandableListView expandable_rout_list;
	HashMap<TripCashBean, List<CashBean>> expandableListDetail;
	ExpandableList_Cash_Adapter expandableListAdapter;
	List<TripCashBean> expandableListTitle;
	String driver_id;
	String date;int dd,mm,yy;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cash);

		tv_trip_date=(TextViewB)findViewById(R.id.tv_trip_date);
		tv_total_received_amount=(TextViewR)findViewById(R.id.tv_total_received_amount);
		frame_calendar=(FrameLayout)findViewById(R.id.frame_calendar);
		frame_calendar.setOnClickListener(this);
		tv_date= (TextViewR) findViewById(R.id.tv_date);

		date= UtilMethod.getTodaysDate();
		tv_date.setText(date.split("-")[0]);
		Log.i("date", date + "....");

		dd=Integer.parseInt(date.split("-")[0]);
		mm=Integer.parseInt(date.split("-")[1])-1;
		yy=Integer.parseInt(date.split("-")[2]);

		expandable_rout_list=(ExpandableListView) findViewById(R.id.expandable_rout_list);
		expandableListDetail = ExpandableListDataPump_Cash.getData();
		expandableListTitle = new ArrayList<TripCashBean>(expandableListDetail.keySet());
		expandableListAdapter = new ExpandableList_Cash_Adapter(this, expandableListTitle, expandableListDetail);
		expandable_rout_list.setAdapter(expandableListAdapter);

		expandable_rout_list.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
			@Override
			public boolean onGroupClick(ExpandableListView expandableListView, View view, int i, long l) {



				return false;
			}
		});


		SharedPreferences sp = getSharedPreferences("driver_info",MODE_PRIVATE);
		driver_id = sp.getString("driver_id",null);
		getCashDetail();

		Date d=new Date();
		SimpleDateFormat dateformat=new SimpleDateFormat("dd-MM-yyyy");
		String date = dateformat.format(d);
		tv_trip_date.setText("Date: "+date);

		iv_back=(ImageView) findViewById(R.id.iv_back);
		iv_back.setOnClickListener(this);
	}

	private void getCashDetail() {
		Log.e("getTripTask","true");
		ArrayList<NameValuePair> inputdata=new ArrayList<NameValuePair>();
		inputdata.add(new BasicNameValuePair("driver_id", driver_id));
		inputdata.add(new BasicNameValuePair("user_date", date));

		new GetTripCashTask(this, inputdata, new TripCashListener()).execute();
	}
	//DatePicker dp;
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.frame_calendar:
				DatePickerDialog dp=new DatePickerDialog(this,android.R.style.Theme_Holo_Light_Dialog_NoActionBar, new DatePickerDialog.OnDateSetListener() {
					@Override
					public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
						date=dayOfMonth + "-"+ (monthOfYear+1) + "-" + year;
						dd=dayOfMonth;
						mm=monthOfYear;
						yy=year;
						tv_date.setText(dd+"");
						tv_trip_date.setText("Date: "+date);
						if (datePicker.isShown())
							getCashDetail();

						date= UtilMethod.getTodaysDate();
						//tv_date.setText(date.split("-")[0]);
						Log.i("date", date + "....");

						dd=Integer.parseInt(date.split("-")[0]);
						mm=Integer.parseInt(date.split("-")[1])-1;
						yy=Integer.parseInt(date.split("-")[2]);
					}
				},yy,mm,dd);
				dp.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
				dp.getDatePicker().setMaxDate(new Date().getTime());
				dp.getDatePicker().setMinDate(System.currentTimeMillis() - (1000 * 60 * 60 * 24 * 3));
				dp.show();

				/*View view = getLayoutInflater().inflate(R.layout.layout_datepicker,null);
				dp=(DatePicker)view.findViewById(R.id.datePicker);
				dp.setMaxDate(System.currentTimeMillis());
				ContextThemeWrapper themedContext;
				if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
					themedContext = new ContextThemeWrapper(CashActivity.this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
				} else {
					themedContext = new ContextThemeWrapper(CashActivity.this, android.R.style.Theme_Light_NoTitleBar);
				}
				final AlertDialog.Builder build = new AlertDialog.Builder(themedContext);
				build.setTitle("Select Date");
				build.setView(view);
				build.setPositiveButton("Done", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {
						date=dp.getDayOfMonth() + "-"+ (dp.getMonth()+1) + "-" + dp.getYear();
						dd=dp.getDayOfMonth();
						mm=dp.getMonth();
						yy=dp.getYear();
						tv_date.setText(dd+"");
						getCashDetail();
					}
				});
				build.show();*/
				break;
		case R.id.iv_back:
			finish();
			break;
		default:
			break;
		}
	}

	class TripCashListener implements TaskListener
	{
		@Override
		public void onSuccess(String msg) {
			expandableListDetail = ExpandableListDataPump_Cash.getData();
			expandableListTitle = new ArrayList<TripCashBean>(expandableListDetail.keySet());
			expandableListAdapter = new ExpandableList_Cash_Adapter(CashActivity.this, expandableListTitle, expandableListDetail);
			expandable_rout_list.setAdapter(expandableListAdapter);

			tv_total_received_amount.setText("Rs."+StaticData.total_received_cash);
		}

		@Override
		public void onError(String msg) {
			// TODO Auto-generated method stub
			if (msg.equals("slow")) {
				UtilMethod.showServerError(CashActivity.this);
			} else {
				expandableListDetail = ExpandableListDataPump_Cash.getData();
				expandableListTitle = new ArrayList<TripCashBean>(expandableListDetail.keySet());
				expandableListAdapter = new ExpandableList_Cash_Adapter(CashActivity.this, expandableListTitle, expandableListDetail);
				expandable_rout_list.setAdapter(expandableListAdapter);

				tv_total_received_amount.setText("Rs." + StaticData.total_received_cash);
				showDialog("No bus assigned to this driver");
			}
		}
		private void showDialog(String msg)
		{
			ContextThemeWrapper themedContext;
			if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
				themedContext = new ContextThemeWrapper(CashActivity.this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar );
			}
			else {
				themedContext = new ContextThemeWrapper(CashActivity.this, android.R.style.Theme_Light_NoTitleBar );
			}
			final AlertDialog.Builder adialog=new AlertDialog.Builder(themedContext);
			adialog.setTitle("Message!!");
			adialog.setMessage(msg);
			adialog.setPositiveButton("Ok",new DialogInterface.OnClickListener()
			{
				@Override
				public void onClick(DialogInterface dialog, int which)
				{
				}
			});
			adialog.show();
		}
	}
}
