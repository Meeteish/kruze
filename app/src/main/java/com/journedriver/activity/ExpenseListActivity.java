package com.journedriver.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ListView;

import com.journedriver.adapter.ExpenseListAdapter;
import com.journedriver.data.UtilMethod;
import com.journedriver.task.GetExpenseCategory;
import com.journedriver.task.GetExpenseListTask;
import com.journedriver.task.TaskListener;
import com.journedriver.view.TextViewB;
import com.journedriver.view.TextViewR;
import com.journyapp.journyapp.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class ExpenseListActivity extends Activity implements OnClickListener,SwipeRefreshLayout.OnRefreshListener{
	ImageView iv_back;
	TextViewB tv_add_more;
	TextViewR tv_date;
	FrameLayout frame_calendar;
	ListView listview_expense;
	private final int REQUEST_FOR_ADD_EXPENSE=1;
	String driver_id;
	String date;int dd,mm,yy;

	SwipeRefreshLayout swipeRefreshLayout;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_expense_list);

		swipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.swipe_refresh_layout);
		listview_expense=(ListView)findViewById(R.id.listview_expense);
		iv_back=(ImageView) findViewById(R.id.iv_back);
		tv_add_more= (TextViewB) findViewById(R.id.tv_add_more);
		tv_date= (TextViewR) findViewById(R.id.tv_date);
		iv_back.setOnClickListener(this);
		tv_add_more.setOnClickListener(this);
		frame_calendar=(FrameLayout)findViewById(R.id.frame_calendar);
		frame_calendar.setOnClickListener(this);

		SharedPreferences sp = getSharedPreferences("driver_info",MODE_PRIVATE);
		driver_id = sp.getString("driver_id",null);
		Log.e("driver_id", driver_id + "...");

		swipeRefreshLayout.setOnRefreshListener(this);

		date= UtilMethod.getTodaysDate();
		tv_date.setText(date.split("-")[0]);
		Log.i("date", date + "....");

		dd=Integer.parseInt(date.split("-")[0]);
		mm=Integer.parseInt(date.split("-")[1])-1;
		yy=Integer.parseInt(date.split("-")[2]);

		getExpenseCategory();
	}
	//DatePicker dp;
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.frame_calendar:
			DatePickerDialog dp=new DatePickerDialog(this,android.R.style.Theme_Holo_Light_Dialog_NoActionBar, new DatePickerDialog.OnDateSetListener() {
				@Override
				public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
					date=dayOfMonth + "-"+ (monthOfYear+1) + "-" + year;
					dd=dayOfMonth;
					mm=monthOfYear;
					yy=year;
					tv_date.setText(dd+"");
					if(datePicker.isShown())
						getExpenseDetail();

					date= UtilMethod.getTodaysDate();
					//tv_date.setText(date.split("-")[0]);
					Log.i("date", date + "....");

					dd=Integer.parseInt(date.split("-")[0]);
					mm=Integer.parseInt(date.split("-")[1])-1;
					yy=Integer.parseInt(date.split("-")[2]);
				}
			},yy,mm,dd);
			dp.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
			dp.getDatePicker().setMaxDate(new Date().getTime());
			dp.getDatePicker().setMinDate(System.currentTimeMillis() - (1000 * 60 * 60 * 24 * 3));
			dp.show();

			/*View view = getLayoutInflater().inflate(R.layout.layout_datepicker,null);
			dp=(DatePicker)view.findViewById(R.id.datePicker);
			dp.setMaxDate(System.currentTimeMillis());
			ContextThemeWrapper themedContext;
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				themedContext = new ContextThemeWrapper(ExpenseListActivity.this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
			} else {
				themedContext = new ContextThemeWrapper(ExpenseListActivity.this, android.R.style.Theme_Light_NoTitleBar);
			}
			final AlertDialog.Builder build = new AlertDialog.Builder(themedContext);
			build.setTitle("Select Date");
			build.setView(view);
			build.setPositiveButton("Done", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialogInterface, int i) {
					date=dp.getDayOfMonth() + "-"+ (dp.getMonth()+1) + "-" + dp.getYear();
					dd=dp.getDayOfMonth();
					mm=dp.getMonth();
					yy=dp.getYear();
					tv_date.setText(dd+"");
					getExpenseDetail();
				}
			});
			build.show();*/
			break;
		case R.id.tv_add_more:
			Intent in=new Intent(this, AddExpenseActivity.class);
			startActivityForResult(in,REQUEST_FOR_ADD_EXPENSE);
			break;
		case R.id.iv_back:
			finish();
			break;
		default:
			break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(requestCode==REQUEST_FOR_ADD_EXPENSE && resultCode==RESULT_OK)
		{
			getExpenseDetail();
		}
	}

	public void getExpenseCategory() {

		new GetExpenseCategory(this,new ArrayList<NameValuePair>(),new ExpenseCategoryListener()).execute();
	}

	private void getExpenseDetail()
	{
		swipeRefreshLayout.setRefreshing(true);
		ArrayList<NameValuePair> inputdata=new ArrayList<>();
		inputdata.add(new BasicNameValuePair("driver_id", driver_id));
		inputdata.add(new BasicNameValuePair("date", date));
		Log.i("date", date + "....");

		new GetExpenseListTask(ExpenseListActivity.this,inputdata,new ExpenseDetailsListener()).execute();
	}
	class ExpenseCategoryListener implements TaskListener
	{
		@Override
		public void onSuccess(String msg) {
			//code to get expense list
			swipeRefreshLayout.post(new Runnable() {
										@Override
										public void run() {
											swipeRefreshLayout.setRefreshing(true);
											getExpenseDetail();
										}
									}
			);
		}
		@Override
		public void onError(String msg) {
			showDialog("Message!!",msg);
		}
		void showDialog(String title, String message) {
			ContextThemeWrapper themedContext;
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				themedContext = new ContextThemeWrapper(ExpenseListActivity.this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
			} else {
				themedContext = new ContextThemeWrapper(ExpenseListActivity.this, android.R.style.Theme_Light_NoTitleBar);
			}
			final AlertDialog.Builder adialog = new AlertDialog.Builder(themedContext);
			adialog.setTitle(title);
			adialog.setMessage(message);
			adialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
				}
			});
			adialog.show();
		}
	}
	class ExpenseDetailsListener implements TaskListener
	{
		@Override
		public void onSuccess(String msg) {
			//code to display expense list
			ExpenseListAdapter adapter=new ExpenseListAdapter(ExpenseListActivity.this);
			listview_expense.setAdapter(adapter);
			swipeRefreshLayout.setRefreshing(false);
		}
		@Override
		public void onError(String msg) {
			/*showDialog("Expense List",msg);*/
			ExpenseListAdapter adapter=new ExpenseListAdapter(ExpenseListActivity.this);
			listview_expense.setAdapter(adapter);
			swipeRefreshLayout.setRefreshing(false);

			showDialog("Message!!",msg);
		}
		void showDialog(String title, String message) {
			ContextThemeWrapper themedContext;
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				themedContext = new ContextThemeWrapper(ExpenseListActivity.this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
			} else {
				themedContext = new ContextThemeWrapper(ExpenseListActivity.this, android.R.style.Theme_Light_NoTitleBar);
			}
			final AlertDialog.Builder adialog = new AlertDialog.Builder(themedContext);
			adialog.setTitle(title);
			adialog.setMessage(message);
			adialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
				}
			});
			adialog.show();
		}
	}
	@Override
	public void onRefresh() {
		// TODO Auto-generated method stub
		getExpenseDetail();
	}
}
