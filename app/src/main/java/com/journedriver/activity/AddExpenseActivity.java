package com.journedriver.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.PopupWindow;
import android.widget.RadioButton;

import com.journedriver.bean.ExpenseCategory;
import com.journedriver.data.CompressImage;
import com.journedriver.data.StaticData;
import com.journedriver.data.UtilMethod;
import com.journedriver.expandable.ExpandableListDataPump_Category;
import com.journedriver.expandable.ExpandableList_Category_Adapter;
import com.journedriver.task.InsertExpenseTask;
import com.journedriver.task.TaskListener;
import com.journedriver.view.EditTextL;
import com.journedriver.view.EditTextR;
import com.journedriver.view.TextViewB;
import com.journedriver.view.TextViewR;
import com.journyapp.journyapp.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class AddExpenseActivity extends Activity implements OnClickListener, OnChildClickListener, View.OnTouchListener {
	ImageView iv_back,iv_bill;
	TextViewB tv_save;
	TextViewR tv_upload_bill,tv_date,tv_expense_date;
	EditTextL edit_amount,edit_kilometeres,edit_description;
	RadioButton rdoPetrol,rdoDiesel;

	LinearLayout layout_for_fuel_selection;
	ExpandableListView expandable_category_list;
	HashMap<String, List<ExpenseCategory>> expandableListDetail;
	ExpandableList_Category_Adapter expandableListAdapter;
	List<String> expandableListTitle;

	Uri cameraImagePath;
	String absPath;
	int currentVersion = Build.VERSION.SDK_INT;
	private int REQUEST_FOR_CAMERA=11,REQUEST_FOR_GALLERY=12;
	Uri picUri;

	String date;int dd,mm,yy;
	boolean addexpense_status=false;
	boolean expense_bill_status=false;
	String driver_id;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_add_expense);

		layout_for_fuel_selection= (LinearLayout) findViewById(R.id.layout_for_fuel_selection);

		expandable_category_list=(ExpandableListView) findViewById(R.id.expandable_category_list);
		if(StaticData.expense_category_list.size()>0)
			expandableListDetail = ExpandableListDataPump_Category.getData(StaticData.expense_category_list.get(0).toString());
		else
			expandableListDetail = ExpandableListDataPump_Category.getData("No Category Found");
		expandableListTitle = new ArrayList<String>(expandableListDetail.keySet());
		expandableListAdapter = new ExpandableList_Category_Adapter(this, expandableListTitle, expandableListDetail);
		expandable_category_list.setAdapter(expandableListAdapter);

		expandable_category_list.setOnChildClickListener(this);
		//expandable_category_list.setOnTouchListener(this);

		rdoDiesel=(RadioButton)findViewById(R.id.rdoDiesel);
		rdoPetrol=(RadioButton)findViewById(R.id.rdoPetrol);
		rdoPetrol.setChecked(true);

		tv_save=(TextViewB)findViewById(R.id.tv_save);
		tv_save.setOnClickListener(this);
		tv_upload_bill=(TextViewR)findViewById(R.id.tv_upload_bill);
		tv_upload_bill.setOnClickListener(this);
		edit_amount=(EditTextL)findViewById(R.id.edit_amount);
		edit_kilometeres=(EditTextL)findViewById(R.id.edit_kilometeres);
		edit_description=(EditTextL)findViewById(R.id.edit_description);
		iv_bill=(ImageView)findViewById(R.id.iv_bill);
		iv_back=(ImageView)findViewById(R.id.iv_back);
		iv_back.setOnClickListener(this);
		tv_date=(TextViewR)findViewById(R.id.tv_date);
		tv_expense_date=(TextViewR)findViewById(R.id.tv_expense_date);
		tv_expense_date.setOnClickListener(this);

		SharedPreferences sp = getSharedPreferences("driver_info",MODE_PRIVATE);
		driver_id = sp.getString("driver_id",null);
		Log.e("driver_id",driver_id);

		date= UtilMethod.getTodaysDate();
		tv_date.setText(date.split("-")[0]);
		Log.i("date", date + "....");

		dd=Integer.parseInt(date.split("-")[0]);
		mm=Integer.parseInt(date.split("-")[1])-1;
		yy=Integer.parseInt(date.split("-")[2]);

		tv_expense_date.setText(date);
	}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.tv_save:
			addExpense();
			break;
		case R.id.tv_upload_bill:
			showPopup_for_ImageOption();
			break;
		case R.id.tv_expense_date:
			showDateDialog();
			break;
		case R.id.iv_back:
			finish();
			break;
		default:
			break;
		}
	}
	//DatePicker dp;
	private void showDateDialog()
	{
		DatePickerDialog dp=new DatePickerDialog(this,android.R.style.Theme_Holo_Light_Dialog_NoActionBar, new DatePickerDialog.OnDateSetListener() {
			@Override
			public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
				date=dayOfMonth + "-"+ (monthOfYear+1) + "-" + year;
				dd=dayOfMonth;
				mm=monthOfYear;
				yy=year;
				tv_date.setText(dd+"");
				tv_expense_date.setText(date);

				date= UtilMethod.getTodaysDate();
				tv_date.setText(date.split("-")[0]);
				Log.i("date", date + "....");

				dd=Integer.parseInt(date.split("-")[0]);
				mm=Integer.parseInt(date.split("-")[1])-1;
				yy=Integer.parseInt(date.split("-")[2]);
			}
		},yy,mm,dd);
		dp.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
		dp.getDatePicker().setMaxDate(new Date().getTime());
		dp.getDatePicker().setMinDate(System.currentTimeMillis()-(1000 * 60 * 60 * 24 * 3));
		dp.show();
		/*View view = getLayoutInflater().inflate(R.layout.layout_datepicker,null);
		dp=(DatePicker)view.findViewById(R.id.datePicker);
		dp.setMinDate(System.currentTimeMillis()-(1000 * 60 * 60 * 24 * 3));
		dp.setMaxDate(System.currentTimeMillis());
		ContextThemeWrapper themedContext;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			themedContext = new ContextThemeWrapper(this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
		} else {
			themedContext = new ContextThemeWrapper(this, android.R.style.Theme_Light_NoTitleBar);
		}
		final AlertDialog.Builder build = new AlertDialog.Builder(themedContext);
		build.setTitle("Select Date");
		build.setView(view);
		build.setPositiveButton("Done", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialogInterface, int i) {
				date=dp.getDayOfMonth() + "-"+ (dp.getMonth()+1) + "-" + dp.getYear();
				dd=dp.getDayOfMonth();
				mm=dp.getMonth();
				yy=dp.getYear();
				tv_expense_date.setText(date);
			}
		});
		build.show();*/
	}
	private void addExpense()
	{
		//code to save Expense

		String cost=edit_amount.getText().toString();
		String km=edit_kilometeres.getText().toString();
		String desc=edit_description.getText().toString();
		String e_date=tv_expense_date.getText().toString();
		String options="";
		if(StaticData.last_selected_category_id==1)
		{
			if(rdoPetrol.isChecked())
				options="Petrol";
			else if(rdoDiesel.isChecked())
				options="Diesel";
		}
		Log.e("selected_category_id",StaticData.last_selected_category_id+"");
		Log.e("options",options+"");
		if(UtilMethod.isStringNullOrBlank(cost))
			UtilMethod.showToast("Cost field should not be left blank", this);
		else if(UtilMethod.isStringNullOrBlank(km))
			UtilMethod.showToast("Kilometeres field should not be left blank",this);
		else {
			if(expense_bill_status)
			{
				upload_image(cost,km,desc,e_date,options);
			}
			else {
				ArrayList<NameValuePair> inputdata = new ArrayList<>();
				inputdata.add(new BasicNameValuePair("driver_id", driver_id));
				inputdata.add(new BasicNameValuePair("category_id", StaticData.last_selected_category_id + ""));
				inputdata.add(new BasicNameValuePair("cost", cost));
				inputdata.add(new BasicNameValuePair("kms", km));
				inputdata.add(new BasicNameValuePair("desc", desc));
				inputdata.add(new BasicNameValuePair("date", e_date));
				inputdata.add(new BasicNameValuePair("options", options));

				new InsertExpenseTask(this, inputdata, new InsertExpenseListener()).execute();
			}
		}
	}

	class InsertExpenseListener implements TaskListener
	{
		@Override
		public void onSuccess(String msg) {
			showDialog("Message!!", "Expense is successfully submitted");
			addexpense_status=true;
		}
		@Override
		public void onError(String msg) {
			showDialog("Message!!",msg);
		}
		void showDialog(String title, String message) {
			ContextThemeWrapper themedContext;
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				themedContext = new ContextThemeWrapper(AddExpenseActivity.this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
			} else {
				themedContext = new ContextThemeWrapper(AddExpenseActivity.this, android.R.style.Theme_Light_NoTitleBar);
			}
			final AlertDialog.Builder adialog = new AlertDialog.Builder(themedContext);
			adialog.setTitle(title);
			adialog.setMessage(message);
			adialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {
					finish();
				}
			});
			adialog.show();
		}
	}

	@Override
	public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i1, long l) {
		StaticData.last_selected_category_id=i1+1;
		expandableListDetail = ExpandableListDataPump_Category.getData(StaticData.expense_category_list.get(i1).toString());
		expandableListTitle = new ArrayList<String>(expandableListDetail.keySet());
		expandableListAdapter = new ExpandableList_Category_Adapter(this, expandableListTitle, expandableListDetail);
		expandable_category_list.setAdapter(expandableListAdapter);

		expandableListView.collapseGroup(i);
		if(i1==0)
			layout_for_fuel_selection.setVisibility(View.VISIBLE);
		else
			layout_for_fuel_selection.setVisibility(View.GONE);

		return false;
	}

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		switch (event.getAction()) {
			case MotionEvent.ACTION_DOWN:
				v.getParent().requestDisallowInterceptTouchEvent (true);
				break;
			case MotionEvent.ACTION_UP:
				v.getParent().requestDisallowInterceptTouchEvent (false);
				break;
		}

		v.onTouchEvent(event);
		return true;
	}

	@Override
	public void finish() {
		if(addexpense_status)
			setResult(RESULT_OK);
		else
			setResult(RESULT_CANCELED);

		super.finish();
	}

	PopupWindow popup;
	void showPopup_for_ImageOption()
	{
		View view = AddExpenseActivity.this.getLayoutInflater().inflate(R.layout.layout_capture_image_option, null);
		TextViewR tv_cancel=(TextViewR) view.findViewById(R.id.tv_cancel);
		TextViewR tv_camera=(TextViewR) view.findViewById(R.id.tv_camera);
		TextViewR tv_gallery=(TextViewR) view.findViewById(R.id.tv_gallery);
		popup = new PopupWindow(AddExpenseActivity.this);
		popup.setContentView(view);
		// popup.setBackgroundDrawable(new BitmapDrawable());
		popup.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
		popup.setBackgroundDrawable(new ColorDrawable(
				Color.TRANSPARENT));
		popup.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
		popup.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
		popup.setFocusable(true);

		tv_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				popup.dismiss();
			}
		});
		tv_camera.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				takeNewPicture();
				popup.dismiss();
			}
		});
		tv_gallery.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				pickfromGallery();
				popup.dismiss();
			}
		});

		popup.showAtLocation(view, Gravity.BOTTOM, 0, 0);
	}
	private void pickfromGallery() {
		Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		startActivityForResult(i, REQUEST_FOR_GALLERY);
	}
	private void takeNewPicture()
	{
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		File sdcard=new File(Environment.getExternalStorageDirectory(),"Kruze");
		if(!sdcard.exists())
			sdcard.mkdir();
		File image=new File(sdcard,"IMG_"+System.currentTimeMillis()+".jpeg");
		cameraImagePath= Uri.fromFile(image);
		takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, cameraImagePath);

		Log.e("", cameraImagePath.getPath());
		startActivityForResult(takePictureIntent, REQUEST_FOR_CAMERA);
	}
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@Override
	protected void onActivityResult(int requestcode, int resultcode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestcode, resultcode, data);
		if(requestcode==REQUEST_FOR_CAMERA && resultcode==RESULT_OK)
		{
			Log.e("picUri", cameraImagePath.getPath());
			picUri=cameraImagePath;
			//showPopup_for_TawkImage();
			iv_bill.setVisibility(View.VISIBLE);
			iv_bill.setImageBitmap(new CompressImage(AddExpenseActivity.this).compress(picUri.getPath()));

			expense_bill_status=true;
		}
		else if(requestcode==REQUEST_FOR_GALLERY && resultcode==RESULT_OK)
		{
			picUri = data.getData();
			String[] projection = { MediaStore.MediaColumns.DATA };
			CursorLoader cursorLoader = new CursorLoader(this,picUri, projection, null, null, null);
			Cursor cursor =cursorLoader.loadInBackground();
			int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
			cursor.moveToFirst();
			String selectedImagePath = cursor.getString(column_index);

			Log.e("picUri", picUri.getPath());
			Log.e("selectedImagePath", selectedImagePath);

			picUri=Uri.fromFile(new File(selectedImagePath));
			//showPopup_for_TawkImage();
			iv_bill.setVisibility(View.VISIBLE);
			iv_bill.setImageBitmap(new CompressImage(AddExpenseActivity.this).compress(picUri.getPath()));

			expense_bill_status=true;
		}
	}

	private void upload_image(String cost,String km, String desc, String e_date, String options) {
		Bitmap bmp=new CompressImage(AddExpenseActivity.this).compress(picUri.getPath());
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bmp.compress(Bitmap.CompressFormat.JPEG, 80, stream);
		byte[] bitmapBytes = stream.toByteArray();

		Map<String, Object> inputdata=new HashMap<>();
		inputdata.put("driver_id", driver_id);
		inputdata.put("category_id", StaticData.last_selected_category_id + "");
		inputdata.put("cost", cost);
		inputdata.put("kms", km);
		inputdata.put("image", bitmapBytes);
		inputdata.put("desc", desc);
		inputdata.put("date", e_date);
		inputdata.put("options", options);

		new InsertExpenseTask(this,inputdata,new InsertExpenseListener()).execute();
	}

	public byte[] bitmapToByteArray(Bitmap b)
	{
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		b.compress(Bitmap.CompressFormat.PNG, 100, stream);
		byte[] byteArray = stream.toByteArray();
		return byteArray;
	}
}
