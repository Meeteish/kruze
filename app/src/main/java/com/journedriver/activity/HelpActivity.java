package com.journedriver.activity;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.PopupWindow;

import com.journedriver.view.EditTextL;
import com.journedriver.view.TextViewL;
import com.journyapp.journyapp.R;
import com.journedriver.data.CompressImage;
import com.journedriver.data.UtilMethod;
import com.journedriver.task.InsertHelpTask;
import com.journedriver.task.TaskListener;
import com.journedriver.view.EditTextR;
import com.journedriver.view.TextViewB;
import com.journedriver.view.TextViewR;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class HelpActivity extends Activity implements OnClickListener{
	EditTextL edit_Description;
	ImageView iv_back;
	TextViewL tv_upload_picture;
	TextViewR tv_date,tv_helpline_number;
	TextViewB tv_send;
	Uri cameraImagePath;
	String absPath;
	int currentVersion = Build.VERSION.SDK_INT;
	private int REQUEST_FOR_CAMERA=11,REQUEST_FOR_GALLERY=12;
	Uri picUri;

	String date;int dd,mm,yy;
	String driver_id;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_help);

		SharedPreferences sp = getSharedPreferences("driver_info",MODE_PRIVATE);
		driver_id = sp.getString("driver_id",null);

		edit_Description=(EditTextL)findViewById(R.id.edit_description);
		tv_send= (TextViewB) findViewById(R.id.tv_Send);
		tv_send.setOnClickListener(this);
		tv_helpline_number=(TextViewR)findViewById(R.id.tv_helpline_number);
		tv_helpline_number.setOnClickListener(this);
		tv_date=(TextViewR)findViewById(R.id.tv_date);
		//tv_upload_picture = (TextViewR) findViewById(R.id.tv_upload_picture);
		//tv_upload_picture.setOnClickListener(this);
		iv_back=(ImageView) findViewById(R.id.iv_back);
		iv_back.setOnClickListener(this);

		date= UtilMethod.getTodaysDate();
		tv_date.setText(date.split("-")[0]);
		Log.i("date", date + "....");

		dd=Integer.parseInt(date.split("-")[0]);
		mm=Integer.parseInt(date.split("-")[1]);
		yy=Integer.parseInt(date.split("-")[2]);

	}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.iv_back:
			finish();
			break;
		case R.id.tv_Send:
			sendHelpDescription();
			break;
		//case R.id.tv_upload_picture:
		//	showPopup_for_ImageOption();
		//	break;
		case R.id.tv_helpline_number:
			Intent intent = new Intent(Intent.ACTION_DIAL);
			intent.setData(Uri.parse("tel:"+tv_helpline_number.getText().toString()));
			startActivity(intent);
			break;
		default:
			break;
		}
	}
	private void sendHelpDescription()
	{
		String description = edit_Description.getText().toString();
		if(UtilMethod.isStringNullOrBlank(description))
			UtilMethod.showToast("Description should not be left blank",this);
		else
		{
			ArrayList<NameValuePair> inputdata=new ArrayList<>();
			inputdata.add(new BasicNameValuePair("driver_id",driver_id));
			inputdata.add(new BasicNameValuePair("text",description));
			//inputdata.add(new BasicNameValuePair("image",image));

			new InsertHelpTask(this,inputdata,new InsertHelpListener()).execute();
		}
	}

	class InsertHelpListener implements TaskListener
	{
		@Override
		public void onSuccess(String msg) {
			showDialog("Message!!", "Your request is successfully submitted");
			edit_Description.setText("");
			edit_Description.requestFocus();
		}
		@Override
		public void onError(String msg) {
			showDialog("Message!!",msg);
		}
		void showDialog(String title, String message) {
			ContextThemeWrapper themedContext;
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				themedContext = new ContextThemeWrapper(HelpActivity.this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
			} else {
				themedContext = new ContextThemeWrapper(HelpActivity.this, android.R.style.Theme_Light_NoTitleBar);
			}
			final AlertDialog.Builder adialog = new AlertDialog.Builder(themedContext);
			adialog.setTitle(title);
			adialog.setMessage(message);
			adialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {

				}
			});
			adialog.show();
		}
	}
	PopupWindow popup;
	void showPopup_for_ImageOption()
	{
		View view = HelpActivity.this.getLayoutInflater().inflate(R.layout.layout_capture_image_option, null);
		TextViewR tv_cancel=(TextViewR) view.findViewById(R.id.tv_cancel);
		TextViewR tv_camera=(TextViewR) view.findViewById(R.id.tv_camera);
		TextViewR tv_gallery=(TextViewR) view.findViewById(R.id.tv_gallery);
		popup = new PopupWindow(HelpActivity.this);
		popup.setContentView(view);
		// popup.setBackgroundDrawable(new BitmapDrawable());
		popup.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
		popup.setBackgroundDrawable(new ColorDrawable(
				android.graphics.Color.TRANSPARENT));
		popup.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
		popup.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
		popup.setFocusable(true);

		tv_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				popup.dismiss();
			}
		});
		tv_camera.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				takeNewPicture();
				popup.dismiss();
			}
		});
		tv_gallery.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				pickfromGallery();
				popup.dismiss();
			}
		});

		popup.showAtLocation(view, Gravity.BOTTOM, 0, 0);
	}
	private void pickfromGallery() {
		Intent i = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		startActivityForResult(i, REQUEST_FOR_GALLERY);
	}
	private void takeNewPicture()
	{
		Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

		File sdcard=new File(Environment.getExternalStorageDirectory(),"Journe");
		if(!sdcard.exists())
			sdcard.mkdir();
		File image=new File(sdcard,"IMG_"+System.currentTimeMillis()+".jpeg");
		cameraImagePath= Uri.fromFile(image);
		takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, cameraImagePath);

		Log.e("", cameraImagePath.getPath());
		startActivityForResult(takePictureIntent, REQUEST_FOR_CAMERA);
	}
	@TargetApi(Build.VERSION_CODES.HONEYCOMB)
	@Override
	protected void onActivityResult(int requestcode, int resultcode, Intent data) {
		// TODO Auto-generated method stub
		super.onActivityResult(requestcode, resultcode, data);
		if(requestcode==REQUEST_FOR_CAMERA && resultcode==RESULT_OK)
		{
			Log.e("picUri", cameraImagePath.getPath());
			picUri=cameraImagePath;
			showPopup_for_TawkImage();
		}
		else if(requestcode==REQUEST_FOR_GALLERY && resultcode==RESULT_OK)
		{
			picUri = data.getData();
			String[] projection = { MediaStore.MediaColumns.DATA };
			CursorLoader cursorLoader = new CursorLoader(this,picUri, projection, null, null, null);
			Cursor cursor =cursorLoader.loadInBackground();
			int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
			cursor.moveToFirst();
			String selectedImagePath = cursor.getString(column_index);

			Log.e("picUri", picUri.getPath());
			Log.e("selectedImagePath", selectedImagePath);

			picUri=Uri.fromFile(new File(selectedImagePath));
			showPopup_for_TawkImage();
		}
	}
	AlertDialog dialog;
	void showPopup_for_TawkImage()
	{
		View view = getLayoutInflater().inflate(R.layout.layout_display_popup_tawk_image, null);
		TextViewR tv_send=(TextViewR) view.findViewById(R.id.tv_send);
		TextViewR tv_cancel=(TextViewR) view.findViewById(R.id.tv_cancel);
		ImageView iv_image=(ImageView) view.findViewById(R.id.iv_image);
		final EditTextR edit_description_text=(EditTextR) view.findViewById(R.id.edit_description_text);

		iv_image.setImageBitmap(new CompressImage(HelpActivity.this).compress(picUri.getPath()));

		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		int height = displaymetrics.heightPixels;
		int width = displaymetrics.widthPixels;

		AlertDialog.Builder build =new AlertDialog.Builder(HelpActivity.this);
		build.setView(view);
		dialog=build.create();
		dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));

		tv_send.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {

				upload_image(edit_description_text.getText().toString());
				dialog.dismiss();
			}
		});
		tv_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				dialog.dismiss();
			}
		});
		dialog.show();
	}

	private void upload_image(String comment) {
		Bitmap bmp=new CompressImage(HelpActivity.this).compress(picUri.getPath());
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bmp.compress(Bitmap.CompressFormat.JPEG, 80, stream);
		byte[] bitmapBytes = stream.toByteArray();

		Map<String, Object> inputdata=new HashMap<>();
		inputdata.put("driver_id", driver_id);
		inputdata.put("text", comment);
		inputdata.put("image", bitmapBytes);

		new InsertHelpTask(this,inputdata,new InsertHelpListener()).execute();
	}

	public byte[] bitmapToByteArray(Bitmap b)
	{
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		b.compress(Bitmap.CompressFormat.PNG, 100, stream);
		byte[] byteArray = stream.toByteArray();
		return byteArray;
	}
}
