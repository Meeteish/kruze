package com.journedriver.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.PopupWindow;

import com.journedriver.bean.TripBean;
import com.journedriver.bean.Trip_Stops;
import com.journedriver.data.StaticData;
import com.journedriver.data.UtilMethod;
import com.journedriver.expandable.ExpandableListDataPump_Trip;
import com.journedriver.expandable.ExpandableList_Trip_Adapter;
import com.journedriver.task.GetTripTask;
import com.journedriver.task.InsertCancelTripTask;
import com.journedriver.task.TaskListener;
import com.journedriver.view.EditTextL;
import com.journedriver.view.TextViewB;
import com.journedriver.view.TextViewL;
import com.journedriver.view.TextViewR;
import com.journyapp.journyapp.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

public class StartRouteActivity extends Activity implements OnClickListener{
	ImageView iv_back;
	TextViewB tv_trip_date;
	ExpandableListView expandable_rout_list;
	HashMap<TripBean, List<Trip_Stops>> expandableListDetail;
	ExpandableList_Trip_Adapter expandableListAdapter;
	List<TripBean> expandableListTitle;

	String driver_id;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_start);
		tv_trip_date=(TextViewB)findViewById(R.id.tv_trip_date);
		expandable_rout_list=(ExpandableListView) findViewById(R.id.expandable_rout_list);

		iv_back=(ImageView) findViewById(R.id.iv_back);
		iv_back.setOnClickListener(this);



		SharedPreferences sp = getSharedPreferences("driver_info",MODE_PRIVATE);
		driver_id = sp.getString("driver_id", null);

		gettripdetails();
/*
		SharedPreferences sp_ruuning_trip_detail=getSharedPreferences("running_trip_detail", MODE_PRIVATE);
		StaticData.running_trip_id=sp_ruuning_trip_detail.getString("id", null);

		if(StaticData.running_trip_id.equals("0")) {
			sp_ruuning_trip_detail.edit().putString("id", StaticData.trip_list.get(0).getTrip_id());
			StaticData.running_trip_id=StaticData.trip_list.get(0).getTrip_id();
		}


		SharedPreferences sharedPreferences=getSharedPreferences("tripdetail", MODE_PRIVATE);
		int trip_stop_id = sharedPreferences.getInt("trip_stop_id",0);
		sharedPreferences.edit().putInt("trip_stop_index", 0).commit();
		if(StaticData.trip_list.size()>0){
			for(TripBean t : StaticData.trip_list)
			{
				if(t.getTrip_id().equals(StaticData.running_trip_id))
				{
					for(int i=0;i<t.getTrip_stops().size();i++)
					{
						//if(t.getTrip_stops().get(i).getT_stop_id().equals(trip_stop_id+""))
						if(t.getTrip_stops().get(i).getT_stop().equals(trip_stop_id+""))
						{
							*//*if(i==t.getTrip_stops().size()-1)
							{

							}*//*

							StaticData.trip_stop_index=i+1;
							sharedPreferences.edit().putInt("trip_stop_index", (i+1)).commit();
						}
					}

				}
			}
		}

		StaticData.is_proceed=sharedPreferences.getBoolean("is_proceed", false);
		StaticData.trip_stop_index=sharedPreferences.getInt("trip_stop_index", 0);
*/

/*
		if(StaticData.trip_list.size()>0){
			SharedPreferences sp_ruuning_trip_detail=getSharedPreferences("running_trip_detail", MODE_PRIVATE);
			if(sp_ruuning_trip_detail.getString("id",null)==null) {
				SharedPreferences.Editor ed = sp_ruuning_trip_detail.edit();
				ed.putString("id", StaticData.trip_list.get(0).getTrip_id());
				ed.commit();

				StaticData.running_trip_id=StaticData.trip_list.get(0).getTrip_id();
			}
			else
			{
				StaticData.running_trip_id = sp_ruuning_trip_detail.getString("id", null);
			}
		}
		if(StaticData.running_trip_id!=null)
			Log.e("running_trip_id",StaticData.running_trip_id);
		else
			Log.e("running_trip_id","null");*/

		expandableListDetail = ExpandableListDataPump_Trip.getData();
		expandableListTitle = new ArrayList<TripBean>(expandableListDetail.keySet());
		expandableListAdapter = new ExpandableList_Trip_Adapter(StartRouteActivity.this, expandableListTitle, expandableListDetail);
		expandable_rout_list.setAdapter(expandableListAdapter);

		Date d=new Date();
		SimpleDateFormat dateformat=new SimpleDateFormat("dd-MM-yyyy");
		String date = dateformat.format(d);
		tv_trip_date.setText(date);

		expandable_rout_list.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
			@Override
			public boolean onItemLongClick(AdapterView<?> adapterView, View view, int i, long l) {
				canceltrip(i);
				return false;
			}
		});
		expandable_rout_list.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
			@Override
			public void onGroupExpand(int i) {
				Log.e("running_trip_id", StaticData.running_trip_id + "...");
				if (!StaticData.trip_list.get(i).getTrip_id().equals(StaticData.running_trip_id))
					expandable_rout_list.collapseGroup(i);
				/*for(int j=0;j<StaticData.trip_list.size();j++)
				{

					if(StaticData.trip_list.get(j).getTrip_id().equals(StaticData.running_trip_id))
					{
						expandable_rout_list.expandGroup(i);
					}
					else
					{
						expandable_rout_list.collapseGroup(i);
					}
				}*/
			}
		});


	}

	private void gettripdetails() {
		ArrayList<NameValuePair> inputdata=new ArrayList<NameValuePair>();
		inputdata.add(new BasicNameValuePair("driver_id", driver_id));

		new GetTripTask(this, inputdata, new TripListener()).execute();
	}

	class CancelTripListener implements TaskListener
	{
		@Override
		public void onSuccess(String msg) {
			showDialog("Message!!", msg);
			//gettripdetails();//refresh the list
			finish();
		}
		@Override
		public void onError(String msg) {
			showDialog("Message!!",msg);
		}
		void showDialog(String title, String message) {
			ContextThemeWrapper themedContext;
			if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				themedContext = new ContextThemeWrapper(StartRouteActivity.this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
			} else {
				themedContext = new ContextThemeWrapper(StartRouteActivity.this, android.R.style.Theme_Light_NoTitleBar);
			}
			final AlertDialog.Builder adialog = new AlertDialog.Builder(themedContext);
			adialog.setTitle(title);
			adialog.setMessage(message);
			adialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
				@Override
				public void onClick(DialogInterface dialog, int which) {

				}
			});
			adialog.show();
		}
	}

	class TripListener implements TaskListener {
		@Override
		public void onSuccess(String msg) {

			SharedPreferences sp_ruuning_trip_detail=getSharedPreferences("running_trip_detail", MODE_PRIVATE);
			StaticData.running_trip_id=sp_ruuning_trip_detail.getString("id", null);

			if(StaticData.running_trip_id.equals("0")) {
				sp_ruuning_trip_detail.edit().putString("id", StaticData.trip_list.get(0).getTrip_id());
				StaticData.running_trip_id=StaticData.trip_list.get(0).getTrip_id();
			}


			SharedPreferences sharedPreferences=getSharedPreferences("tripdetail", MODE_PRIVATE);
			int trip_stop_id = sharedPreferences.getInt("trip_stop_id",0);
			sharedPreferences.edit().putInt("trip_stop_index", 0).commit();
			if(StaticData.trip_list.size()>0){
				for(TripBean t : StaticData.trip_list)
				{
					if(t.getTrip_id().equals(StaticData.running_trip_id))
					{
						for(int i=0;i<t.getTrip_stops().size();i++)
						{
							if(t.getTrip_stops().get(i).getT_stop_id().equals(trip_stop_id+""))
							//if(t.getTrip_stops().get(i).getT_stop().equals(trip_stop_id+""))
							{
							/*if(i==t.getTrip_stops().size()-1)
							{

							}*/

								StaticData.trip_stop_index=i+1;
								sharedPreferences.edit().putInt("trip_stop_index", (i+1)).commit();
							}
						}

					}
				}
			}

			StaticData.is_proceed=sharedPreferences.getBoolean("is_proceed", false);
			StaticData.trip_stop_index=sharedPreferences.getInt("trip_stop_index", 0);

			expandableListDetail = ExpandableListDataPump_Trip.getData();
			expandableListTitle = new ArrayList<TripBean>(expandableListDetail.keySet());
			expandableListAdapter = new ExpandableList_Trip_Adapter(StartRouteActivity.this, expandableListTitle, expandableListDetail);
			expandable_rout_list.setAdapter(expandableListAdapter);
		}
		@Override
		public void onError(String msg) {
			// TODO Auto-generated method stub
			if(msg.equals("slow"))
			{
				UtilMethod.showServerError(StartRouteActivity.this);
			}
			else
			{
				ContextThemeWrapper themedContext;
				if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
					themedContext = new ContextThemeWrapper(StartRouteActivity.this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar );
				}
				else {
					themedContext = new ContextThemeWrapper(StartRouteActivity.this, android.R.style.Theme_Light_NoTitleBar );
				}
				final AlertDialog.Builder adialog=new AlertDialog.Builder(themedContext);
				adialog.setTitle("Message!!");
				adialog.setMessage(msg);
				adialog.setPositiveButton("Ok",new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
					}
				});
				adialog.show();
			}
		}
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
		case R.id.iv_back:
			finish();
			break;
		default:
			break;
		}
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		//gettripdetails();
		expandableListAdapter.notifyDataSetChanged();
	}

	PopupWindow popup;
	private void canceltrip(int i) {

		final TripBean trip=StaticData.trip_list.get(i);
		View view = getLayoutInflater().inflate(R.layout.layout_cancel_trip, null);
		TextViewL tv_route_title= (TextViewL) view.findViewById(R.id.tv_rout_title);
		final EditTextL ed_cancel_reason = (EditTextL) view.findViewById(R.id.edit_cancel_reason);
		TextViewR tv_cancel= (TextViewR) view.findViewById(R.id.tv_cancel);
		TextViewR tv_submit= (TextViewR) view.findViewById(R.id.tv_submit);

		tv_route_title.setText(trip.getBus_route_title());

		popup = new PopupWindow(this);
		popup.setContentView(view);
		popup.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
		popup.setBackgroundDrawable(new ColorDrawable(
				android.graphics.Color.TRANSPARENT));
		popup.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
		popup.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
		popup.setFocusable(true);

		tv_cancel.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				popup.dismiss();
			}
		});
		tv_submit.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View view) {
				String reason=ed_cancel_reason.getText().toString();
				if(UtilMethod.isStringNullOrBlank(reason)) {
					UtilMethod.showToast("Reason required", StartRouteActivity.this);
				}
				else
				{
					ArrayList<NameValuePair> inputdata=new ArrayList<NameValuePair>();
					inputdata.add(new BasicNameValuePair("driver_id",driver_id));
					inputdata.add(new BasicNameValuePair("trip_id",trip.getTrip_id()));
					inputdata.add(new BasicNameValuePair("reason",reason));

					new InsertCancelTripTask(StartRouteActivity.this,inputdata,new CancelTripListener()).execute();
					popup.dismiss();
				}
			}
		});
		popup.showAtLocation(view, Gravity.CENTER, 0, 0);
	}
}
