package com.journedriver.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;

import com.journedriver.bean.Expenses_Details;
import com.journedriver.data.StaticData;
import com.journedriver.data.UtilMethod;
import com.journedriver.view.TextViewL;
import com.journedriver.view.TextViewR;
import com.journepassenger.activity.SigninActivity;
import com.journepassenger.utilities.Session_manager;
import com.journyapp.journyapp.R;
import com.squareup.picasso.Picasso;

public class ExpenseDetailActivity extends Activity implements OnClickListener{
	ImageView iv_back,iv_expense_bill;
	TextViewL tv_expense_name,tv_expense_date,tv_expense_kms,tv_expense_cost,tv_expense_desc;
	TextViewR tv_date;
	Intent in;
	String date;int dd,mm,yy;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.activity_expense_detail);
		setContentView(R.layout.activity_expense_detail2);
		
		iv_expense_bill=(ImageView)findViewById(R.id.iv_expense_bill);
		tv_expense_cost=(TextViewL)findViewById(R.id.tv_expense_cost);
		tv_expense_date=(TextViewL)findViewById(R.id.tv_expense_date);
		tv_expense_kms=(TextViewL)findViewById(R.id.tv_expense_kms);
		tv_expense_name=(TextViewL)findViewById(R.id.tv_expense_name);
		tv_expense_desc=(TextViewL)findViewById(R.id.tv_expense_desc);
		tv_date=(TextViewR)findViewById(R.id.tv_date);

		int index = getIntent().getIntExtra("expense_index",0);
		Expenses_Details detail= StaticData.expense_details_list.get(index);
		tv_expense_cost.setText(detail.getE_detail_cost());
		tv_expense_name.setText(detail.getEc_name()+" "+detail.getE_detail_options());
		tv_expense_date.setText(detail.getE_detail_datetime());
		tv_expense_kms.setText(detail.getE_detail_kms());
		tv_expense_desc.setText(detail.getE_desc());

		Log.e("getE_detail_image()",detail.getE_detail_image());
		if(detail.getE_detail_image().length()>0)
			Picasso.with(this)
				.load(detail.getE_detail_image())
				//.resize(200, 200)
				//.transform(new CircleTransform())
				.into(iv_expense_bill);
		else
			Picasso.with(this)
					.load(R.drawable.nopreview)
							//.resize(200, 200)
							//.transform(new CircleTransform())
					.into(iv_expense_bill);

		iv_back=(ImageView) findViewById(R.id.iv_back);
		iv_back.setOnClickListener(this);

		date= UtilMethod.getTodaysDate();
		tv_date.setText(date.split("-")[0]);
		Log.i("date", date + "....");

		dd=Integer.parseInt(date.split("-")[0]);
		mm=Integer.parseInt(date.split("-")[1]);
		yy=Integer.parseInt(date.split("-")[2]);
	}
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.iv_back:
				finish();
				break;
			default:
				break;
		}
	}
}
