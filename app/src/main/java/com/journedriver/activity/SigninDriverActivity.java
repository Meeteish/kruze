package com.journedriver.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.Window;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginManager;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.plus.Plus;
import com.google.android.gms.plus.model.people.Person;
import com.journedriver.data.StaticData;
import com.journedriver.data.UtilMethod;
import com.journedriver.task.RequestForgotPasswordTask;
import com.journedriver.task.TaskListener;
import com.journedriver.view.EditTextL;
import com.journedriver.view.EditTextR;
import com.journedriver.view.TextViewB;
import com.journedriver.view.TextViewR;
import com.journepassenger.activity.DashBoardActivity;
import com.journepassenger.activity.SignUpActivity;
import com.journepassenger.utilities.GlobalValues;
import com.journepassenger.utilities.MethodManager_Listner;
import com.journepassenger.utilities.PlayServicesHelper;
import com.journepassenger.utilities.Session_manager;
import com.journyapp.journyapp.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by administrator on 22/1/16.
 */
public class SigninDriverActivity extends Activity
{
    private CallbackManager callbackManager;
    EditTextL edEmailId_signin;
    EditTextL edPasswordId_signin;

    TextViewR forgotpass_signin,tv_back_to_passenger;
    TextViewB submit_button_signin;
    private boolean mIntentInProgress;
    private static final int RC_SIGN_IN = 0;


    private static GoogleApiClient mGoogleApiClient;
    Context mContext;
    String device_token;
    String P_name;
    String P_email;
    String P_id;
    static LoginManager mLoginManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        FacebookSdk.sdkInitialize(this.getApplicationContext());
        callbackManager = CallbackManager.Factory.create();


        setContentView(R.layout.driverlogin);
        mContext = this;

        edEmailId_signin = (EditTextL) findViewById(R.id.edEmailId_signin);
        edPasswordId_signin = (EditTextL) findViewById(R.id.edPasswordId_signin);
        forgotpass_signin = (TextViewR) findViewById(R.id.forgotpass_signin);
        tv_back_to_passenger = (TextViewR) findViewById(R.id.tv_back_to_passenger);
        submit_button_signin = (TextViewB) findViewById(R.id.submit_button_signin);

        tv_back_to_passenger.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        forgotpass_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getPassword();
            }
        });
        submit_button_signin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String email=edEmailId_signin.getText().toString();
                String pass = edPasswordId_signin.getText().toString();
                if(UtilMethod.isStringNullOrBlank(email))
                    UtilMethod.showToast("Email Id required",SigninDriverActivity.this);
                else if(UtilMethod.isStringNullOrBlank(pass))
                    UtilMethod.showToast("Password required",SigninDriverActivity.this);
                else if(!UtilMethod.isValidEmail(email))
                    UtilMethod.showToast("Email Id is invalid",SigninDriverActivity.this);
                else {
                    hideKeyboard();
                    showprogress();
                    driverLogin();
                }
            }
        });
    }

    private void getPassword() {
        ContextThemeWrapper themedContext;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            themedContext = new ContextThemeWrapper(SigninDriverActivity.this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
        } else {
            themedContext = new ContextThemeWrapper(SigninDriverActivity.this, android.R.style.Theme_Light_NoTitleBar);
        }
        final AlertDialog.Builder adialog = new AlertDialog.Builder(themedContext);
        adialog.setTitle("Forgot Password");
        View view=getLayoutInflater().inflate(R.layout.driver_forgot_password,null);
        final EditTextR edEmailId = (EditTextR) view.findViewById(R.id.edEmailId_signin);
        adialog.setView(view);
        adialog.setPositiveButton("Get Password", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String email = edEmailId.getText().toString();
                if(UtilMethod.isStringNullOrBlank(email)) {
                    UtilMethod.showToast("Email Id required", SigninDriverActivity.this);
                    return;
                }
                else if(!UtilMethod.isValidEmail(email))
                {
                    UtilMethod.showToast("Email Id is invalid", SigninDriverActivity.this);
                    return;
                }
                else
                {
                    ArrayList<NameValuePair> inputdata=new ArrayList<NameValuePair>();
                    inputdata.add(new BasicNameValuePair("email",email));
                    new RequestForgotPasswordTask(SigninDriverActivity.this,inputdata,new ForgotPasswordListener()).execute();
                }
            }
        });
        adialog.show();
    }

    public void CheckUserLogin() {
        PlayServicesHelper playServiceHelper = new PlayServicesHelper(SigninDriverActivity.this);
        device_token = playServiceHelper.getRegistrationId();
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("passenger_email", edEmailId_signin.getText().toString());
        params.put("passenger_password", edPasswordId_signin.getText().toString());
        params.put("device_token", "abc");

        GlobalValues.getMethodManagerObj(mContext).usersignin(params, new MethodManager_Listner() {
            @Override
            public void onError() {

            }

            //passenger_name//passenger_email
            @Override
            public void onSuccess(String json) {
                Log.e("signin json", json.toString());
                try {
                    JSONObject jobject = new JSONObject(json);
                    String msg = jobject.getString("msg");
                    if (msg.equals("Invalid Usre Name or Password")) {
                        Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();
                    } else if (msg.equals("Login Successfully")) {
                        P_name = jobject.optString("passenger_name");
                        P_email = jobject.optString("passenger_email");
                        P_id = jobject.optString("passenger_id");

                        //Session_manager.saveUserInfo(mContext, P_name, P_email, P_id);
                        Session_manager.saveloginData(mContext, edEmailId_signin.getText().toString(), "Passenger");
                        Intent intent = new Intent(mContext, DashBoardActivity.class);
                        startActivity(intent);
                        finish();
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        });
    }
    ProgressDialog pdialog;
    public void showprogress() {
        ContextThemeWrapper themedContext;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            themedContext = new ContextThemeWrapper(mContext, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
        } else {
            themedContext = new ContextThemeWrapper(mContext, android.R.style.Theme_Light_NoTitleBar);
        }
        pdialog = new ProgressDialog(themedContext);
        pdialog.setCancelable(false);
        pdialog.setCanceledOnTouchOutside(false);
        pdialog.setMessage("Loading...");
        pdialog.show();
    }

    public void hideprogress() {
        if (pdialog != null) {
            pdialog.dismiss();
        }
    }

    public void driverLogin() {
        PlayServicesHelper playServiceHelper = new PlayServicesHelper(SigninDriverActivity.this);
        device_token = playServiceHelper.getRegistrationId();
        Log.e("device_token",device_token);


        Map<String, Object> params = new HashMap<String, Object>();
        params.put("driver_email", edEmailId_signin.getText().toString());
        params.put("driver_password", edPasswordId_signin.getText().toString());
        params.put("driver_device_token", device_token);
        params.put("driver_device_type", "1");

        GlobalValues.getMethodManagerObj(mContext).driversignin(params, new MethodManager_Listner() {
            @Override
            public void onError() {
                hideprogress();
            }

            @Override
            public void onSuccess(String json) {
                try {
                    hideprogress();
                    JSONObject jobject = new JSONObject(json);
                    /*
                    String msg = jobject.getString("msg");
                    if (msg.equals("Invalid Usre Name or Password")) {
                        Toast.makeText(mContext, msg, Toast.LENGTH_LONG).show();
                    } else if (msg.equals("Login Successfully")) {
                        Session_manager.saveloginData(mContext, edEmailId_signin.getText().toString(), "driver");
                        Intent intent = new Intent(mContext, HomeActivity.class);
                        startActivity(intent);
                        finish();
                    }*/
                    //
                    String status = jobject.getString("status");
                    if (status.equals("false")) {
                        Toast.makeText(mContext, jobject.getString("msg"), Toast.LENGTH_LONG).show();
                    } else if (status.equals("true")) {
                        Session_manager.saveloginData(mContext, edEmailId_signin.getText().toString(), "driver");

                        SharedPreferences sp = getSharedPreferences("driver_info", MODE_PRIVATE);
                        SharedPreferences.Editor ed = sp.edit();
                        ed.putString("driver_id", jobject.getString("driver_id"));
                        ed.commit();

                        /*SharedPreferences sharedPreferences=getSharedPreferences("tripdetail", MODE_PRIVATE);

                        int trip_id = jobject.getInt("trip_id");
                        int stop_id = jobject.getInt("stop_id");
                        sharedPreferences.edit().putInt("trip_stop_id", stop_id).commit();
                        //sharedPreferences.edit().putInt("trip_stop_index", stop_id - 1).commit();
                        //sharedPreferences.edit().putBoolean("is_proceed", false).commit();
                        StaticData.is_proceed=sharedPreferences.getBoolean("is_proceed", false);
                       // StaticData.trip_stop_index=sharedPreferences.getInt("trip_stop_index",0);
                        StaticData.trip_stop_index=sharedPreferences.getInt("trip_stop_index",0);

                        SharedPreferences sp_ruuning_trip_detail=getSharedPreferences("running_trip_detail", MODE_PRIVATE);
                        SharedPreferences.Editor edit = sp_ruuning_trip_detail.edit();
                        edit.putString("id", trip_id+"");
                        edit.commit();

                        StaticData.running_trip_id=sp_ruuning_trip_detail.getString("id", null);

                        if(StaticData.running_trip_id!=null)
                            Log.e("running_trip_id",StaticData.running_trip_id);
                        else
                            Log.e("running_trip_id","null");*/

                        Intent intent = new Intent(mContext, HomeActivity.class);
                        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                        startActivity(intent);
                        overridePendingTransition(android.R.anim.slide_in_left,android.R.anim.slide_out_right);
                        finish();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    hideprogress();
                }
            }
        });
    }

    class ForgotPasswordListener implements TaskListener
    {
        @Override
        public void onSuccess(String msg) {
            showDialog("Forgot Password", msg);

        }
        @Override
        public void onError(String msg) {
            showDialog("Forgot Password",msg);
        }
        void showDialog(String title, String message) {
            ContextThemeWrapper themedContext;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                themedContext = new ContextThemeWrapper(SigninDriverActivity.this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
            } else {
                themedContext = new ContextThemeWrapper(SigninDriverActivity.this, android.R.style.Theme_Light_NoTitleBar);
            }
            final AlertDialog.Builder adialog = new AlertDialog.Builder(themedContext);
            adialog.setTitle(title);
            adialog.setMessage(message);
            adialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            adialog.show();
        }
    }
    public void hideKeyboard(){
/* InputMethodManager inputMethodManager = (InputMethodManager) context
.getSystemService(Activity.INPUT_METHOD_SERVICE);
inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);*/

        InputMethodManager inputManager = (InputMethodManager)getSystemService(
                Context.INPUT_METHOD_SERVICE);
        View focusedView = SigninDriverActivity.this.getCurrentFocus();
        if (focusedView != null) {
            inputManager.hideSoftInputFromWindow(focusedView.getWindowToken(),
                    InputMethodManager.HIDE_NOT_ALWAYS);
        }
    }
}


