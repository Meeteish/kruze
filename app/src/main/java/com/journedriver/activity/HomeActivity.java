package com.journedriver.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.journedriver.adapter.DailyRideListAdapter;
import com.journedriver.bean.DailyRide;
import com.journedriver.data.StaticData;
import com.journedriver.data.UtilMethod;
import com.journedriver.task.GetDailyRideListTask;
import com.journedriver.task.GetDailyRideListTaskWithNoDialog;
import com.journedriver.task.GetLogoutStatusTask;
import com.journedriver.task.GetTripTask;
import com.journedriver.task.TaskListener;
import com.journepassenger.activity.SigninActivity;
import com.journepassenger.utilities.Session_manager;
import com.journyapp.journyapp.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;

public class HomeActivity extends Activity implements OnClickListener{
	ImageView iv_start,iv_cash,iv_fuel,iv_km,iv_help,iv_logout;
	Intent in;
	String driver_id;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_home);
		
		iv_start=(ImageView) findViewById(R.id.iv_start);
		iv_cash=(ImageView) findViewById(R.id.iv_cash);
		iv_fuel=(ImageView) findViewById(R.id.iv_fuel);
		iv_km=(ImageView) findViewById(R.id.iv_km);
		iv_help=(ImageView) findViewById(R.id.iv_help);
		iv_logout=(ImageView) findViewById(R.id.iv_logout);
		
		iv_start.setOnClickListener(this);
		iv_cash.setOnClickListener(this);
		iv_fuel.setOnClickListener(this);
		iv_km.setOnClickListener(this);
		iv_help.setOnClickListener(this);
		iv_logout.setOnClickListener(this);

		SharedPreferences sp = getSharedPreferences("driver_info",MODE_PRIVATE);
		driver_id = sp.getString("driver_id",null);
		gettripdetails();

		DisplayMetrics displaymetrics = new DisplayMetrics();
		getWindowManager().getDefaultDisplay().getMetrics(displaymetrics);
		int height = displaymetrics.heightPixels;
		int width = displaymetrics.widthPixels;

		int h = height/3-70;
		int w = width/2-70;

		LinearLayout.LayoutParams lp=new LinearLayout.LayoutParams(w,h);
		lp.bottomMargin=10;
		lp.topMargin=10;
		lp.rightMargin=10;
		lp.leftMargin=10;
		lp.weight=1;


		iv_start.setLayoutParams(lp);
		iv_cash.setLayoutParams(lp);
		iv_fuel.setLayoutParams(lp);
		iv_km.setLayoutParams(lp);
		iv_help.setLayoutParams(lp);
		iv_logout.setLayoutParams(lp);


//		SharedPreferences sp=getSharedPreferences("driver_info",MODE_PRIVATE);
//		SharedPreferences.Editor ed = sp.edit();
//		ed.putString("driver_id","2");
//		ed.commit();

	}
	private void gettripdetails() {
		ArrayList<NameValuePair> inputdata=new ArrayList<NameValuePair>();
		inputdata.add(new BasicNameValuePair("driver_id", driver_id));

		new GetTripTask(this, inputdata, new TripListener()).execute();
	}
	@Override
	public void onClick(View v) {
		in=null;
		switch (v.getId()) {
		case R.id.iv_start:
			in=new Intent(this, StartRouteActivity.class);
			break;
		case R.id.iv_cash:
			in=new Intent(this, CashActivity.class);
			break;
		case R.id.iv_fuel:
			in=new Intent(this, ExpenseListActivity.class);
			break;
		case R.id.iv_km:
			in=new Intent(this, KilometersActivity.class);
			break;
		case R.id.iv_help:
			in=new Intent(this, HelpActivity.class);
			break;
		case R.id.iv_logout:

			doLogout();
			break;
		default:
			break;
		}
		if(in!=null)
		{
			startActivity(in);
			overridePendingTransition(android.R.anim.slide_in_left,android.R.anim.slide_out_right);
			//overridePendingTransition(R.anim.slide_out_right,R.anim.slide_out_left);
			//overridePendingTransition(R.anim.push_down_in,R.anim.push_up_out);
		}
	}

	static boolean flag=false;
	private void doLogout() {
		final ContextThemeWrapper themedContext;
		if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
			themedContext = new ContextThemeWrapper(HomeActivity.this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar );
		}
		else {
			themedContext = new ContextThemeWrapper(HomeActivity.this, android.R.style.Theme_Light_NoTitleBar );
		}
		final AlertDialog.Builder adialog=new AlertDialog.Builder(themedContext);
		adialog.setTitle("Message!!");
		adialog.setMessage("Are you sure you want to Logout?");
		adialog.setPositiveButton("Logout", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				//----------------------

				/*SharedPreferences splogoutstatus = getSharedPreferences("logoutstatus", MODE_PRIVATE);
				boolean status = splogoutstatus.getBoolean("status", true);

				if (status) {
					Session_manager.ClearPrefrence(HomeActivity.this);

					SharedPreferences sp = getSharedPreferences("driver_info", MODE_PRIVATE);
					SharedPreferences.Editor ed = sp.edit();
					ed.putString("driver_id", null);
					ed.commit();

					in = new Intent(HomeActivity.this, SigninActivity.class);
					startActivity(in);
					in = null;
					finish();
				} else {
					final AlertDialog.Builder builder = new AlertDialog.Builder(themedContext);
					builder.setTitle("Unable to LOGOUT!");
					builder.setMessage("Please complete the running trip \nOr\nUpdate End Kms for the trip.");
					builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialogInterface, int i) {

						}
					});
					builder.show();
				}*/

				ArrayList<NameValuePair> inputdata=new ArrayList<>();
				inputdata.add(new BasicNameValuePair("driver_id",driver_id));
				new GetLogoutStatusTask(HomeActivity.this,inputdata,new LogoutListener()).execute();

				/*boolean status = false;
				for (DailyRide ride : StaticData.todays_daily_ride_list) {
					if (ride.getKm_trip_id().equals(StaticData.running_trip_id)) {
						if ((ride.getStart_kms() == null || ride.getStart_kms().equals("")) || (ride.getEnd_kms() == null || ride.getEnd_kms().equals(""))) {
							status = true;
							break;
						}
					}
				}

				if (status) {
					final AlertDialog.Builder builder = new AlertDialog.Builder(themedContext);
					builder.setTitle("Unable to LOGOUT!");
					builder.setMessage("Please complete the running trip \nOr\nUpdate End Kms for the trip.");
					builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
						@Override
						public void onClick(DialogInterface dialogInterface, int i) {

						}
					});
					builder.show();
				} else {
					Session_manager.ClearPrefrence(HomeActivity.this);

					SharedPreferences sp = getSharedPreferences("driver_info", MODE_PRIVATE);
					SharedPreferences.Editor ed = sp.edit();
					ed.putString("driver_id", null);
					ed.commit();

					in = new Intent(HomeActivity.this, SigninActivity.class);
					startActivity(in);
					in = null;
					finish();
				}*/

				/*int trip_stop_index = StaticData.trip_stop_index;
				if (trip_stop_index != 0) {
					if (StaticData.trip_list.size() > 0) {
						String running_trip_id = StaticData.running_trip_id;
						for (DailyRide ride : StaticData.daily_ride_list) {
							if (ride.getKm_trip_id().equals(running_trip_id)) {
								if ((ride.getStart_kms() == null || ride.getStart_kms().equals("")) || (ride.getEnd_kms() == null || ride.getEnd_kms().equals(""))) {
									AlertDialog.Builder d = new AlertDialog.Builder(themedContext);
									d.setTitle("Error");
									d.setMessage("Unable to LOGOUT\nplease enter ride Kilometres detail");
									d.setPositiveButton("OK", new DialogInterface.OnClickListener() {
										@Override
										public void onClick(DialogInterface dialogInterface, int i) {
											flag = true;
											Intent in = new Intent(HomeActivity.this, KilometersActivity.class);
											startActivity(in);
										}
									});
									d.show();
								} else {
									Session_manager.ClearPrefrence(HomeActivity.this);

									SharedPreferences sp = getSharedPreferences("driver_info", MODE_PRIVATE);
									SharedPreferences.Editor ed = sp.edit();
									ed.putString("driver_id", null);
									ed.commit();

									in = new Intent(HomeActivity.this, SigninActivity.class);
									startActivity(in);
									in = null;
									finish();
								}
							}
						}
					}
				}*/

				//----------------------
				/*if (flag == false) {
					Session_manager.ClearPrefrence(HomeActivity.this);

					SharedPreferences sp = getSharedPreferences("driver_info", MODE_PRIVATE);
					SharedPreferences.Editor ed = sp.edit();
					ed.putString("driver_id", null);
					ed.commit();

					in = new Intent(HomeActivity.this, SigninActivity.class);
					startActivity(in);
					in = null;
					finish();
				}*/
			}
		});
		adialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {

			}
		});
		adialog.show();
	}

	class TripListener implements TaskListener
	{
		@Override
		public void onSuccess(String msg) {
				ArrayList<NameValuePair> inputdata=new ArrayList<>();
				inputdata.add(new BasicNameValuePair("driver_id",driver_id));
				inputdata.add(new BasicNameValuePair("date",UtilMethod.getTodaysDate()));
				//inputdata.add(new BasicNameValuePair("date","18-02-2016"));
				new GetDailyRideListTaskWithNoDialog(HomeActivity.this,inputdata,new TaskListener()
				{
					@Override
					public void onSuccess(String msg) {
						Log.e("DailyRideListSize",StaticData.daily_ride_list.size()+"...");
					}
					@Override
					public void onError(String msg) {

					}
				}).execute();
		}

		@Override
		public void onError(String msg) {
			// TODO Auto-generated method stub
			if(msg.equals("slow"))
			{
				UtilMethod.showServerError(HomeActivity.this);
			}
			else
			{
				ContextThemeWrapper themedContext;
				if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
					themedContext = new ContextThemeWrapper(HomeActivity.this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar );
				}
				else {
					themedContext = new ContextThemeWrapper(HomeActivity.this, android.R.style.Theme_Light_NoTitleBar );
				}
				final AlertDialog.Builder adialog=new AlertDialog.Builder(themedContext);
				adialog.setTitle("Login");
				adialog.setMessage(msg);
				adialog.setPositiveButton("OK",new DialogInterface.OnClickListener()
				{
					@Override
					public void onClick(DialogInterface dialog, int which)
					{
					}
				});
				adialog.show();
			}
		}
	}

	class LogoutListener implements TaskListener
	{
		@Override
		public void onSuccess(String msg) {
			Session_manager.ClearPrefrence(HomeActivity.this);

			SharedPreferences sp = getSharedPreferences("driver_info", MODE_PRIVATE);
			SharedPreferences.Editor ed = sp.edit();
			ed.putString("driver_id", null);
			ed.commit();

			in = new Intent(HomeActivity.this, SigninActivity.class);
			startActivity(in);
			in = null;
			finish();
		}

		@Override
		public void onError(String msg) {
			// TODO Auto-generated method stub
			if(msg.equals("slow"))
			{
				UtilMethod.showServerError(HomeActivity.this);
			}
			else
			{
				ContextThemeWrapper themedContext;
				if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
					themedContext = new ContextThemeWrapper(HomeActivity.this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar );
				}
				else {
					themedContext = new ContextThemeWrapper(HomeActivity.this, android.R.style.Theme_Light_NoTitleBar );
				}
				final AlertDialog.Builder builder = new AlertDialog.Builder(themedContext);
				builder.setTitle("Unable to LOGOUT!");
				//builder.setMessage("Please complete the running trip \nOr\nUpdate End Kms for the trip.");
				builder.setMessage(msg+"\nOr\nUpdate End Kms for the trip.");
				builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialogInterface, int i) {

					}
				});
				builder.show();
			}
		}
	}
}
