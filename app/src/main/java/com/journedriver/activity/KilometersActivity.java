package com.journedriver.activity;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.journedriver.data.StaticData;
import com.journedriver.data.UtilMethod;
import com.journedriver.fragment.Fragment_KM_DailyRide;
import com.journedriver.fragment.Fragment_KM_DailyRide_List;
import com.journedriver.fragment.Fragment_KM_WeekendRide;
import com.journedriver.fragment.Fragment_KM_WeekendRide_List;
import com.journedriver.view.TextViewB;
import com.journedriver.view.TextViewR;
import com.journyapp.journyapp.R;

import java.util.Date;

public class KilometersActivity extends FragmentActivity implements OnClickListener {
	ImageView iv_back;
	TextViewB iv_add;
	TextViewR tv_date;
	TextViewB tv_daily_ride_text, tv_weekend_ride_text;
	LinearLayout tv_daily_ride,tv_weekend_ride;
	Fragment_KM_DailyRide_List fragment_km_dailyRide_list;
	Fragment_KM_WeekendRide_List fragment_km_weekendRide_list;
	Fragment_KM_DailyRide fragment_daily_ride;
	Fragment_KM_WeekendRide fragment_weekend_ride;
	public static String date2;String date; int dd,mm,yy;
	FrameLayout frame_calendar;
	DatePicker dp;
	int currentfragment=1;//1=dailyride, 2=weekendride
	Fragment currentFragment;
	public static boolean isadd_running=false;
	public static boolean shouldAddMoreVisible=true;
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_km);

		isadd_running=false;
		shouldAddMoreVisible=true;

		fragment_km_dailyRide_list=new Fragment_KM_DailyRide_List();
		fragment_km_weekendRide_list=new Fragment_KM_WeekendRide_List();
		fragment_daily_ride=new Fragment_KM_DailyRide();
		fragment_weekend_ride=new Fragment_KM_WeekendRide();

		frame_calendar=(FrameLayout)findViewById(R.id.frame_calendar);
		frame_calendar.setOnClickListener(this);

		tv_daily_ride_text=(TextViewB)findViewById(R.id.tv_daily_ride_text);
		tv_weekend_ride_text=(TextViewB)findViewById(R.id.tv_weekend_ride_text);
		tv_daily_ride=(LinearLayout)findViewById(R.id.tv_daily_ride);
		tv_weekend_ride=(LinearLayout)findViewById(R.id.tv_weekend_ride);
		tv_daily_ride.setOnClickListener(this);
		tv_weekend_ride.setOnClickListener(this);
		iv_back=(ImageView) findViewById(R.id.iv_back);
		iv_back.setOnClickListener(this);
		tv_date=(TextViewR)findViewById(R.id.tv_date);
		iv_add=(TextViewB) findViewById(R.id.iv_add);
		iv_add.setOnClickListener(this);

		StaticData.km_date=UtilMethod.getTodaysDate();
		date= UtilMethod.getTodaysDate(); date2=date;
		tv_date.setText(date.split("-")[0]);
		Log.i("date", date + "....");

		dd=Integer.parseInt(date.split("-")[0]);
		mm=Integer.parseInt(date.split("-")[1]);
		yy=Integer.parseInt(date.split("-")[2]);

		StaticData.isTodaysDailyRideFilled=false;
		StaticData.todays_daily_ride_list.clear();

		showDefaultFragment(fragment_km_dailyRide_list);
		currentfragment=1;
	}

	public void showDefaultFragment(Fragment fragment ) {
		FragmentManager fm=getSupportFragmentManager();
		FragmentTransaction ft = fm.beginTransaction();
		ft.replace(R.id.container,fragment);
		ft.commit();
		currentFragment=fragment;
	}

	public void setAddMoreVisibleState()
	{
		if(shouldAddMoreVisible)
		{
			iv_add.setVisibility(View.VISIBLE);
		}
		else
		{
			iv_add.setVisibility(View.GONE);
		}
	}

	Fragment fragment;
	@Override
	public void onClick(View v) {
		fragment=null;
		switch (v.getId()) {
		case R.id.frame_calendar:
			if(!isadd_running) {
				DatePickerDialog dp=new DatePickerDialog(this,android.R.style.Theme_Holo_Light_Dialog_NoActionBar, new DatePickerDialog.OnDateSetListener() {
					@Override
					public void onDateSet(DatePicker datePicker, int year, int monthOfYear, int dayOfMonth) {
						if(datePicker.isShown()) {
							date = dayOfMonth + "-" + (monthOfYear + 1) + "-" + year; date2=date;
							dd = dayOfMonth;
							mm = monthOfYear;
							yy = year;
							tv_date.setText(dd + "");
							StaticData.km_date = date;
							if (currentfragment == 1) {
								fragment_km_dailyRide_list.getDailyRideList();
								currentfragment = 1;
							} else {
								fragment_km_weekendRide_list.getWeekendRideList();
								currentfragment = 2;
							}

							date= UtilMethod.getTodaysDate();
							//tv_date.setText(date.split("-")[0]);
							Log.i("date", date + "....");

							dd=Integer.parseInt(date.split("-")[0]);
							mm=Integer.parseInt(date.split("-")[1])-1;
							yy=Integer.parseInt(date.split("-")[2]);
						}
					}
				},yy,mm,dd);
				dp.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
				dp.getDatePicker().setMaxDate(new Date().getTime());
				dp.getDatePicker().setMinDate(System.currentTimeMillis() - (1000 * 60 * 60 * 24 * 3));
				dp.show();
			}
			else
			{
				Toast.makeText(this,"Facility is available for List only",Toast.LENGTH_LONG).show();
			}
			break;
		case R.id.tv_daily_ride:
			fragment=fragment_km_dailyRide_list;
			tv_daily_ride.setBackgroundResource(R.color.below_header_bg_color);
			tv_weekend_ride.setBackgroundResource(R.color.text_color_black);
			tv_daily_ride_text.setTextColor(getResources().getColor(R.color.text_color_white));
			tv_weekend_ride_text.setTextColor(getResources().getColor(R.color.text_color_gray));

			shouldAddMoreVisible=true;
			setAddMoreVisibleState();

			currentfragment=1;
			/*if(isadd_running)
				getSupportFragmentManager().popBackStack();*/
			isadd_running=false;
			break;
		case R.id.tv_weekend_ride:
			fragment=fragment_km_weekendRide_list;
			tv_daily_ride.setBackgroundResource(R.color.text_color_black);
			tv_weekend_ride.setBackgroundResource(R.color.below_header_bg_color);
			tv_daily_ride_text.setTextColor(getResources().getColor(R.color.text_color_gray));
			tv_weekend_ride_text.setTextColor(getResources().getColor(R.color.text_color_white));

			shouldAddMoreVisible=true;
			setAddMoreVisibleState();

			currentfragment=2;
			/*if(isadd_running)
				getSupportFragmentManager().popBackStack();*/
			isadd_running=false;
			break;
		case R.id.iv_add:
			isadd_running=true;
			if(currentfragment==1)
			{
				fragment=new Fragment_KM_DailyRide();
			}else
			{
				fragment=new Fragment_KM_WeekendRide();
			}
			shouldAddMoreVisible=false;
			setAddMoreVisibleState();
			/*FragmentManager fm=getSupportFragmentManager();
			FragmentTransaction ft = fm.beginTransaction();
			ft.replace(R.id.container,fragment);
			ft.addToBackStack(null);
			ft.commit();
			currentFragment=fragment;
			fragment=null;*/
			break;
		case R.id.iv_back:
			finish();
			break;
		default:
			break;
		}
		if(fragment!=null)
		{
			showDefaultFragment(fragment);
		}
	}

}
