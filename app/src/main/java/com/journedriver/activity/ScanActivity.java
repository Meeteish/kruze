package com.journedriver.activity;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.graphics.drawable.ColorDrawable;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.itextpdf.text.pdf.parser.Line;
import com.journedriver.adapter.PassengersListAdapter;
import com.journedriver.adapter.SeatListAdapter;
import com.journedriver.bean.Passengers;
import com.journedriver.data.StaticData;
import com.journedriver.data.UtilMethod;
import com.journedriver.qr.CameraPreview;
import com.journedriver.task.GetPassengersTask;
import com.journedriver.task.InsertAbsentPassengerTask;
import com.journedriver.task.InsertBoardedPassengerTask;
import com.journedriver.task.RequestForgotPasswordTask;
import com.journedriver.task.TaskListener;
import com.journedriver.view.EditTextL;
import com.journedriver.view.EditTextR;
import com.journedriver.view.TextViewB;
import com.journedriver.view.TextViewL;
import com.journedriver.view.TextViewR;
import com.journyapp.journyapp.R;

import net.sourceforge.zbar.Config;
import net.sourceforge.zbar.Image;
import net.sourceforge.zbar.ImageScanner;
import net.sourceforge.zbar.Symbol;
import net.sourceforge.zbar.SymbolSet;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

public class ScanActivity extends Activity implements OnClickListener, View.OnTouchListener, AdapterView.OnItemClickListener {
    ImageView iv_back, iv_tap;
    LinearLayout layout_scan_again;

    private Camera mCamera;
    private CameraPreview mPreview;
    private Handler autoFocusHandler;

    private Button scanButton;
    private ImageScanner scanner;

    private boolean barcodeScanned = false;
    private boolean previewing = true;

    ListView lv_passengers;
    PassengersListAdapter adapter;

    TextViewR tv_contact_cc;
    LinearLayout tv_verifyuser;
    TextViewB tv_proceed;

    static {
        System.loadLibrary("iconv");
    }

    boolean paymentcollectiostatus;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_scan);
        iv_tap = (ImageView) findViewById(R.id.iv_tap);
        layout_scan_again = (LinearLayout) findViewById(R.id.layout_scan_again);
        initControls();

        iv_back = (ImageView) findViewById(R.id.iv_back);
        iv_back.setOnClickListener(this);

        tv_verifyuser = (LinearLayout) findViewById(R.id.tv_verifyuser);
        tv_verifyuser.setOnClickListener(this);

        tv_contact_cc = (TextViewR) findViewById(R.id.tv_contact_cc);
        tv_contact_cc.setOnClickListener(this);

        tv_proceed = (TextViewB) findViewById(R.id.tv_proceed);
        tv_proceed.setOnClickListener(this);

        lv_passengers = (ListView) findViewById(R.id.lv_passengers);
        lv_passengers.setOnTouchListener(this);
        lv_passengers.setOnItemClickListener(this);

        Intent in = getIntent();
        String b_trip_id = in.getStringExtra("b_trip_id");
        String b_bus_id = in.getStringExtra("b_bus_id");
        String stop_id = in.getStringExtra("stop_id");


        StaticData.boarded_passenger_list.clear();
        StaticData.absent_passenger_list.clear();

        //getPassengers("3", "4", "2");
        getPassengers(b_trip_id, b_bus_id, stop_id);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tv_proceed:
                proceed();
                break;
            case R.id.tv_contact_cc:
                contact_cc();
                break;
            case R.id.tv_verifyuser:
                verifyuser();
                break;
            case R.id.iv_back:
                finish();
                break;
            default:
                break;
        }
    }

    @Override
    public void finish() {
        releaseCamera();
        super.finish();
    }

    private void initControls() {
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        autoFocusHandler = new Handler();
        mCamera = getCameraInstance();

        // Instance barcode scanner
        scanner = new ImageScanner();
        scanner.setConfig(0, Config.X_DENSITY, 3);
        scanner.setConfig(0, Config.Y_DENSITY, 3);

        mPreview = new CameraPreview(ScanActivity.this, mCamera, previewCb,
                autoFocusCB);
        FrameLayout preview = (FrameLayout) findViewById(R.id.cameraPreview);
        preview.addView(mPreview);

        layout_scan_again.setOnClickListener(new OnClickListener() {
            //preview.setOnClickListener(new OnClickListener() {
            //iv_tap.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (barcodeScanned) {
                    barcodeScanned = false;
                    mCamera.setPreviewCallback(previewCb);
                    mCamera.startPreview();
                    previewing = true;
                    mCamera.autoFocus(autoFocusCB);
                    //iv_tap.setVisibility(View.INVISIBLE);
                    layout_scan_again.setVisibility(View.INVISIBLE);
                }
            }
        });
    }

    private void getPassengers(String b_trip_id, String b_bus_id, String stop_id) {
        ArrayList<NameValuePair> inputdata = new ArrayList<>();
        inputdata.add(new BasicNameValuePair("b_trip_id", b_trip_id));
        inputdata.add(new BasicNameValuePair("b_bus_id", b_bus_id));
        inputdata.add(new BasicNameValuePair("stop_id", stop_id));

        new GetPassengersTask(this, inputdata, new GetPassengersListener()).execute();
    }

    public static Camera getCameraInstance() {
        Camera c = null;
        try {
            c = Camera.open();
        } catch (Exception e) {
        }
        return c;
    }

    private void releaseCamera() {
        if (mCamera != null) {
            previewing = false;
            mCamera.setPreviewCallback(null);
            mCamera.release();
            mCamera = null;
        }
    }

    private Runnable doAutoFocus = new Runnable() {
        public void run() {
            if (previewing)
                mCamera.autoFocus(autoFocusCB);
        }
    };
    ContextThemeWrapper themedContext;
    Camera.PreviewCallback previewCb = new Camera.PreviewCallback() {
        public void onPreviewFrame(byte[] data, Camera camera) {
            Camera.Parameters parameters = camera.getParameters();
            Camera.Size size = parameters.getPreviewSize();

            Image barcode = new Image(size.width, size.height, "Y800");
            barcode.setData(data);

            int result = scanner.scanImage(barcode);

            if (result != 0) {
                previewing = false;
                mCamera.setPreviewCallback(null);
                mCamera.stopPreview();

                SymbolSet syms = scanner.getResults();
                for (Symbol sym : syms) {

                    Log.i("<<<<<<Asset Code>>>>> ",
                            "<<<<Bar Code>>> " + sym.getData());
                    String scanResult = sym.getData().trim();

                    //showAlertDialog(scanResult);
                    Log.e("scanResult", scanResult + "...");

                  /*  Toast.makeText(BarcodeScanner.this, scanResult,
                            Toast.LENGTH_SHORT).show();*/

                    barcodeScanned = true;

                    //for(final Passengers p : StaticData.passenger_list)
                    for (int i = 0; i < StaticData.passenger_list.size(); i++) {
                        final int j = i;
                        final Passengers p = StaticData.passenger_list.get(i);
                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                            themedContext = new ContextThemeWrapper(ScanActivity.this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
                        } else {
                            themedContext = new ContextThemeWrapper(ScanActivity.this, android.R.style.Theme_Light_NoTitleBar);
                        }

                        if (p.getB_qr_code().equals(scanResult)) {
                            //code to verify seats
                            String[] seats = p.getSeats().split(",");

                            if (seats.length > 0) {
                                final android.app.AlertDialog.Builder adialog = new android.app.AlertDialog.Builder(themedContext);
                                adialog.setCancelable(false);

                                View v = getLayoutInflater().inflate(R.layout.activity_scan_layout_verify, null);
                                TextViewL tv_passenger_name = (TextViewL) v.findViewById(R.id.tv_passenger_name);
                                TextViewL tv_ticket_no = (TextViewL) v.findViewById(R.id.tv_ticket_no);
                                ListView list_seats = (ListView) v.findViewById(R.id.list_seats);

                                tv_passenger_name.setText(p.getPassenger_name());
                                tv_ticket_no.setText("Ticket No - " + p.getB_id());

                                StaticData.left_seats.clear();
                                for (String ss : seats)
                                    StaticData.left_seats.add(ss);
                                StaticData.seats.clear();
                                SeatListAdapter ad = new SeatListAdapter(ScanActivity.this, seats);
                                list_seats.setAdapter(ad);

                                adialog.setView(v);
                                adialog.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        //code to collect payment if needed
                                        if (p.getB_payment_status().equals("2")) {
                                            final android.app.AlertDialog.Builder adialog = new android.app.AlertDialog.Builder(themedContext);
                                            adialog.setCancelable(false);
                                            adialog.setTitle("Collect Payment");
                                            adialog.setMessage("Did you received Rs. " + p.getB_payment_amount() + " from this user?");
                                            adialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    p.setB_payment_status("1");
                                                    p.setB_verify("1");

                                                    Passengers newp = new Passengers();
                                                    newp.setB_id(p.getB_id());
                                                    newp.setSeats(android.text.TextUtils.join(",", StaticData.seats));
                                                    Log.e("selectedseat", newp.getSeats());
                                                    StaticData.boarded_passenger_list.add(newp);

                                                    Passengers leftp = new Passengers();
                                                    leftp.setB_id(p.getB_id());
                                                    leftp.setSeats(android.text.TextUtils.join(",", StaticData.left_seats));
                                                    Log.e("leftseat", leftp.getSeats());
                                                    StaticData.absent_passenger_list.add(leftp);


                                                    Log.e("boarded_passenger_list", StaticData.boarded_passenger_list.toString());
                                                    Log.e("absent_passenger_list", StaticData.absent_passenger_list.toString());
                                                    //adapter.notifyDataSetChanged();
                                                    updatepassengerlist(j);
                                                    Log.e("QR_code matched", "Yes");
                                                }
                                            });
                                            adialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {

                                                }
                                            });

                                            adialog.show();
                                        } else {
                                            p.setB_verify("1");
                                            //adapter.notifyDataSetChanged();

                                            Passengers newp = new Passengers();
                                            newp.setB_id(p.getB_id());
                                            newp.setSeats(android.text.TextUtils.join(",", StaticData.seats));
                                            Log.e("selectedseat", newp.getSeats());
                                            StaticData.boarded_passenger_list.add(newp);

                                            Passengers leftp = new Passengers();
                                            leftp.setB_id(p.getB_id());
                                            leftp.setSeats(android.text.TextUtils.join(",", StaticData.left_seats));
                                            Log.e("leftseat", leftp.getSeats());
                                            StaticData.absent_passenger_list.add(leftp);

                                            Log.e("boarded_passenger_list", StaticData.boarded_passenger_list.toString());
                                            Log.e("absent_passenger_list", StaticData.absent_passenger_list.toString());

                                            updatepassengerlist(j);
                                            Log.e("QR_code matched", "Yes");
                                        }

                                    }
                                });
                                adialog.show();
                            }
                        } else
                            Log.e("QR_code matched", "No");
                    }
                    break;
                }
                //iv_tap.setVisibility(View.VISIBLE);
                layout_scan_again.setVisibility(View.VISIBLE);
            }
        }
    };

    private void updatepassengerlist(int j) {
        StaticData.passenger_list.remove(j);
        adapter.notifyDataSetChanged();
    }


    // Mimic continuous auto-focusing
    Camera.AutoFocusCallback autoFocusCB = new Camera.AutoFocusCallback() {
        public void onAutoFocus(boolean success, Camera camera) {
            autoFocusHandler.postDelayed(doAutoFocus, 1000);
        }
    };

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if (StaticData.passenger_list.get(position).getB_type().equals("2")) {
//        if (StaticData.passenger_list.get(position).getB_type().equals("1")) {
            verifyMonthlyPass(position);
        }
    }

    class GetPassengersListener implements TaskListener {
        @Override
        public void onSuccess(String msg) {
            //Bind Adapter
            adapter = new PassengersListAdapter(ScanActivity.this);
            lv_passengers.setAdapter(adapter);
        }

        @Override
        public void onError(String msg) {
            showDialog("Message!!", msg);
        }

        void showDialog(String title, String message) {
            ContextThemeWrapper themedContext;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                themedContext = new ContextThemeWrapper(ScanActivity.this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
            } else {
                themedContext = new ContextThemeWrapper(ScanActivity.this, android.R.style.Theme_Light_NoTitleBar);
            }
            final android.app.AlertDialog.Builder adialog = new android.app.AlertDialog.Builder(themedContext);
            adialog.setTitle(title);
            adialog.setMessage(message);
            adialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            adialog.show();
        }
    }

    class InsertAbsentPassengerListener implements TaskListener {
        @Override
        public void onSuccess(String msg) {
            showDialog("Message!!", msg);
        }

        @Override
        public void onError(String msg) {
            showDialog("Message!!", msg);
        }

        void showDialog(String title, String message) {
            ContextThemeWrapper themedContext;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                themedContext = new ContextThemeWrapper(ScanActivity.this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
            } else {
                themedContext = new ContextThemeWrapper(ScanActivity.this, android.R.style.Theme_Light_NoTitleBar);
            }
            final android.app.AlertDialog.Builder adialog = new android.app.AlertDialog.Builder(themedContext);
            adialog.setTitle(title);
            adialog.setMessage(message);
            adialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            adialog.show();
        }
    }

    class InsertBoardedPassengerListener implements TaskListener {
        @Override
        public void onSuccess(String msg) {
            //showDialog("Proceed Info", msg);
            ContextThemeWrapper themedContext;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                themedContext = new ContextThemeWrapper(ScanActivity.this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
            } else {
                themedContext = new ContextThemeWrapper(ScanActivity.this, android.R.style.Theme_Light_NoTitleBar);
            }
            final android.app.AlertDialog.Builder adialog = new android.app.AlertDialog.Builder(themedContext);
            adialog.setTitle("Message!!");
            adialog.setMessage(msg);
            adialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    StaticData.is_proceed = true;

                    SharedPreferences.Editor ed = getSharedPreferences("tripdetail", MODE_PRIVATE).edit();
                    ed.putBoolean("is_proceed", true);
                    ed.commit();

                    finish();
                }
            });
            adialog.show();

        }

        @Override
        public void onError(String msg) {
            showDialog("Message!!", msg);
        }

        void showDialog(String title, final String message) {
            ContextThemeWrapper themedContext;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                themedContext = new ContextThemeWrapper(ScanActivity.this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
            } else {
                themedContext = new ContextThemeWrapper(ScanActivity.this, android.R.style.Theme_Light_NoTitleBar);
            }
            final android.app.AlertDialog.Builder adialog = new android.app.AlertDialog.Builder(themedContext);
            adialog.setTitle(title);
            adialog.setMessage(message);
            adialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {

                }
            });
            adialog.show();
        }
    }

    PopupWindow popup;

    private void verifyuser() {


        View view = getLayoutInflater().inflate(R.layout.layout_verify_passenger, null);
        TextViewR tv_verify = (TextViewR) view.findViewById(R.id.tv_verify);
        final EditTextL edPhone = (EditTextL) view.findViewById(R.id.edit_mobile_number);

        popup = new PopupWindow(this);
        popup.setContentView(view);
        popup.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        popup.setBackgroundDrawable(new ColorDrawable(
                android.graphics.Color.TRANSPARENT));
        popup.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        popup.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        popup.setFocusable(true);

        tv_verify.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String phone = edPhone.getText().toString();
                if (UtilMethod.isStringNullOrBlank(phone)) {
                    /*UtilMethod.showToast("Phone number required", ScanActivity.this);*/
                    ContextThemeWrapper themedContext;
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                        themedContext = new ContextThemeWrapper(ScanActivity.this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
                    } else {
                        themedContext = new ContextThemeWrapper(ScanActivity.this, android.R.style.Theme_Light_NoTitleBar);
                    }
                    final android.app.AlertDialog.Builder adialog = new android.app.AlertDialog.Builder(themedContext);
                    adialog.setTitle("Message!!");
                    adialog.setMessage("Phone number required");
                    adialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });
                    adialog.show();


                    return;
                } else {
                    boolean isfound = false;
                    //for (final Passengers p : StaticData.passenger_list) {
                    for (int i = 0; i < StaticData.passenger_list.size(); i++) {
                        final int j = i;
                        final Passengers p = StaticData.passenger_list.get(i);
                        if (p.getPassenger_mobile().equals(phone)) {
                            isfound = true;
                            //code to verify seats
                            String[] seats = p.getSeats().split(",");

                            if (seats.length > 0) {
                                final android.app.AlertDialog.Builder adialog = new android.app.AlertDialog.Builder(themedContext);
                                adialog.setCancelable(false);

                                View vv = getLayoutInflater().inflate(R.layout.activity_scan_layout_verify, null);
                                TextViewL tv_passenger_name = (TextViewL) vv.findViewById(R.id.tv_passenger_name);
                                TextViewL tv_ticket_no = (TextViewL) vv.findViewById(R.id.tv_ticket_no);
                                ListView list_seats = (ListView) vv.findViewById(R.id.list_seats);

                                tv_passenger_name.setText(p.getPassenger_name());
                                tv_ticket_no.setText("Ticket No - " + p.getB_id());

                                StaticData.left_seats.clear();
                                for (String ss : seats)
                                    StaticData.left_seats.add(ss);
                                StaticData.seats.clear();
                                SeatListAdapter ad = new SeatListAdapter(ScanActivity.this, seats);
                                list_seats.setAdapter(ad);

                                adialog.setView(vv);
                                adialog.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        //code to collect payment if needed
                                        if (p.getB_payment_status().equals("2")) {
                                            final android.app.AlertDialog.Builder adialog = new android.app.AlertDialog.Builder(themedContext);
                                            adialog.setCancelable(false);
                                            adialog.setTitle("Collect Payment");
                                            adialog.setMessage("Did you received Rs. " + p.getB_payment_amount() + " from this user?");
                                            adialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {
                                                    p.setB_payment_status("1");
                                                    p.setB_verify("1");

                                                    Passengers newp = new Passengers();
                                                    newp.setB_id(p.getB_id());
                                                    newp.setSeats(android.text.TextUtils.join(",", StaticData.seats));
                                                    Log.e("selectedseat", newp.getSeats());
                                                    StaticData.boarded_passenger_list.add(newp);

                                                    Passengers leftp = new Passengers();
                                                    leftp.setB_id(p.getB_id());
                                                    leftp.setSeats(android.text.TextUtils.join(",", StaticData.left_seats));
                                                    Log.e("leftseat", leftp.getSeats());
                                                    StaticData.absent_passenger_list.add(leftp);


                                                    Log.e("boarded_passenger_list", StaticData.boarded_passenger_list.toString());
                                                    Log.e("absent_passenger_list", StaticData.absent_passenger_list.toString());
                                                    //adapter.notifyDataSetChanged();
                                                    updatepassengerlist(j);
                                                    Log.e("QR_code matched", "Yes");

                                                }
                                            });
                                            adialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                                                @Override
                                                public void onClick(DialogInterface dialog, int which) {

                                                }
                                            });

                                            adialog.show();
                                        } else {
                                            p.setB_verify("1");
                                            //adapter.notifyDataSetChanged();

                                            Passengers newp = new Passengers();
                                            newp.setB_id(p.getB_id());
                                            newp.setSeats(android.text.TextUtils.join(",", StaticData.seats));
                                            Log.e("selectedseat", newp.getSeats());
                                            StaticData.boarded_passenger_list.add(newp);

                                            Passengers leftp = new Passengers();
                                            leftp.setB_id(p.getB_id());
                                            leftp.setSeats(android.text.TextUtils.join(",", StaticData.left_seats));
                                            Log.e("leftseat", leftp.getSeats());
                                            StaticData.absent_passenger_list.add(leftp);

                                            Log.e("boarded_passenger_list", StaticData.boarded_passenger_list.toString());
                                            Log.e("absent_passenger_list", StaticData.absent_passenger_list.toString());

                                            updatepassengerlist(j);
                                            Log.e("QR_code matched", "Yes");
                                        }

                                    }
                                });
                                adialog.show();
                            }
                            popup.dismiss();
                        } else {
                            popup.dismiss();
                            Log.e("Phone matched", "No");
                        }
                    }
                    if (!isfound)
                        Toast.makeText(ScanActivity.this, "Phone number is invalid", Toast.LENGTH_LONG).show();
                }
            }
        });
        popup.showAtLocation(view, Gravity.CENTER, 0, 0);
    }

    /*private void verifyuser() {
        //ContextThemeWrapper themedContext;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            themedContext = new ContextThemeWrapper(ScanActivity.this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
        } else {
            themedContext = new ContextThemeWrapper(ScanActivity.this, android.R.style.Theme_Light_NoTitleBar);
        }
        final android.app.AlertDialog.Builder adialog = new android.app.AlertDialog.Builder(themedContext);
        adialog.setTitle("Verify User");
        View view=getLayoutInflater().inflate(R.layout.verifypassenger,null);
        final EditTextR edPhone = (EditTextR) view.findViewById(R.id.edPhoneNumber);
        adialog.setView(view);
        adialog.setPositiveButton("Proceed", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                String phone = edPhone.getText().toString();
                if (UtilMethod.isStringNullOrBlank(phone)) {
                    *//*UtilMethod.showToast("Phone number required", ScanActivity.this);*//*

					return;
				} else {
					boolean isfound = false;
					//for (final Passengers p : StaticData.passenger_list) {
					for (int i = 0; i < StaticData.passenger_list.size(); i++) {
						final int j = i;
						final Passengers p = StaticData.passenger_list.get(i);
						if (p.getPassenger_mobile().equals(phone)) {
							isfound = true;
							//code to verify seats
							String[] seats=p.getSeats().split(",");

							if(seats.length>0)
							{
								final android.app.AlertDialog.Builder adialog = new android.app.AlertDialog.Builder(themedContext);
								adialog.setCancelable(false);

								View v = getLayoutInflater().inflate(R.layout.activity_scan_layout_verify,null);
								TextViewL tv_passenger_name =(TextViewL) v.findViewById(R.id.tv_passenger_name);
								TextViewL tv_ticket_no =(TextViewL) v.findViewById(R.id.tv_ticket_no);
								ListView list_seats =(ListView) v.findViewById(R.id.list_seats);

								tv_passenger_name.setText(p.getPassenger_name());
								tv_ticket_no.setText("Ticket No - "+p.getB_id());

								StaticData.left_seats.clear();
								for(String ss : seats)
									StaticData.left_seats.add(ss);
								StaticData.seats.clear();
								SeatListAdapter ad=new SeatListAdapter(ScanActivity.this,seats);
								list_seats.setAdapter(ad);

								adialog.setView(v);
								adialog.setPositiveButton("DONE", new DialogInterface.OnClickListener() {
									@Override
									public void onClick(DialogInterface dialog, int which) {
										//code to collect payment if needed
										if (p.getB_payment_status().equals("2")) {
											final android.app.AlertDialog.Builder adialog = new android.app.AlertDialog.Builder(themedContext);
											adialog.setCancelable(false);
											adialog.setTitle("Collect Payment");
											adialog.setMessage("Did you received Rs. " + p.getB_payment_amount() + " from this user?");
											adialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
												@Override
												public void onClick(DialogInterface dialog, int which) {
													p.setB_payment_status("1");
													p.setB_verify("1");

													Passengers newp=new Passengers();
													newp.setB_id(p.getB_id());
													newp.setSeats(android.text.TextUtils.join(",", StaticData.seats));
													Log.e("selectedseat", newp.getSeats());
													StaticData.boarded_passenger_list.add(newp);

													Passengers leftp=new Passengers();
													leftp.setB_id(p.getB_id());
													leftp.setSeats(android.text.TextUtils.join(",", StaticData.left_seats));
													Log.e("leftseat", leftp.getSeats());
													StaticData.absent_passenger_list.add(leftp);


													Log.e("boarded_passenger_list",StaticData.boarded_passenger_list.toString());
													Log.e("absent_passenger_list", StaticData.absent_passenger_list.toString());
													//adapter.notifyDataSetChanged();
													updatepassengerlist(j);
													Log.e("QR_code matched", "Yes");
												}
											});
											adialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
												@Override
												public void onClick(DialogInterface dialog, int which) {

												}
											});

											adialog.show();
										} else {
											p.setB_verify("1");
											//adapter.notifyDataSetChanged();

											Passengers newp=new Passengers();
											newp.setB_id(p.getB_id());
											newp.setSeats(android.text.TextUtils.join(",", StaticData.seats));
											Log.e("selectedseat", newp.getSeats());
											StaticData.boarded_passenger_list.add(newp);

											Passengers leftp=new Passengers();
											leftp.setB_id(p.getB_id());
											leftp.setSeats(android.text.TextUtils.join(",", StaticData.left_seats));
											Log.e("leftseat", leftp.getSeats());
											StaticData.absent_passenger_list.add(leftp);

											Log.e("boarded_passenger_list",StaticData.boarded_passenger_list.toString());
											Log.e("absent_passenger_list", StaticData.absent_passenger_list.toString());

											updatepassengerlist(j);
											Log.e("QR_code matched", "Yes");
										}

									}
								});
								adialog.show();
							}

						} else {
							Log.e("Phone matched", "No");
						}
					}
					if (!isfound)
						Toast.makeText(ScanActivity.this, "Phone number is invalid", Toast.LENGTH_LONG).show();
				}
			}
		});
		adialog.show();
	}*/
    int counter = 0;

    private void contact_cc() {
        //-------------------


        //-------------------

        ContextThemeWrapper themedContext;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            themedContext = new ContextThemeWrapper(ScanActivity.this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
        } else {
            themedContext = new ContextThemeWrapper(ScanActivity.this, android.R.style.Theme_Light_NoTitleBar);
        }
        final android.app.AlertDialog.Builder adialog = new android.app.AlertDialog.Builder(themedContext);
        adialog.setTitle("Message!!");
        counter = 0;
		/*for(Passengers p:StaticData.passenger_list)
			if(p.getB_verify().equals("1"))
				counter++;
*/
        //String message=counter+ " out of "+StaticData.passenger_list.size()+" passengers are NOT boarded.\nDo you want to update it?";
        String message = StaticData.passenger_list.size() + " out of " + StaticData.passenger_list_x.size() + " passengers(Tickets) are NOT boarded.\nDo you want to update it?";
        adialog.setMessage(message);
        adialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //if(counter==StaticData.passenger_list.size())
                if (StaticData.absent_passenger_list.size() == 0)
                    Toast.makeText(ScanActivity.this, "All Passengers are boarded\nNo need to contact Customer Care\n\nDo Proceed", Toast.LENGTH_LONG).show();
                else {
                    //code to send unclimbed passengers

                    for (Passengers pppp : StaticData.passenger_list) {
                        StaticData.absent_passenger_list.add(pppp);
                    }

                    Log.e("boarded_passenger_list", StaticData.boarded_passenger_list.toString());
                    Log.e("absent_passenger_list", StaticData.absent_passenger_list.toString());

					/*String b_ids="";
					for(Passengers p:StaticData.passenger_list)
					{
						if(p.getB_verify().equals("2"))
						{
							Log.e("b_ids",p.getB_id()+"..");
							b_ids+=p.getB_id()+",";
							Log.e("b_ids",b_ids+"..");
						}
					}
					if(b_ids.length()>=1)
						b_ids=b_ids.substring(0,b_ids.length()-1);
					Log.e("b_ids", b_ids);*/
                    JSONArray jarr = new JSONArray();
                    for (Passengers p : StaticData.absent_passenger_list) {
                        String booking_id = p.getB_id();
                        String[] seats = p.getSeats().split(",");
                        JSONObject job = new JSONObject();
                        try {
                            job.put("booking_id", booking_id);
                            job.put("seat", new JSONArray(Arrays.asList(seats)));
                            jarr.put(job);
                        } catch (Exception e) {

                        }
                        Log.e("job", job.toString());
                        Log.e("jarr", jarr.toString());
                    }
                    Log.e("jarr", jarr.toString());
                    ArrayList<NameValuePair> inputdata = new ArrayList<NameValuePair>();
                    //inputdata.add(new BasicNameValuePair("booking_ids", b_ids));
                    inputdata.add(new BasicNameValuePair("booking_json", jarr.toString()));
                    new InsertAbsentPassengerTask(ScanActivity.this, inputdata, new InsertAbsentPassengerListener()).execute();
                }
            }
        });
        adialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        adialog.show();
    }

    private void proceed() {
        Log.e("boarded_passenger_list", StaticData.boarded_passenger_list.toString());
        Log.e("absent_passenger_list", StaticData.absent_passenger_list.toString());

        ContextThemeWrapper themedContext;
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            themedContext = new ContextThemeWrapper(ScanActivity.this, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
        } else {
            themedContext = new ContextThemeWrapper(ScanActivity.this, android.R.style.Theme_Light_NoTitleBar);
        }
        final android.app.AlertDialog.Builder adialog = new android.app.AlertDialog.Builder(themedContext);
        adialog.setTitle("Message!!");
        counter = 0;
        for (Passengers p : StaticData.passenger_list)
            if (p.getB_verify().equals("1"))
                counter++;

        //String message=counter+ " out of "+StaticData.passenger_list.size()+" passengers are boarded.\nDo you want to proceed now?";
        String message = StaticData.boarded_passenger_list.size() + " out of " + StaticData.passenger_list_x.size() + " Passengers(Tickets) are boarded.\nDo you want to proceed now?";
        adialog.setMessage(message);
        adialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                //code to send unclimbed passengers

                JSONArray jarr = new JSONArray();
                for (Passengers p : StaticData.boarded_passenger_list) {
                    String booking_id = p.getB_id();
                    String[] seats = p.getSeats().split(",");
                    JSONObject job = new JSONObject();
                    try {
                        job.put("booking_id", booking_id);
                        job.put("seat", new JSONArray(Arrays.asList(seats)));
                        jarr.put(job);
                    } catch (Exception e) {

                    }
                    Log.e("job", job.toString());
                    Log.e("jarr", jarr.toString());
                }
                Log.e("jarr", jarr.toString());
                //-------------------------

				/*String b_ids = "";
				for (Passengers p : StaticData.passenger_list) {
					if (p.getB_verify().equals("1")) {
						Log.e("b_ids", p.getB_id() + "..");
						b_ids += p.getB_id() + ",";
						Log.e("b_ids", b_ids + "..");
					}
				}
				if(b_ids.length()>=1)
					b_ids = b_ids.substring(0, b_ids.length() - 1);
				Log.e("b_ids", b_ids);*/
                ArrayList<NameValuePair> inputdata = new ArrayList<NameValuePair>();
                //inputdata.add(new BasicNameValuePair("booking_ids", b_ids));
                inputdata.add(new BasicNameValuePair("booking_json", jarr.toString()));
                new InsertBoardedPassengerTask(ScanActivity.this, inputdata, new InsertBoardedPassengerListener()).execute();
            }
        });
        adialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });
        adialog.show();
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch (event.getAction()) {
            case MotionEvent.ACTION_DOWN:
                v.getParent().requestDisallowInterceptTouchEvent(true);
                break;
            case MotionEvent.ACTION_UP:
                v.getParent().requestDisallowInterceptTouchEvent(false);
                break;
        }

        v.onTouchEvent(event);
        return true;
    }

    PopupWindow popup2;

    private void verifyMonthlyPass(final int position) {


        View view = getLayoutInflater().inflate(R.layout.layout_verify_month_pass, null);
        ImageView iv_passenger_profile = (ImageView) view.findViewById(R.id.iv_passenger_profile);
        final TextViewL tv_seatno = (TextViewL) view.findViewById(R.id.tv_seatno);
        final TextViewR tv_passenger_name = (TextViewR) view.findViewById(R.id.tv_passenger_name);
        final TextViewR tv_verify = (TextViewR) view.findViewById(R.id.tv_verify);
        final TextViewR tv_cancel = (TextViewR) view.findViewById(R.id.tv_cancel);

        popup2 = new PopupWindow(this);
        popup2.setContentView(view);
        popup2.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_RESIZE);
        popup2.setBackgroundDrawable(new ColorDrawable(
                android.graphics.Color.TRANSPARENT));
        popup2.setHeight(ViewGroup.LayoutParams.WRAP_CONTENT);
        popup2.setWidth(ViewGroup.LayoutParams.MATCH_PARENT);
        popup2.setFocusable(true);

        tv_verify.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                popup2.dismiss();
            }
        });
        tv_verify.setOnClickListener(new OnClickListener() {
             @Override
             public void onClick(View v) {
                     Passengers p = StaticData.passenger_list.get(position);
                     p.setB_verify("1");
                     //adapter.notifyDataSetChanged();

                     Passengers newp = new Passengers();
                     newp.setB_id(p.getB_id());
                     newp.setSeats(android.text.TextUtils.join(",", StaticData.seats));
                     Log.e("selectedseat", newp.getSeats());
                     StaticData.boarded_passenger_list.add(newp);

                     Passengers leftp = new Passengers();
                     leftp.setB_id(p.getB_id());
                     leftp.setSeats(android.text.TextUtils.join(",", StaticData.left_seats));
                     Log.e("leftseat", leftp.getSeats());
                     StaticData.absent_passenger_list.add(leftp);

                     Log.e("boarded_passenger_list", StaticData.boarded_passenger_list.toString());
                     Log.e("absent_passenger_list", StaticData.absent_passenger_list.toString());

                     updatepassengerlist(position);
                     Log.e("Monthly pass matched", "Yes");
                     popup2.dismiss();
                }
            }

        );
        popup2.showAtLocation(view, Gravity.CENTER, 0, 0);
    }
}
