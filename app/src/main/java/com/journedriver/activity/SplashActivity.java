package com.journedriver.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;

import com.journepassenger.activity.SigninActivity;
import com.journyapp.journyapp.R;

public class SplashActivity extends Activity {
	public int Thread_TIME=4;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_splash);


		DisplayMetrics metrics = getResources().getDisplayMetrics();
		int width = metrics.widthPixels;
		int height = metrics.heightPixels;
		float density = metrics.density;

		Log.i("density",""+density);
		Log.i("width",""+width);
		Log.i("height",""+height);


		SplashThread sthread=new SplashThread();
		sthread.start();
	}
	class SplashThread extends Thread
	{
		@Override
		public void run()
		{
			super.run();
			try
			{
				Thread.sleep(1000*Thread_TIME);
			}
			catch(Exception e)
			{
				Log.e("@SplashThread", e.toString());
			}

			String driver_id=getSharedPreferences("driver_info", MODE_PRIVATE).getString("driver_id", null);
			Log.e("driver_id",driver_id+"...");
			if(driver_id!=null)
			{
				Intent intent=new Intent(SplashActivity.this,HomeActivity.class);
				startActivity(intent);
				finish();
			}
			else{
				Intent intent=new Intent(SplashActivity.this,SigninActivity.class);
				startActivity(intent);
				finish();
			}
		}
	}
	@Override
	public void onBackPressed() {
		// TODO Auto-generated method stub
//		super.onBackPressed();
	}
}
