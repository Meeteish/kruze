package com.journedriver.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.view.ContextThemeWrapper;

import com.journedriver.bean.CashBean;
import com.journedriver.bean.TripCashBean;
import com.journedriver.data.HttpRequest;
import com.journedriver.data.StaticData;
import com.journedriver.data.UtilMethod;
import com.journedriver.data.WebService;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class GetTripCashTask extends AsyncTask<String, Void, String>
{
	Context cxt;
	List<NameValuePair> inputdata;
	ProgressDialog pdialog;
	TaskListener lListener;


	public GetTripCashTask(Context cxt, List<NameValuePair> inputdata, TaskListener llistener)
	{
		this.cxt=cxt;
		this.inputdata=inputdata;
		this.lListener=llistener;
		ContextThemeWrapper themedContext;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			themedContext = new ContextThemeWrapper(cxt, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
		} else {
			themedContext = new ContextThemeWrapper(cxt, android.R.style.Theme_Light_NoTitleBar);
		}
		pdialog=new ProgressDialog(themedContext);
	}

	@Override
	protected String doInBackground(String... params) 
	{
		try
		{
			String response= HttpRequest.post(WebService.GET_TRIP_CASH_RECEIVED, inputdata);
			Log.e("response",response);
			return response;
		}
		catch(Exception e)
		{
			Log.e("Exception is " ,e.toString());
		}
		return null;
	}

	@Override
	protected void onPostExecute(String result) 
	{
		super.onPostExecute(result);
	
		if(pdialog!=null && pdialog.isShowing())
		{
			pdialog.dismiss();
		}
		if(result!=null)
		{
			try
			{
				JSONObject jobj=new JSONObject(result);
				String status=jobj.getString("status");

				StaticData.trip_cash_list.clear();
				StaticData.total_received_cash=0;
				if(status.equals("true"))
				{
					JSONArray jarray=jobj.getJSONArray("trip_list");
					for(int i=0;i<jarray.length();i++)
					{
						JSONObject object=jarray.getJSONObject(i);
						TripCashBean ibean=new TripCashBean();
						if(object.getString("bus_route_id")!=null)
						{
							ibean.setBus_route_id(object.getString("bus_route_id"));
							ibean.setBus_route_title(object.getString("bus_route_title"));
							ibean.setBus_route_from_title(object.getString("bus_route_from_title"));
							ibean.setBus_route_to_title(object.getString("bus_route_to_title"));
							ibean.setTrip_id(object.getString("trip_id"));
							ibean.setTrip_start_time(object.getString("trip_start_time"));
							ibean.setTrip_end_time(object.getString("trip_end_time"));
							ibean.setTotal_amount(object.getString("total_amount"));

							ArrayList<CashBean> cash_list=new ArrayList<CashBean>();
							JSONArray jarr = object.getJSONArray("passenger_details");
							{
								for(int j=0;j<jarr.length();j++)
								{
									JSONObject job=jarr.getJSONObject(j);
									CashBean cash=new CashBean();
									if(job.getString("passenger_name")!=null)
									{
										cash.setPassenger_name(job.getString("passenger_name"));
										cash.setAmount(job.getString("amount"));
										StaticData.total_received_cash+=Integer.parseInt(job.getString("amount"));
									}
									cash_list.add(cash);
								}
								ibean.setCashlist(cash_list);
							}

							StaticData.trip_cash_list.add(ibean);
						}						
					}					
					lListener.onSuccess(jobj.getString("message"));
				}
				else
				{
					lListener.onError(jobj.getString("message"));
				}
			}
			catch(Exception e)
			{
				Log.e("Exception is ", e.toString());
			}
		}
	}

	@Override
	protected void onPreExecute() 
	{
		super.onPreExecute();
		UtilMethod.showLoading(pdialog, cxt);
	}

}
