package com.journedriver.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.view.ContextThemeWrapper;

import com.journedriver.bean.Expenses_Details;
import com.journedriver.data.HttpRequest;
import com.journedriver.data.StaticData;
import com.journedriver.data.UtilMethod;
import com.journedriver.data.WebService;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

public class GetExpenseListTask extends AsyncTask<String, Void, String>
{
	Context cxt;
	List<NameValuePair> inputdata;
	ProgressDialog pdialog;
	TaskListener lListener;


	public GetExpenseListTask(Context cxt, List<NameValuePair> inputdata, TaskListener llistener)
	{
		this.cxt=cxt;
		this.inputdata=inputdata;
		this.lListener=llistener;
		ContextThemeWrapper themedContext;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			themedContext = new ContextThemeWrapper(cxt, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
		} else {
			themedContext = new ContextThemeWrapper(cxt, android.R.style.Theme_Light_NoTitleBar);
		}
		pdialog=new ProgressDialog(themedContext);
	}

	@Override
	protected String doInBackground(String... params) 
	{
		try
		{
			String response= HttpRequest.post(WebService.GET_EXPENSE_LIST, inputdata);
			return response;
		}
		catch(Exception e)
		{
			Log.e("Exception is " ,e.toString());
		}
		return null;
	}

	@Override
	protected void onPostExecute(String result) 
	{
		super.onPostExecute(result);
	
		if(pdialog!=null && pdialog.isShowing())
		{
			//pdialog.dismiss();
		}
		if(result!=null)
		{
			try
			{
				JSONObject jobj=new JSONObject(result);
				String status=jobj.getString("status");
				StaticData.expense_details_list.clear();
				if(status.equals("true"))
				{

					JSONArray jarray=jobj.getJSONArray("expenses_details");
					for(int i=0;i<jarray.length();i++)
					{
						JSONObject object=jarray.getJSONObject(i);
						Expenses_Details ibean=new Expenses_Details();
						if(object.getString("e_detail_id")!=null) {
							ibean.setE_detail_id(object.getString("e_detail_id"));
							ibean.setE_detail_expense(object.getString("e_detail_expense"));
							ibean.setE_detail_driver(object.getString("e_detail_driver"));
							ibean.setE_detail_cost(object.getString("e_detail_cost"));
							ibean.setE_detail_kms(object.getString("e_detail_kms"));
							ibean.setE_detail_options(object.getString("e_detail_options"));
							ibean.setE_detail_image(object.getString("e_detail_image"));
							ibean.setE_detail_datetime(object.getString("e_detail_datetime"));
							ibean.setEc_name(object.getString("ec_name"));
							ibean.setE_desc(object.getString("e_desc"));
						}
						StaticData.expense_details_list.add(ibean);
					}
					lListener.onSuccess(jobj.getString("message"));
				}
				else
				{
					lListener.onError(jobj.getString("message"));
				}
			}
			catch(Exception e)
			{
				Log.e("Exception is ", e.toString());
			}
		}
	}

	@Override
	protected void onPreExecute() 
	{
		super.onPreExecute();
		//UtilMethod.showLoading(pdialog, cxt);
	}

}
