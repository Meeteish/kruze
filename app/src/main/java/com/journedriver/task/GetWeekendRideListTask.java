package com.journedriver.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.view.ContextThemeWrapper;

import com.journedriver.bean.DailyRide;
import com.journedriver.bean.WeekendRide;
import com.journedriver.data.HttpRequest;
import com.journedriver.data.StaticData;
import com.journedriver.data.UtilMethod;
import com.journedriver.data.WebService;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

public class GetWeekendRideListTask extends AsyncTask<String, Void, String>
{
	Context cxt;
	List<NameValuePair> inputdata;
	ProgressDialog pdialog;
	TaskListener lListener;


	public GetWeekendRideListTask(Context cxt, List<NameValuePair> inputdata, TaskListener llistener)
	{
		this.cxt=cxt;
		this.inputdata=inputdata;
		this.lListener=llistener;
		ContextThemeWrapper themedContext;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			themedContext = new ContextThemeWrapper(cxt, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
		} else {
			themedContext = new ContextThemeWrapper(cxt, android.R.style.Theme_Light_NoTitleBar);
		}
		pdialog=new ProgressDialog(themedContext);
	}

	@Override
	protected String doInBackground(String... params) 
	{
		try
		{
			String response= HttpRequest.post(WebService.GET_WEEKEND_RIDE_LIST, inputdata);
			Log.e("response",response);
			return response;
		}
		catch(Exception e)
		{
			Log.e("Exception is " ,e.toString());
		}
		return null;
	}

	@Override
	protected void onPostExecute(String result) 
	{
		super.onPostExecute(result);
	
		if(pdialog!=null && pdialog.isShowing())
		{
			pdialog.dismiss();
		}
		if(result!=null)
		{
			try
			{
				JSONObject jobj=new JSONObject(result);
				String status=jobj.getString("status");

				StaticData.weekend_ride_list.clear();
				if(status.equals("true"))
				{
					JSONArray jarray=jobj.getJSONArray("weekend_rides");
					for(int i=0;i<jarray.length();i++)
					{
						JSONObject object=jarray.getJSONObject(i);
						WeekendRide ibean=new WeekendRide();
						if(object.getString("weekend_ride_id")!=null)
						{
							ibean.setWeekend_ride_id(object.getString("weekend_ride_id"));
							ibean.setVehicle(object.getString("vehicle"));
							ibean.setVehicle_type(object.getString("vehicle_type"));
							ibean.setStart_location(object.getString("start_location"));
							ibean.setEnd_location(object.getString("end_location"));
							ibean.setStart_time(object.getString("start_time"));
							ibean.setEnd_time(object.getString("end_time"));
							ibean.setOpening_km(object.getString("opening_km"));
							ibean.setClosing_km(object.getString("closing_km"));
							ibean.setTotal_km(object.getString("total_km"));
							ibean.setDuty_hours(object.getString("duty_hours"));
							ibean.setDriver_id(object.getString("driver_id"));
							ibean.setOperator_id(object.getString("operator_id"));
							ibean.setCompany_id(object.getString("company_id"));
							ibean.setDriver_ride_date(object.getString("driver_ride_date"));
							ibean.setWeekend_ride_status(object.getString("weekend_ride_status"));
							ibean.setCompany_name(object.getString("company_name"));

							StaticData.weekend_ride_list.add(ibean);
						}						
					}

					lListener.onSuccess(jobj.getString("message"));
				}
				else
				{
					lListener.onError(jobj.getString("message"));
				}
			}
			catch(Exception e)
			{
				Log.e("Exception is " ,e.toString());
			}
		}
	}

	@Override
	protected void onPreExecute() 
	{
		super.onPreExecute();
		UtilMethod.showLoading(pdialog, cxt);
	}

}
