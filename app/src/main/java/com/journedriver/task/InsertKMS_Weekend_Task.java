package com.journedriver.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.view.ContextThemeWrapper;

import com.journedriver.data.HttpRequest;
import com.journedriver.data.UtilMethod;
import com.journedriver.data.WebService;

import org.apache.http.NameValuePair;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;

public class InsertKMS_Weekend_Task extends AsyncTask<String, Void, String>
{
	Context cxt;
	List<NameValuePair> inputdata;
	Map<String, Object> inputdata_image;
	ProgressDialog pdialog;
	TaskListener lListener;


	public InsertKMS_Weekend_Task(Context cxt, List<NameValuePair> inputdata, TaskListener llistener)
	{
		this.cxt=cxt;
		this.inputdata=inputdata;
		this.lListener=llistener;
		ContextThemeWrapper themedContext;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			themedContext = new ContextThemeWrapper(cxt, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
		} else {
			themedContext = new ContextThemeWrapper(cxt, android.R.style.Theme_Light_NoTitleBar);
		}
		pdialog=new ProgressDialog(themedContext);
	}

	String response="";
	@Override
	protected String doInBackground(String... params) 
	{
		try
		{
			response= HttpRequest.post(WebService.INSERT_KMS_WEEKEND, inputdata);
			return response;
		}
		catch(Exception e)
		{
			Log.e("Exception is " ,e.toString());
		}
		return null;
	}

	@Override
	protected void onPostExecute(String result) 
	{
		super.onPostExecute(result);
	
		if(pdialog!=null && pdialog.isShowing())
		{
			pdialog.dismiss();
		}

		if (result != null) {
			try {
				JSONObject jobj = new JSONObject(result);
				String status = jobj.getString("status");

				if (status.equals("true")) {
					lListener.onSuccess(jobj.getString("msg"));
				} else {
					lListener.onError(jobj.getString("msg"));
				}
			} catch (Exception e) {
				Log.e("Exception is ", e.toString());
			}
		}
	}

	@Override
	protected void onPreExecute() 
	{
		super.onPreExecute();
		UtilMethod.showLoading(pdialog, cxt);
	}

}
