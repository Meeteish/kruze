package com.journedriver.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.view.ContextThemeWrapper;

import com.journedriver.bean.Passengers;
import com.journedriver.data.HttpRequest;
import com.journedriver.data.StaticData;
import com.journedriver.data.UtilMethod;
import com.journedriver.data.WebService;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

public class GetPassengersTask extends AsyncTask<String, Void, String>
{
	Context cxt;
	List<NameValuePair> inputdata;
	ProgressDialog pdialog;
	TaskListener lListener;


	public GetPassengersTask(Context cxt, List<NameValuePair> inputdata, TaskListener llistener)
	{
		this.cxt=cxt;
		this.inputdata=inputdata;
		this.lListener=llistener;
		ContextThemeWrapper themedContext;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			themedContext = new ContextThemeWrapper(cxt, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
		} else {
			themedContext = new ContextThemeWrapper(cxt, android.R.style.Theme_Light_NoTitleBar);
		}
		pdialog=new ProgressDialog(themedContext);
	}

	@Override
	protected String doInBackground(String... params) 
	{
		try
		{
			String response= HttpRequest.post(WebService.GET_PASSENGERS, inputdata);
			return response;
		}
		catch(Exception e)
		{
			Log.e("Exception is " ,e.toString());
		}
		return null;
	}

	@Override
	protected void onPostExecute(String result) 
	{
		super.onPostExecute(result);
	
		if(pdialog!=null && pdialog.isShowing())
		{
			pdialog.dismiss();
		}
		if(result!=null)
		{
			try
			{
				JSONObject jobj=new JSONObject(result);
				String status=jobj.getString("status");
				StaticData.passenger_list.clear();
				StaticData.passenger_list_x.clear();
				if(status.equals("true"))
				{

					JSONArray jarray=jobj.getJSONArray("booking_list");
					for(int i=0;i<jarray.length();i++)
					{
						JSONObject object=jarray.getJSONObject(i);
						Passengers ibean=new Passengers();
						if(object.getString("b_id")!=null)
						{
							ibean.setB_id(object.getString("b_id"));
							ibean.setB_trip_id(object.getString("b_trip_id"));
							ibean.setB_bus_id(object.getString("b_bus_id"));
							//ibean.setB_bus_seat_id(object.getString("b_bus_seat_id"));
							ibean.setB_passenger_id(object.getString("b_passenger_id"));
							ibean.setB_from(object.getString("b_from"));
							ibean.setB_to(object.getString("b_to"));
							ibean.setB_date(object.getString("b_date"));
							ibean.setB_status(object.getString("b_status"));
							ibean.setB_verify(object.getString("b_verify"));
							ibean.setB_payment_amount(object.getString("b_payment_amount"));
							ibean.setB_payment_status(object.getString("b_payment_status"));
							ibean.setPassenger_id(object.getString("passenger_id"));
							ibean.setPassenger_name(object.getString("passenger_name"));
							ibean.setFrom_stop(object.getString("from_stop"));
							ibean.setTo_stop(object.getString("to_stop"));
							ibean.setPassenger_mobile(object.getString("passenger_mobile"));
							ibean.setB_qr_code(object.getString("b_qr_code"));
							ibean.setB_qr_image(object.getString("b_qr_image"));
							ibean.setSeats(object.getString("seats"));
							ibean.setB_type(object.getString("b_type"));

							StaticData.passenger_list.add(ibean);
							StaticData.passenger_list_x.add(ibean);
						}						
					}
					/*Passengers p=new Passengers();
					p.setB_verify("2");
					p.setFrom_stop("Rajwada");
					p.setTo_stop("Khajrana");
					p.setPassenger_name("Meeteish");
					p.setB_qr_code("COD9090");
					p.setB_id("123");
					p.setB_payment_status("2");
					p.setB_payment_amount("500");
					p.setPassenger_mobile("12345");
					p.setSeats("1,2,3");
					StaticData.passenger_list.add(p);
					StaticData.passenger_list_x.add(p);
					p=new Passengers();
					p.setB_verify("2");
					p.setFrom_stop("Rajwada");
					p.setTo_stop("Khajrana");
					p.setPassenger_name("Meeteish");
					p.setB_qr_code("COD9090");
					p.setB_id("125");p.setB_payment_status("2");
					p.setB_payment_amount("400");
					p.setPassenger_mobile("12345");
					p.setSeats("41,42,43");
					StaticData.passenger_list.add(p);
					StaticData.passenger_list_x.add(p);
					p=new Passengers();
					p.setB_verify("2");
					p.setFrom_stop("LIG");
					p.setTo_stop("MR10");
					p.setPassenger_name("Alok");
					p.setB_qr_code("COD8080");
					p.setPassenger_mobile("11111");
					p.setB_id("122");
					p.setB_payment_status("1");
					p.setB_payment_amount("400");
					p.setSeats("14");
					StaticData.passenger_list.add(p);
					StaticData.passenger_list_x.add(p);
					p=new Passengers();
					p.setB_verify("2");
					p.setFrom_stop("HIG");
					p.setTo_stop("Bawarkua");
					p.setPassenger_name("Priya");
					p.setB_qr_code("COD7070");
					p.setPassenger_mobile("22222");
					p.setB_id("121");
					p.setB_payment_status("2");
					p.setB_payment_amount("250");
					p.setSeats("11,12");
					StaticData.passenger_list.add(p);
					StaticData.passenger_list_x.add(p);
					p= new Passengers();
					p.setB_verify("2");
					p.setFrom_stop("Rajwada");
					p.setTo_stop("Khajrana");
					p.setPassenger_name("Deepak");
					p.setB_qr_code("COD6060");
					p.setPassenger_mobile("33333");
					p.setB_id("124");
					p.setB_payment_status("2");
					p.setB_payment_amount("400");
					p.setSeats("20");
					StaticData.passenger_list.add(p);
					StaticData.passenger_list_x.add(p);*/
					lListener.onSuccess(jobj.getString("message"));
				}
				else
				{
					/*Passengers p=new Passengers();
					p.setB_verify("2");
					p.setFrom_stop("Rajwada");
					p.setTo_stop("Khajrana");
					p.setPassenger_name("Meeteish");
					p.setB_qr_code("COD9090");
					p.setB_id("123");p.setB_payment_status("2");
					p.setB_payment_amount("400");
					p.setPassenger_mobile("12345");
					p.setSeats("1,2,3");
					StaticData.passenger_list.add(p);
					StaticData.passenger_list_x.add(p);
					p=new Passengers();
					p.setB_verify("2");
					p.setFrom_stop("Rajwada");
					p.setTo_stop("Khajrana");
					p.setPassenger_name("Meeteish");
					p.setB_qr_code("COD9090");
					p.setB_id("125");p.setB_payment_status("2");
					p.setB_payment_amount("400");
					p.setPassenger_mobile("12345");
					p.setSeats("41,42,43");
					StaticData.passenger_list.add(p);
					StaticData.passenger_list_x.add(p);
					p=new Passengers();
					p.setB_verify("2");
					p.setFrom_stop("LIG");
					p.setTo_stop("MR10");
					p.setPassenger_name("Alok");
					p.setB_qr_code("COD8080");
					p.setPassenger_mobile("11111");
					p.setB_id("122");p.setB_payment_status("2");
					p.setB_payment_amount("400");
					p.setSeats("14");
					StaticData.passenger_list.add(p);
					StaticData.passenger_list_x.add(p);
					p=new Passengers();
					p.setB_verify("2");
					p.setFrom_stop("HIG");
					p.setTo_stop("Bawarkua");
					p.setPassenger_name("Priya");
					p.setB_qr_code("COD7070");
					p.setPassenger_mobile("22222");
					p.setB_id("121");p.setB_payment_status("2");
					p.setB_payment_amount("400");
					p.setSeats("11,12");
					StaticData.passenger_list.add(p);
					StaticData.passenger_list_x.add(p);
					p= new Passengers();
					p.setB_verify("2");
					p.setFrom_stop("Rajwada");
					p.setTo_stop("Khajrana");
					p.setPassenger_name("Deepak");
					p.setB_qr_code("COD6060");
					p.setPassenger_mobile("33333");
					p.setB_id("124");p.setB_payment_status("2");
					p.setB_payment_amount("400");
					p.setSeats("20");
					StaticData.passenger_list.add(p);
					StaticData.passenger_list_x.add(p);
					lListener.onSuccess(jobj.getString("message"));*/
					lListener.onError(jobj.getString("message"));
				}
			}
			catch(Exception e)
			{
				Log.e("Exception is " ,e.toString());
			}
		}
	}

	@Override
	protected void onPreExecute() 
	{
		super.onPreExecute();
		UtilMethod.showLoading(pdialog, cxt);
	}

}
