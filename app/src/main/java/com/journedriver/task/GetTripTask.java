package com.journedriver.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.view.ContextThemeWrapper;

import com.journedriver.bean.TripBean;
import com.journedriver.bean.Trip_Stops;
import com.journedriver.data.HttpRequest;
import com.journedriver.data.StaticData;
import com.journedriver.data.UtilMethod;
import com.journedriver.data.WebService;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class GetTripTask extends AsyncTask<String, Void, String>
{
	Context cxt;
	List<NameValuePair> inputdata;
	ProgressDialog pdialog;
	TaskListener lListener;
	

	public GetTripTask(Context cxt,List<NameValuePair> inputdata,TaskListener llistener)
	{
		this.cxt=cxt;
		this.inputdata=inputdata;
		this.lListener=llistener;
		ContextThemeWrapper themedContext;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			themedContext = new ContextThemeWrapper(cxt, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
		} else {
			themedContext = new ContextThemeWrapper(cxt, android.R.style.Theme_Light_NoTitleBar);
		}
		pdialog=new ProgressDialog(themedContext);
	}

	@Override
	protected String doInBackground(String... params) 
	{
		try
		{
			String response= HttpRequest.post(WebService.GET_TODAYS_TRIP, inputdata);
			Log.e("response_trip",response+"");
			return response;
		}
		catch(Exception e)
		{
			Log.e("Exception is " ,e.toString());
		}
		return null;
	}

	@Override
	protected void onPostExecute(String result) 
	{
		super.onPostExecute(result);
	
		if(pdialog!=null || pdialog.isShowing())
		{
			pdialog.dismiss();
		}
		if(result!=null)
		{
			try
			{
				JSONObject jobj=new JSONObject(result);
				String status=jobj.getString("status");

				StaticData.trip_list.clear();
				if(status.equals("true"))
				{
					/*
					code to update status
					 */
					SharedPreferences sharedPreferences=cxt.getSharedPreferences("tripdetail", cxt.MODE_PRIVATE);
					int trip_id = jobj.getInt("trip_id");
					int stop_id = jobj.getInt("stop_id");
					sharedPreferences.edit().putInt("trip_stop_id", stop_id).commit();
					//sharedPreferences.edit().putInt("trip_stop_index", stop_id - 1).commit();
					//sharedPreferences.edit().putBoolean("is_proceed", false).commit();
					StaticData.is_proceed=sharedPreferences.getBoolean("is_proceed", false);
					// StaticData.trip_stop_index=sharedPreferences.getInt("trip_stop_index",0);
					StaticData.trip_stop_index=sharedPreferences.getInt("trip_stop_index",0);

					SharedPreferences sp_ruuning_trip_detail=cxt.getSharedPreferences("running_trip_detail", cxt.MODE_PRIVATE);
					SharedPreferences.Editor edit = sp_ruuning_trip_detail.edit();
					edit.putString("id", trip_id+"");
					edit.commit();

					StaticData.running_trip_id=sp_ruuning_trip_detail.getString("id", null);

					if(StaticData.running_trip_id!=null)
						Log.e("running_trip_id",StaticData.running_trip_id);
					else
						Log.e("running_trip_id","null");

					//------------------------------------------------------

					JSONArray jarray=jobj.getJSONArray("trip_list");
					for(int i=0;i<jarray.length();i++)
					{
						JSONObject object=jarray.getJSONObject(i);
						TripBean ibean=new TripBean();
						if(object.getString("bus_route_id")!=null)
						{
							ibean.setBus_route_id(object.getString("bus_route_id"));
							ibean.setBus_operator_id(object.getString("bus_operator_id"));
							ibean.setBus_city_id(object.getString("bus_city_id"));
							ibean.setBus_route_title(object.getString("bus_route_title"));
//							ibean.setBus_route_from(object.getString("bus_route_from"));
//							ibean.setBus_route_to(object.getString("bus_route_to"));
							ibean.setBus_route_from(object.getString("bus_route_from_title"));
							ibean.setBus_route_to(object.getString("bus_route_to_title"));
							ibean.setBus_route_start_lat(object.getString("bus_route_start_lat"));
							ibean.setBus_route_start_long(object.getString("bus_route_start_long"));
							ibean.setBus_route_end_lat(object.getString("bus_route_end_lat"));
							ibean.setBus_route_end_long(object.getString("bus_route_end_long"));
							ibean.setBus_route_status(object.getString("bus_route_status"));
							ibean.setTrip_id(object.getString("trip_id"));
							ibean.setTrip_route_id(object.getString("trip_route_id"));
							ibean.setTrip_start_time(object.getString("trip_start_time"));
							ibean.setTrip_end_time(object.getString("trip_end_time"));
							ibean.setTrip_bus_id(object.getString("trip_bus_id"));
							ibean.setTrip_operator_id(object.getString("trip_operator_id"));
							ibean.setTrip_create_datetime(object.getString("trip_create_datetime"));
							ibean.setTrip_status(object.getString("trip_status"));
							ibean.setComplete_status(object.getString("complete_status"));
							
							ArrayList<Trip_Stops> trip_stops_list=new ArrayList<Trip_Stops>();
							//Trip_Stops origin=new Trip_Stops();
							//origin.setStop_name(ibean.getBus_route_from());
							//trip_stops_list.add(origin);
							JSONArray jarr = object.getJSONArray("trip_stops");
							{
								for(int j=0;j<jarr.length();j++)
								{
									JSONObject job=jarr.getJSONObject(j);
									Trip_Stops trip_stops=new Trip_Stops();
									if(job.getString("t_stop_id")!=null)
									{
										trip_stops.setT_stop_id(job.getString("t_stop_id"));
										trip_stops.setT_stop_trip_id(job.getString("t_stop_trip_id"));
										trip_stops.setT_stop(job.getString("t_stop"));
										trip_stops.setT_stop_time(job.getString("t_stop_time"));
										trip_stops.setT_stop_status(job.getString("t_stop_status"));
										trip_stops.setStop_name(job.getString("stop_name"));
									}
									trip_stops_list.add(trip_stops);
								}
							}
							//Trip_Stops dest=new Trip_Stops();
							//dest.setStop_name(ibean.getBus_route_to());
							//trip_stops_list.add(dest);
							Log.e("trip_stops_list", trip_stops_list.toString());
							ibean.setTrip_stops(trip_stops_list);
							
							StaticData.trip_list.add(ibean);
						}						
					}					
					lListener.onSuccess(jobj.getString("message"));
				}
				else
				{
					lListener.onError(jobj.getString("message"));
				}
			}
			catch(Exception e)
			{
				Log.e("Exception is ", e.toString());
			}
		}
	}

	@Override
	protected void onPreExecute() 
	{
		super.onPreExecute();
		UtilMethod.showLoading(pdialog, cxt);
	}

}
