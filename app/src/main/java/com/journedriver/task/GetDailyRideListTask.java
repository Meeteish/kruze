package com.journedriver.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.view.ContextThemeWrapper;

import com.journedriver.bean.DailyRide;
import com.journedriver.bean.ExpenseCategory;
import com.journedriver.data.HttpRequest;
import com.journedriver.data.StaticData;
import com.journedriver.data.UtilMethod;
import com.journedriver.data.WebService;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

public class GetDailyRideListTask extends AsyncTask<String, Void, String>
{
	Context cxt;
	List<NameValuePair> inputdata;
	ProgressDialog pdialog;
	TaskListener lListener;


	public GetDailyRideListTask(Context cxt, List<NameValuePair> inputdata, TaskListener llistener)
	{
		this.cxt=cxt;
		this.inputdata=inputdata;
		this.lListener=llistener;
		ContextThemeWrapper themedContext;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			themedContext = new ContextThemeWrapper(cxt, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
		} else {
			themedContext = new ContextThemeWrapper(cxt, android.R.style.Theme_Light_NoTitleBar);
		}
		pdialog=new ProgressDialog(themedContext);
	}

	@Override
	protected String doInBackground(String... params) 
	{
		try
		{
			String response= HttpRequest.post(WebService.GET_DAILY_RIDE_LIST, inputdata);
			Log.e("response",response);
			return response;
		}
		catch(Exception e)
		{
			Log.e("Exception is " ,e.toString());
		}
		return null;
	}

	@Override
	protected void onPostExecute(String result) 
	{
		super.onPostExecute(result);
	
		if(pdialog!=null && pdialog.isShowing())
		{
			pdialog.dismiss();
		}
		if(result!=null)
		{
			try
			{
				JSONObject jobj=new JSONObject(result);
				String status=jobj.getString("status");

				StaticData.daily_ride_list.clear();
				if(status.equals("true"))
				{
					JSONArray jarray=jobj.getJSONArray("km_list");
					for(int i=0;i<jarray.length();i++)
					{
						JSONObject object=jarray.getJSONObject(i);
						DailyRide ibean=new DailyRide();
						if(object.getString("km_trip_id")!=null)
						{
							ibean.setKm_trip_id(object.getString("km_trip_id"));
							ibean.setFrom(object.getString("from"));
							ibean.setTo(object.getString("to"));
							ibean.setStart(object.getString("start"));
							ibean.setEnd(object.getString("end"));
							ibean.setStart_kms(object.getString("start_kms"));
							ibean.setEnd_kms(object.getString("end_kms"));
							ibean.setDifference(object.getString("difference"));

							StaticData.daily_ride_list.add(ibean);
							if(!StaticData.isTodaysDailyRideFilled)
							{
								StaticData.todays_daily_ride_list.add(ibean);
							}
						}						
					}
					Log.e("todays_daily_ride_list",StaticData.todays_daily_ride_list.toString());
					StaticData.isTodaysDailyRideFilled=true;
					lListener.onSuccess(jobj.getString("message"));
				}
				else
				{
					lListener.onError(jobj.getString("message"));
				}
			}
			catch(Exception e)
			{
				Log.e("Exception is " ,e.toString());
			}
		}
	}

	@Override
	protected void onPreExecute() 
	{
		super.onPreExecute();
		UtilMethod.showLoading(pdialog, cxt);
	}

}
