package com.journedriver.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.view.ContextThemeWrapper;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.journedriver.data.HttpRequest;
import com.journedriver.data.UtilMethod;
import com.journedriver.data.WebService;

import org.apache.http.NameValuePair;
import org.json.JSONObject;

import java.util.List;
import java.util.Map;

public class InsertHelpTask extends AsyncTask<String, Void, String>
{
	Context cxt;
	List<NameValuePair> inputdata;
	Map<String, Object> inputdata_image;
	ProgressDialog pdialog;
	TaskListener lListener;


	public InsertHelpTask(Context cxt, List<NameValuePair> inputdata, TaskListener llistener)
	{
		this.cxt=cxt;
		this.inputdata=inputdata;
		this.lListener=llistener;
		ContextThemeWrapper themedContext;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			themedContext = new ContextThemeWrapper(cxt, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
		} else {
			themedContext = new ContextThemeWrapper(cxt, android.R.style.Theme_Light_NoTitleBar);
		}
		pdialog=new ProgressDialog(themedContext);
	}

	public InsertHelpTask(Context cxt, Map<String, Object> inputdata_image, TaskListener llistener)
	{
		this.cxt=cxt;
		this.inputdata_image=inputdata_image;
		this.lListener=llistener;
		pdialog=new ProgressDialog(cxt);
	}
	String response="";
	@Override
	protected String doInBackground(String... params) 
	{
		try
		{
			if(inputdata_image==null)
				response= HttpRequest.post(WebService.INSERT_HELP, inputdata);
			else
			{
				AQuery aq = new AQuery(cxt);
				aq.ajax(WebService.INSERT_HELP, inputdata_image, JSONObject.class, new AjaxCallback<JSONObject>()
				{
					@Override
					public void callback(String url, JSONObject object, AjaxStatus status) {
						// TODO Auto-generated method stub
						response=object.toString();
					}
				});
				Log.e("response_image_upload", response);
			}
			return response;
		}
		catch(Exception e)
		{
			Log.e("Exception is " ,e.toString());
		}
		return null;
	}

	@Override
	protected void onPostExecute(String result) 
	{
		super.onPostExecute(result);
	
		if(pdialog!=null && pdialog.isShowing())
		{
			pdialog.dismiss();
		}
		if(inputdata==null)
			lListener.onSuccess("");
		else {
			if (result != null) {
				try {
					JSONObject jobj = new JSONObject(result);
					String status = jobj.getString("status");

					if (status.equals("true")) {
						lListener.onSuccess(jobj.getString("message"));
					} else {
						lListener.onError(jobj.getString("message"));
					}
				} catch (Exception e) {
					Log.e("Exception is ", e.toString());
				}
			}
		}
	}

	@Override
	protected void onPreExecute() 
	{
		super.onPreExecute();
		UtilMethod.showLoading(pdialog, cxt);
	}

}
