package com.journedriver.task;

public interface TaskListener {
	void onSuccess(String msg);
	void onError(String msg);
}
