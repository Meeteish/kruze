package com.journedriver.task;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Build;
import android.util.Log;
import android.view.ContextThemeWrapper;

import com.journedriver.bean.StartedTrip;
import com.journedriver.bean.TripKMS;
import com.journedriver.data.HttpRequest;
import com.journedriver.data.StaticData;
import com.journedriver.data.UtilMethod;
import com.journedriver.data.WebService;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class GetStartedTripTask extends AsyncTask<String, Void, String>
{
	Context cxt;
	List<NameValuePair> inputdata;
	ProgressDialog pdialog;
	TaskListener lListener;


	public GetStartedTripTask(Context cxt, List<NameValuePair> inputdata, TaskListener llistener)
	{
		this.cxt=cxt;
		this.inputdata=inputdata;
		this.lListener=llistener;
		ContextThemeWrapper themedContext;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			themedContext = new ContextThemeWrapper(cxt, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
		} else {
			themedContext = new ContextThemeWrapper(cxt, android.R.style.Theme_Light_NoTitleBar);
		}
		pdialog=new ProgressDialog(themedContext);
	}

	@Override
	protected String doInBackground(String... params) 
	{
		try
		{
			String response= HttpRequest.post(WebService.GET_STARTED_TRIPS, inputdata);
			Log.e("response",response);
			return response;
		}
		catch(Exception e)
		{
			Log.e("Exception is " ,e.toString());
		}
		return null;
	}

	@Override
	protected void onPostExecute(String result) 
	{
		super.onPostExecute(result);
	
		if(pdialog!=null && pdialog.isShowing())
		{
			pdialog.dismiss();
		}
		if(result!=null)
		{
			try
			{
				JSONObject jobj=new JSONObject(result);
				String status=jobj.getString("status");

				StaticData.started_trip_list.clear();
				if(status.equals("true"))
				{
					JSONArray jarray=jobj.getJSONArray("trips");
					for(int i=0;i<jarray.length();i++)
					{
						JSONObject object=jarray.getJSONObject(i);
						StartedTrip ibean=new StartedTrip();
						if(object.getString("trip_id")!=null)
						{
							ibean.setTrip_id(object.getString("trip_id"));
							ibean.setTrip_route_id(object.getString("trip_route_id"));
							ibean.setTrip_ab_id(object.getString("trip_ab_id"));
							ibean.setTrip_assign_bus(object.getString("trip_assign_bus"));
							ibean.setTrip_start_time(object.getString("trip_start_time"));
							ibean.setTrip_end_time(object.getString("trip_end_time"));
							ibean.setTrip_bus_id(object.getString("trip_bus_id"));
							ibean.setTrip_operator_id(object.getString("trip_operator_id"));
							ibean.setTrip_status(object.getString("trip_status"));

							ArrayList<TripKMS> cash_list=new ArrayList<TripKMS>();
							JSONArray jarr = object.getJSONArray("kms");
							{
								for(int j=0;j<jarr.length();j++)
								{
									JSONObject job=jarr.getJSONObject(j);
									TripKMS cash=new TripKMS();
									if(job.getString("km_id")!=null)
									{
										cash.setKm_id(job.getString("km_id"));
										cash.setKm_trip_id(job.getString("km_trip_id"));
										cash.setKm_value(job.getString("km_value"));
										cash.setKm_type(job.getString("km_type"));
										cash.setKm_date(job.getString("km_date"));
									}
									cash_list.add(cash);
								}
								ibean.setKms(cash_list);
							}

							StaticData.started_trip_list.add(ibean);
						}						
					}					
					lListener.onSuccess(jobj.getString("message"));
				}
				else
				{
					lListener.onError(jobj.getString("message"));
				}
			}
			catch(Exception e)
			{
				Log.e("Exception is ", e.toString());
			}
		}
	}

	@Override
	protected void onPreExecute() 
	{
		super.onPreExecute();
		UtilMethod.showLoading(pdialog, cxt);
	}

}
