package com.journedriver.expandable;

import android.util.Log;

import com.journedriver.bean.ExpenseCategory;
import com.journedriver.data.StaticData;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class ExpandableListDataPump_Category {
    public static HashMap<String, List<ExpenseCategory>> getData(String title) {
        LinkedHashMap<String, List<ExpenseCategory>> expandableListDetail = new LinkedHashMap<String, List<ExpenseCategory>>();

        expandableListDetail.put(title, StaticData.expense_category_list);
        Log.e("expandableListDetail",expandableListDetail.toString());
        return expandableListDetail;
    }
}
