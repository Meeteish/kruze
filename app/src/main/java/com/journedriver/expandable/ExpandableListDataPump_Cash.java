package com.journedriver.expandable;

import com.journedriver.bean.CashBean;
import com.journedriver.bean.TripCashBean;
import com.journedriver.data.StaticData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class ExpandableListDataPump_Cash {
    public static HashMap<TripCashBean, List<CashBean>> getData() {
        LinkedHashMap<TripCashBean, List<CashBean>> expandableListDetail = new LinkedHashMap<TripCashBean, List<CashBean>>();

        for(TripCashBean cash : StaticData.trip_cash_list)
        {
            ArrayList<CashBean> passengers = cash.getCashlist();
            expandableListDetail.put(cash,passengers);
        }
        return expandableListDetail;
    }
}

/*ArrayList<String> list1=new ArrayList<String>();
list1.add("John Carter");
        list1.add("Jonny Carlson");
        list1.add("Johnson Carlson");
        expandableListDetail.put("Andheri West BKC	", list1);
        expandableListDetail.put("Bandra Station BKC", list1);
        expandableListDetail.put("Borivali Andheri East", list1);
        expandableListDetail.put("Borivali BKC", list1);*/
