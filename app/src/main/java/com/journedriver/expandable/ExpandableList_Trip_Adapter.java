package com.journedriver.expandable;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.journedriver.activity.ScanActivity;
import com.journedriver.activity.StartRouteActivity;
import com.journedriver.bean.TripBean;
import com.journedriver.bean.Trip_Stops;
import com.journedriver.data.StaticData;
import com.journedriver.data.UtilMethod;
import com.journedriver.task.InsertReachStationTask;
import com.journedriver.task.TaskListener;
import com.journyapp.journyapp.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.ListIterator;

public class ExpandableList_Trip_Adapter extends BaseExpandableListAdapter {

    private Context context;
    private List<TripBean> expandableListTitle;
    private HashMap<TripBean, List<Trip_Stops>> expandableListDetail;

    SharedPreferences sharedPreferences;
    SharedPreferences sp_ruuning_trip_detail;
    boolean is_stoppped=false;

    public ExpandableList_Trip_Adapter(Context context, List<TripBean> expandableListTitle,
    		HashMap<TripBean, List<Trip_Stops>> expandableListDetail) {
        this.context = context;
        this.expandableListTitle = expandableListTitle;
        this.expandableListDetail = expandableListDetail;

        sharedPreferences=context.getSharedPreferences("tripdetail",context.MODE_PRIVATE);
        sp_ruuning_trip_detail=context.getSharedPreferences("running_trip_detail", context.MODE_PRIVATE);
    }

    @Override
    public Object getChild(int listPosition, int expandedListPosition) {
        return this.expandableListDetail.get(this.expandableListTitle.get(listPosition))
                .get(expandedListPosition);
    }

    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }

    @Override
    public View getChildView(final int listPosition, final int expandedListPosition,
                             final boolean isLastChild, View convertView, ViewGroup parent) {
        //final String expandedListText = (String) getChild(listPosition, expandedListPosition);
    	final Trip_Stops trip_stops = (Trip_Stops) getChild(listPosition, expandedListPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.layout_station_detail, null);
        }
        TextView tv_stop_name = (TextView) convertView.findViewById(R.id.tv_station);
        final TextView tv_reach = (TextView) convertView.findViewById(R.id.tv_reach);
        TextView tv_time = (TextView) convertView.findViewById(R.id.tv_time);
        ImageView iv_stop=(ImageView) convertView.findViewById(R.id.iv_stop);

        if(expandedListPosition== StaticData.trip_stop_index)
            tv_reach.setVisibility(View.VISIBLE);
        else
            tv_reach.setVisibility(View.GONE);

        if(expandedListPosition==0)
        {
            if(StaticData.is_proceed)
                tv_reach.setText("Next");
            else
                tv_reach.setText("Start");
            iv_stop.setImageResource(R.drawable.location_icon);
            tv_stop_name.setTextColor(context.getResources().getColor(R.color.text_color_blue));
        }
        else if(isLastChild)
        {
            tv_reach.setText("Stop");
            iv_stop.setImageResource(R.drawable.destination_icon);
            tv_stop_name.setTextColor(context.getResources().getColor(R.color.text_color_blue));
        }
        else
        {
            if(StaticData.is_proceed)
                tv_reach.setText("Next");
            else
                tv_reach.setText("Reach");
            iv_stop.setImageResource(R.drawable.circle);
        }

        tv_stop_name.setText(trip_stops.toString());
        tv_time.setText(trip_stops.getT_stop_time());
        /*if(expandedListPosition>0)
        {
        	if(isLastChild)
            {
            	//tv_reach.setVisibility(View.GONE);
        		iv_stop.setImageResource(R.drawable.destination_icon);
        		tv_stop_name.setTextColor(context.getResources().getColor(R.color.text_color_blue));
            }
        	else
        	{
        		*//*if(expandedListPosition==1) {
                    tv_reach.setVisibility(View.VISIBLE);
                }
        		else
        			tv_reach.setVisibility(View.GONE);*//*
	       		iv_stop.setImageResource(R.drawable.circle);
        	}
            //if(isLastChild==true && StaticData.is_proceed==true)
            if(is_stoppped)
                //tv_reach.setText("");
                tv_reach.setVisibility(View.GONE);
            else if(isLastChild==true)
                tv_reach.setText("Stop");
            else if(isLastChild==true && StaticData.is_proceed == true)
                tv_reach.setVisibility(View.GONE);
            else if(StaticData.is_proceed)
                tv_reach.setText("Next");
            else
                tv_reach.setText("Reach");
        }
        else if(expandedListPosition==0)
        {
            tv_reach.setText("Start");
            //tv_reach.setVisibility(View.VISIBLE);
        	iv_stop.setImageResource(R.drawable.location_icon);
        	tv_stop_name.setTextColor(context.getResources().getColor(R.color.text_color_blue));
        }
        tv_stop_name.setText(trip_stops.toString());
        tv_time.setText(trip_stops.getT_stop_time());*/
        /*tv_reach.setOnClickListener(onreachClickListener);*/
        tv_reach.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                //if (isLastChild == true && tv_reach.getText().toString().equals("Stop")) {
                if (isLastChild == true) {
                    //code to update webserver about we are proceeding for the next stop

                    SharedPreferences sp = context.getSharedPreferences("driver_info", context.MODE_PRIVATE);
                    String driver_id = sp.getString("driver_id", null);
                    String b_trip_id=StaticData.trip_list.get(listPosition).getTrip_id();
                    String stop_id=StaticData.trip_list.get(listPosition).getTrip_stops().get(expandedListPosition).getT_stop_id();
                    //String stop_id=StaticData.trip_list.get(listPosition).getTrip_stops().get(expandedListPosition).getT_stop();
                    Log.e("reach_driver_id", driver_id);
                    Log.e("reach_b_trip_id", b_trip_id);
                    Log.e("reach_stop_id", stop_id);



                    ArrayList<NameValuePair> inputdata=new ArrayList<NameValuePair>();
                    inputdata.add(new BasicNameValuePair("driver_id", driver_id));
                    inputdata.add(new BasicNameValuePair("trip_id", b_trip_id));
                    inputdata.add(new BasicNameValuePair("stop_id", stop_id));
                    inputdata.add(new BasicNameValuePair("last", "1"));
                    new InsertReachStationTask(context, inputdata, new TaskListener() {
                        @Override
                        public void onSuccess(String msg) {
                            for (int i = 0; i < StaticData.trip_list.size(); i++) {
                                if (StaticData.trip_list.get(i).getTrip_id().equals(StaticData.running_trip_id)) {
                                    ListIterator<TripBean> it = StaticData.trip_list.listIterator(i+1);
                                    if (it.hasNext()) {
                                        String id = it.next().getTrip_id();
                                        StaticData.running_trip_id = id;
                                        SharedPreferences.Editor ed = sp_ruuning_trip_detail.edit();
                                        ed.putString("id", id);
                                        ed.commit();
                                        break;
                                    } else {
                                        StaticData.running_trip_id = null;
                                        SharedPreferences.Editor ed = sp_ruuning_trip_detail.edit();
                                        ed.putString("id", null);
                                        ed.commit();
                                    }
                                }
                            }
                            StaticData.is_proceed=false;
                            StaticData.trip_stop_index=0;

                            sharedPreferences.edit().putInt("trip_stop_id",0);
                            sharedPreferences.edit().putBoolean("is_proceed",false).commit();
                            sharedPreferences.edit().putInt("trip_stop_index",0).commit();

                            is_stoppped=true;
                            notifyDataSetChanged();

                            StartRouteActivity activity = (StartRouteActivity)context;
                            activity.finish();
                        }

                        @Override
                        public void onError(String msg) {
                            UtilMethod.showToast(msg,context);
                        }
                    }).execute();

                    //--------------------------------------------------------

                }
                else if((getChildrenCount(0)-1)==StaticData.trip_stop_index)
                {

                    notifyDataSetChanged();
                }
                /*else if(StaticData.trip_stop_index==0)
                {
                    is_stoppped=false;


                    StaticData.is_proceed=false;
                    StaticData.trip_stop_index++;

                    SharedPreferences.Editor ed = sharedPreferences.edit();
                    ed.putBoolean("is_proceed",false);
                    ed.putInt("trip_stop_index", StaticData.trip_stop_index);
                    ed.commit();

                    notifyDataSetChanged();
                }*/
                else if(StaticData.trip_stop_index>=0 && StaticData.is_proceed==true)
                {
                    //code to update webserver about we are proceeding for the next stop
                    SharedPreferences splogoutstatus = context.getSharedPreferences("logoutstatus",context.MODE_PRIVATE);
                    splogoutstatus.edit().putBoolean("status",false).commit();

                    SharedPreferences sp = context.getSharedPreferences("driver_info", context.MODE_PRIVATE);
                    String driver_id = sp.getString("driver_id", null);
                    String b_trip_id=StaticData.trip_list.get(listPosition).getTrip_id();
                    String stop_id=StaticData.trip_list.get(listPosition).getTrip_stops().get(expandedListPosition).getT_stop_id();
                    //String stop_id=StaticData.trip_list.get(listPosition).getTrip_stops().get(expandedListPosition).getT_stop();
                    Log.e("reach_driver_id", driver_id);
                    Log.e("reach_b_trip_id", b_trip_id);
                    Log.e("reach_stop_id", stop_id);


                    ArrayList<NameValuePair> inputdata=new ArrayList<NameValuePair>();
                    inputdata.add(new BasicNameValuePair("driver_id", driver_id));
                    inputdata.add(new BasicNameValuePair("trip_id", b_trip_id));
                    inputdata.add(new BasicNameValuePair("stop_id", stop_id));
                    new InsertReachStationTask(context, inputdata, new TaskListener() {
                        @Override
                        public void onSuccess(String msg) {
                            StaticData.is_proceed=false;
                            StaticData.trip_stop_index++;
                            sharedPreferences.edit().putInt("trip_stop_id", Integer.parseInt(trip_stops.getT_stop_id()));
                            //sharedPreferences.edit().putInt("trip_stop_id", Integer.parseInt(trip_stops.getT_stop()));

                            SharedPreferences.Editor ed = sharedPreferences.edit();
                            ed.putBoolean("is_proceed", false);
                            ed.putInt("trip_stop_index", StaticData.trip_stop_index);
                            ed.commit();

                            expandableListTitle.get(listPosition).setComplete_status("2");
                            notifyDataSetChanged();
                        }

                        @Override
                        public void onError(String msg) {
                            UtilMethod.showToast(msg,context);
                        }
                    }).execute();

                    //--------------------------------------------------------

                }
                else {
                    Intent in = new Intent(context, ScanActivity.class);
                    //context.startActivity(in);
                    //b_trip_id*,b_bus_id*, stop_id*

                    String b_trip_id=StaticData.trip_list.get(listPosition).getTrip_id();
                    String b_bus_id=StaticData.trip_list.get(listPosition).getTrip_bus_id();
                    //String stop_id=StaticData.trip_list.get(listPosition).getTrip_stops().get(expandedListPosition).getT_stop_id();
                    String stop_id=StaticData.trip_list.get(listPosition).getTrip_stops().get(expandedListPosition).getT_stop();
                    Log.e("b_trip_id", b_trip_id);
                    Log.e("b_bus_id", b_bus_id);
                    Log.e("stop_id", stop_id);

                    in.putExtra("b_trip_id", b_trip_id);
                    in.putExtra("b_bus_id", b_bus_id);
                    in.putExtra("stop_id", stop_id);

                    StartRouteActivity activity = (StartRouteActivity)context;
                    activity.startActivityForResult(in,101);
                }
            }
        });
        
        return convertView;
    }

    @Override
    public int getChildrenCount(int listPosition) {
        return this.expandableListDetail.get(this.expandableListTitle.get(listPosition)).size();
    }

    @Override
    public Object getGroup(int listPosition) {
        return this.expandableListTitle.get(listPosition);
    }

    @Override
    public int getGroupCount() {
        return this.expandableListTitle.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public View getGroupView(int listPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        //String listTitle = (String) getGroup(listPosition);
    	TripBean listTitle = (TripBean) getGroup(listPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.layout_rout_detail, null);
        }
        TextView listTitleTextView = (TextView) convertView
                .findViewById(R.id.tv_rout_title);
        TextView listTimeTextView = (TextView) convertView
                .findViewById(R.id.tv_rout_time);
        TextView listStatusTextView = (TextView) convertView
                .findViewById(R.id.tv_rout_status);
        
        ImageView iv_expand_arrow= (ImageView) convertView.findViewById(R.id.iv_expand_arrow);
        if(isExpanded)
        	iv_expand_arrow.setImageResource(R.drawable.up_arrow);
        else
        	iv_expand_arrow.setImageResource(R.drawable.down_arrow);       	
        //listTitleTextView.setTypeface(null, Typeface.BOLD);
        listTitleTextView.setText(listTitle.toString());
        listTimeTextView.setText(listTitle.getTrip_start_time()+" - "+listTitle.getTrip_end_time());

        /*if(listTitle.getTrip_id().equals(StaticData.running_trip_id))
        {
            listStatusTextView.setTextColor(context.getResources().getColor(R.color.runningtext));
            listStatusTextView.setText("Running");
        }
        else {*/
            if (listTitle.getComplete_status().equals("3")) {
                listStatusTextView.setTextColor(context.getResources().getColor(R.color.canceltext));
                listStatusTextView.setText("Cancelled");
            }
            if (listTitle.getComplete_status().equals("2")) {
                listStatusTextView.setTextColor(context.getResources().getColor(R.color.runningtext));
                listStatusTextView.setText("Running");
            }
            else if (listTitle.getComplete_status().equals("1")) {
                listStatusTextView.setTextColor(context.getResources().getColor(R.color.completedtext));
                listStatusTextView.setText("Completed");
            } else if (listTitle.getComplete_status().equals("0")) {
                listStatusTextView.setTextColor(context.getResources().getColor(R.color.pendingtext));
                listStatusTextView.setText("Not Started Yet");
            }
        /*}*/
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }
    
    /*OnClickListener onreachClickListener=new OnClickListener() {
		@Override
		public void onClick(View v) {
            if((getChildrenCount(0)-1)==StaticData.trip_stop_index)
            {

            }
            if(StaticData.trip_stop_index==0)
            {
                StaticData.is_proceed=false;
                StaticData.trip_stop_index++;

                SharedPreferences.Editor ed = sharedPreferences.edit();
                ed.putBoolean("is_proceed",false);
                ed.putInt("trip_stop_index", StaticData.trip_stop_index);
                ed.commit();

                notifyDataSetChanged();
            }
            else if(StaticData.trip_stop_index>=1 && StaticData.is_proceed==true)
            {
                StaticData.is_proceed=false;
                StaticData.trip_stop_index++;

                SharedPreferences.Editor ed = sharedPreferences.edit();
                ed.putBoolean("is_proceed",false);
                ed.putInt("trip_stop_index",StaticData.trip_stop_index);
                ed.commit();

                notifyDataSetChanged();
            }
            else {
                Intent in = new Intent(context, ScanActivity.class);
                //context.startActivity(in);
                StartRouteActivity activity = (StartRouteActivity)context;
                activity.startActivityForResult(in,101);
            }
		}
	};*/
}