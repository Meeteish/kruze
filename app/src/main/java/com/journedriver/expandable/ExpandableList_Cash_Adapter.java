package com.journedriver.expandable;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.journedriver.bean.CashBean;
import com.journedriver.bean.TripCashBean;
import com.journyapp.journyapp.R;

import java.util.HashMap;
import java.util.List;

public class ExpandableList_Cash_Adapter extends BaseExpandableListAdapter {

    private Context context;
    private List<TripCashBean> expandableListTitle;
    private HashMap<TripCashBean, List<CashBean>> expandableListDetail;

    public ExpandableList_Cash_Adapter(Context context, List<TripCashBean> expandableListTitle,
                                 HashMap<TripCashBean, List<CashBean>> expandableListDetail) {
        this.context = context;
        this.expandableListTitle = expandableListTitle;
        this.expandableListDetail = expandableListDetail;
    }

    @Override
    public Object getChild(int listPosition, int expandedListPosition) {
        return this.expandableListDetail.get(this.expandableListTitle.get(listPosition))
                .get(expandedListPosition);
    }

    @Override
    public long getChildId(int listPosition, int expandedListPosition) {
        return expandedListPosition;
    }

    @Override
    public View getChildView(int listPosition, final int expandedListPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        final CashBean expandedListText = (CashBean) getChild(listPosition, expandedListPosition);
        Log.e("child",expandedListPosition+"...");

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.layout_cash_detail, null);
        }
        TextView tv_passenger = (TextView) convertView
                .findViewById(R.id.tv_passenger);
        TextView tv_amount = (TextView) convertView
                .findViewById(R.id.tv_amount);
        tv_passenger.setText(expandedListText.getPassenger_name());
        tv_amount.setText("Rs."+expandedListText.getAmount());
        return convertView;
    }

    @Override
    public int getChildrenCount(int listPosition) {
        return this.expandableListDetail.get(this.expandableListTitle.get(listPosition)).size();
    }

    @Override
    public Object getGroup(int listPosition) {
        return this.expandableListTitle.get(listPosition);
    }

    @Override
    public int getGroupCount() {
        return this.expandableListTitle.size();
    }

    @Override
    public long getGroupId(int listPosition) {
        return listPosition;
    }

    @Override
    public View getGroupView(int listPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        final TripCashBean listTitle = (TripCashBean) getGroup(listPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.layout_route_cash_detail, null);
        }
        TextView tv_rout_title = (TextView) convertView
                .findViewById(R.id.tv_rout_title);
        TextView tv_rout_timings = (TextView) convertView
                .findViewById(R.id.tv_rout_timings);
        TextView tv_rout_status = (TextView) convertView
                .findViewById(R.id.tv_rout_status);
        
        ImageView iv_expand_arrow= (ImageView) convertView.findViewById(R.id.iv_expand_arrow);
        if(isExpanded)
        	iv_expand_arrow.setImageResource(R.drawable.up_arrow);
        else
        	iv_expand_arrow.setImageResource(R.drawable.down_arrow);       	
        //listTitleTextView.setTypeface(null, Typeface.BOLD);
        tv_rout_title.setText(listTitle.getBus_route_title());
        tv_rout_timings.setText(listTitle.getTrip_start_time() + " - " + listTitle.getTrip_end_time());
        tv_rout_status.setText("Rs. "+listTitle.getTotal_amount());

        if(getChildrenCount(listPosition)==0) {
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                     Toast.makeText(context, "Trip doesn't have any cash detail", Toast.LENGTH_LONG).show();
                }
            });
        }
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int listPosition, int expandedListPosition) {
        return true;
    }
}