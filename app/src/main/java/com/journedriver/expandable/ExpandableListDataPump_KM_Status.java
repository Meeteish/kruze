package com.journedriver.expandable;

import android.util.Log;

import com.journedriver.data.StaticData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class ExpandableListDataPump_KM_Status {
    public static HashMap<String, List<String>> getData(String title) {
        LinkedHashMap<String, List<String>> expandableListDetail = new LinkedHashMap<String, List<String>>();

        StaticData.km_weekend_status.clear();


        StaticData.km_weekend_status.add("Empty");
        StaticData.km_weekend_status.add("Pick up");
        StaticData.km_weekend_status.add("Drop");
        expandableListDetail.put(title, StaticData.km_weekend_status);

        //expandableListDetail.put(title, StaticData.expense_category_list);
        Log.e("expandableListDetail", expandableListDetail.toString());
        return expandableListDetail;
    }
}
