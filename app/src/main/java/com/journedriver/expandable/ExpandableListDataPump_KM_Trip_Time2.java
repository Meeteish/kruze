package com.journedriver.expandable;

import android.util.Log;

import com.journedriver.bean.TripBean;
import com.journedriver.data.StaticData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

public class ExpandableListDataPump_KM_Trip_Time2 {
    public static HashMap<String, List<String>> getData(String title) {
        LinkedHashMap<String, List<String>> expandableListDetail = new LinkedHashMap<String, List<String>>();

        ArrayList<String> trip_timings=new ArrayList<>();

        StaticData.trip_time_list.clear();
        for (TripBean trip : StaticData.trip_list)
        {
            String time = trip.getTrip_start_time()+" - "+trip.getTrip_end_time();
            trip_timings.add(time);
            StaticData.trip_time_list.add(time);
        }

        expandableListDetail.put(title,trip_timings);
        //expandableListDetail.put(title, StaticData.expense_category_list);
        Log.e("expandableListDetail",expandableListDetail.toString());
        return expandableListDetail;
    }
}
