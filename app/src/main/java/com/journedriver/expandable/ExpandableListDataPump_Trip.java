package com.journedriver.expandable;

import com.journedriver.bean.TripBean;
import com.journedriver.bean.Trip_Stops;
import com.journedriver.data.StaticData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;


public class ExpandableListDataPump_Trip {
	
	 public static HashMap<TripBean, List<Trip_Stops>> getData() {
		 LinkedHashMap<TripBean, List<Trip_Stops>> expandableListDetail = new LinkedHashMap<TripBean, List<Trip_Stops>>();	
//		 if(StaticData.trip_list.size()>0)
//		 {
			 for(TripBean trip : StaticData.trip_list)
			 {
				 ArrayList<Trip_Stops> withOrigin_Stop=trip.getTrip_stops();
				 /*Trip_Stops trip_origin=new Trip_Stops();
//				 trip_origin.setStop_name(trip.getBus_route_from().split(",")[0]);
				 trip_origin.setStop_name(trip.getBus_route_from());
				 trip_origin.setT_stop_time(trip.getTrip_start_time());
			
				 Trip_Stops trip_stop=new Trip_Stops();
//				 trip_stop.setStop_name(trip.getBus_route_to().split(",")[0]);
				 trip_stop.setStop_name(trip.getBus_route_to());
				 trip_stop.setT_stop_time(trip.getTrip_end_time());
				 
				 withOrigin_Stop.add(0, trip_origin);
				 withOrigin_Stop.add(trip_stop);*/

				 //expandableListDetail.put(trip, trip.getTrip_stops());
				 expandableListDetail.put(trip, withOrigin_Stop);
			 }
//		 }
        return expandableListDetail;
    }
}
