package com.journedriver.bean;

/**
 * Created by administrator on 17/3/16.
 */
public class TripKMS {
    String km_id, km_trip_id, km_value, km_type, km_date;

    public TripKMS() {
    }

    public TripKMS(String km_id, String km_trip_id, String km_value, String km_type, String km_date) {
        this.km_id = km_id;
        this.km_trip_id = km_trip_id;
        this.km_value = km_value;
        this.km_type = km_type;
        this.km_date = km_date;
    }

    public String getKm_id() {
        return km_id;
    }

    public void setKm_id(String km_id) {
        this.km_id = km_id;
    }

    public String getKm_trip_id() {
        return km_trip_id;
    }

    public void setKm_trip_id(String km_trip_id) {
        this.km_trip_id = km_trip_id;
    }

    public String getKm_value() {
        return km_value;
    }

    public void setKm_value(String km_value) {
        this.km_value = km_value;
    }

    public String getKm_type() {
        return km_type;
    }

    public void setKm_type(String km_type) {
        this.km_type = km_type;
    }

    public String getKm_date() {
        return km_date;
    }

    public void setKm_date(String km_date) {
        this.km_date = km_date;
    }
}
