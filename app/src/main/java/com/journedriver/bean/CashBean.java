package com.journedriver.bean;

import java.io.Serializable;

/**
 * Created by administrator on 26/2/16.
 */
public class CashBean implements Serializable {
    private String passenger_name,amount;

    public CashBean() {
    }

    public CashBean(String passenger_name, String amount) {
        this.passenger_name = passenger_name;
        this.amount = amount;
    }

    public String getPassenger_name() {
        return passenger_name;
    }

    public void setPassenger_name(String passenger_name) {
        this.passenger_name = passenger_name;
    }

    public String getAmount() {
        return amount;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }
}
