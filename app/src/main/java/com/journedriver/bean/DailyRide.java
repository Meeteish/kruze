package com.journedriver.bean;

import java.io.Serializable;

/**
 * Created by administrator on 20/2/16.
 */
public class DailyRide implements Serializable {
    private String km_trip_id,from,to,start,end,start_kms,end_kms;
    private String difference;

    public DailyRide() {
    }

    public DailyRide(String km_trip_id, String from, String to, String start, String end, String start_kms, String end_kms, String difference) {
        this.km_trip_id = km_trip_id;
        this.from = from;
        this.to = to;
        this.start = start;
        this.end = end;
        this.start_kms = start_kms;
        this.end_kms = end_kms;
        this.difference=difference;
    }

    public String getDifference() {
        return difference;
    }

    public void setDifference(String difference) {
        this.difference = difference;
    }

    public String getKm_trip_id() {
        return km_trip_id;
    }

    public void setKm_trip_id(String km_trip_id) {
        this.km_trip_id = km_trip_id;
    }

    public String getFrom() {
        return from;
    }

    public void setFrom(String from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getStart() {
        return start;
    }

    public void setStart(String start) {
        this.start = start;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }

    public String getStart_kms() {
        return start_kms;
    }

    public void setStart_kms(String start_kms) {
        this.start_kms = start_kms;
    }

    public String getEnd_kms() {
        return end_kms;
    }

    public void setEnd_kms(String end_kms) {
        this.end_kms = end_kms;
    }
}
