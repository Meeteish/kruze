package com.journedriver.bean;

/**
 * Created by administrator on 11/2/16.
 */
public class Passengers {
    private String b_id,b_trip_id,b_bus_id,b_passenger_id,b_from,b_to,b_qr_code,b_qr_image,b_date,b_status,b_verify,b_payment_amount,b_payment_status,passenger_id,passenger_name,from_stop,to_stop,passenger_mobile;
    private String seats,b_type;
    public Passengers() {
    }

    public Passengers(String b_id, String b_trip_id, String b_bus_id, String b_passenger_id, String b_from, String b_to, String b_qr_code, String b_qr_image, String b_date, String b_status, String b_verify, String b_payment_amount, String b_payment_status, String passenger_id, String passenger_name, String from_stop, String to_stop, String passenger_mobile, String seats,String b_type) {
        this.b_id = b_id;
        this.b_trip_id = b_trip_id;
        this.b_bus_id = b_bus_id;
        this.b_passenger_id = b_passenger_id;
        this.b_from = b_from;
        this.b_to = b_to;
        this.b_qr_code = b_qr_code;
        this.b_qr_image = b_qr_image;
        this.b_date = b_date;
        this.b_status = b_status;
        this.b_verify = b_verify;
        this.b_payment_amount = b_payment_amount;
        this.b_payment_status = b_payment_status;
        this.passenger_id = passenger_id;
        this.passenger_name = passenger_name;
        this.from_stop = from_stop;
        this.to_stop = to_stop;
        this.passenger_mobile = passenger_mobile;
        this.seats = seats;
        this.b_type=b_type;
    }

    public String getB_type() {
        return b_type;
    }

    public void setB_type(String b_type) {
        this.b_type = b_type;
    }

    public String getB_id() {
        return b_id;
    }

    public void setB_id(String b_id) {
        this.b_id = b_id;
    }

    public String getB_trip_id() {
        return b_trip_id;
    }

    public void setB_trip_id(String b_trip_id) {
        this.b_trip_id = b_trip_id;
    }

    public String getB_bus_id() {
        return b_bus_id;
    }

    public void setB_bus_id(String b_bus_id) {
        this.b_bus_id = b_bus_id;
    }

    public String getB_passenger_id() {
        return b_passenger_id;
    }

    public void setB_passenger_id(String b_passenger_id) {
        this.b_passenger_id = b_passenger_id;
    }

    public String getB_from() {
        return b_from;
    }

    public void setB_from(String b_from) {
        this.b_from = b_from;
    }

    public String getB_to() {
        return b_to;
    }

    public void setB_to(String b_to) {
        this.b_to = b_to;
    }

    public String getB_qr_code() {
        return b_qr_code;
    }

    public void setB_qr_code(String b_qr_code) {
        this.b_qr_code = b_qr_code;
    }

    public String getB_qr_image() {
        return b_qr_image;
    }

    public void setB_qr_image(String b_qr_image) {
        this.b_qr_image = b_qr_image;
    }

    public String getB_date() {
        return b_date;
    }

    public void setB_date(String b_date) {
        this.b_date = b_date;
    }

    public String getB_status() {
        return b_status;
    }

    public void setB_status(String b_status) {
        this.b_status = b_status;
    }

    public String getB_verify() {
        return b_verify;
    }

    public void setB_verify(String b_verify) {
        this.b_verify = b_verify;
    }

    public String getB_payment_amount() {
        return b_payment_amount;
    }

    public void setB_payment_amount(String b_payment_amount) {
        this.b_payment_amount = b_payment_amount;
    }

    public String getB_payment_status() {
        return b_payment_status;
    }

    public void setB_payment_status(String b_payment_status) {
        this.b_payment_status = b_payment_status;
    }

    public String getPassenger_id() {
        return passenger_id;
    }

    public void setPassenger_id(String passenger_id) {
        this.passenger_id = passenger_id;
    }

    public String getPassenger_name() {
        return passenger_name;
    }

    public void setPassenger_name(String passenger_name) {
        this.passenger_name = passenger_name;
    }

    public String getFrom_stop() {
        return from_stop;
    }

    public void setFrom_stop(String from_stop) {
        this.from_stop = from_stop;
    }

    public String getTo_stop() {
        return to_stop;
    }

    public void setTo_stop(String to_stop) {
        this.to_stop = to_stop;
    }

    public String getPassenger_mobile() {
        return passenger_mobile;
    }

    public void setPassenger_mobile(String passenger_mobile) {
        this.passenger_mobile = passenger_mobile;
    }

    public String getSeats() {
        return seats;
    }

    public void setSeats(String seats) {
        this.seats = seats;
    }

    @Override
    public String toString() {
        return b_id+"    "+ seats;
    }
}
