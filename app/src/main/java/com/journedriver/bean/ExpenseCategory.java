package com.journedriver.bean;

/**
 * Created by administrator on 3/2/16.
 */
public class ExpenseCategory {
    private String ec_id, ec_name;

    public ExpenseCategory() {
    }

    public ExpenseCategory(String ec_id, String ec_name) {
        this.ec_id = ec_id;
        this.ec_name = ec_name;
    }

    public String getEc_id() {
        return ec_id;
    }

    public void setEc_id(String ec_id) {
        this.ec_id = ec_id;
    }

    public String getEc_name() {
        return ec_name;
    }

    public void setEc_name(String ec_name) {
        this.ec_name = ec_name;
    }

    @Override
    public String toString() {
        return ec_name;
    }
}
