package com.journedriver.bean;

import java.io.Serializable;

public class Trip_Stops implements Serializable {
	private String t_stop_id,t_stop_trip_id, t_stop,t_stop_time,t_stop_status,stop_name;

	public Trip_Stops() {
		super();
	}

	public Trip_Stops(String t_stop_id, String t_stop_trip_id, String t_stop, String t_stop_time, String t_stop_status,
			String stop_name) {
		super();
		this.t_stop_id = t_stop_id;
		this.t_stop_trip_id = t_stop_trip_id;
		this.t_stop = t_stop;
		this.t_stop_time = t_stop_time;
		this.t_stop_status = t_stop_status;
		this.stop_name = stop_name;
	}

	public String getT_stop_id() {
		return t_stop_id;
	}

	public void setT_stop_id(String t_stop_id) {
		this.t_stop_id = t_stop_id;
	}

	public String getT_stop_trip_id() {
		return t_stop_trip_id;
	}

	public void setT_stop_trip_id(String t_stop_trip_id) {
		this.t_stop_trip_id = t_stop_trip_id;
	}

	public String getT_stop() {
		return t_stop;
	}

	public void setT_stop(String t_stop) {
		this.t_stop = t_stop;
	}

	public String getT_stop_time() {
		return t_stop_time;
	}

	public void setT_stop_time(String t_stop_time) {
		this.t_stop_time = t_stop_time;
	}

	public String getT_stop_status() {
		return t_stop_status;
	}

	public void setT_stop_status(String t_stop_status) {
		this.t_stop_status = t_stop_status;
	}

	public String getStop_name() {
		return stop_name;
	}

	public void setStop_name(String stop_name) {
		this.stop_name = stop_name;
	}
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return stop_name;
	}
}
