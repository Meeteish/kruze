package com.journedriver.bean;

import java.io.Serializable;

/**
 * Created by administrator on 20/2/16.
 */
public class WeekendRide implements Serializable {
    private String weekend_ride_id,vehicle,vehicle_type,start_location,end_location,start_time,end_time,opening_km,closing_km,total_km,duty_hours,driver_id,operator_id,company_id,driver_ride_date,weekend_ride_status,company_name;

    public WeekendRide() {
    }

    public WeekendRide(String weekend_ride_id, String vehicle, String vehicle_type, String start_location, String end_location, String start_time, String end_time, String opening_km, String closing_km, String total_km, String duty_hours, String driver_id, String operator_id, String company_id, String driver_ride_date, String weekend_ride_status, String company_name) {
        this.weekend_ride_id = weekend_ride_id;
        this.vehicle = vehicle;
        this.vehicle_type = vehicle_type;
        this.start_location = start_location;
        this.end_location = end_location;
        this.start_time = start_time;
        this.end_time = end_time;
        this.opening_km = opening_km;
        this.closing_km = closing_km;
        this.total_km = total_km;
        this.duty_hours = duty_hours;
        this.driver_id = driver_id;
        this.operator_id = operator_id;
        this.company_id = company_id;
        this.driver_ride_date = driver_ride_date;
        this.weekend_ride_status = weekend_ride_status;
        this.company_name = company_name;
    }

    public String getWeekend_ride_id() {
        return weekend_ride_id;
    }

    public void setWeekend_ride_id(String weekend_ride_id) {
        this.weekend_ride_id = weekend_ride_id;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public String getVehicle_type() {
        return vehicle_type;
    }

    public void setVehicle_type(String vehicle_type) {
        this.vehicle_type = vehicle_type;
    }

    public String getStart_location() {
        return start_location;
    }

    public void setStart_location(String start_location) {
        this.start_location = start_location;
    }

    public String getEnd_location() {
        return end_location;
    }

    public void setEnd_location(String end_location) {
        this.end_location = end_location;
    }

    public String getStart_time() {
        return start_time;
    }

    public void setStart_time(String start_time) {
        this.start_time = start_time;
    }

    public String getEnd_time() {
        return end_time;
    }

    public void setEnd_time(String end_time) {
        this.end_time = end_time;
    }

    public String getOpening_km() {
        return opening_km;
    }

    public void setOpening_km(String opening_km) {
        this.opening_km = opening_km;
    }

    public String getClosing_km() {
        return closing_km;
    }

    public void setClosing_km(String closing_km) {
        this.closing_km = closing_km;
    }

    public String getTotal_km() {
        return total_km;
    }

    public void setTotal_km(String total_km) {
        this.total_km = total_km;
    }

    public String getDuty_hours() {
        return duty_hours;
    }

    public void setDuty_hours(String duty_hours) {
        this.duty_hours = duty_hours;
    }

    public String getDriver_id() {
        return driver_id;
    }

    public void setDriver_id(String driver_id) {
        this.driver_id = driver_id;
    }

    public String getOperator_id() {
        return operator_id;
    }

    public void setOperator_id(String operator_id) {
        this.operator_id = operator_id;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getDriver_ride_date() {
        return driver_ride_date;
    }

    public void setDriver_ride_date(String driver_ride_date) {
        this.driver_ride_date = driver_ride_date;
    }

    public String getWeekend_ride_status() {
        return weekend_ride_status;
    }

    public void setWeekend_ride_status(String weekend_ride_status) {
        this.weekend_ride_status = weekend_ride_status;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }
}
