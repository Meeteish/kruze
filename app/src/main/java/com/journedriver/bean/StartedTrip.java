package com.journedriver.bean;

import java.util.ArrayList;

/**
 * Created by administrator on 17/3/16.
 */
public class StartedTrip {
    String trip_id,trip_route_id,trip_ab_id,trip_assign_bus,trip_start_time,trip_end_time,trip_bus_id,trip_operator_id,trip_status;
    ArrayList<TripKMS> kms;

    public StartedTrip() {
    }

    public StartedTrip(String trip_id, String trip_route_id, String trip_ab_id, String trip_assign_bus, String trip_start_time, String trip_end_time, String trip_bus_id, String trip_operator_id, String trip_status, ArrayList<TripKMS> kms) {
        this.trip_id = trip_id;
        this.trip_route_id = trip_route_id;
        this.trip_ab_id = trip_ab_id;
        this.trip_assign_bus = trip_assign_bus;
        this.trip_start_time = trip_start_time;
        this.trip_end_time = trip_end_time;
        this.trip_bus_id = trip_bus_id;
        this.trip_operator_id = trip_operator_id;
        this.trip_status = trip_status;
        this.kms = kms;
    }

    public String getTrip_id() {
        return trip_id;
    }

    public void setTrip_id(String trip_id) {
        this.trip_id = trip_id;
    }

    public String getTrip_route_id() {
        return trip_route_id;
    }

    public void setTrip_route_id(String trip_route_id) {
        this.trip_route_id = trip_route_id;
    }

    public String getTrip_ab_id() {
        return trip_ab_id;
    }

    public void setTrip_ab_id(String trip_ab_id) {
        this.trip_ab_id = trip_ab_id;
    }

    public String getTrip_assign_bus() {
        return trip_assign_bus;
    }

    public void setTrip_assign_bus(String trip_assign_bus) {
        this.trip_assign_bus = trip_assign_bus;
    }

    public String getTrip_start_time() {
        return trip_start_time;
    }

    public void setTrip_start_time(String trip_start_time) {
        this.trip_start_time = trip_start_time;
    }

    public String getTrip_end_time() {
        return trip_end_time;
    }

    public void setTrip_end_time(String trip_end_time) {
        this.trip_end_time = trip_end_time;
    }

    public String getTrip_bus_id() {
        return trip_bus_id;
    }

    public void setTrip_bus_id(String trip_bus_id) {
        this.trip_bus_id = trip_bus_id;
    }

    public String getTrip_operator_id() {
        return trip_operator_id;
    }

    public void setTrip_operator_id(String trip_operator_id) {
        this.trip_operator_id = trip_operator_id;
    }

    public String getTrip_status() {
        return trip_status;
    }

    public void setTrip_status(String trip_status) {
        this.trip_status = trip_status;
    }

    public ArrayList<TripKMS> getKms() {
        return kms;
    }

    public void setKms(ArrayList<TripKMS> kms) {
        this.kms = kms;
    }
}
