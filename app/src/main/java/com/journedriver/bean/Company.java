package com.journedriver.bean;

/**
 * Created by administrator on 23/2/16.
 */
public class Company {
    private String company_id,company_name,company_operator,company_status;

    public Company() {
    }

    public Company(String company_id, String company_name, String company_operator, String company_status) {
        this.company_id = company_id;
        this.company_name = company_name;
        this.company_operator = company_operator;
        this.company_status = company_status;
    }

    public String getCompany_id() {
        return company_id;
    }

    public void setCompany_id(String company_id) {
        this.company_id = company_id;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getCompany_operator() {
        return company_operator;
    }

    public void setCompany_operator(String company_operator) {
        this.company_operator = company_operator;
    }

    public String getCompany_status() {
        return company_status;
    }

    public void setCompany_status(String company_status) {
        this.company_status = company_status;
    }

    @Override
    public String toString() {
        return company_name;
    }
}
