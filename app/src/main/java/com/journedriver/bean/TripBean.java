package com.journedriver.bean;
import java.io.Serializable;
import java.util.ArrayList;

public class TripBean implements Serializable {
	private String bus_route_id,bus_operator_id,bus_city_id,bus_route_title, bus_route_from,bus_route_to,bus_route_start_lat,bus_route_start_long,bus_route_end_lat,bus_route_end_long,bus_route_status,trip_id,trip_route_id,trip_start_time,trip_end_time,trip_bus_id,trip_operator_id,trip_create_datetime,trip_status,complete_status;
	private ArrayList<Trip_Stops> trip_stops;

	
	public TripBean() {
		super();
	}

	public TripBean(String bus_route_id, String bus_operator_id, String bus_city_id, String bus_route_title,
			String bus_route_from, String bus_route_to, String bus_route_start_lat, String bus_route_start_long,
			String bus_route_end_lat, String bus_route_end_long, String bus_route_status, String trip_id,
			String trip_route_id, String trip_start_time, String trip_end_time, String trip_bus_id,
			String trip_operator_id, String trip_create_datetime, String trip_status, String complete_status) {
		super();
		this.bus_route_id = bus_route_id;
		this.bus_operator_id = bus_operator_id;
		this.bus_city_id = bus_city_id;
		this.bus_route_title = bus_route_title;
		this.bus_route_from = bus_route_from;
		this.bus_route_to = bus_route_to;
		this.bus_route_start_lat = bus_route_start_lat;
		this.bus_route_start_long = bus_route_start_long;
		this.bus_route_end_lat = bus_route_end_lat;
		this.bus_route_end_long = bus_route_end_long;
		this.bus_route_status = bus_route_status;
		this.trip_id = trip_id;
		this.trip_route_id = trip_route_id;
		this.trip_start_time = trip_start_time;
		this.trip_end_time = trip_end_time;
		this.trip_bus_id = trip_bus_id;
		this.trip_operator_id = trip_operator_id;
		this.trip_create_datetime = trip_create_datetime;
		this.trip_status = trip_status;
		this.complete_status=complete_status;
	}

	public TripBean(String bus_route_id, String bus_operator_id, String bus_city_id, String bus_route_title,
			String bus_route_from, String bus_route_to, String bus_route_start_lat, String bus_route_start_long,
			String bus_route_end_lat, String bus_route_end_long, String bus_route_status, String trip_id,
			String trip_route_id, String trip_start_time, String trip_end_time, String trip_bus_id,
			String trip_operator_id, String trip_create_datetime, String trip_status,String complete_status,
			ArrayList<Trip_Stops> trip_stops) {
		super();
		this.bus_route_id = bus_route_id;
		this.bus_operator_id = bus_operator_id;
		this.bus_city_id = bus_city_id;
		this.bus_route_title = bus_route_title;
		this.bus_route_from = bus_route_from;
		this.bus_route_to = bus_route_to;
		this.bus_route_start_lat = bus_route_start_lat;
		this.bus_route_start_long = bus_route_start_long;
		this.bus_route_end_lat = bus_route_end_lat;
		this.bus_route_end_long = bus_route_end_long;
		this.bus_route_status = bus_route_status;
		this.trip_id = trip_id;
		this.trip_route_id = trip_route_id;
		this.trip_start_time = trip_start_time;
		this.trip_end_time = trip_end_time;
		this.trip_bus_id = trip_bus_id;
		this.trip_operator_id = trip_operator_id;
		this.trip_create_datetime = trip_create_datetime;
		this.trip_status = trip_status;
		this.complete_status=complete_status;
		this.trip_stops = trip_stops;
	}

	public String getBus_route_id() {
		return bus_route_id;
	}

	public void setBus_route_id(String bus_route_id) {
		this.bus_route_id = bus_route_id;
	}

	public String getBus_operator_id() {
		return bus_operator_id;
	}

	public void setBus_operator_id(String bus_operator_id) {
		this.bus_operator_id = bus_operator_id;
	}

	public String getBus_city_id() {
		return bus_city_id;
	}

	public void setBus_city_id(String bus_city_id) {
		this.bus_city_id = bus_city_id;
	}

	public String getBus_route_title() {
		return bus_route_title;
	}

	public void setBus_route_title(String bus_route_title) {
		this.bus_route_title = bus_route_title;
	}

	public String getBus_route_from() {
		return bus_route_from;
	}

	public void setBus_route_from(String bus_route_from) {
		this.bus_route_from = bus_route_from;
	}

	public String getBus_route_to() {
		return bus_route_to;
	}

	public void setBus_route_to(String bus_route_to) {
		this.bus_route_to = bus_route_to;
	}

	public String getBus_route_start_lat() {
		return bus_route_start_lat;
	}

	public void setBus_route_start_lat(String bus_route_start_lat) {
		this.bus_route_start_lat = bus_route_start_lat;
	}

	public String getBus_route_start_long() {
		return bus_route_start_long;
	}

	public void setBus_route_start_long(String bus_route_start_long) {
		this.bus_route_start_long = bus_route_start_long;
	}

	public String getBus_route_end_lat() {
		return bus_route_end_lat;
	}

	public void setBus_route_end_lat(String bus_route_end_lat) {
		this.bus_route_end_lat = bus_route_end_lat;
	}

	public String getBus_route_end_long() {
		return bus_route_end_long;
	}

	public void setBus_route_end_long(String bus_route_end_long) {
		this.bus_route_end_long = bus_route_end_long;
	}

	public String getBus_route_status() {
		return bus_route_status;
	}

	public void setBus_route_status(String bus_route_status) {
		this.bus_route_status = bus_route_status;
	}

	public String getTrip_id() {
		return trip_id;
	}

	public void setTrip_id(String trip_id) {
		this.trip_id = trip_id;
	}

	public String getTrip_route_id() {
		return trip_route_id;
	}

	public void setTrip_route_id(String trip_route_id) {
		this.trip_route_id = trip_route_id;
	}

	public String getTrip_start_time() {
		return trip_start_time;
	}

	public void setTrip_start_time(String trip_start_time) {
		this.trip_start_time = trip_start_time;
	}

	public String getTrip_end_time() {
		return trip_end_time;
	}

	public void setTrip_end_time(String trip_end_time) {
		this.trip_end_time = trip_end_time;
	}

	public String getTrip_bus_id() {
		return trip_bus_id;
	}

	public void setTrip_bus_id(String trip_bus_id) {
		this.trip_bus_id = trip_bus_id;
	}

	public String getTrip_operator_id() {
		return trip_operator_id;
	}

	public void setTrip_operator_id(String trip_operator_id) {
		this.trip_operator_id = trip_operator_id;
	}

	public String getTrip_create_datetime() {
		return trip_create_datetime;
	}

	public void setTrip_create_datetime(String trip_create_datetime) {
		this.trip_create_datetime = trip_create_datetime;
	}

	public String getTrip_status() {
		return trip_status;
	}

	public void setTrip_status(String trip_status) {
		this.trip_status = trip_status;
	}

	public ArrayList<Trip_Stops> getTrip_stops() {
		return trip_stops;
	}

	public void setTrip_stops(ArrayList<Trip_Stops> trip_stops) {
		this.trip_stops = trip_stops;
	}

	public String getComplete_status() {
		return complete_status;
	}

	public void setComplete_status(String complete_status) {
		this.complete_status = complete_status;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return bus_route_title;
	}
}
