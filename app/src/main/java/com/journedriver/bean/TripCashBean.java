package com.journedriver.bean;
import java.io.Serializable;
import java.util.ArrayList;

public class TripCashBean implements Serializable {
	private String bus_route_id,bus_route_title, bus_route_from_title,bus_route_to_title,trip_id,trip_start_time,trip_end_time,total_amount;
	private ArrayList<CashBean> cashlist;

	public TripCashBean() {
		super();
	}

	public TripCashBean(String bus_route_id, String bus_route_title, String bus_route_from_title, String bus_route_to_title, String trip_id, String trip_start_time, String trip_end_time, String total_amount, ArrayList<CashBean> cashlist) {
		this.bus_route_id = bus_route_id;
		this.bus_route_title = bus_route_title;
		this.bus_route_from_title = bus_route_from_title;
		this.bus_route_to_title = bus_route_to_title;
		this.trip_id = trip_id;
		this.trip_start_time = trip_start_time;
		this.trip_end_time = trip_end_time;
		this.total_amount=total_amount;
		this.cashlist = cashlist;
	}

	public String getBus_route_id() {
		return bus_route_id;
	}

	public void setBus_route_id(String bus_route_id) {
		this.bus_route_id = bus_route_id;
	}

	public String getBus_route_title() {
		return bus_route_title;
	}

	public void setBus_route_title(String bus_route_title) {
		this.bus_route_title = bus_route_title;
	}

	public String getBus_route_from_title() {
		return bus_route_from_title;
	}

	public void setBus_route_from_title(String bus_route_from_title) {
		this.bus_route_from_title = bus_route_from_title;
	}

	public String getBus_route_to_title() {
		return bus_route_to_title;
	}

	public void setBus_route_to_title(String bus_route_to_title) {
		this.bus_route_to_title = bus_route_to_title;
	}

	public String getTrip_id() {
		return trip_id;
	}

	public void setTrip_id(String trip_id) {
		this.trip_id = trip_id;
	}

	public String getTrip_start_time() {
		return trip_start_time;
	}

	public void setTrip_start_time(String trip_start_time) {
		this.trip_start_time = trip_start_time;
	}

	public String getTrip_end_time() {
		return trip_end_time;
	}

	public void setTrip_end_time(String trip_end_time) {
		this.trip_end_time = trip_end_time;
	}

	public ArrayList<CashBean> getCashlist() {
		return cashlist;
	}

	public void setCashlist(ArrayList<CashBean> cashlist) {
		this.cashlist = cashlist;
	}

	public String getTotal_amount() {
		return total_amount;
	}

	public void setTotal_amount(String total_amount) {
		this.total_amount = total_amount;
	}

	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return bus_route_title;
	}
}
