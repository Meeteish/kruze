package com.journedriver.bean;

/**
 * Created by administrator on 4/2/16.
 */
public class Expenses_Details {
    private String e_detail_id, e_detail_expense,e_detail_driver, e_detail_cost,e_detail_kms,e_detail_options,e_detail_image,e_detail_datetime,ec_name, e_desc;

    public Expenses_Details() {
    }

    public Expenses_Details(String e_detail_id, String e_detail_expense, String e_detail_driver, String e_detail_cost, String e_detail_kms, String e_detail_options, String e_detail_image, String e_detail_datetime, String ec_name, String e_desc) {
        this.e_detail_id = e_detail_id;
        this.e_detail_expense = e_detail_expense;
        this.e_detail_driver = e_detail_driver;
        this.e_detail_cost = e_detail_cost;
        this.e_detail_kms = e_detail_kms;
        this.e_detail_options = e_detail_options;
        this.e_detail_image = e_detail_image;
        this.e_detail_datetime = e_detail_datetime;
        this.ec_name = ec_name;
        this.e_desc = e_desc;
    }

    public String getE_detail_id() {
        return e_detail_id;
    }

    public void setE_detail_id(String e_detail_id) {
        this.e_detail_id = e_detail_id;
    }

    public String getE_detail_expense() {
        return e_detail_expense;
    }

    public void setE_detail_expense(String e_detail_expense) {
        this.e_detail_expense = e_detail_expense;
    }

    public String getE_detail_driver() {
        return e_detail_driver;
    }

    public void setE_detail_driver(String e_detail_driver) {
        this.e_detail_driver = e_detail_driver;
    }

    public String getE_detail_cost() {
        return e_detail_cost;
    }

    public void setE_detail_cost(String e_detail_cost) {
        this.e_detail_cost = e_detail_cost;
    }

    public String getE_detail_kms() {
        return e_detail_kms;
    }

    public void setE_detail_kms(String e_detail_kms) {
        this.e_detail_kms = e_detail_kms;
    }

    public String getE_detail_options() {
        return e_detail_options;
    }

    public void setE_detail_options(String e_detail_options) {
        this.e_detail_options = e_detail_options;
    }

    public String getE_detail_image() {
        return e_detail_image;
    }

    public void setE_detail_image(String e_detail_image) {
        this.e_detail_image = e_detail_image;
    }

    public String getE_detail_datetime() {
        return e_detail_datetime;
    }

    public void setE_detail_datetime(String e_detail_datetime) {
        this.e_detail_datetime = e_detail_datetime;
    }

    public String getEc_name() {
        return ec_name;
    }

    public void setEc_name(String ec_name) {
        this.ec_name = ec_name;
    }

    public String getE_desc() {
        return e_desc;
    }

    public void setE_desc(String e_desc) {
        this.e_desc = e_desc;
    }
}
