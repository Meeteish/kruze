package com.journedriver.data;

import com.journedriver.bean.Company;
import com.journedriver.bean.DailyRide;
import com.journedriver.bean.ExpenseCategory;
import com.journedriver.bean.Expenses_Details;
import com.journedriver.bean.Passengers;
import com.journedriver.bean.StartedTrip;
import com.journedriver.bean.TripBean;
import com.journedriver.bean.TripCashBean;
import com.journedriver.bean.WeekendRide;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.LinkedHashMap;


public class StaticData
{	
	public static ArrayList<TripBean> trip_list=new ArrayList<TripBean>();

	public static ArrayList<TripCashBean> trip_cash_list=new ArrayList<TripCashBean>();

	public static ArrayList<String> trip_time_list=new ArrayList<String>();

	public static ArrayList<StartedTrip> started_trip_list=new ArrayList<StartedTrip>();

	public static ArrayList<String> km_weekend_status=new ArrayList<String>();

	public static ArrayList<Company> company_list=new ArrayList<Company>();

	public static ArrayList<ExpenseCategory> expense_category_list=new ArrayList<ExpenseCategory>();

	public static ArrayList<DailyRide> daily_ride_list=new ArrayList<DailyRide>();

	public static ArrayList<DailyRide> todays_daily_ride_list=new ArrayList<DailyRide>();

	public static ArrayList<WeekendRide> weekend_ride_list=new ArrayList<WeekendRide>();

	public static ArrayList<Passengers> passenger_list=new ArrayList<Passengers>();

	public static ArrayList<Passengers> passenger_list_x=new ArrayList<Passengers>();

	public static ArrayList<Passengers> boarded_passenger_list=new ArrayList<Passengers>();

	public static ArrayList<Passengers> absent_passenger_list=new ArrayList<Passengers>();

	public static HashSet<String> seats=new HashSet<>();
	public static HashSet<String> left_seats=new HashSet<>();

	//public static LinkedHashMap<TripBean,ArrayList<Passengers>> cash_list=new LinkedHashMap<>();

	public static ArrayList<Expenses_Details> expense_details_list=new ArrayList<>();

	public static int last_selected_category_id=1;

	public static int last_selected_trip_time=-1;

	public static DailyRide selected_trip_daily_ride=null;

	public static String last_selected_km_weekend_status=null;

	public static String start_km_sent=null;
	
	public static LinkedHashMap<String,ArrayList<String>> tripkms=new LinkedHashMap<>();

	public static String km_sent_trip_id=null;

	public static String km_date=null;

	public static int trip_stop_index=0;

	public static boolean is_proceed=false;

	public static boolean isTodaysDailyRideFilled=false;

	public static int total_received_cash=0;

	public static String running_trip_id=null;
}
