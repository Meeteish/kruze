package com.journedriver.data;

public class WebService
{
	//public static String HOST_URL="http://72.167.41.165/journe/webservices/";
	//public static String HOST_URL="http://192.168.0.144/journe/webservices/";
	//public static String HOST_URL="http://54.67.85.189/journe/webservices/";
	public static String HOST_URL="http://expertteam.in/journe/webservices/";

	public static String GET_TODAYS_TRIP = HOST_URL+"driver/bus_route_list.php";

	public static String GET_TRIP_CASH_RECEIVED = HOST_URL+"driver/cash_received.php";

	public static String GET_STARTED_TRIPS = HOST_URL+"driver/started_trips.php";

	public static String INSERT_HELP = HOST_URL+"driver/driver_help.php";

	public static String INSERT_CANCEL_TRIP = HOST_URL+"driver/cancel_trip.php";

	public static String INSERT_REACH_STOP = HOST_URL+"driver/stop_reach.php";

	public static String INSERT_ABSENT_PASSENGER = HOST_URL+"driver/absent_passenger.php";

	public static String INSERT_BOARDED_PASSENGER = HOST_URL+"driver/boarded_passenger.php";

	public static String INSERT_EXPENSE = HOST_URL+"driver/add_expenses.php";

	//public static String INSERT_KMS_DAILY = HOST_URL+"driver/km_management.php";
	public static String INSERT_KMS_DAILY = HOST_URL+"driver/insert_kms.php";

	public static String INSERT_KMS_WEEKEND = HOST_URL+"driver/add_weekend_ride.php";

	public static String GET_EXPENSE_CATEGORY = HOST_URL+"driver/expenses_categories.php";

	public static String GET_DAILY_RIDE_LIST = HOST_URL+"driver/km_list.php";

	public static String GET_WEEKEND_RIDE_LIST = HOST_URL+"driver/weekend_list.php";

	public static String GET_EXPENSE_LIST = HOST_URL+"driver/get_driver_expenses.php";

	public static String GET_PASSENGERS = HOST_URL+"driver/passenger_booking_list.php";

	public static String GET_COMPANY_LIST = HOST_URL+"driver/company_list.php";

	public static String GET_LOGOUT_STATUS = HOST_URL+"driver/driver_logout.php";

	public static String REQUEST_FORGOT_PASSWORD = HOST_URL+"driver/forgot_passowrd.php";

	public static String DRIVER_LOGIN="http://54.67.85.189/journe/webservices/driver/driver_login";
}
