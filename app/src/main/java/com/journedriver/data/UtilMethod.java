package com.journedriver.data;

import com.journyapp.journyapp.R;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.text.TextUtils;
import android.view.ContextThemeWrapper;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.Calendar;

public class UtilMethod {

	public static ProgressDialog showLoading(ProgressDialog progress,
			Context context) {
		ContextThemeWrapper themedContext;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			themedContext = new ContextThemeWrapper(context, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
		} else {
			themedContext = new ContextThemeWrapper(context, android.R.style.Theme_Light_NoTitleBar);
		}

		try {
			if (progress == null) {
				progress = new ProgressDialog(themedContext);
			} else {
			}
			progress.setTitle("Loading");
			progress.setMessage("Please Wait...");
			progress.setCancelable(false);

			progress.show();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return progress;
	}

	public static void showDialog(String msg,
											 Context context) {
		ContextThemeWrapper themedContext;
		if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
			themedContext = new ContextThemeWrapper(context, android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
		} else {
			themedContext = new ContextThemeWrapper(context, android.R.style.Theme_Light_NoTitleBar);
		}
		AlertDialog.Builder build=new AlertDialog.Builder(themedContext);
		build.setTitle("Message!!");
		build.setMessage(msg);
		build.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {

			}
		});
		build.show();
	}

	public static void showServerError(Context ctx) {
		try {
			if (ctx != null) {
				String message = ctx.getResources().getString(
						R.string.server_error_message);
				Toast.makeText(ctx, message, Toast.LENGTH_LONG).show();
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void showNetworkError(Context ctx) {
		try {
			if (ctx != null) {
				String message = ctx.getResources().getString(
						R.string.network_error_message);
				/* Toast.makeText(ctx, message, Toast.LENGTH_LONG).show(); */
				
				ContextThemeWrapper themedContext;
				if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
				    themedContext = new ContextThemeWrapper( ctx, android.R.style.Theme_Holo_Light_Dialog_NoActionBar );
				}
				else {
				    themedContext = new ContextThemeWrapper( ctx, android.R.style.Theme_Light_NoTitleBar );
				}
				final AlertDialog.Builder adialog = new AlertDialog.Builder(themedContext);
				adialog.setTitle("Message");
				adialog.setMessage("Internet Error!");
				adialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						// TODO Auto-generated method stub
						//adialog.dismiss();
					}
				});
				adialog.show();

			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void hideLoading(ProgressDialog progress) {
		try {
			if (progress != null) {
				if (progress.isShowing()) {
					progress.dismiss();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public static void showDownloading(ProgressBar progress) {
		if (progress != null) {
			progress.setVisibility(View.VISIBLE);
		}
	}

	public static void hideDownloading(ProgressBar progress) {
		if (progress != null) {
			progress.setVisibility(View.GONE);
		}
	}

	public static boolean isNetworkAvailable(Context context) {
		NetworkInfo localNetworkInfo = ((ConnectivityManager) context
				.getSystemService("connectivity")).getActiveNetworkInfo();
		return (localNetworkInfo != null) && (localNetworkInfo.isConnected());
	}

	public static boolean isStringNullOrBlank(String str) {
		if (str == null) {
			return true;
		} else if (str.equals("null") || str.equals("")) {
			return true;
		}
		return false;
	}

	public static void showToast(String message, Context ctx) {
		try {
			if (ctx != null)
				Toast.makeText(ctx, message, Toast.LENGTH_LONG).show();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public final static boolean isValidEmail(CharSequence target) {
		return !TextUtils.isEmpty(target)
				&& android.util.Patterns.EMAIL_ADDRESS.matcher(target)
						.matches();
	}

	public static String getTodaysDate()
	{
		Calendar c=Calendar.getInstance();
		c.setTimeInMillis(System.currentTimeMillis());
		int dd = c.get(Calendar.DAY_OF_MONTH);
		int mm = c.get(Calendar.MONTH);
		int yy = c.get(Calendar.YEAR);
		String date = dd + "-" + (mm+1)+"-"+yy;
		//String date = dd + "-" + mm+"-"+yy;
		return date;
	}
}
