package com.journedriver.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.journedriver.bean.DailyRide;
import com.journedriver.bean.WeekendRide;
import com.journedriver.data.StaticData;
import com.journedriver.view.TextViewB;
import com.journedriver.view.TextViewL;
import com.journedriver.view.TextViewR;
import com.journyapp.journyapp.R;

public class WeekendRideListAdapter extends BaseAdapter
{
	Activity context;
	TextViewR tv_source,tv_destination;
	TextViewL tv_time_from,tv_time_to,tv_km_from,tv_km_to,tv_loc_from,tv_loc_to;
	TextViewB tv_company,tv_km_total;

	public WeekendRideListAdapter(Activity context)
	{
		this.context = context;		
	}

	@Override
	public int getCount() 
	{
		return StaticData.weekend_ride_list.size();
	}

	@Override
	public Object getItem(int position) 
	{
		return position;
	}

	@Override
	public long getItemId(int position) 
	{
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) 
	{

		WeekendRide details = StaticData.weekend_ride_list.get(position);
		LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

		convertView = mInflater.inflate(R.layout.fragment_layout_weekend_ride_list_item, null);
		//tv_source = (TextViewR)convertView.findViewById(R.id.tv_source);
		//tv_destination = (TextViewR)convertView.findViewById(R.id.tv_destination);
		tv_company = (TextViewB) convertView.findViewById(R.id.tv_company);
		tv_time_from = (TextViewL) convertView.findViewById(R.id.tv_time_from);
		tv_time_to = (TextViewL) convertView.findViewById(R.id.tv_time_to);
		tv_km_from = (TextViewL) convertView.findViewById(R.id.tv_km_from);
		tv_km_to = (TextViewL) convertView.findViewById(R.id.tv_km_to);
		tv_loc_from = (TextViewL) convertView.findViewById(R.id.tv_loc_from);
		tv_loc_to = (TextViewL) convertView.findViewById(R.id.tv_loc_to);
		tv_km_total = (TextViewB) convertView.findViewById(R.id.tv_km_total);

		tv_company.setText(details.getCompany_name());
		tv_time_from.setText(details.getStart_time());
		tv_time_to.setText(details.getEnd_time());
		if (details.getOpening_km().equals(""))
			tv_km_from.setText(details.getOpening_km());
		else
			tv_km_from.setText(details.getOpening_km() + "km");
		if (details.getClosing_km().equals(""))
			tv_km_to.setText(details.getClosing_km());
		else
			tv_km_to.setText(details.getClosing_km() + "km");
		tv_loc_from.setText(details.getStart_location());
		tv_loc_to.setText(details.getEnd_location());
		if(details.getTotal_km().equals("0"))
			tv_km_total.setText("");
		else
			tv_km_total.setText(details.getTotal_km());

		convertView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				/*Intent in =new Intent(context, ExpenseDetailActivity.class);
				in.putExtra("expense_index",position);
				context.startActivity(in);*/
			}
		});

		return convertView;
	}

}

