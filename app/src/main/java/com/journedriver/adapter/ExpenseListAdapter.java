package com.journedriver.adapter;

import android.app.Activity;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.journedriver.activity.ExpenseDetailActivity;
import com.journedriver.view.TextViewL;
import com.journyapp.journyapp.R;
import com.journedriver.bean.Expenses_Details;
import com.journedriver.data.StaticData;
import com.journedriver.view.TextViewR;

public class ExpenseListAdapter extends BaseAdapter
{
	Activity context;
	public ExpenseListAdapter(Activity context)
	{
		this.context = context;		
	}

	@Override
	public int getCount() 
	{
		return StaticData.expense_details_list.size();
	}

	@Override
	public Object getItem(int position) 
	{
		return position;
	}

	@Override
	public long getItemId(int position) 
	{
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) 
	{
		Expenses_Details details=StaticData.expense_details_list.get(position);
		LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

		convertView = mInflater.inflate(R.layout.layout_expense_detail, null);
		TextViewR tv_expense_name= (TextViewR) convertView.findViewById(R.id.tv_expense_name);
		TextViewL tv_kms = (TextViewL) convertView.findViewById(R.id.tv_kms);
		TextViewL tv_amount = (TextViewL) convertView.findViewById(R.id.tv_amount);

		tv_expense_name.setText(details.getEc_name());
		tv_kms.setText(details.getE_detail_kms()+"Km");
		tv_amount.setText("Rs."+details.getE_detail_cost());

		convertView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				Intent in =new Intent(context, ExpenseDetailActivity.class);
				in.putExtra("expense_index",position);
				context.startActivity(in);
			}
		});

		return convertView;
	}

}

