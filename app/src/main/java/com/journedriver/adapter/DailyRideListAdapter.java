package com.journedriver.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.journedriver.activity.KilometersActivity;
import com.journedriver.bean.DailyRide;
import com.journedriver.data.StaticData;
import com.journedriver.fragment.Fragment_KM_DailyRide;
import com.journedriver.fragment.Fragment_KM_DailyRide_Selected;
import com.journedriver.view.TextViewL;
import com.journedriver.view.TextViewR;
import com.journyapp.journyapp.R;

public class DailyRideListAdapter extends BaseAdapter
{
	Activity context;
	TextViewR tv_source,tv_destination;
	TextViewL tv_time_from,tv_time_to,tv_km_from,tv_km_to,tv_loc_from,tv_loc_to,tv_km_total;

	public DailyRideListAdapter(Activity context)
	{
		this.context = context;		
	}

	@Override
	public int getCount() 
	{
		return StaticData.daily_ride_list.size();
	}

	@Override
	public Object getItem(int position) 
	{
		return position;
	}

	@Override
	public long getItemId(int position) 
	{
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) 
	{
		DailyRide details=StaticData.daily_ride_list.get(position);
		LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

		convertView = mInflater.inflate(R.layout.fragment_layout_ride_list_item, null);
		//tv_source = (TextViewR)convertView.findViewById(R.id.tv_source);
		//tv_destination = (TextViewR)convertView.findViewById(R.id.tv_destination);
		tv_time_from = (TextViewL)convertView.findViewById(R.id.tv_time_from);
		tv_time_to = (TextViewL)convertView.findViewById(R.id.tv_time_to);
		tv_km_from = (TextViewL)convertView.findViewById(R.id.tv_km_from);
		tv_km_to = (TextViewL)convertView.findViewById(R.id.tv_km_to);
		tv_loc_from = (TextViewL)convertView.findViewById(R.id.tv_loc_from);
		tv_loc_to = (TextViewL)convertView.findViewById(R.id.tv_loc_to);
		tv_km_total = (TextViewL)convertView.findViewById(R.id.tv_km_total);

		tv_time_from.setText(details.getStart());
		tv_time_to.setText(details.getEnd());
		if(details.getStart_kms().equals(""))
			tv_km_from.setText(details.getStart_kms());
		else
			tv_km_from.setText(details.getStart_kms()+"km");
		if(details.getEnd_kms().equals(""))
			tv_km_to.setText(details.getEnd_kms());
		else
			tv_km_to.setText(details.getEnd_kms()+"km");
		tv_loc_from.setText(details.getFrom());
		tv_loc_to.setText(details.getTo());
		if(details.getDifference().equals("0"))
			tv_km_total.setText("");
		else
			tv_km_total.setText(details.getDifference());


		convertView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				StaticData.selected_trip_daily_ride=StaticData.daily_ride_list.get(position);
				KilometersActivity kmActivity = (KilometersActivity)context;
				kmActivity.showDefaultFragment(new Fragment_KM_DailyRide_Selected());
				kmActivity.isadd_running=true;
				kmActivity.shouldAddMoreVisible=false;
				kmActivity.setAddMoreVisibleState();
			}
		});

		return convertView;
	}

}

