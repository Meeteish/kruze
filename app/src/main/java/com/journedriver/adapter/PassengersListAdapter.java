package com.journedriver.adapter;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.journedriver.bean.Passengers;
import com.journedriver.data.StaticData;
import com.journedriver.view.TextViewL;
import com.journedriver.view.TextViewR;
import com.journyapp.journyapp.R;

public class PassengersListAdapter extends BaseAdapter
{
	Activity context;
	public PassengersListAdapter(Activity context)
	{
		this.context = context;		
	}

	@Override
	public int getCount() 
	{
		return StaticData.passenger_list.size();
	}

	@Override
	public Object getItem(int position) 
	{
		return position;
	}

	@Override
	public long getItemId(int position) 
	{
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) 
	{
		Passengers details=StaticData.passenger_list.get(position);
		LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

		convertView = mInflater.inflate(R.layout.layout_passengers_list, null);
		TextViewR tv_passenger_name= (TextViewR) convertView.findViewById(R.id.tv_passenger_name);
		TextViewL tv_ticket_for = (TextViewL) convertView.findViewById(R.id.tv_ticket_for);
		TextViewL tv_ticket_no = (TextViewL) convertView.findViewById(R.id.tv_ticket_no);
		TextViewL tv_qrcode = (TextViewL) convertView.findViewById(R.id.tv_qrcode);
		//ImageView iv_status = (ImageView) convertView.findViewById(R.id.iv_status);

		tv_passenger_name.setText(details.getPassenger_name());
		Log.e("details.getFrom_stop()", details.getFrom_stop());
		Log.e("details.getTo_stop()",details.getTo_stop());
		String[] from = details.getFrom_stop().split(" ");
		String[] to = details.getTo_stop().split(" ");

		tv_ticket_for.setText(from[0]+"..To.."+to[0]);
		tv_ticket_no.setText(details.getB_id());
		tv_qrcode.setText(details.getB_qr_code());

		/*if(details.getB_verify().equals("2"))
			iv_status.setImageResource(R.drawable.cancel3x);
		else
			iv_status.setImageResource(R.drawable.done3x);*/



		return convertView;
	}

}

