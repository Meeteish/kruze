package com.journedriver.adapter;

import android.app.Activity;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.journedriver.activity.ExpenseDetailActivity;
import com.journedriver.bean.Expenses_Details;
import com.journedriver.data.StaticData;
import com.journedriver.view.TextViewL;
import com.journedriver.view.TextViewR;
import com.journyapp.journyapp.R;

public class SeatListAdapter extends BaseAdapter
{
	Activity context;
	String[] slist;
	boolean isverify=false;
	public SeatListAdapter(Activity context, String[] slist)
	{
		this.context = context; this.slist=slist;
	}

	@Override
	public int getCount() 
	{
		return slist.length;
	}

	@Override
	public Object getItem(int position) 
	{
		return position;
	}

	@Override
	public long getItemId(int position) 
	{
		return position;
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) 
	{
		String sno =slist[position];
		LayoutInflater mInflater = (LayoutInflater) context.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

		convertView = mInflater.inflate(R.layout.activity_scan_layout_seat, null);
		TextViewL tv_seat_no= (TextViewL) convertView.findViewById(R.id.tv_seatno);
		final ImageView iv_verify = (ImageView) convertView.findViewById(R.id.iv_verify);

		tv_seat_no.setText(sno);

		convertView.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View view) {
				if (isverify) {
					StaticData.seats.remove(slist[position]);
					StaticData.left_seats.add(slist[position]);
					//iv_verify.setImageResource(R.drawable.cancel3x);
					iv_verify.setVisibility(View.INVISIBLE);
					isverify = false;
				} else {
					StaticData.seats.add(slist[position]);
					StaticData.left_seats.remove(slist[position]);
					iv_verify.setImageResource(R.drawable.done3x);
					iv_verify.setVisibility(View.VISIBLE);
					isverify = true;
				}
				Log.e("StaticData.seats",StaticData.seats.toString());
				Log.e("StaticData.left_seats",StaticData.left_seats.toString());
			}
		});

		return convertView;
	}

}

