package com.journedriver.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextViewBI extends TextView {
	 
	   public TextViewBI(Context context, AttributeSet attrs, int defStyle) {
	      super(context, attrs, defStyle);
	      init();
	   }
	 
	   public TextViewBI(Context context, AttributeSet attrs) {
	      super(context, attrs);
	      init();
	   }
	 
	   public TextViewBI(Context context) {
	       super(context);
	       init();
	   }
	 
	   private void init() {
	       Typeface tf = Typeface.createFromAsset(getContext().getAssets(),"fonts/Roboto-BoldItalic.ttf");
	       setTypeface(tf);
	   }
	}