package com.journedriver.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextViewBoldItalic extends TextView {
	 
	   public TextViewBoldItalic(Context context, AttributeSet attrs, int defStyle) {
	      super(context, attrs, defStyle);
	      init();
	   }
	 
	   public TextViewBoldItalic(Context context, AttributeSet attrs) {
	      super(context, attrs);
	      init();
	   }
	 
	   public TextViewBoldItalic(Context context) {
	       super(context);
	       init();
	   }
	 
	   private void init() {
	       Typeface tf = Typeface.createFromAsset(getContext().getAssets(),"fonts/Roboto-BoldItalic.ttf");
	       setTypeface(tf);
	   }
	}