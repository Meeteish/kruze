package com.journedriver.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextViewMediumItalic extends TextView {

	   public TextViewMediumItalic(Context context, AttributeSet attrs, int defStyle) {
	      super(context, attrs, defStyle);
	      init();
	   }

	   public TextViewMediumItalic(Context context, AttributeSet attrs) {
	      super(context, attrs);
	      init();
	   }

	   public TextViewMediumItalic(Context context) {
	       super(context);
	       init();
	   }
	 
	   private void init() {
	       Typeface tf = Typeface.createFromAsset(getContext().getAssets(),"fonts/Roboto-MediumItalic.ttf");
	       setTypeface(tf);
	   }
	}