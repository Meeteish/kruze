package com.journedriver.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

public class EditTextR extends EditText {
	 
	   public EditTextR(Context context, AttributeSet attrs, int defStyle) {
	      super(context, attrs, defStyle);
	      init();
	   }
	 
	   public EditTextR(Context context, AttributeSet attrs) {
	      super(context, attrs);
	      init();
	   }
	 
	   public EditTextR(Context context) {
	       super(context);
	       init();
	   }
	 
	   private void init() {
	       Typeface tf = Typeface.createFromAsset(getContext().getAssets(),"fonts/Roboto-Regular.ttf");
	       setTypeface(tf);
	   }
	}