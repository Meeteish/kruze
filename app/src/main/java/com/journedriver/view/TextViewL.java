package com.journedriver.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextViewL extends TextView {

	   public TextViewL(Context context, AttributeSet attrs, int defStyle) {
	      super(context, attrs, defStyle);
	      init();
	   }

	   public TextViewL(Context context, AttributeSet attrs) {
	      super(context, attrs);
	      init();
	   }

	   public TextViewL(Context context) {
	       super(context);
	       init();
	   }
	 
	   private void init() {
	       Typeface tf = Typeface.createFromAsset(getContext().getAssets(),"fonts/Roboto-Light.ttf");
	       setTypeface(tf);
	   }
	}