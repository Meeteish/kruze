package com.journedriver.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.EditText;

public class EditTextThin extends EditText {

	   public EditTextThin(Context context, AttributeSet attrs, int defStyle) {
	      super(context, attrs, defStyle);
	      init();
	   }

	   public EditTextThin(Context context, AttributeSet attrs) {
	      super(context, attrs);
	      init();
	   }

	   public EditTextThin(Context context) {
	       super(context);
	       init();
	   }
	 
	   private void init() {
	       Typeface tf = Typeface.createFromAsset(getContext().getAssets(),"fonts/Roboto-Thin.ttf");
	       setTypeface(tf);
	   }
	}