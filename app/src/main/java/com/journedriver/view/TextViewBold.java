package com.journedriver.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

//   com.journedriver.view.TextViewB

public class TextViewBold extends TextView {
	 
	   public TextViewBold(Context context, AttributeSet attrs, int defStyle) {
	      super(context, attrs, defStyle);
	      init();
	   }
	 
	   public TextViewBold(Context context, AttributeSet attrs) {
	      super(context, attrs);
	      init();
	   }
	 
	   public TextViewBold(Context context) {
	       super(context);
	       init();
	   }
	 
	   private void init() {
	       Typeface tf = Typeface.createFromAsset(getContext().getAssets(),"fonts/Roboto-Bold.ttf");
	       setTypeface(tf);
	   }
	}