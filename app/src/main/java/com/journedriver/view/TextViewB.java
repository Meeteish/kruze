package com.journedriver.view;

import android.content.Context;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.widget.TextView;

public class TextViewB extends TextView {
	 
	   public TextViewB(Context context, AttributeSet attrs, int defStyle) {
	      super(context, attrs, defStyle);
	      init();
	   }
	 
	   public TextViewB(Context context, AttributeSet attrs) {
	      super(context, attrs);
	      init();
	   }
	 
	   public TextViewB(Context context) {
	       super(context);
	       init();
	   }
	 
	   private void init() {
	       Typeface tf = Typeface.createFromAsset(getContext().getAssets(),"fonts/Roboto-Bold.ttf");
	       setTypeface(tf);
	   }
	}