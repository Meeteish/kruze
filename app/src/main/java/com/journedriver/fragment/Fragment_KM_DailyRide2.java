package com.journedriver.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.journedriver.activity.KilometersActivity;
import com.journedriver.bean.DailyRide;
import com.journedriver.bean.TripBean;
import com.journedriver.data.StaticData;
import com.journedriver.data.UtilMethod;
import com.journedriver.expandable.ExpandableListDataPump_KM_Trip_Time;
import com.journedriver.expandable.ExpandableList_Trip_Timings_Adapter;
import com.journedriver.task.InsertKMS_Daily_Task;
import com.journedriver.task.TaskListener;
import com.journedriver.view.EditTextL;
import com.journedriver.view.TextViewB;
import com.journyapp.journyapp.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by administrator on 6/2/16.
 */
public class Fragment_KM_DailyRide2 extends Fragment implements ExpandableListView.OnChildClickListener, View.OnClickListener {
    ExpandableListView expandable_trip_list;
    HashMap<String, List<String>> expandableListDetail;
    ExpandableList_Trip_Timings_Adapter expandableListAdapter;
    List<String> expandableListTitle;

    EditTextL edit_start_kms,edit_end_kms;
    TextViewB tv_save;
    String driver_id;
    String selectedtime=null;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_layout_daily_ride,null);

        StaticData.last_selected_trip_time=-1;

        expandable_trip_list=(ExpandableListView) view.findViewById(R.id.expandable_trip_list);
//		if(StaticData.expense_category_list.size()>0)
        expandableListDetail = ExpandableListDataPump_KM_Trip_Time.getData("Trip timings");
//		else
//			expandableListDetail = ExpandableListDataPump_Category.getData("No Category Found");
        expandableListTitle = new ArrayList<String>(expandableListDetail.keySet());
        expandableListAdapter = new ExpandableList_Trip_Timings_Adapter(getActivity(), expandableListTitle, expandableListDetail);
        expandable_trip_list.setAdapter(expandableListAdapter);

        expandable_trip_list.setOnChildClickListener(this);

        edit_start_kms=(EditTextL)view.findViewById(R.id.edit_start_kms);
        edit_end_kms=(EditTextL)view.findViewById(R.id.edit_end_kms);
        tv_save=(TextViewB)view.findViewById(R.id.tv_save);

        tv_save.setOnClickListener(this);


        return view;
    }

    @Override
    public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i1, long l) {

        StaticData.last_selected_trip_time=i1;
        expandableListDetail = ExpandableListDataPump_KM_Trip_Time.getData(StaticData.trip_time_list.get(i1));
        expandableListTitle = new ArrayList<String>(expandableListDetail.keySet());
        expandableListAdapter = new ExpandableList_Trip_Timings_Adapter(getActivity(), expandableListTitle, expandableListDetail);
        expandable_trip_list.setAdapter(expandableListAdapter);

        expandableListView.collapseGroup(i);

        Log.e("expandableListTitle", expandableListTitle.toString());

        String startkms="";
        String endkms="";
        for(TripBean trip : StaticData.trip_list) {
            String[] times = expandableListTitle.get(0).split("-");
            if (times[0].trim().equals(trip.getTrip_start_time()) && times[1].trim().equals(trip.getTrip_end_time()))
            {
                String trip_id = trip.getTrip_id();
                for(DailyRide ride:StaticData.todays_daily_ride_list)
                {
                    if(ride.getKm_trip_id().equals(trip_id))
                    {
                        startkms=ride.getStart_kms();
                        endkms=ride.getEnd_kms();
                    }
                }
            }
        }
        Log.e("endkms",endkms+"...");
        edit_start_kms.setText(startkms);
        edit_end_kms.setText(endkms);
        return false;
    }

    @Override
    public void onClick(View view) {
        String kms="";

        if(StaticData.last_selected_trip_time==-1)
        {
            UtilMethod.showToast("Select Trip Time",getActivity());
        }
        else {
                if (UtilMethod.isStringNullOrBlank(edit_start_kms.getText().toString()))
                    UtilMethod.showToast("Start Kms required", getActivity());
                else {
                    if (UtilMethod.isStringNullOrBlank(edit_end_kms.getText().toString()))
                        save_kms(edit_start_kms.getText().toString(), "1");
                    else
                        save_kms(edit_end_kms.getText().toString(), "2");
                }
            }
    }

    private void save_kms(String kms, String kms_type) {
        SharedPreferences sp = getActivity().getSharedPreferences("driver_info", getActivity().MODE_PRIVATE);
        driver_id = sp.getString("driver_id",null);

        String trip_id = StaticData.trip_list.get(StaticData.last_selected_trip_time).getTrip_id();

        ArrayList<NameValuePair> inputdata=new ArrayList<>();
        inputdata.add(new BasicNameValuePair("driver_id",driver_id));
        inputdata.add(new BasicNameValuePair("kms",kms));
        inputdata.add(new BasicNameValuePair("kms_type",kms_type));
        inputdata.add(new BasicNameValuePair("trip_id",trip_id));

        new InsertKMS_Daily_Task(getActivity(),inputdata,new InsertKMS_DailyListener(kms_type)).execute();
    }

    class InsertKMS_DailyListener implements TaskListener
    {
        String kmstype;
        public InsertKMS_DailyListener(String kmstype)
        {
            this.kmstype=kmstype;
        }
        @Override
        public void onSuccess(String msg) {

            SharedPreferences splogoutstatus = getActivity().getSharedPreferences("logoutstatus",getActivity().MODE_PRIVATE);
            splogoutstatus.edit().putBoolean("status",true).commit();

            Log.e("StaticData.tripkms",StaticData.tripkms.toString());
            showDialog("Trip_Kms", "Trip kms successfully submitted");
        }
        @Override
        public void onError(String msg) {
            showDialog("Trip_Kms",msg);
        }
        void showDialog(String title, String message) {
            ContextThemeWrapper themedContext;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                themedContext = new ContextThemeWrapper(getActivity(), android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
            } else {
                themedContext = new ContextThemeWrapper(getActivity(), android.R.style.Theme_Light_NoTitleBar);
            }
            final AlertDialog.Builder adialog = new AlertDialog.Builder(themedContext);
            adialog.setTitle(title);
            adialog.setMessage(message);
            adialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //getActivity().finish();
                    KilometersActivity kmActivity = (KilometersActivity)getActivity();
                    kmActivity.showDefaultFragment(new Fragment_KM_DailyRide_List());
                    kmActivity.isadd_running=false;
                    kmActivity.shouldAddMoreVisible=true;
                    kmActivity.setAddMoreVisibleState();
                }
            });
            adialog.show();
        }
    }
}
