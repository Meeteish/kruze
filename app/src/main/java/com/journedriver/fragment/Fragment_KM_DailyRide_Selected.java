package com.journedriver.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.journedriver.activity.KilometersActivity;
import com.journedriver.bean.StartedTrip;
import com.journedriver.bean.TripKMS;
import com.journedriver.data.StaticData;
import com.journedriver.data.UtilMethod;
import com.journedriver.expandable.ExpandableListDataPump_KM_Trip_Time;
import com.journedriver.expandable.ExpandableList_Trip_Timings_Adapter;
import com.journedriver.task.GetStartedTripTask;
import com.journedriver.task.InsertKMS_Daily_Task;
import com.journedriver.task.TaskListener;
import com.journedriver.view.EditTextL;
import com.journedriver.view.TextViewB;
import com.journedriver.view.TextViewR;
import com.journyapp.journyapp.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by administrator on 6/2/16.
 */
public class Fragment_KM_DailyRide_Selected extends Fragment implements View.OnClickListener {
    TextViewR tv_trip_timing;

    EditTextL edit_start_kms,edit_end_kms;
    TextViewB tv_save;
    String driver_id;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        SharedPreferences sp = getActivity().getSharedPreferences("driver_info", getActivity().MODE_PRIVATE);
        driver_id = sp.getString("driver_id", null);

        View view = inflater.inflate(R.layout.fragment_layout_daily_ride_selected, null);

        tv_trip_timing = (TextViewR) view.findViewById(R.id.tv_trip_timing);

        edit_start_kms = (EditTextL) view.findViewById(R.id.edit_start_kms);
        edit_end_kms = (EditTextL) view.findViewById(R.id.edit_end_kms);

        edit_start_kms.setText(StaticData.selected_trip_daily_ride.getStart_kms());
        edit_end_kms.setText(StaticData.selected_trip_daily_ride.getEnd_kms());
        tv_trip_timing.setText(StaticData.selected_trip_daily_ride.getStart() + " - " + StaticData.selected_trip_daily_ride.getEnd());

        if (StaticData.selected_trip_daily_ride.getStart_kms().equals("")) {
            edit_start_kms.setEnabled(true);
            edit_end_kms.setEnabled(false);
        } else if (StaticData.selected_trip_daily_ride.getEnd_kms().equals("")) {
            edit_start_kms.setEnabled(false);
            edit_end_kms.setEnabled(true);
        } else {
            edit_start_kms.setEnabled(false);
            edit_end_kms.setEnabled(false);
        }

        tv_save = (TextViewB) view.findViewById(R.id.tv_save);
        tv_save.setOnClickListener(this);

        if (KilometersActivity.date2.equals(UtilMethod.getTodaysDate()))
        {
            tv_save.setTextColor(getResources().getColor(R.color.text_color_black));
            tv_save.setEnabled(true);
        }
        else {
            tv_save.setTextColor(getResources().getColor(R.color.text_color_gray));
            tv_save.setEnabled(false);
        }

        return view;
    }

    @Override
    public void onClick(View view) {
        String kms="";

        /*if(edit_start_kms.getText().toString().equals("")||edit_end_kms.getText().toString().equals(""))
        {
            UtilMethod.showToast("Trip Kms is already updated",getActivity());
        }*/
       /* if(StaticData.last_selected_trip_time==-1)
        {
            //UtilMethod.showToast("Select Trip Time",getActivity());
        }
        else {*/
                if (UtilMethod.isStringNullOrBlank(edit_start_kms.getText().toString()))
                    UtilMethod.showToast("Start Kms required", getActivity());
                else {
                    if (UtilMethod.isStringNullOrBlank(edit_end_kms.getText().toString()))
                        save_kms(edit_start_kms.getText().toString(), "1");
                    else {
                        if (!UtilMethod.isStringNullOrBlank(edit_end_kms.getText().toString()))
                            if(Float.parseFloat(edit_start_kms.getText().toString())>Float.parseFloat(edit_end_kms.getText().toString())) {
                                UtilMethod.showDialog("End Kms should be greater than Start Kms", getActivity());
                                return;
                            }
                        save_kms(edit_start_kms.getText().toString(),edit_end_kms.getText().toString());
                        /*if(Float.parseFloat(edit_start_kms.getText().toString())>Float.parseFloat(edit_end_kms.getText().toString()))
                            UtilMethod.showDialog("End Kms should be greater than Start Kms", getActivity());
                        else
                            save_kms(edit_end_kms.getText().toString(), "2");*/
                    }
                }
           /* }*/
    }

    private void save_kms(String skms, String ekms) {
        SharedPreferences sp = getActivity().getSharedPreferences("driver_info", getActivity().MODE_PRIVATE);
        driver_id = sp.getString("driver_id",null);

        String trip_id = StaticData.selected_trip_daily_ride.getKm_trip_id();
        Log.e("trip_id",trip_id+"");

        if(ekms.equals(""))
            ekms="0";
        ArrayList<NameValuePair> inputdata=new ArrayList<>();
        inputdata.add(new BasicNameValuePair("driver_id",driver_id));
        inputdata.add(new BasicNameValuePair("start_kms",skms));
        inputdata.add(new BasicNameValuePair("end_kms",ekms));
        inputdata.add(new BasicNameValuePair("trip_id",trip_id));

        new InsertKMS_Daily_Task(getActivity(),inputdata,new InsertKMS_DailyListener(skms)).execute();
    }

    /*private void save_kms(String kms, String kms_type) {
        SharedPreferences sp = getActivity().getSharedPreferences("driver_info", getActivity().MODE_PRIVATE);
        driver_id = sp.getString("driver_id",null);

        //String trip_id = StaticData.trip_list.get(StaticData.last_selected_trip_time).getTrip_id();
        String trip_id = StaticData.selected_trip_daily_ride.getKm_trip_id();
        Log.e("trip_id",trip_id+"");

        ArrayList<NameValuePair> inputdata=new ArrayList<>();
        inputdata.add(new BasicNameValuePair("driver_id",driver_id));
        inputdata.add(new BasicNameValuePair("kms", kms));
        inputdata.add(new BasicNameValuePair("kms_type", kms_type));
        inputdata.add(new BasicNameValuePair("trip_id", trip_id));

        new InsertKMS_Daily_Task(getActivity(),inputdata,new InsertKMS_DailyListener(kms_type)).execute();
    }*/

    class InsertKMS_DailyListener implements TaskListener
    {
        String kmstype;
        public InsertKMS_DailyListener(String kmstype)
        {
            this.kmstype=kmstype;
        }
        @Override
        public void onSuccess(String msg) {

            SharedPreferences splogoutstatus = getActivity().getSharedPreferences("logoutstatus",getActivity().MODE_PRIVATE);
            splogoutstatus.edit().putBoolean("status",true).commit();

            Log.e("StaticData.tripkms", StaticData.tripkms.toString());
            showDialog("Message!!", msg);
        }
        @Override
        public void onError(String msg) {
            showDialog("Message!!",msg);
        }
        void showDialog(String title, String message) {
            ContextThemeWrapper themedContext;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                themedContext = new ContextThemeWrapper(getActivity(), android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
            } else {
                themedContext = new ContextThemeWrapper(getActivity(), android.R.style.Theme_Light_NoTitleBar);
            }
            final AlertDialog.Builder adialog = new AlertDialog.Builder(themedContext);
            adialog.setTitle(title);
            adialog.setMessage(message);
            adialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    //getActivity().finish();
                    KilometersActivity kmActivity = (KilometersActivity) getActivity();
                    kmActivity.showDefaultFragment(new Fragment_KM_DailyRide_List());
                    kmActivity.isadd_running = false;
                    kmActivity.shouldAddMoreVisible = true;
                    kmActivity.setAddMoreVisibleState();
                }
            });
            adialog.show();
        }
    }
}
