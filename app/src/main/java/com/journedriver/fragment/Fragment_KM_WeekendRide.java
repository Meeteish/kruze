package com.journedriver.fragment;

import android.app.AlertDialog;
import android.app.TimePickerDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TimePicker;

import com.journedriver.activity.KilometersActivity;
import com.journedriver.bean.Company;
import com.journedriver.data.StaticData;
import com.journedriver.data.UtilMethod;
import com.journedriver.expandable.ExpandableList_Trip_Status_Adapter;
import com.journedriver.task.GetCompanyTask;
import com.journedriver.task.InsertKMS_Weekend_Task;
import com.journedriver.task.TaskListener;
import com.journedriver.view.EditTextL;
import com.journedriver.view.TextViewB;
import com.journedriver.view.TextViewR;
import com.journyapp.journyapp.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by administrator on 6/2/16.
 */
public class Fragment_KM_WeekendRide extends Fragment implements View.OnClickListener {
    LinearLayout expandable_status_list,expandable_company_list;
    TextViewR tv_status,tv_company;
    ImageView iv_expand_arrow,iv_expand_arrow_company;
    HashMap<String, List<String>> expandableListDetail;
    ExpandableList_Trip_Status_Adapter expandableListAdapter;
    List<String> expandableListTitle;

    EditTextL edit_vehicle,edit_vehicle_type,edit_start_loc,edit_end_loc,edit_start_time,edit_end_time,edit_opening_km,edit_closing_km,edit_total_km,edit_duty_hours;
    TextViewB tv_save;
    String driver_id;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_layout_weekend_ride,null);

        expandable_status_list=(LinearLayout) view.findViewById(R.id.expandable_status_list);
        expandable_status_list.setOnClickListener(on_statusClick);
        tv_status=(TextViewR)view.findViewById(R.id.tv_status);
        iv_expand_arrow=(ImageView)view.findViewById(R.id.iv_expand_arrow);

        expandable_company_list=(LinearLayout) view.findViewById(R.id.expandable_company_list);
        expandable_company_list.setOnClickListener(on_companyClick);
        tv_company=(TextViewR)view.findViewById(R.id.tv_company);
        iv_expand_arrow_company=(ImageView)view.findViewById(R.id.iv_expand_arrow_company);

        edit_vehicle=(EditTextL)view.findViewById(R.id.edit_vehicle);
        edit_vehicle_type=(EditTextL)view.findViewById(R.id.edit_vehicle_type);
        edit_start_loc=(EditTextL)view.findViewById(R.id.edit_start_loc);
        edit_end_loc=(EditTextL)view.findViewById(R.id.edit_end_loc);
        edit_start_time=(EditTextL)view.findViewById(R.id.edit_start_time);
        edit_start_time.setOnClickListener(onStarttimeClickListener);
        edit_start_time.setOnFocusChangeListener(onStarttimeFocusListener);

        edit_end_time=(EditTextL)view.findViewById(R.id.edit_end_time);
        edit_end_time.setOnClickListener(onEndtimeClickListener);
        edit_end_time.setOnFocusChangeListener(onEndtimeFocusListener);
        edit_opening_km=(EditTextL)view.findViewById(R.id.edit_opening_km);
        edit_closing_km=(EditTextL)view.findViewById(R.id.edit_closing_km);
        //edit_total_km=(EditTextL)view.findViewById(R.id.edit_total_km);
        edit_duty_hours=(EditTextL)view.findViewById(R.id.edit_duty_hours);
        tv_save=(TextViewB)view.findViewById(R.id.tv_save);
        tv_save.setOnClickListener(this);

        SharedPreferences sp = getActivity().getSharedPreferences("driver_info", getActivity().MODE_PRIVATE);
        driver_id = sp.getString("driver_id",null);

        if(StaticData.company_list.size()==0)
        {
            getCompanyDetail();
        }

        return view;
    }

    private void getCompanyDetail() {
        ArrayList<NameValuePair> inputdata=new ArrayList<NameValuePair>();
        inputdata.add(new BasicNameValuePair("driver_id", driver_id));
        new GetCompanyTask(getActivity(),inputdata,new CompanyListener()).execute();
    }

    AlertDialog dialog=null;
    View.OnClickListener on_statusClick=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            ContextThemeWrapper themedContext;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                themedContext = new ContextThemeWrapper(getActivity(), android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
            } else {
                themedContext = new ContextThemeWrapper(getActivity(), android.R.style.Theme_Light_NoTitleBar);
            }

            final AlertDialog.Builder adialog = new AlertDialog.Builder(themedContext);
            ListView lv=new ListView(getActivity());
            StaticData.km_weekend_status.clear();
            StaticData.km_weekend_status.add("Drop");
            StaticData.km_weekend_status.add("Pick up");
            StaticData.km_weekend_status.add("Empty");

            ArrayAdapter<String> adap=new ArrayAdapter<String>(getActivity(),android.R.layout.simple_list_item_single_choice,StaticData.km_weekend_status);
            lv.setAdapter(adap);
            lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    String currentStatus = StaticData.km_weekend_status.get(i);
                    if(i==0)
                        StaticData.last_selected_km_weekend_status="1";
                    else if(i==1)
                        StaticData.last_selected_km_weekend_status="2";
                    else if(i==2)
                        StaticData.last_selected_km_weekend_status="3";
                    tv_status.setText(currentStatus);
                    iv_expand_arrow.setImageResource(R.drawable.down_arrow);
                    dialog.cancel();
                }
            });
            //adialog.setCancelable(false);
            adialog.setView(lv);
            dialog=adialog.create();
            dialog.show();
            iv_expand_arrow.setImageResource(R.drawable.up_arrow);
        }
    };
    View.OnClickListener on_companyClick=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            ContextThemeWrapper themedContext;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                themedContext = new ContextThemeWrapper(getActivity(), android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
            } else {
                themedContext = new ContextThemeWrapper(getActivity(), android.R.style.Theme_Light_NoTitleBar);
            }

            final AlertDialog.Builder adialog = new AlertDialog.Builder(themedContext);
            ListView lv=new ListView(getActivity());

            ArrayAdapter<Company> adap=new ArrayAdapter<Company>(getActivity(),android.R.layout.simple_list_item_single_choice,StaticData.company_list);
            lv.setAdapter(adap);
            lv.setChoiceMode(ListView.CHOICE_MODE_SINGLE);
            lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                    String currentStatus = StaticData.company_list.get(i).getCompany_name();
                    tv_company.setText(currentStatus);
                    iv_expand_arrow_company.setImageResource(R.drawable.down_arrow);
                    dialog.cancel();
                }
            });
            //adialog.setCancelable(false);
            adialog.setView(lv);
            dialog=adialog.create();
            dialog.show();
            iv_expand_arrow_company.setImageResource(R.drawable.up_arrow);
        }
    };

    View.OnClickListener onStarttimeClickListener=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            TimePickerDialog time = new TimePickerDialog(getActivity(),android.R.style.Theme_Holo_Light_Dialog_NoActionBar, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int i, int i1) {
                    Log.e("h", i + "");
                    Log.e("m", i1 + "");
                    edit_start_time.setText(i+":"+i1);
                }
            }, 10, 30, false);
            time.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            time.show();
        }
    };
    View.OnClickListener onEndtimeClickListener=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            TimePickerDialog time = new TimePickerDialog(getActivity(), android.R.style.Theme_Holo_Light_Dialog_NoActionBar, new TimePickerDialog.OnTimeSetListener() {
                @Override
                public void onTimeSet(TimePicker timePicker, int i, int i1) {
                    Log.e("h", i + "");
                    Log.e("m", i1 + "");
                    edit_start_time.setText(i+":"+i1);
                }
            }, 10, 30, false);
            time.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            time.show();
        }
    };
    View.OnFocusChangeListener onStarttimeFocusListener=new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View view, boolean b) {
            if (b) {
                TimePickerDialog time = new TimePickerDialog(getActivity(),android.R.style.Theme_Holo_Light_Dialog_NoActionBar, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int i, int i1) {
                        Log.e("h", i + "");
                        Log.e("m", i1 + "");
                        edit_start_time.setText(i+":"+i1);
                    }
                }, 10, 30, false);
                time.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                time.show();

            }
        }
    };
    View.OnFocusChangeListener onEndtimeFocusListener=new View.OnFocusChangeListener() {
        @Override
        public void onFocusChange(View view, boolean b) {
            if (b) {
                TimePickerDialog time = new TimePickerDialog(getActivity(),android.R.style.Theme_Holo_Light_Dialog_NoActionBar, new TimePickerDialog.OnTimeSetListener() {
                    @Override
                    public void onTimeSet(TimePicker timePicker, int i, int i1) {
                        Log.e("h", i + "");
                        Log.e("m", i1 + "");
                        edit_end_time.setText(i+":"+i1);
                    }
                }, 10, 30, false);
                time.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
                time.show();

            }
        }
    };

    @Override
    public void onClick(View view) {

        String vehicle= edit_vehicle.getText().toString();;
        String vehicle_type= edit_vehicle_type.getText().toString();
        String start_loc= edit_start_loc.getText().toString();
        String end_loc=  edit_end_loc.getText().toString();
        String start_time= edit_start_time.getText().toString();
        String end_time= edit_end_time.getText().toString();
        String opening_km= edit_opening_km.getText().toString();
        String closing_km= edit_closing_km.getText().toString();
        //String total_km= edit_total_km.getText().toString();
        String duty_hours= edit_duty_hours.getText().toString();

        if(tv_company.getText().toString().equals("Company"))
            UtilMethod.showToast("Company name required",getActivity());
        else if(UtilMethod.isStringNullOrBlank(vehicle))
            UtilMethod.showToast("Vehicle info required",getActivity());
        else if(UtilMethod.isStringNullOrBlank(vehicle_type))
            UtilMethod.showToast("Vehicle type required",getActivity());
        else if(UtilMethod.isStringNullOrBlank(start_loc))
            UtilMethod.showToast("Start Location required",getActivity());
        else if(UtilMethod.isStringNullOrBlank(end_loc))
            UtilMethod.showToast("End location required",getActivity());
        else if(UtilMethod.isStringNullOrBlank(start_time))
            UtilMethod.showToast("Start time required",getActivity());
        else if(UtilMethod.isStringNullOrBlank(end_time))
            UtilMethod.showToast("End time required",getActivity());
        else if(UtilMethod.isStringNullOrBlank(opening_km))
            UtilMethod.showToast("Opening km required",getActivity());
        else if(UtilMethod.isStringNullOrBlank(closing_km))
            UtilMethod.showToast("Closing km required",getActivity());
        //else if(UtilMethod.isStringNullOrBlank(total_km))
        //    UtilMethod.showToast("Total km required",getActivity());
        else if(UtilMethod.isStringNullOrBlank(duty_hours))
            UtilMethod.showToast("Duty hours required",getActivity());
        else if(tv_status.getText().toString().equals("Status"))
            UtilMethod.showToast("Status required",getActivity());
        else if((Float.parseFloat(closing_km)<Float.parseFloat(opening_km)))
            UtilMethod.showDialog("Closing Kms is less than Opening Kms",getActivity());
        else {
            String total_km="";
            total_km = ""+(Float.parseFloat(closing_km)-Float.parseFloat(opening_km));
            Log.e("total_km",total_km);

            ArrayList<NameValuePair> inputdata = new ArrayList<>();
            inputdata.add(new BasicNameValuePair("vehicle", vehicle));
            inputdata.add(new BasicNameValuePair("vehicle_type", vehicle_type));
            inputdata.add(new BasicNameValuePair("start_location", start_loc));
            inputdata.add(new BasicNameValuePair("end_location", end_loc));
            inputdata.add(new BasicNameValuePair("start_time", start_time));
            inputdata.add(new BasicNameValuePair("end_time", end_time));
            inputdata.add(new BasicNameValuePair("opening_km", opening_km));
            inputdata.add(new BasicNameValuePair("closing_km", closing_km));
            inputdata.add(new BasicNameValuePair("total_km", total_km));
            inputdata.add(new BasicNameValuePair("driver_id", driver_id));
            //inputdata.add(new BasicNameValuePair("driver_ride_date", ""));
            inputdata.add(new BasicNameValuePair("weekend_ride_status", StaticData.last_selected_km_weekend_status));
            inputdata.add(new BasicNameValuePair("duty_hours", duty_hours));

            String company_id=StaticData.company_list.get(0).getCompany_id();
            for(Company c:StaticData.company_list)
            {
                if(c.getCompany_name().equals(tv_company.getText().toString()))
                    company_id=c.getCompany_id();
            }

            inputdata.add(new BasicNameValuePair("company_id", company_id));

            new InsertKMS_Weekend_Task(getActivity(),inputdata,new InsertKMS_WeekendListener()).execute();
        }

    }
    class InsertKMS_WeekendListener implements TaskListener
    {
        @Override
        public void onSuccess(String msg) {
            showDialog("Weekend Trip", "Trip info successfully submitted");
        }
        @Override
        public void onError(String msg) {
            showDialog("Weekend Trip",msg);
        }
        void showDialog(String title, String message) {
            ContextThemeWrapper themedContext;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                themedContext = new ContextThemeWrapper(getActivity(), android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
            } else {
                themedContext = new ContextThemeWrapper(getActivity(), android.R.style.Theme_Light_NoTitleBar);
            }
            final AlertDialog.Builder adialog = new AlertDialog.Builder(themedContext);
            adialog.setTitle(title);
            adialog.setMessage(message);
            adialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
//                    getActivity().finish();
                    KilometersActivity kmActivity = (KilometersActivity)getActivity();
                    kmActivity.showDefaultFragment(new Fragment_KM_WeekendRide_List());
                    kmActivity.isadd_running=false;
                    kmActivity.shouldAddMoreVisible=true;
                    kmActivity.setAddMoreVisibleState();
                }
            });
            adialog.show();
        }
    }
    class CompanyListener implements TaskListener
    {
        @Override
        public void onSuccess(String msg) {

        }
        @Override
        public void onError(String msg) {
            showDialog("Company Detail",msg);
        }
        void showDialog(String title, String message) {
            ContextThemeWrapper themedContext;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                themedContext = new ContextThemeWrapper(getActivity(), android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
            } else {
                themedContext = new ContextThemeWrapper(getActivity(), android.R.style.Theme_Light_NoTitleBar);
            }
            final AlertDialog.Builder adialog = new AlertDialog.Builder(themedContext);
            adialog.setTitle(title);
            adialog.setMessage(message);
            adialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getActivity().finish();
                }
            });
            adialog.show();
        }
    }
}
