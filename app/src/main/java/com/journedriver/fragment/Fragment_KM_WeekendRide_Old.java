package com.journedriver.fragment;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;

import com.journedriver.data.StaticData;
import com.journedriver.data.UtilMethod;
import com.journedriver.expandable.ExpandableListDataPump_KM_Status;
import com.journedriver.expandable.ExpandableList_Trip_Timings_Adapter;
import com.journedriver.task.InsertKMS_Weekend_Task;
import com.journedriver.task.TaskListener;
import com.journedriver.view.EditTextR;
import com.journedriver.view.TextViewB;
import com.journyapp.journyapp.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by administrator on 6/2/16.
 */
public class Fragment_KM_WeekendRide_Old extends Fragment implements ExpandableListView.OnChildClickListener,View.OnClickListener {
    ExpandableListView expandable_status_list;
    HashMap<String, List<String>> expandableListDetail;
    ExpandableList_Trip_Timings_Adapter expandableListAdapter;
    List<String> expandableListTitle;

    EditTextR edit_vehicle,edit_vehicle_type,edit_start_loc,edit_end_loc,edit_start_time,edit_end_time,edit_opening_km,edit_closing_km,edit_total_km,edit_duty_hours;
    TextViewB tv_save;
    String driver_id;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_layout_weekend_ride_old,null);

        expandable_status_list=(ExpandableListView) view.findViewById(R.id.expandable_status_list);
//		if(StaticData.expense_category_list.size()>0)
        expandableListDetail = ExpandableListDataPump_KM_Status.getData("Status");
//		else
//			expandableListDetail = ExpandableListDataPump_Category.getData("No Category Found");
        expandableListTitle = new ArrayList<String>(expandableListDetail.keySet());
        expandableListAdapter = new ExpandableList_Trip_Timings_Adapter(getActivity(), expandableListTitle, expandableListDetail);
        expandable_status_list.setAdapter(expandableListAdapter);

        expandable_status_list.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                setListViewHeight(parent, groupPosition);
                return false;
            }
        });
        expandable_status_list.setOnChildClickListener(this);

        edit_vehicle=(EditTextR)view.findViewById(R.id.edit_vehicle);
        edit_vehicle_type=(EditTextR)view.findViewById(R.id.edit_vehicle_type);
        edit_start_loc=(EditTextR)view.findViewById(R.id.edit_start_loc);
        edit_end_loc=(EditTextR)view.findViewById(R.id.edit_end_loc);
        edit_start_time=(EditTextR)view.findViewById(R.id.edit_start_time);
        edit_end_time=(EditTextR)view.findViewById(R.id.edit_end_time);
        edit_opening_km=(EditTextR)view.findViewById(R.id.edit_opening_km);
        edit_closing_km=(EditTextR)view.findViewById(R.id.edit_closing_km);
        edit_total_km=(EditTextR)view.findViewById(R.id.edit_total_km);
        edit_duty_hours=(EditTextR)view.findViewById(R.id.edit_duty_hours);
        tv_save=(TextViewB)view.findViewById(R.id.tv_save);
        tv_save.setOnClickListener(this);

        SharedPreferences sp = getActivity().getSharedPreferences("driver_info", getActivity().MODE_PRIVATE);
        driver_id = sp.getString("driver_id",null);

        StaticData.last_selected_km_weekend_status=null;
        return view;
    }
    @Override
    public boolean onChildClick(ExpandableListView expandableListView, View view, int i, int i1, long l) {
        StaticData.last_selected_km_weekend_status=StaticData.km_weekend_status.get(i1);
        expandableListDetail = ExpandableListDataPump_KM_Status.getData(StaticData.km_weekend_status.get(i1));
        expandableListTitle = new ArrayList<String>(expandableListDetail.keySet());
        expandableListAdapter = new ExpandableList_Trip_Timings_Adapter(getActivity(), expandableListTitle, expandableListDetail);
        expandable_status_list.setAdapter(expandableListAdapter);

        expandableListView.collapseGroup(i);

        return false;
    }

    private void setListViewHeight(ExpandableListView listView,
                                   int group) {
        int totalHeight = 0;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(),
                View.MeasureSpec.EXACTLY);
        for (int i = 0; i < expandableListAdapter.getGroupCount(); i++) {
            View groupItem = expandableListAdapter.getGroupView(i, false, null, listView);
            groupItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);

            totalHeight += groupItem.getMeasuredHeight();

            if (((listView.isGroupExpanded(i)) && (i != group))
                    || ((!listView.isGroupExpanded(i)) && (i == group))) {
                for (int j = 0; j < expandableListAdapter.getChildrenCount(i); j++) {
                    View listItem = expandableListAdapter.getChildView(i, j, false, null,
                            listView);
                    listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);

                    totalHeight += listItem.getMeasuredHeight();

                }
            }
        }

        ViewGroup.LayoutParams params = listView.getLayoutParams();
        int height = totalHeight
                + (listView.getDividerHeight() * (expandableListAdapter.getGroupCount() - 1));
        if (height < 10)
            height = 200;
        params.height = height;
        listView.setLayoutParams(params);
        listView.requestLayout();

    }

    @Override
    public void onClick(View view) {

        String vehicle= edit_vehicle.getText().toString();;
        String vehicle_type= edit_vehicle_type.getText().toString();
        String start_loc= edit_start_loc.getText().toString();
        String end_loc=  edit_end_loc.getText().toString();
        String start_time= edit_start_time.getText().toString();
        String end_time= edit_end_time.getText().toString();
        String opening_km= edit_opening_km.getText().toString();
        String closing_km= edit_closing_km.getText().toString();
        String total_km= edit_total_km.getText().toString();
        String duty_hours= edit_duty_hours.getText().toString();

        if(UtilMethod.isStringNullOrBlank(vehicle))
            UtilMethod.showToast("Vehicle info required",getActivity());
        else if(UtilMethod.isStringNullOrBlank(vehicle_type))
            UtilMethod.showToast("Vehicle type required",getActivity());
        else if(UtilMethod.isStringNullOrBlank(start_loc))
            UtilMethod.showToast("Start Location required",getActivity());
        else if(UtilMethod.isStringNullOrBlank(end_loc))
            UtilMethod.showToast("End location required",getActivity());
        else if(UtilMethod.isStringNullOrBlank(start_time))
            UtilMethod.showToast("Start time required",getActivity());
        else if(UtilMethod.isStringNullOrBlank(end_time))
            UtilMethod.showToast("End time required",getActivity());
        else if(UtilMethod.isStringNullOrBlank(opening_km))
            UtilMethod.showToast("Opening km required",getActivity());
        else if(UtilMethod.isStringNullOrBlank(closing_km))
            UtilMethod.showToast("Closing km required",getActivity());
        else if(UtilMethod.isStringNullOrBlank(total_km))
            UtilMethod.showToast("Total km required",getActivity());
        else if(UtilMethod.isStringNullOrBlank(duty_hours))
            UtilMethod.showToast("Duty hours required",getActivity());
        else if(UtilMethod.isStringNullOrBlank(StaticData.last_selected_km_weekend_status))
            UtilMethod.showToast("Status required",getActivity());
        else {
            ArrayList<NameValuePair> inputdata = new ArrayList<>();
            inputdata.add(new BasicNameValuePair("vehicle", vehicle));
            inputdata.add(new BasicNameValuePair("vehicle_type", vehicle_type));
            inputdata.add(new BasicNameValuePair("start_location", start_loc));
            inputdata.add(new BasicNameValuePair("end_location", end_loc));
            inputdata.add(new BasicNameValuePair("start_time", start_time));
            inputdata.add(new BasicNameValuePair("end_time", end_time));
            inputdata.add(new BasicNameValuePair("opening_km", opening_km));
            inputdata.add(new BasicNameValuePair("closing_km", closing_km));
            inputdata.add(new BasicNameValuePair("total_km", total_km));
            inputdata.add(new BasicNameValuePair("driver_id", driver_id));
            inputdata.add(new BasicNameValuePair("driver_ride_date", ""));
            inputdata.add(new BasicNameValuePair("weekend_ride_status", StaticData.last_selected_km_weekend_status));
            inputdata.add(new BasicNameValuePair("duty_hours", duty_hours));

            new InsertKMS_Weekend_Task(getActivity(),inputdata,new InsertKMS_WeekendListener()).execute();
        }

    }
    class InsertKMS_WeekendListener implements TaskListener
    {
        @Override
        public void onSuccess(String msg) {
            showDialog("Weekend Trip", "Trip info successfully submitted");
        }
        @Override
        public void onError(String msg) {
            showDialog("Weekend Trip",msg);
        }
        void showDialog(String title, String message) {
            ContextThemeWrapper themedContext;
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
                themedContext = new ContextThemeWrapper(getActivity(), android.R.style.Theme_Holo_Light_Dialog_NoActionBar);
            } else {
                themedContext = new ContextThemeWrapper(getActivity(), android.R.style.Theme_Light_NoTitleBar);
            }
            final AlertDialog.Builder adialog = new AlertDialog.Builder(themedContext);
            adialog.setTitle(title);
            adialog.setMessage(message);
            adialog.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    getActivity().finish();
                }
            });
            adialog.show();
        }
    }
}
