package com.journedriver.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.journedriver.adapter.WeekendRideListAdapter;
import com.journedriver.data.StaticData;
import com.journedriver.task.GetWeekendRideListTask;
import com.journedriver.task.TaskListener;
import com.journyapp.journyapp.R;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;

import java.util.ArrayList;

/**
 * Created by administrator on 6/2/16.
 */
public class Fragment_KM_WeekendRide_List extends Fragment implements View.OnClickListener, SwipeRefreshLayout.OnRefreshListener {

    SwipeRefreshLayout swipeRefreshLayout;
    ListView lv_daily_ride;
    WeekendRideListAdapter adapter;
    String driver_id;
    Context mContext;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_layout_ride_list,null);
        mContext=getActivity();
        SharedPreferences sp = mContext.getSharedPreferences("driver_info", getActivity().MODE_PRIVATE);
        driver_id = sp.getString("driver_id",null);
        Log.e("driver_id", driver_id);

        lv_daily_ride=(ListView)view.findViewById(R.id.lv_kms_list);


        swipeRefreshLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_refresh_layout);
        swipeRefreshLayout.setOnRefreshListener(this);
        swipeRefreshLayout.post(new Runnable() {
                                    @Override
                                    public void run() {
                                        swipeRefreshLayout.setRefreshing(true);
                                        getWeekendRideList();
                                    }
                                }
        );


        return view;
    }

    public void getWeekendRideList()
    {
        ArrayList<NameValuePair> inputdata=new ArrayList<>();
        inputdata.add(new BasicNameValuePair("driver_id",driver_id));
        inputdata.add(new BasicNameValuePair("date",StaticData.km_date));
        Log.e("StaticData.km_date",StaticData.km_date);
        //inputdata.add(new BasicNameValuePair("date","18-02-2016"));
        new GetWeekendRideListTask(mContext,inputdata,new GetWeekendRideListListener()).execute();
    }


    @Override
    public void onClick(View view) {

    }

    class GetWeekendRideListListener implements TaskListener
    {
        @Override
        public void onSuccess(String msg) {
            adapter=new WeekendRideListAdapter(getActivity());
            lv_daily_ride.setAdapter(adapter);
            swipeRefreshLayout.setRefreshing(false);
        }
        @Override
        public void onError(String msg) {
            adapter=new WeekendRideListAdapter(getActivity());
            lv_daily_ride.setAdapter(adapter);
            swipeRefreshLayout.setRefreshing(false);

            ContextThemeWrapper themedContext;
            if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ) {
                themedContext = new ContextThemeWrapper(getActivity(), android.R.style.Theme_Holo_Light_Dialog_NoActionBar );
            }
            else {
                themedContext = new ContextThemeWrapper(getActivity(), android.R.style.Theme_Light_NoTitleBar );
            }
            final AlertDialog.Builder adialog=new AlertDialog.Builder(themedContext);
            adialog.setTitle("Message!!");
            adialog.setMessage(msg);
            adialog.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                }
            });
            adialog.show();
        }
    }

    @Override
    public void onRefresh() {
        // TODO Auto-generated method stub
        getWeekendRideList();
    }
}
