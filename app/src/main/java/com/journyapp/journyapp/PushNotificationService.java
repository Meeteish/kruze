package com.journyapp.journyapp;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;

import com.google.android.gms.gcm.GcmListenerService;
import com.journepassenger.activity.DashBoardActivity;
import com.journepassenger.activity.SigninActivity;
import com.journepassenger.utilities.Session_manager;

import org.json.JSONException;
import org.json.JSONObject;
//AIzaSyCPpQgoCD4ufW6Voi6YD72gWRGAN_kE7p0
/**
 * Created by khan on 10/22/2015.
 */
public class PushNotificationService extends GcmListenerService {
    String msg;
    Context context;

    @Override
    public void onMessageReceived(String from, Bundle data) {
        String message = data.getString("msg");
        JSONObject jsob = null;
        context = this;
        try {
            jsob = new JSONObject(message);
            String status = jsob.getString("status");
            msg = jsob.getString("message");
            CreateNotification(context, msg);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        /*NotificationManager notificationManager= (NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE);
        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(this)
                .setSmallIcon(R.drawable.app_icon)
                .setContentTitle("Kruze")
                .setContentText(msg);
        notificationManager.notify(1, mBuilder.build());*/
    }

    public void CreateNotification(Context mContext, String msg) {

        PendingIntent pendingIntent = null;
        Intent myIntent = null;
        int requestId = (int) System.currentTimeMillis();
        if (Session_manager.getemail_pref(mContext) != null) {

            if (Session_manager.getstatus(mContext).equals("Passenger")) {

                myIntent = new Intent(context, DashBoardActivity.class);
                myIntent.putExtra("notificationId", requestId);
                pendingIntent = PendingIntent.getActivity(context, 0, myIntent, PendingIntent.FLAG_CANCEL_CURRENT);
            } else if (Session_manager.getloginstatus(mContext) == 1)
            {
                myIntent = new Intent(context, DashBoardActivity.class);
                myIntent.putExtra("notificationId", requestId);
                pendingIntent = PendingIntent.getActivity(context, 0, myIntent, PendingIntent.FLAG_CANCEL_CURRENT);
            } else if (Session_manager.getloginstatus(mContext) == 2)
            {
                myIntent = new Intent(context, DashBoardActivity.class);
                myIntent.putExtra("notificationId", requestId);
                pendingIntent = PendingIntent.getActivity(context, 0, myIntent, PendingIntent.FLAG_CANCEL_CURRENT);
            }
            else if (Session_manager.getstatus(mContext).equals("driver"))
            {

            }
            } else {
                myIntent = new Intent(context, SigninActivity.class);
                pendingIntent = PendingIntent.getActivity(context, 0, myIntent, PendingIntent.FLAG_CANCEL_CURRENT);
            }


            myIntent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
            NotificationManager nm = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
            Resources res = context.getResources();
            NotificationCompat.Builder builder = new NotificationCompat.Builder(context);

            builder.setContentIntent(pendingIntent)
                    .setSmallIcon(R.drawable.app_icon)
                    .setLargeIcon(BitmapFactory.decodeResource(res, R.drawable.app_icon))
                    .setTicker("Kruze")
                    .setWhen(System.currentTimeMillis())
                    .setAutoCancel(true)
                    .setContentTitle("Kruze")
                    .setContentText(msg);
            Notification n = builder.getNotification();

            n.defaults |= Notification.DEFAULT_ALL;
            nm.notify(requestId, n);

        }
    }

